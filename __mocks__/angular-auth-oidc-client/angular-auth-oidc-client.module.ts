import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthModule, OidcSecurityService, OpenIdConfiguration} from 'angular-auth-oidc-client';
import {OidcSecurityServiceMock} from "./oidc-security.service.mock";

@NgModule({
  imports: [
    CommonModule,
    AuthModule.forRoot({
      config: {
        authority: 'test',
        redirectUrl: 'test',
        clientId: 'test'
      } as OpenIdConfiguration
    }),
  ],
  providers: [
    {provide: OidcSecurityService, useClass: OidcSecurityServiceMock}
  ]
})
export class AngularAuthOidcClientTestingModule {
}
