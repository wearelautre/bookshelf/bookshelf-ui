import {OpenIdConfiguration} from "angular-auth-oidc-client";
import {Observable, of} from "rxjs";

export class ConfigurationServiceMock {

  withConfigs(passedConfigs: OpenIdConfiguration[]): Observable<OpenIdConfiguration[]> {
    return of([]);
  }

  hasManyConfigs(): boolean {
    return true;
  }

  getAllConfigurations(): OpenIdConfiguration[] {
    return [];
  }

  getOpenIDConfiguration(configId?: string): Observable<OpenIdConfiguration> {
    return of({});
  }

  getOpenIDConfigurations(configId?: string): Observable<{ allConfigs; currentConfig }> {
    return of({allConfigs: {}, currentConfig: {}});
  }
}
