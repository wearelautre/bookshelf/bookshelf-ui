import {BehaviorSubject, of} from 'rxjs';

export class OidcSecurityServiceMock {
  getToken() {
    return 'some_token_eVbnasdQ324';
  }

  getIdToken() {
    return 'some_token_eVbnasdQ324';
  }

  logoff() {
    return;
  }

  login() {
    return;
  }

  checkAuth() {
    return of(true);
  }

  authorize() {
    return;
  }

  isAuthenticated$ = new BehaviorSubject<{
    isAuthenticated: boolean,
    allConfigsAuthenticated: []
  }>({
    isAuthenticated: true,
    allConfigsAuthenticated: []
  });
  // similarly mock other methods "oidcSecurityService" as per the component requirement

}
