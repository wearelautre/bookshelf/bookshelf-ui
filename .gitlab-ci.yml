image: alpine:latest

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

variables:
  # DOCKER variables
  DOCKER_DRIVER: overlay
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  SERVICE_NAME: webui

stages:
  - test
  - analysis
  - build
  - tag-image
  - release
  - deploy

.node:init:
  image: node:18-alpine
  before_script:
    - npm ci
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - node_modules

build:docker:
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [ "" ]
  stage: build
  before_script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --build-arg CI_PIPELINE_ID=${CI_PIPELINE_ID}
      --build-arg CI_JOB_ID=${CI_JOB_ID}
      --build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA}
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  only:
    - master
    - develop
    - /^feature\/.*$/

linter:
  extends: .node:init
  stage: test
  script:
    - npm run lint
  only:
    - master
    - develop
    - /^feature\/.*$/

unit-tests:
  extends: .node:init
  stage: test
  script:
    - npm run test-ci
  artifacts:
    paths:
      - coverage
    when: always
    reports:
      junit:
        - junit.xml
  only:
    - master
    - develop
    - /^feature\/.*$/

.analysis:sonar:
  stage: analysis
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [ "" ]
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name=${BRANCH_NAME} -Dsonar.projectKey=wearelautre_bookshelf-ui -Dsonar.organization=wearelautre -Dsonar.projectVersion=${VERSION}
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - "${SONAR_USER_HOME}/cache"

analysis:sonar:set-version:
  stage: analysis
  extends: .analysis:sonar
  variables:
    VERSION: $CI_COMMIT_TAG
    BRANCH_NAME: master
  rules:
    - if: $CI_COMMIT_TAG

analysis:sonar:
  stage: analysis
  extends: .analysis:sonar
  variables:
    VERSION: $CI_COMMIT_TAG
    BRANCH_NAME: $CI_COMMIT_BRANCH
  only:
    - master
    - develop
    - /^feature\/.*$/

release:tag-create:
  stage: release
  image: alpine:latest
  needs:
    - job: build:docker
  before_script:
    - apk add git
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - tag=$(date +%Y%m%d)
    - git tag "$tag"
    - git push --tags https://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git
  only:
    - master
  when: manual

release:tag-image:
  stage: tag-image
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [ "" ]
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - crane cp $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: "release:tag-image"
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Releasing $CI_COMMIT_TAG"
  release:
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG'

.deploy:
  before_script:
    - apk add wget
    - wget https://github.com/argoproj/argo-cd/releases/download/v2.6.5/argocd-linux-amd64
    - chmod +x argocd-linux-amd64
  script:
    - ./argocd-linux-amd64 login argocd.grim-is-a-geek.fr --grpc-web --username admin --password $ARGO_PASSWORD
    - ./argocd-linux-amd64 app set ${ENVIRONMENT}-bookshelf -p global.${SERVICE_NAME}.version=${TAG}
    - ./argocd-linux-amd64 app sync ${ENVIRONMENT}-bookshelf

deploy:develop:
  stage: deploy
  extends: .deploy
  variables:
    TAG: ${CI_COMMIT_SHORT_SHA}
    ENVIRONMENT: develop
  only:
    - develop
