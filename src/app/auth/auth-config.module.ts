import {NgModule} from '@angular/core';
import {
  AbstractSecurityStorage,
  AuthInterceptor,
  AuthModule,
  DefaultLocalStorageService,
  LogLevel,
  OpenIdConfiguration,
  StsConfigHttpLoader,
  StsConfigLoader
} from 'angular-auth-oidc-client';
import {HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {ConfigService} from "../core/services/config.service";
import {AppConfig} from "../core/model/app-config";
import {environment} from "../../environments/environment";

export function httpLoaderFactory(httpClient: HttpClient, configService: ConfigService) {
  const config$ = httpClient.get<AppConfig>(`./assets/config/config.json`).pipe(
    tap(customConfig => configService.setAppConfig(customConfig)),
    map<AppConfig, OpenIdConfiguration>((customConfig) => ({
        configId: 'config',
        authority: `${customConfig.oauth.stsServer}/realms/${customConfig.oauth.realm}`,
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: window.location.origin,
        clientId: customConfig.oauth.clientId,
        scope: 'openid profile email offline_access',
        responseType: 'code',
        triggerRefreshWhenIdTokenExpired: true,
        silentRenew: true,
        useRefreshToken: true,
        autoUserInfo: true,
        ignoreNonceAfterRefresh: true,
        logLevel: environment.production ? LogLevel.None : LogLevel.Warn,
        secureRoutes: ['/api'],
      }) as OpenIdConfiguration
    )
  );

  return new StsConfigHttpLoader(config$);
}

@NgModule({
  imports: [
    AuthModule.forRoot({
      loader: {
        provide: StsConfigLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient, ConfigService],
      }
    })
  ],
  exports: [AuthModule],
  providers: [
    {
      provide: AbstractSecurityStorage,
      useClass: DefaultLocalStorageService,
    },
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ]
})
export class AuthConfigModule {
}
