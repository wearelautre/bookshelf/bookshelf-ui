import {TestBed, waitForAsync} from '@angular/core/testing';

import {AuthGuard} from './auth.guard';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {ActivatedRouteSnapshot, UrlTree} from "@angular/router";
import {OidcSecurityServiceMock} from "../../../../__mocks__/angular-auth-oidc-client/oidc-security.service.mock";
import {LoginResponse} from "angular-auth-oidc-client";
import {of} from "rxjs";

describe('AuthGuard', () => {
  let httpTestingController: HttpTestingController;
  let guard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AngularAuthOidcClientTestingModule
      ],
      providers: [
        AuthGuard
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    guard = TestBed.inject(AuthGuard);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should instantiate', () => {
      expect(guard).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should return isAuthenticated$ ', waitForAsync(() => {
      guard.canActivateChild(new ActivatedRouteSnapshot(), undefined)
        .subscribe(value => expect(value).toBeTruthy());
    }));
    test('should call check auth', waitForAsync(() => {
      (guard['oidcSecurityService'] as unknown as OidcSecurityServiceMock).isAuthenticated$
        .next({isAuthenticated: false, allConfigsAuthenticated: []});

      const spy = jest.spyOn(guard['oidcSecurityService'], 'checkAuth')
        .mockReturnValue(of({isAuthenticated: false} as LoginResponse));

      guard.canActivateChild(new ActivatedRouteSnapshot(), undefined)
        .subscribe(value => {
          expect(value instanceof UrlTree).toBe(true)
          expect(spy).toHaveBeenCalledTimes(1)
        });
    }));
  });
});
