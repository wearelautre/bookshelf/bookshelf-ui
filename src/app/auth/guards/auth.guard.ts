import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of, switchMap, take} from 'rxjs';
import {AuthenticatedResult, OidcSecurityService} from 'angular-auth-oidc-client';
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
  constructor(
    private oidcSecurityService: OidcSecurityService,
    private router: Router
  ) {
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.oidcSecurityService.isAuthenticated$.pipe(
      switchMap((authenticatedResult: AuthenticatedResult) =>
        authenticatedResult.isAuthenticated ? of(authenticatedResult) : this.oidcSecurityService.checkAuth()),
      take(1),
      map(({isAuthenticated}) => {
        // allow navigation if authenticated
        if (isAuthenticated) {
          return true;
        }

        // redirect if not authenticated
        return this.router.parseUrl('/book');
      })
    );
  }
}
