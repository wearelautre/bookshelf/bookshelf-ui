import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LegalRoutingModule} from "./legal-routing.module";
import { PrivacyComponent } from './components/privacy/privacy.component';



@NgModule({
  declarations: [
    PrivacyComponent
  ],
  imports: [
    CommonModule,
    LegalRoutingModule
  ]
})
export class LegalModule { }
