import { Component } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styles: [`
    .container {
      display: flex;
      justify-content: center;
      .content {
        width: 50%;
      }
    }

    h1, h2, h3 {
      text-transform: uppercase;
    }

    .bold {
      font-weight: bold;
    }
  `]
})
export class PrivacyComponent {

}
