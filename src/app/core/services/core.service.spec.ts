import {TestBed, waitForAsync} from '@angular/core/testing';

import {CoreService} from './core.service';
import {Type} from '@angular/core';

describe('CoreService', () => {
  let service: CoreService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoreService as Type<CoreService>);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should update loading state to false', waitForAsync(() => {
      service.isLoading$.subscribe(value => expect(value).toBeFalsy());
      service.updateLoadingState(false);
    }));
    test('should update loading state to true', waitForAsync(() => {
      service.updateLoadingState(true);
      service.isLoading$.subscribe(value => expect(value).toBeTruthy());
    }));
  });
});
