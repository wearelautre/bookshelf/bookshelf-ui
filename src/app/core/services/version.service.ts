import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, forkJoin, Observable, of} from 'rxjs';
import {VersionInformation} from '../model/version-information';
import {ConfigService} from './config.service';
import {catchError, filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  private globalVersion: BehaviorSubject<string> = new BehaviorSubject(null);

  get globalVersion$(): Observable<string> {
    return this.globalVersion.asObservable();
  }

  private uiVersion: BehaviorSubject<VersionInformation | null> = new BehaviorSubject(null);

  get uiVersion$(): Observable<VersionInformation> {
    return this.uiVersion.asObservable();
  }

  private apiVersion: BehaviorSubject<VersionInformation | null> = new BehaviorSubject(null);

  get apiVersion$(): Observable<VersionInformation | null> {
    return this.apiVersion.asObservable();
  }

  private bnfApiVersion: BehaviorSubject<VersionInformation | null> = new BehaviorSubject(null);

  get bnfApiVersion$(): Observable<VersionInformation | null> {
    return this.bnfApiVersion.asObservable();
  }

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
  }

  getVersionInformation(component: string): Observable<VersionInformation> {
    return this.http.get<VersionInformation>(`${component}/version`)
      .pipe(catchError(() => of(null))
      );
  }

  getUiAndGlobalVersion(): void {
    this.configService.appConfig$

      .pipe(filter(appConfig => appConfig !== null))
      .subscribe(appConfig => {
        this.globalVersion.next(appConfig.globalVersion);
        this.uiVersion.next(appConfig.versionInformation);
      });
  }

  getVersions(): void {
    this.getUiAndGlobalVersion();
    forkJoin([
      this.getVersionInformation('/bnf-api'),
      this.getVersionInformation('/api')
    ])
      .subscribe(versionInformations => {
        this.bnfApiVersion.next(versionInformations[0]);
        this.apiVersion.next(versionInformations[1]);
      });
  }
}
