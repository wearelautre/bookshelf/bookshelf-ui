import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  private isLoading = new BehaviorSubject<boolean>(false);

  get isLoading$(): Observable<boolean> {
    return this.isLoading.asObservable();
  }

  updateLoadingState(next: boolean): void {
    this.isLoading.next(next);
  }

}
