import {BehaviorSubject} from "rxjs";

export const versionServiceMock = {
  getVersions: jest.fn((next: boolean) => {
  }),
  globalVersion$: new BehaviorSubject<string>('N/A'),
  uiVersion$: new BehaviorSubject<string>('N/A'),
  bnfApiVersion$: new BehaviorSubject<string>('N/A'),
  apiVersion$: new BehaviorSubject<string>('N/A')
};
