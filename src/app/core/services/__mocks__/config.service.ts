import {BehaviorSubject} from 'rxjs';
import {AppConfig} from "../../model/app-config";

export const configServiceMock = {
  appConfig$: new BehaviorSubject<AppConfig | null>(null),
};
