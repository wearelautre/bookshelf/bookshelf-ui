import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  faBan as fasBan,
  faBell as fasBell,
  faCheck as fasCheck,
  faCog as fasCog,
  faEllipsisV,
  faEnvelope as fasEnvelope,
  faEye as fasEye,
  faLocationArrow as fasLocationArrow,
  faPencilAlt as fasPencilAlt,
  faPlus as fasPlus,
  faSave as fasSave,
  faSearch as fasSearch,
  faSignOutAlt,
  faTrash as fasTrash,
  faUserCircle
} from '@fortawesome/free-solid-svg-icons';
import {HeaderInputComponent} from './components/header-input/header-input.component';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {LicensesModalComponent} from './components/licenses-modal/licenses-modal.component';
import {ContactModalComponent} from './components/contact-modal/contact-modal.component';
import {faTwitter} from '@fortawesome/free-brands-svg-icons';
import {MatListModule} from '@angular/material/list';
import {faBell as farBell} from "@fortawesome/free-regular-svg-icons";
import {MatExpansionModule} from "@angular/material/expansion";
import {AdministrationModule} from "../administration/administration.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {
  HeaderMenuAdministrationComponent
} from './components/header-menu-administration/header-menu-administration.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    FontAwesomeModule,
    MatSelectModule,
    MatTooltipModule,
    MatListModule,
    MatExpansionModule,
    AdministrationModule,
    MatFormFieldModule,
    SharedModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    HeaderInputComponent,
    LicensesModalComponent,
    ContactModalComponent,
    HeaderMenuAdministrationComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent
  ]
})
export class CoreModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(fasPlus, fasPencilAlt, fasTrash, fasCheck, faEllipsisV,
      fasSave, fasBan, faTwitter, fasEnvelope,
      fasSearch, fasCog, fasBell, fasEye, fasLocationArrow,
      faUserCircle, faSignOutAlt
    );
    library.addIcons(farBell);
  }

}
