import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {Observable} from 'rxjs';
import {VersionService} from '../../services/version.service';
import {VersionInformation} from '../../model/version-information';
import {LicensesModalComponent} from '../licenses-modal/licenses-modal.component';
import {TranslateService} from "@ngx-translate/core";
import {map} from "rxjs/operators";

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['footer.component.scss']
})
export class FooterComponent implements OnInit {

    version = 'N/A';
    isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

    private versions: { component: string, version: VersionInformation }[] = [];

    constructor(
        public dialog: MatDialog,
        private oidcSecurityService: OidcSecurityService,
        private versionService: VersionService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit(): void {
        this.versionService.getVersions();
        this.versionService.globalVersion$.subscribe(version => this.version = version);
        this.versionService.uiVersion$.subscribe(version => this.pushComponentVersion({component: 'UI', version}));
        this.versionService.bnfApiVersion$.subscribe(version => this.pushComponentVersion({component: 'BNF API', version}));
        this.versionService.apiVersion$.subscribe(version => this.pushComponentVersion({component: 'API', version}));
    }


    getVersionsDisplay(): string {
        return this.versions
            .map(version => `${version.component} : ${this.getVersionDisplay(version.version)}`)
            .join('\n');
    }

    getVersionDisplay(version: VersionInformation): string {
        return version === null ? `KO` : `${version.projectVersion} ${this.getBuildInfo(version)}`;
    }

    getBuildInfo(version: VersionInformation) {
        return version === null || (version.pipelineId === '0' || version.jobId === '0') ? `` : `#${version.pipelineId}/#${version.jobId}`;
    }

    pushComponentVersion(componentVersion: { component: string, version: VersionInformation }) {
        const index = this.versions.findIndex(v => v.component === componentVersion.component);
        if (index === -1) {
            this.versions.push(componentVersion);
        } else {
            this.versions.splice(index, 1, componentVersion);
        }
    }

    openLicenseModal() {
        this.dialog.open(LicensesModalComponent, {
            width: '500px'
        });
    }

    changeLang(event: string) {
        this.translateService.use(event)
    }
}
