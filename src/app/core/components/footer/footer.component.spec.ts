import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {FooterComponent} from './footer.component';
import {MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {LicensesModalComponent} from "../licenses-modal/licenses-modal.component";
import {RouterTestingModule} from "@angular/router/testing";
import {VersionService} from "../../services/version.service";
import {versionServiceMock} from "../../services/__mocks__/version.service";

const matDialogMock = {
  open: jest.fn()
};


describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FooterComponent],
      imports: [
        MatDialogModule,
        FontAwesomeTestingModule,
        NgxTranslateTestingModule,
        RouterTestingModule,
        AngularAuthOidcClientTestingModule,
        MatTooltipModule
      ],
      providers: [
        {provide: VersionService, useValue: versionServiceMock},
        {provide: MatDialog, useValue: matDialogMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', waitForAsync(() => {
      expect(component).toBeTruthy();
    }));
  });

  describe('Typescript test', () => {
    describe('[GET VERSION DISPLAY]', () => {
      test('Version displayed KO', waitForAsync(() => {
        expect(component.getVersionDisplay(null)).toStrictEqual('KO');
      }));
      test('Version displayed OK', waitForAsync(() => {
        const version = {projectVersion: 'test', jobId: '0', pipelineId: '0', commitSha: '0'};
        const spy = jest.spyOn(component, 'getBuildInfo').mockImplementation(() => ('toto'));
        expect(component.getVersionDisplay(version)).toStrictEqual('test toto');
        expect(spy).toHaveBeenNthCalledWith(1, version);
      }));
    });
    describe('[GET BUILD INFO]', () => {
      test('Build info KO', waitForAsync(() => {
        expect(component.getBuildInfo(null)).toStrictEqual('');
      }));
      test('pipeline id is 0', waitForAsync(() => {
        const version = {projectVersion: 'test', jobId: '10', pipelineId: '0', commitSha: '0'};
        expect(component.getBuildInfo(version)).toStrictEqual('');
      }));
      test('job id is 0', waitForAsync(() => {
        const version = {projectVersion: 'test', jobId: '0', pipelineId: '10', commitSha: '0'};
        expect(component.getBuildInfo(version)).toStrictEqual('');
      }));
      test('Build info OK', waitForAsync(() => {
        const version = {projectVersion: 'test', jobId: '15', pipelineId: '10', commitSha: '0'};
        expect(component.getBuildInfo(version)).toStrictEqual('#10/#15');
      }));
    });
    describe('[OPEN MODAL]', () => {
      test('should close the modal', waitForAsync(() => {
        const spy = jest.spyOn(component.dialog, 'open').mockImplementation(() => ({}) as MatDialogRef<LicensesModalComponent>);
        component.openLicenseModal();
        expect(spy).toHaveBeenNthCalledWith(1, LicensesModalComponent, {
          width: '500px'
        });
      }));
    });
  });
});
