import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {map} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
    .app-toolbar {
      position: fixed;
      top: 0; /* Sets the sticky toolbar to be on top */
      right: 0; /* Sets the sticky toolbar to be on top */
      left: 0; /* Sets the sticky toolbar to be on top */
      z-index: 1000;
    }

    .panelClass {
      width: 25%;
    }

    .mat-icon {
      transform: scale(2);
    }
  `]
})
export class HeaderComponent {

  public listMenu = [{
    label: 'HEADER.LINK.BOOKS',
    path: '/book/list'
  }, {
    label: 'HEADER.LINK.ARTISTS',
    path: '/artist'
  }, {
    label: 'HEADER.LINK.WISHLIST',
    path: '/wishlist'
  }]

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  constructor(
    private oidcSecurityService: OidcSecurityService,
  ) {
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  signOut() {
    this.oidcSecurityService.logoff().subscribe();
  }
}
