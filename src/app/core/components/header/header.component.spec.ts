import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HeaderComponent} from './header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MockHeaderInputComponent} from '../header-input/__mocks__/header-input.component';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {
    AngularAuthOidcClientTestingModule
} from '../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MockBookDashboardComponent} from '../../../book/pages/book-dashboard/__mocks__/book-dashboard.component';
import {of, Subject} from 'rxjs';
import {ActivatedRoute, Router, RouterEvent, RouterModule,} from '@angular/router';
import {
    MockTaskHeaderMenuComponent
} from "../../../administration/components/tasks/task-header-menu/__mocks__/task-header-menu.component";
import {configServiceMock} from "../../services/__mocks__/config.service";
import {ConfigService} from "../../services/config.service";

const eventSubject = new Subject<RouterEvent>();

const routerMock = {
    navigate: jest.fn(() => Promise.resolve(true)),
    events: eventSubject.asObservable(),
    url: ''
};

const routeMock = {
    queryParams: of({get: (val) => 'BD'}),
    queryParamMap: of({get: (val) => 'BD'})
};

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // deprecated
            removeListener: jest.fn(), // deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeaderComponent,
                MockHeaderInputComponent,
                MockBookDashboardComponent,
                MockTaskHeaderMenuComponent
            ],
            imports: [
                FontAwesomeTestingModule,
                NgxTranslateTestingModule,
                RouterModule,
                FlexLayoutModule,
                NoopAnimationsModule,
                MatIconModule,
                MatToolbarModule,
                MatButtonModule,
                MatMenuModule,
                MatSelectModule,
                ReactiveFormsModule,
                AngularAuthOidcClientTestingModule
            ],
            providers: [
                {provide: Router, useValue: routerMock},
                {provide: ConfigService, useValue: configServiceMock},
                {provide: ActivatedRoute, useValue: routeMock}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', waitForAsync(() => {
            expect(component).toBeTruthy();
        }));
    });

    describe('Typescript test', () => {
        describe('[LOGIN]', () => {
            test('should call authorize()', waitForAsync(() => {
                const oidcSecurityService = TestBed.inject(OidcSecurityService);
                const spy = jest.spyOn(oidcSecurityService, 'authorize');
                component.login();
                expect(spy).toHaveBeenCalledTimes(1);
            }));
        });
        describe('[LOGOUT]', () => {
            test('should call logout and redirect to / OK', waitForAsync(() => {
                jest.clearAllMocks();
                const oidcSecurityService = TestBed.inject(OidcSecurityService);
                const spy = jest.spyOn(oidcSecurityService, 'logoff').mockImplementation(() => of());
                component.signOut();
                expect(spy).toHaveBeenCalledTimes(1);
            }));
        });
    });
});
