import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HeaderInputComponent} from './header-input.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {
    AngularAuthOidcClientTestingModule
} from '../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';

describe('HeaderInputComponent', () => {
    let component: HeaderInputComponent;
    let fixture: ComponentFixture<HeaderInputComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [HeaderInputComponent],
            imports: [
                NoopAnimationsModule,
                FontAwesomeTestingModule,
                NgxTranslateTestingModule,
                AngularAuthOidcClientTestingModule,
                ReactiveFormsModule,
                RouterTestingModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                MatButtonModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderInputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        test('should navigate on submit', waitForAsync(() => {
            const spy = jest.spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));

            component.form.setValue({search: '1234567891231'});
            expect(component.form.valid).toBeTruthy();
            component.onSubmit();
            expect(spy).toHaveBeenNthCalledWith(1, ['administration', 'book', 'add', '1234567891231']);
        }));
        test('should not navigate on submit if form not valid', waitForAsync(() => {
            const spy = jest.spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));

            component.form.setValue({search: 'testISBN'});
            expect(component.form.valid).toBeFalsy();
            component.onSubmit();
            expect(spy).toHaveBeenCalledTimes(0);
        }));
    });
});
