import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LicensesModalComponent} from './licenses-modal.component';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';

describe('LicensesModalComponent', () => {
    let component: LicensesModalComponent;
    let fixture: ComponentFixture<LicensesModalComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [LicensesModalComponent],
            imports: [
                NgxTranslateTestingModule,
                MatDialogModule
            ],
            providers: [{
                provide: MatDialogRef, useValue: {
                    close() {
                    }
                }
            }]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LicensesModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create the app', () => {
            expect(component).toBeTruthy();
        });
    });
});
