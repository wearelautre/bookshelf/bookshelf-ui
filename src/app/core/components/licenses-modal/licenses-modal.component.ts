import {Component} from '@angular/core';

@Component({
    selector: 'app-licenses-modal',
    template: `
        <h1 mat-dialog-title>{{'FOOTER.LICENSE.MODAL.TITLE' | translate}}</h1>
        <div mat-dialog-content>
            <p>
                <img style="width: 32px; height: 32px" src="favicon.svg" alt="bookshelf icon">
                {{'FOOTER.LICENSE.MODAL.BODY.ICONS' | translate}}
                <a href="https://www.flaticon.com/artist/smalllikeart" title="smalllikeart">smalllikeart</a>
                {{'FOOTER.LICENSE.MODAL.BODY.FROM' | translate}}
                <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            </p>
        </div>
        <div mat-dialog-actions>
            <button mat-button mat-dialog-close>{{'FOOTER.LICENSE.MODAL.BUTTON.CLOSE' | translate}}</button>
        </div>
    `
})
export class LicensesModalComponent {

}
