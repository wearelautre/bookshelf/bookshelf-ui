import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-header-menu-administration',
  template: `
    <div *ngIf="isAuthenticated; else loginButton" fxLayout="row" fxLayoutAlign="center center"
         fxLayoutGap="1rem">
      <div>
        <app-task-header-menu></app-task-header-menu>
      </div>
      <button class="btn-icon"
              mat-icon-button [matMenuTriggerFor]="menu"
              aria-label="Example icon-button with a menu">
        <fa-icon [icon]="['fas', 'user-circle']" [size]="'2x'"></fa-icon>
      </button>
      <mat-menu #menu="matMenu">
        <button mat-menu-item [matMenuTriggerFor]="books">
          {{'HEADER.MENU.BOOK.LABEL' | translate}}
        </button>
        <button mat-menu-item [matMenuTriggerFor]="series">
          {{'HEADER.MENU.SERIES.LABEL' | translate}}
        </button>
        <button mat-menu-item [matMenuTriggerFor]="artists">
          {{'HEADER.MENU.ARTIST.LABEL' | translate}}
        </button>
        <button mat-menu-item [routerLink]="'/administration/edition/borrower'">
          {{'HEADER.MENU.BORROWER.LABEL' | translate}}
        </button>
        <button mat-menu-item [matMenuTriggerFor]="bookTypes">
          {{'HEADER.MENU.BOOK_TYPE.LABEL' | translate}}
        </button>
        <button (click)="sendLogout()" mat-menu-item>
          <fa-icon [icon]="['fas', 'sign-out-alt']" style="padding-right: 10px"></fa-icon>
          {{'HEADER.SIGN_OUT' | translate}}
        </button>
      </mat-menu>
      <mat-menu #books="matMenu">
        <button mat-menu-item [routerLink]="'/administration/edition/book'">
          {{'HEADER.MENU.BOOK.SUB_MENU.EDITION' | translate}}
        </button>
      </mat-menu>
      <mat-menu #artists="matMenu">
        <button mat-menu-item [routerLink]="'/administration/edition/artist'">
          {{'HEADER.MENU.ARTIST.SUB_MENU.EDITION' | translate}}
        </button>
      </mat-menu>
      <mat-menu #series="matMenu">
        <button mat-menu-item [routerLink]="'/administration/edition/series'">
          {{'HEADER.MENU.SERIES.SUB_MENU.EDITION' | translate}}
        </button>
      </mat-menu>
      <mat-menu #bookTypes="matMenu">
        <button mat-menu-item [routerLink]="'/administration/book-type/gestion'">
          {{'HEADER.MENU.BOOK_TYPE.SUB_MENU.EDITION' | translate}}
        </button>
      </mat-menu>
    </div>
    <ng-template #loginButton>
      <div fxFlex="25" *ngIf="!isAuthenticated" class="clickable" (click)="sendLogin()">
        <button mat-stroked-button color="primary" style="padding-left: 1rem">
          <span>{{'HEADER.SIGN_IN.LABEL' | translate}}</span>
        </button>
      </div>
    </ng-template>
  `
})
export class HeaderMenuAdministrationComponent {

  @Input() isAuthenticated: boolean;
  @Input() urlScanner: string;
  @Output() login: EventEmitter<void> = new EventEmitter();
  @Output() logout: EventEmitter<void> = new EventEmitter();

  sendLogin() {
    this.login.emit();
  }

  sendLogout() {
    this.logout.emit();
  }
}
