import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderMenuAdministrationComponent} from './header-menu-administration.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";

describe('HeaderMenuAdministrationComponent', () => {
  let component: HeaderMenuAdministrationComponent;
  let fixture: ComponentFixture<HeaderMenuAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        HeaderMenuAdministrationComponent,
      ],
      imports: [
        NgxTranslateTestingModule
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMenuAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
