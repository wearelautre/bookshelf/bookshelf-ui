import {KeyValue} from "./key-value";

export interface LibraryCount {
  booksCount: number;
  bookTypesCount: number;
  bookTypesBooks: KeyValue[];
}
