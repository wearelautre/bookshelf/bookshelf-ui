import {VersionInformation} from './version-information';

export interface AppConfig {
  oauth: OAuthConfig;
  versionInformation: VersionInformation;
  globalVersion: string;
}

export interface OAuthConfig {
  realm: string;
  stsServer: string;
  clientId: string;
  silentRenew: boolean;
  renewTimeBeforeTokenExpiresInSeconds: number;
}
