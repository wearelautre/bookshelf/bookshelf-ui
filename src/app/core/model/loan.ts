import {RestEntity} from './rest-entity';
import {MinimalBook} from "./minimal-book";

export interface Loan extends RestEntity {
  book?: MinimalBook;
  bookIsbn: string;
  returnDate?: Date;
  borrowDate: Date;
}
