import {RestEntity} from './rest-entity';
import {BookType} from "./book-type";

export interface SeriesBase extends RestEntity {
  status: SeriesStatus;
  oneShot?: boolean;
  name: string;
  tome?: number
  bookType?: BookType;
}

export interface Series extends SeriesBase {
  cycles?: SeriesCycle[];
  editor?: string;
}

export interface SeriesStatus {
  readCount: number,
  possessedCount: number,
  totalCount: number,
  status: Series.SeriesStatusEnum
}

export namespace Series {
  export type SeriesStatusEnum = 'PLANNED' | 'ON_GOING' | 'FINISHED' | 'UNKNOWN';

  export const SeriesStatusEnum = {
    PLANNED: 'PLANNED' as SeriesStatusEnum,
    ON_GOING: 'ON_GOING' as SeriesStatusEnum,
    FINISHED: 'FINISHED' as SeriesStatusEnum,
    UNKNOWN: 'UNKNOWN' as SeriesStatusEnum
  }

  export namespace SeriesPublicationStatusEnum {
    export function values(): SeriesStatusEnum[] {
      return Object.keys(Series.SeriesStatusEnum).filter((v: string | number) => typeof v == 'string' && isNaN(Number(v))) as SeriesStatusEnum[]
    }
  }
}

export interface BookSeries extends SeriesBase {
  seriesCycle?: BookSeriesCycle;
  tome: number
}

export interface SeriesCycle extends RestEntity {
  name: string
}

export interface BookSeriesCycle extends SeriesCycle {
  tome: number
}
