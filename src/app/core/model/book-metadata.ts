export class BookMetadata {
    acquisitionDate: string;
    offered: boolean;
    originalReleaseDate: string;
    pageCount: number;
    price: number;
    priceCurrency: string;
}
