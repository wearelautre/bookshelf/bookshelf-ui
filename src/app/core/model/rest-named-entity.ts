import {RestEntity} from "./rest-entity";

export type idType = string | number

export interface RestNamedEntity extends RestEntity {
  name?: string;
}
