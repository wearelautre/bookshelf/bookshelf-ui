import {Editor} from './editor';
import {BookSeries} from './series';
import {RestEntity} from './rest-entity';

export interface MinimalBook extends RestEntity {
  editor?: Editor;
  isbn: string;
  series?: BookSeries;
  title: string;
}
