import {RestNamedEntity} from "./rest-named-entity";
import {idType} from "./rest-entity";

export interface Role extends RestNamedEntity {
  id: idType;
  name: string;
}
