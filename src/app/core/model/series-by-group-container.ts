import {Book} from './book';
import {SeriesStatus} from "./series";

export interface SeriesInfo {
  status: SeriesStatus;
  editor?: string;
  books: Book[];
  oneShot: boolean;
}

export type SeriesByGroupContainer = Map<string, BookBySeriesContainer>;
export type BookBySeriesContainer = Map<string, SeriesInfo>;
