import {BookSeries, BookSeriesCycle, Series, SeriesStatus} from '../series';
import {idType} from "../rest-entity";
import {BookType} from "../book-type";

export class BookSeriesImpl implements BookSeries {

  id?: idType;
  status: SeriesStatus;

  constructor(
    public name: string = '',
    public oneShot: boolean = false,
    public bookType: BookType = null,
    public tome: number = null,
    public seriesCycle: BookSeriesCycle = null
  ) {
    this.id = null;
    this.status = {
      possessedCount: 0,
      readCount: 0,
      totalCount: 0,
      status: Series.SeriesStatusEnum.UNKNOWN
    };
  }

  static fromSeriesSearch(seriesSearch: string): BookSeries {
    return new BookSeriesImpl(seriesSearch);
  }

  static fromSeries(series: BookSeries): BookSeriesImpl {
    return {
      ...new BookSeriesImpl(series.name, series.oneShot, series.bookType, series.tome, series.seriesCycle),
      id: series.id,
      status: series.status
    };
  }
}
