import {BookImpl} from './book-impl';
import {EditorImpl} from './editor-impl';
import {BookSeriesImpl} from './book-series-impl';
import {ArtistImpl} from './artist-impl';
import {Book} from '../book';
import {ContractImpl} from './contract-impl';
import {Series} from "../series";
import StatusEnum = Book.StatusEnum;

describe('BookImpl', () => {
    test('should create an instance', () => {
        expect(new BookImpl()).toBeTruthy();
    });
    test('fromBook', () => {
        expect(BookImpl.fromBook({
            arkId: 'arkId',
            contracts: [{artists: [new ArtistImpl('authorName', [], 1)], role: {name: 'role1', id: null}}],
            collection: 'collection',
            cover: 'cover',
            editor: {name: 'editorName'},
            isbn: 'isbn',
            series: {
                id: 34,
                tome: 7,
                status: {
                    possessedCount: 0,
                    readCount: 0,
                    totalCount: 0,
                    status: Series.SeriesStatusEnum.UNKNOWN
                },
                oneShot: false,
                name: 'name'
            },
            status: undefined,
            title: 'title',
            year: 'year',
            metadata: null,
            lent: false
        })).toStrictEqual({
            arkId: 'arkId',
            contracts: [new ContractImpl({name: 'role1', id: null}, [new ArtistImpl('authorName', [], 1)])],
            collection: 'collection',
            cover: 'cover',
            editor: new EditorImpl('editorName'),
            isbn: 'isbn',
            series: {
                bookType: null,
                id: 34,
                name: "name",
                oneShot: false,
                status: {
                    possessedCount: 0,
                    readCount: 0,
                    totalCount: 0,
                    status: Series.SeriesStatusEnum.UNKNOWN
                },
                seriesCycle: null,
                tome: 7,
            },
            status: StatusEnum.UNREAD,
            title: 'title',
            year: 'year',
            metadata: null,
            lent: false
        });
    });
    test('fromBookSearch', () => {
        const book = new BookImpl();
        book.arkId = 'arkId';
        book.contracts = [
            new ContractImpl({name: 'role1', id: null}, [new ArtistImpl('authorName1'), new ArtistImpl('authorName2')]),
            new ContractImpl({name: 'role2', id: null}, [new ArtistImpl('authorName1')])
        ];
        book.collection = 'collection';
        book.cover = 'cover';
        book.editor = new EditorImpl('editorName');
        book.isbn = 'isbn';
        book.series = new BookSeriesImpl('name');
        book.status = StatusEnum.UNREAD;
        book.title = 'title';
        book.year = 'year';
        expect(BookImpl.fromBookSearch({
            arkId: 'arkId',
            artists: [{roles: ['role1', 'role2'], name: 'authorName1'}, {roles: ['role1'], name: 'authorName2'}],
            collection: 'collection',
            cover: 'cover',
            editor: 'editorName',
            isbn: 'isbn',
            series: 'name',
            title: 'title',
            tome: 'tome',
            year: 'year'
        })).toStrictEqual(book);
    });
    test('fromBookSearch no contract', () => {
        const book = new BookImpl();
        book.arkId = 'arkId';
        book.contracts = [];
        book.collection = 'collection';
        book.cover = 'cover';
        book.editor = new EditorImpl('editorName');
        book.isbn = 'isbn';
        book.series = new BookSeriesImpl('name');
        book.status = StatusEnum.UNREAD;
        book.title = 'title';
        book.year = 'year';
        expect(BookImpl.fromBookSearch({
            arkId: 'arkId',
            artists: null,
            collection: 'collection',
            cover: 'cover',
            editor: 'editorName',
            isbn: 'isbn',
            series: 'name',
            title: 'title',
            tome: 'tome',
            year: 'year'
        })).toStrictEqual(book);
    });
});
