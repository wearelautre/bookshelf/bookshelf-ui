import {Series, SeriesCycle, SeriesStatus} from '../series';
import {idType} from "../rest-entity";
import {BookType} from "../book-type";

export class SeriesImpl implements Series {

  id?: idType;
  status: SeriesStatus;

  constructor(
    public name: string = '',
    public editor = null,
    public oneShot = false,
    public bookType: BookType = null,
    public cycles: SeriesCycle[] = []
  ) {
    this.id = null;
    this.status = {
      readCount: 0,
      possessedCount: 0,
      totalCount: 0,
      status: Series.SeriesStatusEnum.UNKNOWN
    };
    this.oneShot = oneShot
  }
}
