import {ContractImpl} from './contract-impl';
import {ArtistImpl} from './artist-impl';

describe('ContractImpl', () => {
    test('should create an instance', () => {
        expect(new ContractImpl({name: 'role1', id: null})).toBeTruthy();
    });

    test('fromContract', () => {
        expect(ContractImpl.fromContract({artists: [{name: 'name', webLinks: [], id: 0}], role: {name: 'role1', id: null}}))
            .toStrictEqual(new ContractImpl({name: 'role1', id: null}, [{name: 'name', webLinks: [], id: 0}]));
    });
    test('fromBookSearch', () => {
        expect(ContractImpl.fromArtistSearch({name: 'name', roles: ['role1']}))
            .toStrictEqual([new ContractImpl({name: 'role1', id: null}, [new ArtistImpl('name')])]);
    });
});
