import {Artist} from '../artist';
import {ArtistSearch} from '../../../administration/models/artist-search';
import {Role} from '../role';
import {ArtistImpl} from './artist-impl';
import {Contract} from '../contract';

export class ContractImpl implements Contract {
    constructor(
        public role: Role,
        public artists: Artist[] = []
    ) {
    }

    static fromContract(contract: Contract): ContractImpl {
        return new ContractImpl(
            contract.role,
            contract.artists
        );
    }

    static fromArtistSearch(artistSearch: ArtistSearch): Contract[] {
        return artistSearch.roles.map(role => new ContractImpl({name: role, id: null}, [new ArtistImpl(artistSearch.name)]));
    }
}
