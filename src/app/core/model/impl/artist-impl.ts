import {Artist} from '../artist';
import {ArtistSearch} from '../../../administration/models/artist-search';
import {WebLink} from '../web-link';
import {idType} from "../rest-entity";

export class ArtistImpl implements Artist {
    constructor(
        public name: string = '',
        public webLinks: WebLink[] = [],
        public id: idType = null
    ) {
    }

    static fromArtist(artist: Artist): ArtistImpl {
        return new ArtistImpl(
            artist.name,
            artist.webLinks,
            artist.id
        );
    }

    static fromartistearch(artistearch: ArtistSearch): Artist {
        return new ArtistImpl(artistearch.name);
    }
}
