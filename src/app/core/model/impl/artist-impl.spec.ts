import {ArtistImpl} from './artist-impl';

describe('AuthorImpl', () => {
    test('should create an instance', () => {
        expect(new ArtistImpl()).toBeTruthy();
    });

    test('fromArtist', () => {
        expect(ArtistImpl.fromArtist({name: 'name', webLinks: [], id: 0}))
            .toStrictEqual(new ArtistImpl('name', [], 0));
    });
    test('fromBookSearch', () => {
        expect(ArtistImpl.fromartistearch({name: 'name', roles: ['role1']}))
            .toStrictEqual(new ArtistImpl('name'));
    });
});
