export interface KeyValue {
  value: number|string;
  key: string;
}
