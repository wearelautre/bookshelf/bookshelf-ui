import {BookSeries} from "./series";

export interface MinimalLibraryItem<T extends MinimalBook = MinimalBook> {
  id: number,
  status: LibraryItem.StatusEnum;
  possessionStatus: LibraryItem.PossessionStatusEnum;
  lent: false,
  book: T
}

export interface MinimalLibraryItemWithCover extends MinimalLibraryItem<MinimalBookWithCover> {
}

export interface MinimalBook {
  editor: MinimalEditor;
  isbn?: string;
  series: BookSeries | null;
  title: string;
}

export interface MinimalBookWithCover extends MinimalBook {
  cover: string;
}

export interface MinimalEditor {
  name: string;
}

export type LibraryItemStatus = keyof typeof LibraryItem.StatusEnum;
export type LibraryItemPossessionStatus = keyof typeof LibraryItem.PossessionStatusEnum;

export namespace LibraryItem {
  export type StatusEnum = 'READ' | 'UNREAD' | 'READING';
  export const StatusEnum = {
    READ: 'READ' as StatusEnum,
    UNREAD: 'UNREAD' as StatusEnum,
    READING: 'READING' as StatusEnum
  };

  export namespace LibraryItemStatus {
    export function values(): LibraryItemStatus[] {
      return Object.keys(LibraryItem.StatusEnum).filter((v: string | number) => typeof v == 'string' && isNaN(Number(v))) as LibraryItemStatus[]
    }
  }

  export type PossessionStatusEnum = 'NOT_POSSESSED' | 'POSSESSED';
  export const PossessionStatusEnum = {
    NOT_POSSESSED: 'NOT_POSSESSED' as PossessionStatusEnum,
    POSSESSED: 'POSSESSED' as PossessionStatusEnum
  };

  export namespace LibraryItemPossessionStatus {
    export function values(): LibraryItemPossessionStatus[] {
      return Object.keys(LibraryItem.PossessionStatusEnum).filter((v: string | number) => typeof v == 'string' && isNaN(Number(v))) as LibraryItemPossessionStatus[]
    }
  }
}
