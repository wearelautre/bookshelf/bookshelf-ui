export type idType = string | number

export interface RestEntity {
  id?: idType;
}
