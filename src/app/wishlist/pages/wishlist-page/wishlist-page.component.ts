import {Component, OnDestroy, OnInit} from '@angular/core';
import {WishlistService} from "../../services/wishlist.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {BehaviorSubject, Observable, Subscription, withLatestFrom} from "rxjs";
import {filter, map} from "rxjs/operators";
import {ScrollEventService} from "../../../shared/services/scroll-event.service";
import {HttpResponse} from "@angular/common/http";
import {mapToResolvedDataPaginated, ResolvedDataPaginated} from "../../../shared/resolvers/list-resolver";
import {WishlistBook} from "../../models/wishlist-book";
import {ActivatedRoute} from "@angular/router";
import {WishlistBookAdministrationService} from "../../../administration/services/wishlist-book-administration.service";

@Component({
  selector: 'app-wishlist-page',
  template: `
    <div style="height: 100%">
      <div fxLayout="column" class="content">
        <div fxLayout="row" fxLayoutAlign="center">
          <h1 style="text-align: center">{{'WISHLIST.LIST.TITLE' | translate}}</h1>
        </div>
        <div fxLayout="row" fxLayoutAlign="center">
          <div fxFlex.lt-sm="100" fxFlex="70" fxLayoutGap.lt-sm="2rem" fxLayoutGap="1rem" fxLayout="column">
            <app-wishlist-book-card
              *ngFor="let element of list$ | async"
              [book]="element"
              (delete)="delete($event)"
            >
            </app-wishlist-book-card>
            <div fxLayoutAlign="center">
              <mat-progress-spinner
                *ngIf="nextPageLoading$ | async"
                class="example-margin"
                color="primary"
                mode="indeterminate">
              </mat-progress-spinner>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class WishlistPageComponent implements OnInit, OnDestroy {
  public list$ = this.wishlistService.list$;

  private nextPageLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  get nextPageLoading$(): Observable<boolean> {
    return this.nextPageLoading.asObservable();
  }

  private rootSubscription: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private wishlistService: WishlistService,
    private wishlistBookAdministrationService: WishlistBookAdministrationService,
    private scrollEventService: ScrollEventService,
    private paginationService: PaginationService
  ) {
  }

  ngOnInit(): void {
    this.rootSubscription.add(
      this.route.data.subscribe({
        next: (data) => this.paginationService.updatePaginationAndFilter(data.resolvedData)
      })
    );
    this.rootSubscription.add(
      this.scrollEventService.bottomReached$.pipe(
        withLatestFrom(this.nextPageLoading$),
        filter(([_, nextPageAlreadyLoading]) => !nextPageAlreadyLoading),
      ).subscribe(([_, _nextPageAlreadyLoading]) => {
        const nextPage = this.paginationService.getNextPage()
        console.log('nextPage', nextPage)
        if (nextPage !== null) {
          this.search(nextPage)
        }
      })
    )
  }

  private search(page = 0): void {
    const nextPage = page > 0;
    if (nextPage) {
      this.nextPageLoading.next(true);
    }
    this.wishlistService.search(
      [], page, 100, true, page == 0
    )
      .pipe(map((response: HttpResponse<WishlistBook[]>) => mapToResolvedDataPaginated(response)))
      .subscribe({
        next: (data: ResolvedDataPaginated<WishlistBook>) => {
          this.wishlistService.updateList(data.items, nextPage)
          this.paginationService.updatePagination(data)
          if (nextPage) {
            this.nextPageLoading.next(false);
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.rootSubscription.unsubscribe();
  }

  delete(wishlistBook: WishlistBook) {
    this.wishlistBookAdministrationService.delete(wishlistBook).subscribe(_ => this.search());
  }
}
