import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {WishlistPageComponent} from './wishlist-page.component';
import {PaginationService} from "../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {RouterTestingModule} from "@angular/router/testing";
import {WishlistService} from "../../services/wishlist.service";
import {wishlistServiceMock} from "../../services/__mocks__/wishlist.service";
import {scrollEventServiceMock} from "../../../shared/services/__mocks__/scroll-event.service";
import {ScrollEventService} from "../../../shared/services/scroll-event.service";
import {
  wishlistBookAdministrationServiceMock
} from "../../../administration/services/__mocks__/wishlist-book-administration.service";
import {WishlistBookAdministrationService} from "../../../administration/services/wishlist-book-administration.service";
import {WishlistBook} from "../../models/wishlist-book";
import clearAllMocks = jest.clearAllMocks;

describe('WishlistPageComponent', () => {
  let component: WishlistPageComponent;
  let fixture: ComponentFixture<WishlistPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WishlistPageComponent],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {provide: ScrollEventService, useValue: scrollEventServiceMock},
        {provide: WishlistService, useValue: wishlistServiceMock},
        {provide: WishlistBookAdministrationService, useValue: wishlistBookAdministrationServiceMock}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should update on next page event', waitForAsync(() => {
      clearAllMocks();
      const spy = jest.spyOn<any, any>(component[`nextPageLoading`], 'next').mockImplementation(() => {
      })
      paginationServiceMock.getNextPage.mockImplementation(() => 2)
      scrollEventServiceMock.bottomReached$.next()
      expect(wishlistServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 2, 100, true, false
      );
      expect(spy).toHaveBeenNthCalledWith(1, true)
      expect(spy).toHaveBeenNthCalledWith(2, false)
    }));

    describe('nextPageLoading', () => {
      test('should emit an event', waitForAsync(() => {
        jest.clearAllMocks();
        component[`nextPageLoading`].next(true);
        component.nextPageLoading$.subscribe(value => expect(value).toBeTruthy());
      }));
    });
    test('should call the delete method on service', () => {
      const spy = jest.spyOn<any, any>(component, `search`).mockImplementation(() => {
      });
      const wishlistBook: WishlistBook = {editor: undefined, isbn: "", series: undefined, title: ""}
      component.delete(wishlistBook);
      expect(wishlistBookAdministrationServiceMock.delete).toHaveBeenNthCalledWith(1, wishlistBook);
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
