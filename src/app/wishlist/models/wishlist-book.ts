import {Editor} from "../../core/model/editor";
import {BookSeries} from "../../core/model/series";
import {RestEntity} from "../../core/model/rest-entity";

export interface WishlistBook extends RestEntity {
  isbn: string;
  cover?: string;
  editor: Editor;
  series: BookSeries;
  title: string;
}
