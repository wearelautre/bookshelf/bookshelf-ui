import {TestBed} from '@angular/core/testing';

import {WishlistService} from './wishlist.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('WishlistService', () => {
  let httpTestingController: HttpTestingController;
  let service: WishlistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(WishlistService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['wishlist-books']);
      expect(service).toBeTruthy();
    });
  });
});
