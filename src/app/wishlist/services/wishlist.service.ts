import {Injectable} from '@angular/core';
import {EntityService} from "../../shared/services/entity.service";
import {HttpClient} from "@angular/common/http";
import {CoreService} from "../../core/services/core.service";
import {WishlistBook} from "../models/wishlist-book";

type T = WishlistBook;

@Injectable({
  providedIn: 'root'
})
export class WishlistService extends EntityService<T> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'wishlist-books');
  }
}
