import fn = jest.fn;
import {of} from 'rxjs';
import {HttpResponse} from "@angular/common/http";

export const wishlistServiceMock = {
  list$: of([]),
  search: fn(() => of(new HttpResponse({body: []}))),
  updateList: jest.fn(() => []),
};

