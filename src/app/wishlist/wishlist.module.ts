import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {WishlistPageComponent} from './pages/wishlist-page/wishlist-page.component';
import {WishlistResolver} from "./resolvers/wishlist.resolver";
import {SharedModule} from "../shared/shared.module";
import {WishlistBookCardComponent} from './components/wishlist-book-card/wishlist-book-card.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

const routes: Routes = [{
    path: '',
    resolve: {resolvedData: WishlistResolver},
    runGuardsAndResolvers: 'always',
    component: WishlistPageComponent
}];

@NgModule({
    declarations: [
        WishlistPageComponent,
        WishlistBookCardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        MatCardModule,
        MatDividerModule,
        MatProgressSpinnerModule
    ]
})
export class WishlistModule {
}
