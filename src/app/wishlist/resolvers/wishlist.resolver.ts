import {Injectable} from '@angular/core';
import {ListResolver} from "../../shared/resolvers/list-resolver";
import {WishlistService} from "../services/wishlist.service";
import {WishlistBook} from "../models/wishlist-book";
import {PaginationService} from "../../shared/services/pagination.service";

@Injectable({
  providedIn: 'root'
})
export class WishlistResolver extends ListResolver<WishlistBook, WishlistService> {
  private static pageSize: number = 100

  constructor(
    service: WishlistService,
    paginationService: PaginationService
  ) {
    super(
      service,
      WishlistResolver.pageSize,
      null,
      paginationService
    )
  }
}
