import {TestBed} from '@angular/core/testing';

import {WishlistResolver} from './wishlist.resolver';
import {WishlistService} from "../services/wishlist.service";
import {wishlistServiceMock} from "../services/__mocks__/wishlist.service";

describe('WishlistResolver', () => {
  let resolver: WishlistResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: WishlistService, useValue: wishlistServiceMock}
      ]
    });
    resolver = TestBed.inject(WishlistResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(resolver).toBeTruthy();
    });
  });
});
