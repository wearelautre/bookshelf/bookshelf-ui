import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WishlistBookCardComponent} from './wishlist-book-card.component';
import {
  AngularAuthOidcClientTestingModule
} from "../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module";

describe('WishlistBookCardComponent', () => {
  let component: WishlistBookCardComponent;
  let fixture: ComponentFixture<WishlistBookCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WishlistBookCardComponent],
      imports: [
        AngularAuthOidcClientTestingModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistBookCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });

    test('should init without book', () => {
      expect(component.img).toBeNull();
    });

    test('should init with book', () => {
      component.book = {editor: undefined, isbn: "", series: undefined, title: "", cover: 'coverName'};
      component.ngOnInit();
      expect(component.img.alt).toStrictEqual('coverName');
      expect(component.img.src.startsWith('/files/covers/coverName')).toBeTruthy();
    });
    describe('[DELETE]', () => {
      test('emit delete event', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.delete, 'emit').mockImplementation(() => ({}));
        component.book = {editor: undefined, isbn: "123456789", series: undefined, title: "", cover: 'coverName'};
        component.removeFromWishlist();
        expect(spy).toHaveBeenNthCalledWith(1, component.book);
      });
    });
  });
});
