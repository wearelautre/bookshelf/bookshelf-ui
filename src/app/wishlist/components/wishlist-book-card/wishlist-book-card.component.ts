import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DisplayImage} from "../../../shared/display-image";
import {Image} from "../../../shared/models/image";
import {WishlistBook} from "../../models/wishlist-book";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {OidcSecurityService} from 'angular-auth-oidc-client';

@Component({
  selector: 'app-wishlist-book-card',
  template: `
    <div class="wrapper" fxLayoutGap="1rem" fxLayout="row" *ngIf="book">
      <div class="cover-wrapper">
        <app-loader-img [img]="img">
        </app-loader-img>
      </div>
      <div fxLayout="row" fxFlex="80" fxLayoutAlign="space-between">
        <div fxLayout="column" fxFlex>
          <h3 fxLayout="row" fxLayoutAlign="space-between center">
            <span>
              <span style="display: block">
                {{book.title}}
                <span *ngIf="!book.series?.oneShot && book.series?.tome !== null">(T{{book.series?.tome}})</span>
              </span>
              <app-series-name-display *ngIf="book.series" [series]="book.series"></app-series-name-display>
            </span>
          </h3>
          <div fxLayout="row" fxLayoutGap="1rem" disabled *ngIf="isAuthenticated$ | async;">
            <button mat-button mat-stroked-button disabled color="accent">
              {{ 'WISHLIST.LIST.BOOK.BUTTONS.ADD' | translate }}
            </button>
            <button mat-button mat-stroked-button (click)="removeFromWishlist()" color="warn">
              {{ 'WISHLIST.LIST.BOOK.BUTTONS.REMOVE' | translate }}
            </button>
          </div>
        </div>
        <div fxLayout="column" fxLayoutGap="0.5rem" fxHide fxShow.lt-sm style="align-self: center"
             *ngIf="isAuthenticated$ | async;">
          <button mat-button mat-stroked-button disabled color="accent">
            <fa-icon [icon]="['fas', 'plus']"></fa-icon>
          </button>
          <button mat-button mat-stroked-button disabled color="warn">
            <fa-icon [icon]="['fas', 'trash']"></fa-icon>
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .cover-wrapper {
      width: 150px;
    }

    mat-checkbox ::ng-deep .mat-checkbox-inner-container {
      width: 40px;
      height: 40px;
    }

    div.wrapper {
      padding-bottom: 0.25rem;
      padding-top: 0.25rem
    }
  `]
})
export class WishlistBookCardComponent extends DisplayImage implements OnInit {
  @Input() book: WishlistBook

  public img: Image;

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));
  @Output()
  delete: EventEmitter<WishlistBook> = new EventEmitter<WishlistBook>();

  constructor(
    private oidcSecurityService: OidcSecurityService
  ) {
    super('/files/covers');
  }

  ngOnInit(): void {
    this.img = this.book ? this.getImg(this.book.cover) : null;
  }

  removeFromWishlist(): void {
    this.delete.emit(this.book)
  }
}
