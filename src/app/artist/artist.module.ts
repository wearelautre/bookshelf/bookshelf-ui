import {RouterModule, Routes} from "@angular/router";
import {InjectionToken, NgModule} from "@angular/core";
import {ArtistListResolver} from "./resolvers/artist-list-resolver";
import {ArtistListPageComponent} from "./pages/artist-list-page/artist-list-page.component";
import {ArtistResolver} from "./resolvers/artist-resolver";
import {ArtistDetailsComponent} from "./components/artist-details/artist-details.component";
import {ArtistListComponent} from "./components/artist-list/artist-list.component";
import {ArtistDetailsCardComponent} from "./components/artist-details-card/artist-details-card.component";
import {ArtistBookListComponent} from "./components/artist-book-list/artist-book-list.component";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatSortModule} from "@angular/material/sort";
import {MatListModule} from '@angular/material/list';
import {PaginationService} from "../shared/services/pagination.service";
import {ArtistService} from "./services/artist.service";
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";


export const PAGINATION_ARTIST_VIEW = new InjectionToken<string>("paginationArtistView");
export const ARTIST_VIEW = new InjectionToken<string>("artistView");

const routes: Routes = [{
    path: '',
    resolve: {resolvedData: ArtistListResolver},
    runGuardsAndResolvers: 'always',
    component: ArtistListPageComponent,
    children: [
        {
            path: 'details/:id',
            resolve: {artist: ArtistResolver},
            component: ArtistDetailsComponent
        }
    ]
}];

@NgModule({
    declarations: [
        ArtistListPageComponent,
        ArtistListComponent,
        ArtistBookListComponent,
        ArtistDetailsCardComponent,
        ArtistDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        MatSortModule,
        MatExpansionModule,
        MatListModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule
    ],
    providers: [
        {provide: PAGINATION_ARTIST_VIEW, useClass: PaginationService},
        {provide: ARTIST_VIEW, useClass: ArtistService},
        ArtistListResolver
    ]
})
export class ArtistModule {
}
