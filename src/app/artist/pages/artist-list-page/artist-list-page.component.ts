import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Artist} from "../../../core/model/artist";
import {PaginationService} from "../../../shared/services/pagination.service";

export interface QueryParams {
  search?: string,
  page?: number,
  size?: number
}

@Component({
  selector: 'app-artist-list-page',
  templateUrl: './artist-list-page.component.html'
})
export class ArtistListPageComponent implements OnInit {

  public filterStr$ = this.paginationService.filterStr$

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private paginationService: PaginationService
  ) {
  }

  ngOnInit(): void {
    this.route.data.subscribe({
      next: (data) => this.paginationService.updatePaginationAndFilter(data.resolvedData)
    });
  }

  onFilter(search: string) {
    this.updateQueryParams({search, page: 0})
  }

  onNewPage(pageEvent: { page: number, size: number }): void {
    this.updateQueryParams({page: pageEvent.page, size: pageEvent.size})
  }

  updateQueryParams(queryParams: QueryParams) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams,
        queryParamsHandling: 'merge',
      }
    );
  }

  onSelectedItem(item: Artist) {
    this.router.navigate(
      ['details', item.id],
      {
        relativeTo: this.route,
        queryParamsHandling: 'merge',
      })
  }
}
