import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Location} from "@angular/common";
import {ArtistListPageComponent} from './artist-list-page.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {RouterTestingModule} from "@angular/router/testing";
import {PaginationService} from "../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {MockListFilterComponent} from "../../../shared/component/list-filter/__mocks__/list-filter.component";
import {MockArtistListComponent} from "../../components/artist-list/__mocks__/artist-list.component";
import {ArtistImpl} from "../../../core/model/impl/artist-impl";
import {MockArtistBookListComponent} from "../../components/artist-book-list/__mocks__/artist-book-list.component";

describe('ArtistListComponent', () => {
  let location: Location;
  let component: ArtistListPageComponent;
  let fixture: ComponentFixture<ArtistListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ArtistListPageComponent,
        MockListFilterComponent,
        MockArtistListComponent,
        MockArtistBookListComponent
      ],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule.withRoutes(
          [{path: 'details/:id', component: MockArtistBookListComponent}]
        )
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistListPageComponent);
    component = fixture.componentInstance;
    location = TestBed.inject(Location);
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(component).toBeTruthy();
    });
  });


  describe('Typescript test', () => {
    describe('onNewPage', () => {
      test('should call updateQueryParams', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'updateQueryParams').mockImplementation(() => ({}));
        const newPage = {size: 300, page: 43}
        component.onNewPage(newPage)
        expect(spy).toHaveBeenNthCalledWith(1, {page: 43, size: 300});
      });
    });
    describe('onFilter', () => {
      test('should call updateQueryParams', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'updateQueryParams').mockImplementation(() => ({}));
        component.onFilter('str')
        expect(spy).toHaveBeenNthCalledWith(1, {page: 0, search: 'str'});
      });
    });
    describe('onSelectItem', () => {
      test('should navigate to book list', fakeAsync(() => {
        jest.clearAllMocks();
        component.onSelectedItem({...new ArtistImpl(), id: 3})
        tick();
        expect(location.path()).toBe('/details/3');
      }));
    });
    describe('updateQueryParams', () => {
      test('should navigate with passing params', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));
        component.updateQueryParams({page: 0, search: 'str'})
        expect(spy).toHaveBeenNthCalledWith(1, [],
          {
            relativeTo: component[`route`],
            queryParams: {page: 0, search: 'str'},
            queryParamsHandling: 'merge',
          });
      });
    });
  });
});
