import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl, PageEvent} from '@angular/material/paginator';
import {Artist} from "../../../core/model/artist";
import {TranslateService} from "@ngx-translate/core";
import {MyCustomPaginatorIntlService} from "../../../shared/services/my-custom-paginator-intl.service";
import {ArtistService} from "../../services/artist.service";
import {PaginationService} from "../../../shared/services/pagination.service";

@Component({
    selector: 'app-artist-list',
    template: `
        <div fxLayout="column">
            <mat-paginator [pageSizeOptions]="pageSizes"
                           [pageIndex]="currentPage$ | async"
                           [length]="totalCount$ | async"
                           [pageSize]="pageSize$ | async"
                           (page)="onNewPage($event)">
            </mat-paginator>
            <table mat-table [dataSource]="list$ | async"
                   [attr.aria-describedby]="'ARTIST.LIST.TABLE.DESCRIBED_BY' | translate">
                <ng-container matColumnDef="name">
                    <th scope="col" mat-header-cell *matHeaderCellDef> Name</th>
                    <td mat-cell *matCellDef="let element"> {{element.name}} </td>
                </ng-container>

                <ng-container matColumnDef="WebLinks">
                    <th scope="col" mat-header-cell *matHeaderCellDef> WebLinks</th>
                    <td mat-cell *matCellDef="let element">
                        <app-web-link-list [webLinks]="element.webLinks"></app-web-link-list>
                    </td>
                </ng-container>

                <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
                <tr mat-row (click)="selectArtist(row)" *matRowDef="let row; columns: displayedColumns;"></tr>
            </table>
        </div>
    `,
    styles: [`
      td {
        cursor: pointer;
      }

      .mat-column-name {
        flex: none;
        width: 60%;
      }
    `],
    providers: [
        {
            provide: MatPaginatorIntl,
            useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('ARTISTS', translate),
            deps: [TranslateService]
        }
    ]
})
export class ArtistListComponent {

    public list$ = this.artistService.list$;
    public pageSize$ = this.paginationService.pageSize$
    public currentPage$ = this.paginationService.currentPage$
    public totalCount$ = this.paginationService.totalCount$
    public pageSizes = PaginationService.pageSizes;
    displayedColumns: string[] = ['name', 'WebLinks'];

    @ViewChild(MatPaginator)
    public paginator: MatPaginator;

    @Output()
    public newPage: EventEmitter<{ page: number, size: number }> = new EventEmitter<{ page: number, size: number }>()

    @Output()
    public selectedItem: EventEmitter<Artist> = new EventEmitter<Artist>()

    constructor(
        private artistService: ArtistService,
        private paginationService: PaginationService
    ) {
    }

    onNewPage(page: PageEvent): void {
        this.newPage.emit({page: page.previousPageIndex !== page.pageIndex ? page.pageIndex : 0, size: page.pageSize})
    }

    selectArtist(item: Artist) {
        this.selectedItem.emit(item)
    }
}
