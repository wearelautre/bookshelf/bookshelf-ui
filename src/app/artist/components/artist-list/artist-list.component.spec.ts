import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArtistListComponent} from './artist-list.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MockWebLinkListComponent} from "../../../shared/web-link-list/__mocks__/web-link-list.component";
import {MatPaginatorModule, PageEvent} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from "@angular/material/sort";
import {ArtistService} from "../../services/artist.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {artistServiceMock} from "../../services/__mocks__/artist.service";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {Artist} from "../../../core/model/artist";
import {ArtistImpl} from "../../../core/model/impl/artist-impl";

describe('ArtistListComponent', () => {
    let component: ArtistListComponent;
    let fixture: ComponentFixture<ArtistListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ArtistListComponent, MockWebLinkListComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                MatPaginatorModule,
                MatTableModule,
                MatSortModule
            ],
            providers: [
                {provide: ArtistService, useValue: artistServiceMock},
                {provide: PaginationService, useValue: paginationServiceMock}
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ArtistListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should be created', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        describe('onNewPage', () => {
            test('should emit', () => {
                jest.clearAllMocks();
                const spy = jest.spyOn(component[`newPage`], 'emit').mockImplementation(() => ({}));
                const newPage: PageEvent = {length: 13, pageSize: 300, pageIndex: 43, previousPageIndex: 42}
                component.onNewPage(newPage)
                expect(spy).toHaveBeenNthCalledWith(1, {page: 43, size: 300});
            });
            test('should emit with page = 0', () => {
                jest.clearAllMocks();
                const spy = jest.spyOn(component[`newPage`], 'emit').mockImplementation(() => ({}));
                const newPage: PageEvent = {length: 13, pageSize: 300, pageIndex: 43, previousPageIndex: 43}
                component.onNewPage(newPage)
                expect(spy).toHaveBeenNthCalledWith(1, {page: 0, size: 300});
            });
        });
        describe('selectArtist', () => {
            test('should emit', () => {
                jest.clearAllMocks();
                const spy = jest.spyOn(component[`selectedItem`], 'emit').mockImplementation(() => ({}));
                const artist: Artist = new ArtistImpl();
                component.selectArtist(artist)
                expect(spy).toHaveBeenNthCalledWith(1, artist);
            });
        });
    });
});
