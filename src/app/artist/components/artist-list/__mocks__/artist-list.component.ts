import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-artist-list',
  template: `Mock Artist List`
})
export class MockArtistListComponent {
  @Output() public newPage: any = new EventEmitter<void>();
  @Output() public selectedItem: any = new EventEmitter<void>();
}
