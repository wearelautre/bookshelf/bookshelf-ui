import {Component, Input} from '@angular/core';
import {Artist} from "../../../core/model/artist";

@Component({
  selector: 'app-artist-details-card',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title> {{ artist.name }} </mat-card-title>
      </mat-card-header>
      <mat-card-content *ngIf="artist.webLinks.length > 0">
        <app-web-link-list [webLinks]="artist.webLinks"></app-web-link-list>
      </mat-card-content>
    </mat-card>
  `
})
export class ArtistDetailsCardComponent {

  @Input() artist: Artist;
}
