import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-artist-details-card',
  template: `mock app-artist-details-card`
})
export class MockArtistDetailsCardComponent {

  @Input() artist: any;
}
