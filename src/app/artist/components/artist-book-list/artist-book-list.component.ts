import {Component, Input} from '@angular/core';
import {BooksByRole} from "../../models/books-by-role";

@Component({
  selector: 'app-artist-book-list',
  template: `
    <mat-list style="padding-top: 2rem">
      <ng-container *ngFor="let booksByRole of booksByRoles">
        <div class="mat-h4" style="opacity: 0.5"> {{ booksByRole.role.name | uppercase }} </div>
        <div *ngFor="let book of booksByRole.books; let i = index">
          <mat-list-item>
            <app-title-display [title]="book.title" [series]="book.series"></app-title-display>
          </mat-list-item>
        </div>
      </ng-container>
    </mat-list>
  `
})
export class ArtistBookListComponent {

  @Input()
  public booksByRoles: BooksByRole[] = [];
}
