import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-artist-book-list',
  template: ''
})
export class MockArtistBookListComponent {

  @Input() public booksByRoles: any[];
}
