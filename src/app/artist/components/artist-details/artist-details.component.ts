import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ArtistService} from "../../services/artist.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {BooksByRole} from "../../models/books-by-role";
import {Artist} from "../../../core/model/artist";

@Component({
  selector: 'app-artist-details',
  template: `
    <div fxFlex="100" fxLayout="column" fxLayoutGap="1rem">
      <button mat-button (click)="close()"> {{ 'ARTIST.LIST.DETAILS.BUTTONS.CLOSE' | translate }} </button>
      <div fxLayout="column">

        <app-artist-details-card *ngIf="artist"
                                 [artist]="artist"></app-artist-details-card>

        <app-artist-book-list *ngIf="booksByRoles$ | async as booksByRoles"
                              [booksByRoles]="booksByRoles"></app-artist-book-list>
      </div>
    </div>
  `
})
export class ArtistDetailsComponent implements OnInit {

  artist!: Artist;
  booksByRoles$!: Observable<BooksByRole[]>;

  constructor(
    private artistService: ArtistService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.route.data.pipe(map(data => data.artist)).subscribe(artist => {
      this.artist = artist
      this.booksByRoles$ = this.artistService.getAllBooksByRole(artist.id);
    })
  }

  public close(): void {
    this.router.navigate(
      ['artist'],
      {
        queryParamsHandling: 'preserve'
      })
  }
}
