import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArtistDetailsComponent} from './artist-details.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MockArtistDetailsCardComponent} from "../artist-details-card/__mocks__/artist-details-card.component";
import {MockArtistBookListComponent} from "../artist-book-list/__mocks__/artist-book-list.component";
import {ArtistService} from "../../services/artist.service";
import {artistServiceMock} from "../../services/__mocks__/artist.service";
import {RouterTestingModule} from "@angular/router/testing";

describe('ArtistDetailsComponent', () => {
  let component: ArtistDetailsComponent;
  let fixture: ComponentFixture<ArtistDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistDetailsComponent, MockArtistDetailsCardComponent, MockArtistBookListComponent ],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule
      ],
      providers: [{
        provide: ArtistService, useValue: artistServiceMock
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(component).toBeTruthy();
    });
  });
});
