import {Role} from "../../core/model/role";
import {MinimalBook} from "../../core/model/minimal-book";

export class BooksByRole {
  role: Role;
  books: MinimalBook[];
}
