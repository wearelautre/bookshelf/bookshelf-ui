import {Injectable} from '@angular/core';
import {Artist} from "../../core/model/artist";
import {EntityService} from "../../shared/services/entity.service";
import {HttpClient} from "@angular/common/http";
import {CoreService} from "../../core/services/core.service";
import {Observable, throwError} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {BooksByRole} from "../models/books-by-role";
import {Utils} from "../../shared/utils";
import {idType} from "../../core/model/rest-entity";

type T = Artist;

@Injectable({
  providedIn: 'root'
})
export class ArtistService extends EntityService<T> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'artists')
  }

  getAllBooksByRole(id: idType): Observable<BooksByRole[]> {
    this.coreService.updateLoadingState(true);
    return this.http.get<BooksByRole[]>(Utils.buildUrl([...this.apiEndpoints, 'contracts'], id)).pipe(
      tap(() => this.coreService.updateLoadingState(false)),
      catchError(err => throwError(() => this.manageError(err)))
    );
  }
}
