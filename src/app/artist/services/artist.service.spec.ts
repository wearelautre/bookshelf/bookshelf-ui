import {TestBed, waitForAsync} from '@angular/core/testing';

import {ArtistService} from './artist.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('ArtistService', () => {
  let httpTestingController: HttpTestingController;
  let service: ArtistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ArtistService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['artists']);
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('[GET_ALL_BOOKS_BY_ROLE]', () => {
      test('should be the right one', waitForAsync(() => {
        service.getAllBooksByRole('id')
          .subscribe((value) => {
            expect(value).toStrictEqual(['test']);
          });
        const req = httpTestingController.expectOne('/api/artists/id/contracts');
        expect(req.request.method).toStrictEqual('GET');
        req.flush(['test']);
      }));
      test('should be the right one -- error', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `manageError`)
          .mockImplementation(() => 'tutu');
        service.getAllBooksByRole('id')
          .subscribe({
            error: (err) => {
              expect(spy).toHaveBeenCalledTimes(1);
              expect(err).toStrictEqual('tutu')
            }
          });
        const req = httpTestingController.expectOne('/api/artists/id/contracts');
        expect(req.request.method).toStrictEqual('GET');
        req.flush('toto', {status: 400, statusText: "Bad Request"});
      }));
    });
  });
});
