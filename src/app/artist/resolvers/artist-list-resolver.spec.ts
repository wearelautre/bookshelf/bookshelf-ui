import {TestBed} from '@angular/core/testing';
import {ArtistListResolver} from './artist-list-resolver';
import {ArtistService} from "../services/artist.service";
import {artistServiceMock} from "../services/__mocks__/artist.service";
import {paginationServiceMock} from "../../shared/services/__mocks__/pagination.service";
import {PaginationService} from "../../shared/services/pagination.service";


describe('ArtistListResolver', () => {
  let service: ArtistListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ArtistListResolver,
        {provide: ArtistService, useValue: artistServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    service = TestBed.inject(ArtistListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
});
