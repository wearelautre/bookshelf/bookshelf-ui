import {Injectable} from '@angular/core';
import {Artist} from "../../core/model/artist";
import {ListResolver} from "../../shared/resolvers/list-resolver";
import {ArtistService} from "../services/artist.service";

@Injectable()
export class ArtistListResolver extends ListResolver<Artist, ArtistService> {

  constructor(
    service: ArtistService
  ) {
    super(service)
  }
}
