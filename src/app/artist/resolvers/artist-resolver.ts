import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {ArtistService} from "../services/artist.service";
import {Artist} from "../../core/model/artist";

@Injectable({
  providedIn: 'root'
})
export class ArtistResolver  {

  constructor(
    private artistService: ArtistService
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Artist> {
    return this.artistService.getById(route.params.id);
  }
}
