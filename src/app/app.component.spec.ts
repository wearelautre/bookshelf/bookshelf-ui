import {TestBed, waitForAsync} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MockHeaderComponent} from './core/components/header/__mocks__/header.component';
import {MockFooterComponent} from './core/components/footer/__mocks__/footer.component';
import {MockLoaderBarComponent} from './shared/loader-bar/__mocks__/loader-bar.component';
import {
  AngularAuthOidcClientTestingModule
} from '../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {ScrollEventService} from "./shared/services/scroll-event.service";
import {scrollEventServiceMock} from "./shared/services/__mocks__/scroll-event.service";

describe('AppComponent', () => {
  let fixture;
  let component;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockHeaderComponent,
        MockFooterComponent,
        MockLoaderBarComponent
      ],
      imports: [
        RouterTestingModule,
        AngularAuthOidcClientTestingModule,
        ScrollingModule,
      ],
      providers: [{provide: ScrollEventService, useValue: scrollEventServiceMock}]
    }).compileComponents();
  }));

  beforeEach((() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterAll((() => {
    jest.clearAllMocks();
  }));

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });
});
