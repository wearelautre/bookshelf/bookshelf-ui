import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Data, Router} from "@angular/router";
import {PaginationService} from "../../../shared/services/pagination.service";
import {QueryParams} from "../../../artist/pages/artist-list-page/artist-list-page.component";
import {filter} from "rxjs/operators";

@Component({
    selector: 'app-edition',
    template: `
        <div fxLayout="column" class="content">
            <div fxLayout="row" fxLayoutAlign="center">
                <h1 class="center-text">{{ translationRootKey + '.EDITION.TITLE' | translate}}</h1>
            </div>
            <div fxLayout="row" fxLayoutAlign="space-around">
                <app-list-filter fxFlex="100"
                                 [initFilter$]="filterStr$"
                                 (filter)="onFilter($event)"
                                 [translateKey]="translationRootKey + '.EDITION.FILTER_PLACEHOLDER'">
                </app-list-filter>
            </div>
            <router-outlet fxFlex="100"></router-outlet>
        </div>
    `,
    styles: [`
      .edition-page-wrapper {
        display: flex;
        flex-direction: column;
      }

      .center-text {
        text-align: center;
      }
    `]
})
export class EditionComponent implements OnInit {

    public filterStr$ = this.paginationService.filterStr$
    public translationRootKey: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private paginationService: PaginationService
    ) {

    }

    ngOnInit(): void {
        let activatedRoute = this.route;
        while (activatedRoute.firstChild) activatedRoute = activatedRoute.firstChild;

        activatedRoute.data
            .subscribe({
                next: (data: Data) => {
                    this.paginationService.updatePaginationAndFilter(data.resolvedData)
                    this.translationRootKey = data.translationRootKey
                }
            });
        this.paginationService.newPageEvent$.pipe(filter(event => event !== null))
            .subscribe(newPage => this.onNewPage(newPage))
    }

    onFilter(search: string) {
        this.updateQueryParams({search, page: 0})
    }

    onNewPage(pageEvent: { page: number, size: number }): void {
        this.updateQueryParams({page: pageEvent.page, size: pageEvent.size})
    }

    updateQueryParams(queryParams: QueryParams) {
        this.router.navigate(
            [],
            {
                relativeTo: this.route,
                queryParams,
                queryParamsHandling: 'merge',
            }
        );
    }
}
