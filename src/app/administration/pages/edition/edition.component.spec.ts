import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {EditionComponent} from './edition.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MockListFilterComponent} from "../../../shared/component/list-filter/__mocks__/list-filter.component";
import {RouterTestingModule} from "@angular/router/testing";
import {
  MockBookEditionListComponent
} from "../../components/book/edition/book-edition-list/__mocks__/book-edition-list.component";
import {ActivatedRoute} from "@angular/router";
import {of} from "rxjs";
import {PaginationService} from "../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";

describe('EditionComponent', () => {
  let component: EditionComponent;
  let fixture: ComponentFixture<EditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditionComponent, MockListFilterComponent, MockBookEditionListComponent],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {
          provide: ActivatedRoute,
          useValue: {
            firstChild: {
              data: of({
                items: [],
                pagination: {
                  totalCount: 0,
                  pageSize: 0,
                  totalPage: 0,
                  pageNumber: 0,
                },
                filter: 'string'
              })
            }
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', waitForAsync(() => {
      expect(component).toBeTruthy();
    }));
  });

  describe('Typescript test', () => {
    describe('onNewPage', () => {
      test('should call updateQueryParams', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'updateQueryParams').mockImplementation(() => ({}));
        const newPage = {size: 300, page: 43}
        component.onNewPage(newPage)
        expect(spy).toHaveBeenNthCalledWith(1, {page: 43, size: 300});
      });
    });
    describe('onFilter', () => {
      test('should call updateQueryParams', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'updateQueryParams').mockImplementation(() => ({}));
        component.onFilter('str')
        expect(spy).toHaveBeenNthCalledWith(1, {page: 0, search: 'str'});
      });
    });
    describe('updateQueryParams', () => {
      test('should navigate with passing params', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));
        component.updateQueryParams({page: 0, search: 'str'})
        expect(spy).toHaveBeenNthCalledWith(1, [],
          {
            relativeTo: component[`route`],
            queryParams: {page: 0, search: 'str'},
            queryParamsHandling: 'merge',
          });
      });
    });
  });
});
