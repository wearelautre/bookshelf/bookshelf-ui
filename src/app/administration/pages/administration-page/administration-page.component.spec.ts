import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AdministrationPageComponent} from './administration-page.component';
import {MockTaskMenuComponent} from "../../components/tasks/task-menu/__mocks__/task-menu.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterTestingModule} from "@angular/router/testing";

describe('AdministrationPageComponent', () => {
  let component: AdministrationPageComponent;
  let fixture: ComponentFixture<AdministrationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministrationPageComponent, MockTaskMenuComponent ],
      imports: [
        RouterTestingModule,
        FlexLayoutModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
