export interface TaskCount {
  ALL: number
  [type: string]: number;
}
