import {RestEntity} from "../../core/model/rest-entity";

export enum TaskStatusEnum {
  DONE = 'DONE',
  TODO = 'TODO'
}

export enum TaskTypeEnum {
  ADD_BOOK = 'ADD_BOOK',
  OTHER = 'OTHER'
}

export interface Task extends RestEntity {
  createDate: Date;
  extra: string;
  status: TaskStatusEnum;
  type: TaskTypeEnum;
}
