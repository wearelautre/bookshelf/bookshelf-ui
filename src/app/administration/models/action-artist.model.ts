import {Artist} from "../../core/model/artist";

export enum ActionArtistTypeEnum {
  DELETE = 'delete',
  MOVE = 'move',
  DUPLICATE = 'duplicate'
}

export interface ActionArtist {
  action: ActionArtistTypeEnum,
  artistWithIndex: { artist: Artist, index?: number },
  roleIndex?: number
}

export interface ActionArtistMinimal {
  action: ActionArtistTypeEnum,
  index: number
}

export const moveArtist = (artist: Artist, roleIndex: number, index: number): ActionArtist => {
  return {action: ActionArtistTypeEnum.MOVE, artistWithIndex: {artist, index}, roleIndex}
}

export const duplicateArtist = (artist: Artist, index: number): ActionArtist => {
  return {action: ActionArtistTypeEnum.DUPLICATE, artistWithIndex: {artist, index}}
}

