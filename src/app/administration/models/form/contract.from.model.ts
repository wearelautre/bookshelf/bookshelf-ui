import {FormArray, FormGroup, Validators} from '@angular/forms';
import {Contract} from '../../../core/model/contract';
import {ArtistForm, ArtistFormType} from './artist-form.model';
import {RoleForm, RoleFormType} from "./role-form.model";

export interface ContractFormType {
  artists: FormArray<FormGroup<ArtistFormType>>;
  role: FormGroup<RoleFormType>;
}

export class ContractFrom {

  static formGroupWithValue(contract: Contract): FormGroup<ContractFormType> {
    return new FormGroup<ContractFormType>({
      artists: new FormArray<FormGroup<ArtistFormType>>(contract.artists.map(artist => ArtistForm.formGroupWithValue(artist)), Validators.required),
      role: RoleForm.formGroupWithValue(contract.role)
    });
  }

  static formGroup(): FormGroup<ContractFormType> {
    return new FormGroup<ContractFormType>({
      artists: new FormArray<FormGroup<ArtistFormType>>([], Validators.required),
      role: RoleForm.formGroup(true)
    });
  }
}
