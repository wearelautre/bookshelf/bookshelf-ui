import {FormControl, FormGroup, Validators} from "@angular/forms";
import {idType} from "../../../core/model/rest-entity";
import {Role} from "../../../core/model/role";

export interface RoleFormType {
  id: FormControl<idType | null>;
  name: FormControl<string | null>;
}

export class RoleForm {

  static formGroupWithValue(role: Role): FormGroup<RoleFormType> {
    return new FormGroup<RoleFormType>({
      id: new FormControl(role.id),
      name: new FormControl(role.name, Validators.required)
    });
  }

  static formGroup(required: boolean = false): FormGroup<RoleFormType> {
    return new FormGroup<RoleFormType>({
      id: new FormControl(null),
      name: new FormControl(null, Validators.required)
    }, required ? Validators.required : null);
  }
}
