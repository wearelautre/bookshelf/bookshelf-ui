import {RoleForm} from "./role-form.model";
import {Validators} from "@angular/forms";

describe('RoleForm', () => {
  test('should return a FormGroup with values', () => {
    const fromGroup = RoleForm.formGroupWithValue({id: 10, name: 'nameRole'})
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: 10, name: 'nameRole'
    });
  });

  test('should return a FormGroup without values not required', () => {
    const fromGroup = RoleForm.formGroup()
    expect(fromGroup).toBeTruthy();
    expect(fromGroup[`hasValidator`](Validators.required)).toBeFalsy();
    expect(fromGroup.value).toStrictEqual({
      id: null, name: null
    });
  });

  test('should return a FormGroup without values required', () => {
    const fromGroup = RoleForm.formGroup(true)
    expect(fromGroup).toBeTruthy();
    expect(fromGroup[`hasValidator`](Validators.required)).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: null, name: null
    });
  });
});
