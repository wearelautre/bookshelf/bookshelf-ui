import {BookForm} from './book-form.model';
import {BookSeriesImpl} from '../../../core/model/impl/book-series-impl';
import moment from "moment";

describe('BookForm', () => {
  test('should create an empty instance ', () => {
    const bookForm: BookForm = new BookForm(
      {
        arkId: undefined,
        contracts: undefined,
        collection: undefined,
        cover: undefined,
        editor: undefined,
        id: 0,
        isbn: undefined,
        series: undefined,
        status: undefined,
        title: undefined,
        year: undefined,
        metadata: null,
        lent: false
      }
    );
    expect(bookForm).toBeTruthy();
    expect(bookForm.arkId.value).toBeNull();
    expect(bookForm.contracts.value).toStrictEqual([]);
    expect(bookForm.collection.value).toBeNull();
    expect(bookForm.editor.value).toStrictEqual({name: null, id: null});
    expect(bookForm.isbn.value).toBeNull();
    expect(bookForm.series.value).toStrictEqual({
      name: null,
      tome: 0,
      bookType: {name: null, id: null},
      id: null,
      seriesCycle: {name: null, id: null, tome: null}
    });
    expect(bookForm.title.value).toStrictEqual(null);
    expect(bookForm.year.value).toBeNull();
  });
  test('should create an partial empty instance ', () => {
    const bookForm: BookForm = new BookForm(
      {
        arkId: undefined,
        contracts: undefined,
        collection: undefined,
        cover: undefined,
        editor: undefined,
        id: 0,
        isbn: undefined,
        series: undefined,
        status: undefined,
        title: undefined,
        year: undefined,
        metadata: {
          acquisitionDate: null,
          offered: null,
          originalReleaseDate: null,
          pageCount: null,
          price: null,
          priceCurrency: null
        },
        lent: false
      }
    );
    expect(bookForm).toBeTruthy();
    expect(bookForm.arkId.value).toBeNull();
    expect(bookForm.contracts.value).toStrictEqual([]);
    expect(bookForm.collection.value).toBeNull();
    expect(bookForm.editor.value).toStrictEqual({name: null, id: null});
    expect(bookForm.isbn.value).toBeNull();
    expect(bookForm.series.value).toStrictEqual({
      name: null,
      tome: 0,
      bookType: {name: null, id: null},
      id: null,
      seriesCycle: {name: null, id: null, tome: null}
    });
    expect(bookForm.title.value).toStrictEqual(null);
    expect(bookForm.year.value).toBeNull();
    expect(bookForm.metadata.value).toStrictEqual({
      acquisitionDate: null,
      offered: null,
      originalReleaseDate: null,
      pageCount: null,
      price: null,
      priceCurrency: null
    });
  });
  test('should create an instance ', () => {
    const series = new BookSeriesImpl('Les 5 Terres');
    series.tome = 7
    series.seriesCycle = {
      tome: 1,
      id: 1,
      name: 'Cycle 2',
    }
    series.bookType = {name: 'BD', id: null}
    const bookForm: BookForm = new BookForm(
      {
        arkId: 'arkId',
        contracts: [],
        collection: 'collection',
        cover: undefined,
        editor: {name: 'e', id: 23},
        id: 0,
        isbn: '1234567890123',
        series,
        status: undefined,
        title: 'title',
        year: 'year',
        metadata: {
          acquisitionDate: '2022/11/30',
          offered: true,
          originalReleaseDate: '2205/04',
          pageCount: 123,
          price: 456,
          priceCurrency: 'EUR'
        },
        lent: false
      }
    );
    expect(bookForm).toBeTruthy();
    expect(bookForm.arkId.value).toStrictEqual('arkId');
    expect(bookForm.isbn.value).toStrictEqual('1234567890123');
    expect(bookForm.collection.value).toStrictEqual('collection');
    expect(bookForm.title.value).toStrictEqual('title');
    expect(bookForm.year.value).toStrictEqual('year');
    expect(bookForm.series.value).toStrictEqual(
      {
        name: 'Les 5 Terres', tome: 7,
        bookType: {name: 'BD', id: null}, id: null,
        seriesCycle: {name: 'Cycle 2', id: 1, tome: 1}
      }
    );
    expect(bookForm.editor.value).toStrictEqual({name: 'e', id: 23});
    expect(bookForm.contracts.value).toStrictEqual([]);
    expect(bookForm.metadata.value).toStrictEqual({
      acquisitionDate: moment(["2022", "11", "30"], 'yyyy/MM/DD'),
      offered: true,
      originalReleaseDate: moment(["2205", "04"], 'yyyy/MM'),
      pageCount: 123,
      price: 456,
      priceCurrency: 'EUR'
    });
  });
});
