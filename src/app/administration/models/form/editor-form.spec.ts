import {EditorForm} from './editor-form.model';

describe('EditorForm', () => {
  test('should return a FormGroup with values', () => {
    const fromGroup = EditorForm.formGroupWithValue({id: 10, name: 'nameEditor'})
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: 10, name: 'nameEditor'
    });
  });

  test('should return a FormGroup without values', () => {
    const fromGroup = EditorForm.formGroup()
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: null, name: null
    });
  });
});
