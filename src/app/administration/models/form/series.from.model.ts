import {FormArray, FormControl, FormGroup, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';

import {Series} from "../../../core/model/series";
import {SeriesCycleForm, SeriesCycleFormType} from "./series-cycle-form.model";
import {idType} from "../../../core/model/rest-entity";


export class SeriesForm {
  id = new FormControl<idType | null>(null);
  name = new FormControl<string | null>(null, [Validators.required]);
  totalCount = new FormControl<number>(0, [Validators.min(0)],);
  status = new FormControl<string | null>(Series.SeriesStatusEnum.UNKNOWN);
  cycles = new FormArray<FormGroup<SeriesCycleFormType>>([]);
  bookType = new UntypedFormGroup({
    id: new UntypedFormControl(null),
    name: new UntypedFormControl(null, [Validators.required])
  });

  constructor(series: Series) {
    if (series != null) {
      (series.cycles ?? []).forEach(cycle => this.cycles.push(SeriesCycleForm.formGroupWithValue(cycle)));
      this.id.patchValue(series.id);
      this.bookType.setValue({
        id: series.bookType?.id ?? null,
        name: series.bookType?.name ?? null
      });
      this.name.setValue(series.name);
      this.totalCount.setValue(series.status.totalCount);
      this.totalCount.setValidators([Validators.min(series.status.possessedCount)]);
      this.status.setValue(series.status.status);
    }
  }

  get value(): any {
    return {
      id: this.id.value,
      bookType: this.bookType.value,
      name: this.name.value,
      cycles: this.cycles.value
    }
  }
}
