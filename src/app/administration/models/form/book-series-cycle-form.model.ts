import {FormControl, Validators} from '@angular/forms';
import {BookSeriesCycle} from "../../../core/model/series";
import {idType} from "../../../core/model/rest-entity";

export class BookSeriesCycleForm {
  id = new FormControl<idType | null>(null);
  name = new FormControl<string | null>(null, Validators.required);
  tome = new FormControl<number>(null, [Validators.min(0)]);

  constructor(bookSeriesCycle: BookSeriesCycle) {
    if (bookSeriesCycle != null) {
      this.id.setValue(bookSeriesCycle?.id);
      this.name.setValue(bookSeriesCycle?.name);
      this.tome.setValue(bookSeriesCycle?.tome);
    }
  }

  get value(): any {
    return {
      id: this.id.value,
      name: this.name.value,
      tome: this.tome.value,
    }
  }
}
