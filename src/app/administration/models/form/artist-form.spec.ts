import {ArtistForm} from './artist-form.model';

describe('ArtistForm', () => {
  test('should return a FormGroup with values', () => {
    const fromGroup = ArtistForm.formGroupWithValue({id: 10, webLinks: [], name: 'nameArtist'})
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: 10, name: 'nameArtist'
    });
  });

  test('should return a FormGroup without values', () => {
    const fromGroup = ArtistForm.formGroup()
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: null, name: null
    });
  });
});
