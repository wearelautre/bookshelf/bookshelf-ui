import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SeriesCycle} from "../../../core/model/series";
import {idType} from "../../../core/model/rest-entity";

export interface SeriesCycleFormType {
  id: FormControl<idType | null>;
  name: FormControl<string | null>;
}

export class SeriesCycleForm {

  static formGroupWithValue(seriesCycle: SeriesCycle): FormGroup<SeriesCycleFormType> {
    return new FormGroup<SeriesCycleFormType>({
      id: new FormControl(seriesCycle.id),
      name: new FormControl(seriesCycle.name, Validators.required)
    });
  }

  static formGroup(): FormGroup<SeriesCycleFormType> {
    return new FormGroup<SeriesCycleFormType>({
      id: new FormControl(null),
      name: new FormControl(null, Validators.required)
    });
  }
}
