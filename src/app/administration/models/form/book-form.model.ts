import {FormArray, FormControl, FormGroup, UntypedFormGroup, Validators} from '@angular/forms';
import {Book} from '../../../core/model/book';
import {ContractFormType, ContractFrom} from './contract.from.model';
import moment from "moment";
import {BookSeriesForm} from "./book-series.from.model";
import {EditorForm} from "./editor-form.model";

export class BookForm {
  title = new FormControl<string | null>(null, [Validators.required]);
  arkId = new FormControl<string | null>(null);
  isbn = new FormControl<string | null>(null, [Validators.required]);
  year = new FormControl<string | null>(null);
  collection = new FormControl<string | null>(null);
  series = new UntypedFormGroup({...new BookSeriesForm(null)});
  editor = EditorForm.formGroup();
  metadata = new UntypedFormGroup({
    acquisitionDate: new FormControl<string | null>(null),
    offered: new FormControl<boolean | null>(false),
    originalReleaseDate: new FormControl<string | null>(null),
    pageCount: new FormControl<number | null>(null, Validators.min(0)),
    price: new FormControl<number | null>(null, [Validators.min(0), Validators.pattern(/^\d*(\.\d{0,2})?$/)]),
    priceCurrency: new FormControl<string | null>(null)
  });
  contracts = new FormArray<FormGroup<ContractFormType>>([]);

  constructor(
    book: Book
  ) {
    // Init form logic
    if (book.title) {
      this.title.setValue(book.title);
    }
    if (book.arkId) {
      this.arkId.setValue(book.arkId);
    }
    if (book.isbn) {
      this.isbn.setValue(book.isbn);
    }
    if (book.year) {
      this.year.setValue(book.year);
    }
    if (book.collection) {
      this.collection.setValue(book.collection);
    }
    if (book.series) {
      this.series.patchValue({...new BookSeriesForm(book.series).value});
    }
    if (book.editor) {
      this.editor.setValue({
        id: book.editor.id,
        name: book.editor.name
      });
    }
    if (book.metadata) {
      this.metadata.setValue({
        acquisitionDate: book.metadata.acquisitionDate === null ? null : moment(moment(book.metadata.acquisitionDate.split("/"), 'yyyy/MM/DD')),
        offered: book.metadata.offered,
        originalReleaseDate: book.metadata.originalReleaseDate === null ? null : moment(moment(book.metadata.originalReleaseDate.split("/"), 'yyyy/MM')),
        pageCount: book.metadata.pageCount,
        price: book.metadata.price,
        priceCurrency: book.metadata.priceCurrency,
      });
    }

    if (book.contracts) {
      book.contracts.forEach(contract => this.contracts.push(
          ContractFrom.formGroupWithValue(contract)
        )
      );
    }
  }
}
