import {FormControl, FormGroup, Validators} from "@angular/forms";
import {idType} from "../../../core/model/rest-entity";
import {Editor} from "../../../core/model/editor";

export interface EditorFormType {
  id: FormControl<idType | null>;
  name: FormControl<string | null>;
}

export class EditorForm {

  static formGroupWithValue(editor: Editor): FormGroup<EditorFormType> {
    return new FormGroup<EditorFormType>({
      id: new FormControl(editor.id),
      name: new FormControl(editor.name, Validators.required)
    });
  }

  static formGroup(): FormGroup<EditorFormType> {
    return new FormGroup<EditorFormType>({
      id: new FormControl(null),
      name: new FormControl(null, Validators.required)
    });
  }
}
