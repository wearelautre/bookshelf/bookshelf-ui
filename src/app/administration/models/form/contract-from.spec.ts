import {ContractFrom} from './contract.from.model';

describe('ContractFrom', () => {
  test('should return a FormGroup with values', () => {
    const formGroup = ContractFrom.formGroupWithValue({artists: [], role: {id: 9, name: 'role1'}});
    expect(formGroup).toBeTruthy();
    expect(formGroup.value.artists).toStrictEqual([]);
    expect(formGroup.value.role).toStrictEqual({id: 9, name: 'role1'});
  });

  test('should return a FormGroup without values', () => {
    const formGroup = ContractFrom.formGroup();
    expect(formGroup).toBeTruthy();
    expect(formGroup.value.artists).toStrictEqual([]);
    expect(formGroup.value.role).toStrictEqual({id: null, name: null});
  });
});
