import {FormControl, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';

import {BookSeries} from "../../../core/model/series";
import {BookSeriesCycleForm} from "./book-series-cycle-form.model";
import {idType} from "../../../core/model/rest-entity";

export class BookSeriesForm {
  id = new FormControl<idType | null>(null);
  name = new FormControl<string | null>(null, [Validators.required]);
  tome = new FormControl<number>(0, [Validators.min(0)]);
  seriesCycle = new UntypedFormGroup({...new BookSeriesCycleForm(null)});
  bookType = new UntypedFormGroup({
    id: new UntypedFormControl(null),
    name: new UntypedFormControl(null, [Validators.required])
  });

  constructor(series: BookSeries) {
    if (series != null) {
      this.seriesCycle.patchValue({...new BookSeriesCycleForm(series.seriesCycle).value});
      this.id.patchValue(series.id);
      this.bookType.setValue({
        id: series.bookType?.id ?? null,
        name: series.bookType?.name ?? null
      });
      this.name.setValue(series.name);
      this.tome.setValue(series.tome);
    }
  }

  get value(): any {
    return {
      id: this.id.value,
      bookType: this.bookType.value,
      name: this.name.value,
      seriesCycle: this.seriesCycle.value,
      tome: this.tome.value
    }
  }

  get rawValue(): any {
    return {
      id: this.id.value,
      bookType: this.bookType.getRawValue(),
      name: this.name.value,
      seriesCycle: this.seriesCycle.getRawValue()
    }
  }
}
