import {SeriesCycleForm} from './series-cycle-form.model';

describe('SeriesCycleForm', () => {
  test('should return a FormGroup with values', () => {
    const fromGroup = SeriesCycleForm.formGroupWithValue({id: 10, name: 'nameSeriesCycle'})
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: 10, name: 'nameSeriesCycle'
    });
  });

  test('should return a FormGroup without values', () => {
    const fromGroup = SeriesCycleForm.formGroup()
    expect(fromGroup).toBeTruthy();
    expect(fromGroup.value).toStrictEqual({
      id: null, name: null
    });
  });
});
