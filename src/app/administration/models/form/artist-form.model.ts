import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Artist} from '../../../core/model/artist';
import {idType} from "../../../core/model/rest-entity";

export interface ArtistFormType {
  id: FormControl<idType | null>;
  name: FormControl<string | null>;
}

export class ArtistForm {

  static formGroupWithValue(artist: Artist): FormGroup<ArtistFormType> {
    return new FormGroup<ArtistFormType>({
      id: new FormControl(artist.id),
      name: new FormControl(artist.name, Validators.required)
    });
  }

  static formGroup(): FormGroup<ArtistFormType> {
    return new FormGroup<ArtistFormType>({
      id: new FormControl(null),
      name: new FormControl(null, Validators.required)
    });
  }
}
