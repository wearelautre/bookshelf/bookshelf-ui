import {Directive, ViewChild} from '@angular/core';
import {BehaviorSubject, Subscription, switchMap} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {UntypedFormGroup} from '@angular/forms';
import {AdministrationService} from '../services/administration.service';
import {UiData} from '../../shared/ui-data';
import {PaginationService} from "../../shared/services/pagination.service";
import {ActivatedRoute} from "@angular/router";
import {SearchCriteria} from "../../shared/services/entity.service";

@Directive()
export abstract class EditionList<T> {

    public currentPage$ = this.paginationService.currentPage$
    public pageSize$ = this.paginationService.pageSize$
    public totalCount$ = this.paginationService.totalCount$
    public pageSizes = PaginationService.pageSizes;

    @ViewChild(MatPaginator)
    public paginator: MatPaginator;

    public uiDatas: UiData<T>[] = [];

    protected subscriptions: Subscription[] = [];

    protected constructor(
        protected adminService: AdministrationService<T>,
        protected paginationService: PaginationService,
        private route: ActivatedRoute,
    ) {
    }

    protected onInit(): void {
        this.route.data.subscribe({
            next: (data) => this.paginationService.updatePaginationAndFilter(data.resolvedData)
        });
        this.adminService.list$.subscribe(apiData => this.uiDatas = apiData.map(r => this.convertToUiData(r)))
    }

    protected onDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    isRemovable(index: number): boolean {
        return true;
    }

    delete(index: number) {
        this.subscriptions.push(
            this.adminService.delete(this.uiDatas[index].item).pipe(
                switchMap(() => {
                    const {filterStr, pageSize, currentPage} = this.paginationService.getCurrentPagination()
                    return this.adminService.search(filterStr ? this.getSearchCriteriaList(filterStr) : [], currentPage, pageSize)
                })).subscribe()
        );
    }

    protected getSearchCriteriaList(search: string): SearchCriteria[] {
        return [{name: 'name', operation: ':', value: `*${search}*`}]
    }

    onFinish(index: number): void {
        const uiData = this.uiDatas[index]
        this.send(uiData, {...uiData.item, ...uiData.form.value})
    }

    protected send(uiData: UiData<T>, t: T) {
        this.subscriptions.push(
            this.adminService.update(t, false)
                .subscribe((value) => {
                    uiData.item = value;
                    uiData.start.next(false);
                    uiData.isSaved.next(true);
                    setTimeout(() => uiData.isSaved.next(false), 3000);
                })
        );
    }

    onNewPage(page: PageEvent): void {
        this.paginationService.onNewPage({
            page: page.previousPageIndex !== page.pageIndex ? page.pageIndex : 0,
            size: page.pageSize
        })
    }

    private convertToUiData(item: T): UiData<T> {
        const form: UntypedFormGroup = this.initForm(item);
        const start = new BehaviorSubject(false);
        this.subscriptions.push(form.valueChanges.subscribe(() => start.next(form.valid)));
        return {form, start, item, isSaved: new BehaviorSubject<boolean>(false)};
    }

    protected abstract initForm(item: T): UntypedFormGroup
}
