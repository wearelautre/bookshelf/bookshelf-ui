import {ComponentFixture, fakeAsync, flush, TestBed, tick} from '@angular/core/testing';

import {ProgressbarComponent} from './progressbar.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {BehaviorSubject} from 'rxjs';

describe('ProgressbarComponent', () => {
    let component: ProgressbarComponent;
    let fixture: ComponentFixture<ProgressbarComponent>;
    let start = new BehaviorSubject<boolean>(false);

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                MatProgressBarModule
            ],
            declarations: [ProgressbarComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgressbarComponent);
        component = fixture.componentInstance;
        component.start = start;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        test('should return the progress bar value', fakeAsync(() => {
            expect(component.value).toStrictEqual(0);
            component[`time`] = 4;
            start.next(true);
            expect(component.value).toStrictEqual(0);
            tick(1000);
            expect(component.value).toStrictEqual(25);
            tick(2000);
            expect(component.value).toStrictEqual(75);
            tick(1000);
            expect(component.value).toStrictEqual(100);
            tick(1000);
            expect(component.value).toStrictEqual(100);
            flush()
        }));
        describe('end', () => {
            test('should emit', () => {
                expect(component.value).toStrictEqual(0);
                const spy = jest.spyOn(component.finish, 'emit')
                    .mockImplementation(() => ({}));
                component.end({value: 100});
                expect(spy).toHaveBeenNthCalledWith(1, true)
            });
            test('should not emit', () => {
                expect(component.value).toStrictEqual(0);
                const spy = jest.spyOn(component.finish, 'emit')
                    .mockImplementation(() => ({}));
                component.end({value: 99});
                expect(spy).toHaveBeenCalledTimes(0)
            });
        });
    });
});
