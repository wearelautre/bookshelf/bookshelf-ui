import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ProgressAnimationEnd, ProgressBarMode} from '@angular/material/progress-bar';
import {Observable, of, Subscription, timer} from 'rxjs';
import {switchMap, takeWhile} from 'rxjs/operators';

interface ProgressBarState {
    display: boolean,
    type: ProgressBarMode
}

@Component({
    selector: 'app-progressbar',
    templateUrl: './progressbar.component.html'
})
export class ProgressbarComponent implements OnInit, OnDestroy {

    @Input()
    public start: Observable<boolean>;

    @Output()
    public finish: EventEmitter<boolean> = new EventEmitter();

    private subscriptions: Subscription[] = [];
    public progressBarState: ProgressBarState = {display: false, type: 'determinate'};

    @Input()
    private time = 3;
    public value: number = 0;

    ngOnInit(): void {
        this.subscriptions.push(
            this.start.pipe(
                switchMap((running: boolean) => (running ? timer(0, 1000) : of(0))),
                takeWhile(t => t <= this.time)
            ).subscribe(v => this.value = (v / this.time) * 100),
            this.start.subscribe(v => this.progressBarState = {display: v, type: 'determinate'})
        );
    }

    end($event: ProgressAnimationEnd) {
        if ($event.value >= 100) {
            this.finish.emit(true);
        }
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
