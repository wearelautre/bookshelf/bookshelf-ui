import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-progressbar',
  template: '<div> progressbar Mock</div>'
})
export class MockProgressbarComponent {
  @Input() public start: any;
  @Output() private time: any;
  @Input() public finish: any;
}
