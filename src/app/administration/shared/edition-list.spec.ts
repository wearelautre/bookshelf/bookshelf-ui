import {EditionList} from './edition-list';
import {FormControl, FormGroup} from '@angular/forms';
import {AdministrationService} from '../services/administration.service';
import {RestEntity} from '../../core/model/rest-entity';
import {Component} from '@angular/core';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {BehaviorSubject, of} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {PaginationService} from "../../shared/services/pagination.service";
import {paginationServiceMock} from "../../shared/services/__mocks__/pagination.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ActivatedRoute} from "@angular/router";
import fn = jest.fn;

interface Data extends RestEntity {
}

class DataAdministrationService extends AdministrationService<Data> {
}

export const dataAdministrationServiceMock = {
  list: of([]),
  list$: of([]),
  getAll: fn(() => of([])),
  search: fn(() => of([])),
  update: fn((series: any) => of(series)),
  delete: fn(() => of({}))
};
type T = Data

@Component({
  selector: 'app-test-component',
  template: ''
})
class TestComponent extends EditionList<T> {
  constructor(
    adminService: DataAdministrationService,
    paginationService: PaginationService,
    route: ActivatedRoute
  ) {
    super(adminService, paginationService, route);
  }

  protected initForm(item: Data): FormGroup {
    return new FormGroup({
      id: new FormControl(item.id)
    });
  }
}

describe('EditionList', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent],
      imports: [
        NgxTranslateTestingModule,
        MatProgressBarModule,
        RouterTestingModule
      ],
      providers: [
        {provide: DataAdministrationService, useValue: dataAdministrationServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('isRemovable', () => {
      test('should return true', () => {
        expect(component.isRemovable(null)).toBeTruthy();
      });
    });

    describe('onNewPage', () => {
      test('should emit', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component[`paginationService`], 'onNewPage').mockImplementation(() => ({}));
        const newPage: PageEvent = {length: 13, pageSize: 300, pageIndex: 43, previousPageIndex: 42}
        component.onNewPage(newPage)
        expect(spy).toHaveBeenNthCalledWith(1, {page: 43, size: 300});
      });
      test('should emit with page = 0', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component[`paginationService`], 'onNewPage').mockImplementation(() => ({}));
        const newPage: PageEvent = {length: 13, pageSize: 300, pageIndex: 43, previousPageIndex: 43}
        component.onNewPage(newPage)
        expect(spy).toHaveBeenNthCalledWith(1, {page: 0, size: 300});
      });
    });

    describe('convertToUiData', () => {
      test('should convert data to UiData', () => {
        jest.clearAllMocks();
        const item = {
          id: 45
        }
        const spy = jest.spyOn<any, any>(component, `initForm`)
          .mockImplementation(() => new FormGroup({id: new FormControl('')}));
        const newPage: PageEvent = {length: 13, pageSize: 300, pageIndex: 43, previousPageIndex: 42}
        component.onNewPage(newPage);
        const res = component[`convertToUiData`](item);
        expect(res.form.value).toStrictEqual({id: ''});
        expect(res.item).toStrictEqual(item);
        expect(res.start).toStrictEqual(new BehaviorSubject(false));
        expect(res.isSaved).toStrictEqual(new BehaviorSubject(false));
        expect(component[`subscriptions`].length).toEqual(1);
        expect(spy).toHaveBeenNthCalledWith(1, item)

      });
    });

    describe('delete', () => {
      test('should call service to delete', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: 0
          }
        })
        const spy = jest.spyOn<any, any>(component[`adminService`], `delete`)
          .mockImplementation(() => of({}));
        expect(component[`subscriptions`].length).toEqual(0)
        component.delete(0);
        expect(spy).toHaveBeenNthCalledWith(1, component.uiDatas[0].item)
        expect(component[`subscriptions`].length).toEqual(1)
      });
    });
    describe('onFinish', () => {
      test('should call service to delete', () => {
        jest.clearAllMocks();

        const form: FormGroup = new FormGroup({id: new FormControl(0)});
        const isSaved: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
        const start: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
        component.uiDatas.push({
          form,
          isSaved,
          start,
          item: {
            id: 0
          }
        })
        const spy = jest.spyOn<any, any>(component[`adminService`], `update`)
          .mockImplementation(() => of({}));
        expect(component[`subscriptions`].length).toEqual(0)
        component.onFinish(0);
        expect(spy).toHaveBeenNthCalledWith(1,
          {...component.uiDatas[0].item, ...form.value},
          false)
        expect(component.uiDatas[0].item).toStrictEqual({});
        expect(component.uiDatas[0].start.value).toBeFalsy();
        expect(component.uiDatas[0].isSaved.value).toBeTruthy();
        expect(component[`subscriptions`].length).toEqual(1)
      });
    });
    test('should return the searchCriteriaList', () => {
      expect(component[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'name', operation: ':', value: `*toto*`}
        ]);
    });
  });
});
