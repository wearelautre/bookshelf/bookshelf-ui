import {TestBed} from '@angular/core/testing';

import {RoleAdministrationService} from './role-administration.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Role} from '../../core/model/role';

const mockData1 = {} as Role;
const mockData2 = {} as Role;


describe('RoleAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: RoleAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(RoleAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['roles']);
      expect(service).toBeTruthy();
    });
  });
});
