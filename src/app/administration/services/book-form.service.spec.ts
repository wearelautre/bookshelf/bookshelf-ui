import {TestBed, waitForAsync} from '@angular/core/testing';

import {BookFormService} from './book-form.service';
import {FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {BookForm} from '../models/form/book-form.model';
import {BookImpl} from '../../core/model/impl/book-impl';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {ContractImpl} from '../../core/model/impl/contract-impl';
import {Series} from "../../core/model/series";

describe('BookFormService', () => {
    let service: BookFormService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxTranslateTestingModule,
                ReactiveFormsModule
            ]
        });
        service = TestBed.inject(BookFormService);
    });

    describe('Init test', () => {
        test('should be created', () => {
            expect(service).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        describe('[DELETE CONTRACT]', () => {
            test('Contract should be deleted', waitForAsync(() => {
                const book = new BookImpl();
                book.contracts.push(new ContractImpl({name: 'role1', id: null}, []));
                book.contracts.push(new ContractImpl({name: 'role2', id: null}, []));
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));

                service.deleteContract(0);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.contracts.length).toStrictEqual(1);
                    expect(form.value.contracts[0].role.name).toStrictEqual('role2');
                });
            }));
        });
        describe('[UPDATE SERIES]', () => {
            test('update form with id not null', waitForAsync(() => {
                service.updateSeries({
                    id: 34,
                    tome: 500,
                    bookType: {name: 'bookType', id: 23},
                    status: {
                        readCount: 0,
                        possessedCount: 0,
                        totalCount: 0,
                        status: Series.SeriesStatusEnum.UNKNOWN
                    },
                    oneShot: false,
                    name: 'name'
                });

                service.bookForm$.subscribe((form) => {
                    expect(form.value.series).toStrictEqual({
                        name: 'name',
                        id: 34,
                        tome: null,
                        seriesCycle: {name: null, id: null, tome: null},
                        bookType: {name: 'bookType', id: 23}
                    });
                });
            }));
            test('update form with id null', waitForAsync(() => {
                service.updateSeries({
                    id: null,
                    bookType: {name: 'bookType', id: 23},
                    status: {
                        readCount: 0,
                        possessedCount: 0,
                        totalCount: 0,
                        status: Series.SeriesStatusEnum.UNKNOWN
                    },
                    oneShot: false,
                    name: 'name'
                });

                service.bookForm$.subscribe((form) => {
                    expect(form.value.series).toStrictEqual({
                        name: 'name',
                        seriesCycle: {name: null, id: null, tome: null},
                        id: null,
                        tome: null,
                        bookType: {name: 'bookType', id: 23}
                    });
                });
            }));
        });
        describe('[GET CONTRACT]', () => {
            test('get all contract', waitForAsync(() => {
                const book = new BookImpl()
                book.contracts.push({artists: [], role: {name: 'test', id: 16}})
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));
                expect(service.getContractRoles()).toStrictEqual([{name: 'test', id: 16}]);
            }));
        });
        describe('[GET CURRENT SERIES]', () => {
            test('get current series', waitForAsync(() => {
                const book = new BookImpl()
                book.series = {
                    name: 'test',
                    bookType: null,
                    id: null,
                    oneShot: false,
                    status: {
                        readCount: 0,
                        possessedCount: 0,
                        totalCount: 0,
                        status: Series.SeriesStatusEnum.UNKNOWN
                    },
                    tome: null
                }
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));
                expect(service.getCurrentSeries()).toStrictEqual({
                    name: 'test',
                    tome: null,
                    seriesCycle: {name: null, id: null, tome: null},
                    bookType: {id: null, name: null},
                    id: null
                });
            }));
        });
        describe('[UPDATE EDITOR]', () => {
            test('update form no id', waitForAsync(() => {
                service.updateEditor({
                    name: 'name'
                });

                service.bookForm$.subscribe((form) => {
                    expect(form.value.editor).toStrictEqual({
                        id: undefined,
                        name: 'name'
                    });
                });
            }));
            test('update form with id', waitForAsync(() => {
                service.updateEditor({
                    id: 23,
                    name: 'name'
                });

                service.bookForm$.subscribe((form) => {
                    expect(form.value.editor).toStrictEqual({
                        id: 23,
                        name: 'name'
                    });
                });
            }));
        });
        describe('[UPDATE OFFERED BOOK STATUS]', () => {
            test('update to false', waitForAsync(() => {
                service[`bookForm`].value.get('metadata').patchValue({
                    acquisitionDate: "null",
                    originalReleaseDate: "null",
                    pageCount: "null",
                    offered: true,
                    price: "null",
                    priceCurrency: "null"
                })
                service.updateOfferedBookStatus(false);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.metadata).toStrictEqual({
                        acquisitionDate: "null",
                        originalReleaseDate: "null",
                        pageCount: "null",
                        offered: false,
                        price: null,
                        priceCurrency: null
                    });
                });
            }));
            test('update to true', waitForAsync(() => {
                service[`bookForm`].value.get('metadata').patchValue({
                    acquisitionDate: "null",
                    originalReleaseDate: "null",
                    pageCount: "null",
                    offered: false,
                    price: "null",
                    priceCurrency: "null"
                })
                service.updateOfferedBookStatus(true);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.metadata).toStrictEqual({
                        acquisitionDate: "null",
                        originalReleaseDate: "null",
                        pageCount: "null",
                        offered: true,
                        price: null,
                        priceCurrency: null
                    });
                });
            }));
        });
        describe('[DELETE CONTRACT\'S ARTIST]', () => {
            test('Artist should be deleted', waitForAsync(() => {
                const book = new BookImpl();
                book.contracts.push(new ContractImpl({name: 'role1', id: null}, [{name: 'artist1', webLinks: []}, {
                    name: 'artist2',
                    webLinks: []
                }]));
                book.contracts.push(new ContractImpl({name: 'role2', id: null}, []));
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));

                service.deleteArtist(0, 0);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.contracts[0].artists.length).toStrictEqual(1);
                    expect(form.value.contracts[0].artists[0].name).toStrictEqual('artist2');
                });
            }));
        });
        describe('[ADD CONTRACT]', () => {
            test('Contract should be added', waitForAsync(() => {
                const book = new BookImpl();
                book.contracts.push(new ContractImpl({name: 'role1', id: null}, []));
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));

                service.addContract({name: 'role3', id: null});

                service.bookForm$.subscribe((form) => {
                    expect(form.value.contracts.length).toStrictEqual(2);
                    expect(form.value.contracts[0].role.name).toStrictEqual('role1');
                    expect(form.value.contracts[1].role.name).toStrictEqual('role3');
                });
            }));
        });
        describe('[ADD CONTRACT\'S ARTIST]', () => {
            test('Artist should be added', waitForAsync(() => {
                const book = new BookImpl();
                book.contracts.push(new ContractImpl({name: 'role1', id: null}, []));
                book.contracts.push(new ContractImpl({name: 'role2', id: null}, []));
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));

                service.addArtist(1, {name: 'artist1', webLinks: []});

                service.bookForm$.subscribe((form) => {
                    expect(form.value.contracts[0].artists.length).toStrictEqual(0);
                    expect(form.value.contracts[1].artists.length).toStrictEqual(1);
                    expect(form.value.contracts[1].artists[0].name).toStrictEqual('artist1');
                });
            }));
        });
        describe('[INIT FORM]', () => {
            test('from should be init with books with a non one shot series', (done) => {
                const book = new BookImpl();
                book.title = 'toto';
                book.series.name = 'best series';

                const spy = jest.spyOn(service, 'toggleOneShot').mockImplementation(() => ({}));

                service.initForm(book);

                expect(spy).toHaveBeenNthCalledWith(1, false);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.title).toStrictEqual('toto');
                    expect(form.value.series.name).toStrictEqual('best series');
                    done();
                });
            });
            test('from should be init with books with a one shot series', (done) => {
                const book = new BookImpl();
                book.title = 'toto';
                book.series.name = 'One-shot';
                book.series.oneShot = true;

                const spy = jest.spyOn(service, 'toggleOneShot').mockImplementation(() => ({}));

                service.initForm(book);

                expect(spy).toHaveBeenNthCalledWith(1, true);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.title).toStrictEqual('toto');
                    expect(form.value.series.name).toStrictEqual('One-shot');
                    done();
                });
            });
        });
        describe('[TOOGLE ONE SHOT]', () => {
            test('Set form for One Shot series', waitForAsync(() => {
                const book = new BookImpl();
                book.title = 'toto';
                book.series.name = 'best series';
                book.series.bookType = {name: 'bookType', id: 23};
                service[`book`] = book
                service[`bookForm`].next(new FormGroup({...new BookForm(book)}));

                service.toggleOneShot(true);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.series).toStrictEqual({bookType: {name: 'bookType', id: 23}});
                    expect(form.getRawValue().series).toStrictEqual({
                        bookType: {name: 'bookType', id: 23},
                        name: 'toto',
                        tome: null,
                        seriesCycle: {name: null, id: null, tome: null},
                        id: null
                    });
                });
            }));
            test('Set form for non One Shot series', waitForAsync(() => {
                const book = new BookImpl();
                book.title = 'toto';
                book.series.name = 'best series';
                book.series.tome = 54;
                service.initForm(book);
                service.toggleOneShot(false);

                service.bookForm$.subscribe((form) => {
                    expect(form.value.series).toStrictEqual({
                        id: null,
                        tome: 54,
                        name: 'best series',
                        bookType: {name: null, id: null}
                    });
                });
            }));
        });
        describe('[SET CURRENCY MANDATORY]', () => {
            test('addValidators', waitForAsync(() => {
                service[`bookForm`].value.get('metadata.priceCurrency').removeValidators(Validators.required)
                service.setCurrencyMandatory(10);
                expect(service[`bookForm`].value.get('metadata.priceCurrency').hasValidator(Validators.required)).toBeTruthy()
            }));
            test('still hasValidators', waitForAsync(() => {
                service[`bookForm`].value.get('metadata.priceCurrency').addValidators(Validators.required)
                service.setCurrencyMandatory(10);
                expect(service[`bookForm`].value.get('metadata.priceCurrency').hasValidator(Validators.required)).toBeTruthy()
            }));
            test('removeValidators', waitForAsync(() => {
                service[`bookForm`].value.get('metadata.priceCurrency').setValue("toto")
                service[`bookForm`].value.get('metadata.priceCurrency').addValidators(Validators.required)
                service.setCurrencyMandatory(null);
                expect(service[`bookForm`].value.get('metadata.priceCurrency').value).toBeNull()
                expect(service[`bookForm`].value.get('metadata.priceCurrency').hasValidator(Validators.required)).toBeFalsy()
            }));
            test('still hasn\'t Validators', waitForAsync(() => {
                service[`bookForm`].value.get('metadata.priceCurrency').setValue("toto")
                service[`bookForm`].value.get('metadata.priceCurrency').removeValidators(Validators.required)
                service.setCurrencyMandatory(null);
                expect(service[`bookForm`].value.get('metadata.priceCurrency').value).toBeNull()
                expect(service[`bookForm`].value.get('metadata.priceCurrency').hasValidator(Validators.required)).toBeFalsy()
            }));
        });
    });
});
