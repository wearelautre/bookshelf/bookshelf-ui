import {TestBed} from '@angular/core/testing';

import {EditorAdministrationService} from './editor-administration.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('EditorAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: EditorAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(EditorAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['editors']);
      expect(service).toBeTruthy();
    });
  });
});

