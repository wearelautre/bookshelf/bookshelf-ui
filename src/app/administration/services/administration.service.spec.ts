import {TestBed, waitForAsync} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {RestEntity} from '../../core/model/rest-entity';

class MockClass implements RestEntity {
  id?: number;
}

@Injectable({
  providedIn: 'root'
})
class TestAdministrationService extends AdministrationService<MockClass> {

  constructor(http: HttpClient, coreService: CoreService) {
    super(http, coreService, 'mockClass');
  }
}

describe('AdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: TestAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock},
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TestAdministrationService);
  });

  afterEach(() => {
    httpTestingController.verify();
    jest.clearAllMocks();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['mockClass']);
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[DELETE]', () => {
      test('Should call delete book OK', waitForAsync(() => {
        const mockObject = {} as MockClass;
        mockObject.id = 123456789123;

        const spyOnGetAll = jest.spyOn(service, 'getAll').mockImplementation(() => of([]));

        jest.clearAllMocks();
        service.delete(mockObject).subscribe(() => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(spyOnGetAll).toHaveBeenCalledTimes(0);
        });

        const req = httpTestingController.expectOne(`/api/mockClass/123456789123`);

        expect(req.request.method).toEqual('DELETE');
        req.flush('flush');
      }));
      test('Should call delete book KO', waitForAsync(() => {
        const mockObject = {} as MockClass;
        mockObject.id = 123456789123;

        jest.clearAllMocks();
        service.delete(mockObject).subscribe({
        error: () => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        }});

        const req = httpTestingController.expectOne(`/api/mockClass/123456789123`);

        expect(req.request.method).toEqual('DELETE');
        req.error(new ErrorEvent('error'));

      }));
    });
  });
});
