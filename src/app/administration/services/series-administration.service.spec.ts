import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {SeriesAdministrationService} from './series-administration.service';
import {BookSeriesImpl} from '../../core/model/impl/book-series-impl';


const mockData1 = new BookSeriesImpl();
mockData1.name = 'TEST';
mockData1.status = {
  possessedCount: 1, status: undefined, totalCount: 0, readCount: 0
};

const mockData2 = new BookSeriesImpl();
mockData2.name = 'totototo';
mockData2.status = {
  possessedCount: 0, status: undefined, totalCount: 0, readCount: 0
};

describe('SeriesAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: SeriesAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SeriesAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['series']);
      expect(service).toBeTruthy();
    });
  });
});
