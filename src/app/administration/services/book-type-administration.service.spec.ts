import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BookTypeAdministrationService} from './book-type-administration.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';

describe('BookTypeAdministrationService', () => {
    let httpTestingController: HttpTestingController;
    let service: BookTypeAdministrationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                MatSnackBarModule,
                NgxTranslateTestingModule
            ]
        });
        httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(BookTypeAdministrationService);
    });

    afterEach(() => httpTestingController.verify());

    describe('Init test', () => {
        test('should be created', () => {
            expect(service[`apiEndpoints`]).toStrictEqual(['bookTypes']);
            expect(service).toBeTruthy();
        });
    });
});
