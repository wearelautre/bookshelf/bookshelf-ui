import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {AdministrationService} from './administration.service';
import {WishlistBook} from "../../wishlist/models/wishlist-book";

@Injectable({
  providedIn: 'root'
})
export class WishlistBookAdministrationService extends AdministrationService<WishlistBook> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'wishlist-books');
  }

  protected getId(wishlistBook: WishlistBook): string | number {
    return wishlistBook.isbn;
  }
}
