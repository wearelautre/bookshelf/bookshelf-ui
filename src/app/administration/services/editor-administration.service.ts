import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Editor} from '../../core/model/editor';

@Injectable({
  providedIn: 'root'
})
export class EditorAdministrationService extends AdministrationService<Editor> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'editors');
  }
}
