import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Artist} from '../../core/model/artist';

@Injectable({
  providedIn: 'root'
})
export class ArtistAdministrationService extends AdministrationService<Artist> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'artists');
  }
}
