import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Loan} from "../../core/model/loan";

type S = Loan

@Injectable({
  providedIn: 'root'
})
export class LoanAdministrationService extends AdministrationService<S> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'borrowers', 'loans');
  }
}
