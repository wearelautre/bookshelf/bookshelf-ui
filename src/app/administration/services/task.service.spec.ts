import {TestBed, waitForAsync} from '@angular/core/testing';

import {TaskService} from './task.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Task, TaskStatusEnum, TaskTypeEnum} from "../models/task";
import {coreServiceMock} from "../../core/services/__mocks__/core.service";
import {of} from "rxjs";

describe('TaskService', () => {
  let httpTestingController: HttpTestingController;
  let service: TaskService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TaskService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  })

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoint`]).toStrictEqual('tasks');
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe("[REST CALL]", () => {
      describe("[getTodoCount]", () => {
        test('OK', waitForAsync(() => {
          service.getTaskTodoCount()
            .subscribe(_ => {
              expect(service["count"].value).toStrictEqual({ALL: 2, TOTO: 8});
            });
          const req = httpTestingController.expectOne('/api/tasks/TODO/count');
          expect(req.request.method).toStrictEqual('GET');
          req.flush({ALL: 2, TOTO: 8});
        }));
        test('KO', waitForAsync(() => {
          service.getTaskTodoCount()
            .subscribe({
              next: () => expect(service["count"].value).toStrictEqual({ALL: 0}),
              error: _ => {
                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
              }
            });
          const req = httpTestingController.expectOne('/api/tasks/TODO/count');
          expect(req.request.method).toStrictEqual('GET');
          req.error(new ErrorEvent('error'));
        }));
      });
      describe("[getTaskTodo]", () => {
        test('OK', waitForAsync(() => {
          const createDate = new Date();
          const todo = {createDate, extra: "123456789", type: TaskTypeEnum.ADD_BOOK, status: TaskStatusEnum.TODO}
          const pageTodo = {
            list: [todo],
            currentPage: 0,
            totalPages: 0,
            totalElements: 0
          }
          service.getTaskTodo()
            .subscribe(_ => {
              expect(service["listTodo"].value).toStrictEqual(pageTodo.list);
            });
          const req = httpTestingController.expectOne('/api/tasks/ADD_BOOK/TODO');
          expect(req.request.method).toStrictEqual('GET');
          req.flush(pageTodo);
        }));
        test('KO', waitForAsync(() => {
          const pageTodo = {
            list: [],
            currentPage: 0,
            totalPages: 0,
            totalElements: 0
          }
          service.getTaskTodo()
            .subscribe({
              next: () => expect(service["listTodo"].value).toStrictEqual(pageTodo.list),
              error: _ => {
                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
              }
            });
          const req = httpTestingController.expectOne('/api/tasks/ADD_BOOK/TODO');
          expect(req.request.method).toStrictEqual('GET');
          req.error(new ErrorEvent('error'));
        }));
      });
      describe('[delete]', () => {
        test('Should call delete book OK', waitForAsync(() => {
          const mockObject = {id: 1} as Task;

          const spyOnGetTaskTodo = jest.spyOn(service, 'getTaskTodo').mockImplementation(() => of({
            list: [],
            currentPage: 0,
            totalPages: 0,
            totalElements: 0
          }));

          jest.clearAllMocks();
          service.delete(mockObject).subscribe((res) => {
            expect(res).toStrictEqual({
              list: [],
              currentPage: 0,
              totalPages: 0,
              totalElements: 0
            });
            expect(spyOnGetTaskTodo).toHaveBeenCalledTimes(1);
          });

          const req = httpTestingController.expectOne(`/api/tasks/1`);

          expect(req.request.method).toEqual('DELETE');
          req.flush('flush');
        }));
        test('Should call delete book KO', waitForAsync(() => {
          const mockObject = {id: 1} as Task;

          jest.clearAllMocks();
          service.delete(mockObject).subscribe({
            error: () => {
            }});

          const req = httpTestingController.expectOne(`/api/tasks/1`);

          expect(req.request.method).toEqual('DELETE');
          req.error(new ErrorEvent('error'));

        }));
      });
    });
    describe("[OBSERVABLES]", () => {
      describe("[count$]", () => {
        test('should return default value of count', waitForAsync(() => {
          service.count$.subscribe(value => expect(value).toStrictEqual({ALL: 0}));
        }));
        test('should update value of count', waitForAsync(() => {
          service["count"].next({ALL: 2, TOTO: 8});
          service.count$.subscribe(value => expect(value).toStrictEqual({ALL: 2, TOTO: 8}));
        }));
      })
      describe("[listTodo$]", () => {
        test('should return default value of count', waitForAsync(() => {
          service.listTodo$.subscribe(value => expect(value).toStrictEqual([]));
        }));
        test('should update value of count', waitForAsync(() => {
          const createDate = new Date();
          const todo = {createDate, extra: "123456789", type: TaskTypeEnum.ADD_BOOK, status: TaskStatusEnum.TODO}
          service["listTodo"].next([todo]);
          service.listTodo$.subscribe(value => expect(value).toStrictEqual([todo]));
        }));
      })
    });
  });
});
