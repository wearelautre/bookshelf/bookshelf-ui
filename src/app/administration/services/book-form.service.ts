import {Injectable} from '@angular/core';
import {FormArray, FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {BookForm} from '../models/form/book-form.model';
import {BookImpl} from '../../core/model/impl/book-impl';
import {Book} from '../../core/model/book';
import {BehaviorSubject, Observable} from 'rxjs';
import {ContractFormType, ContractFrom} from '../models/form/contract.from.model';
import {Role} from '../../core/model/role';
import {Artist} from '../../core/model/artist';
import {ArtistForm, ArtistFormType} from '../models/form/artist-form.model';
import {Series} from "../../core/model/series";
import {Editor} from "../../core/model/editor";

@Injectable({
  providedIn: 'root'
})
export class BookFormService {

  private readonly contractControlName = 'contracts';

  private book: Book;

  private bookForm: BehaviorSubject<UntypedFormGroup | undefined> = new BehaviorSubject(this.fb.group(new BookForm(new BookImpl())));


  get bookForm$(): Observable<UntypedFormGroup> {
    return this.bookForm.asObservable();
  }

  constructor(
    private fb: UntypedFormBuilder
  ) {
  }

  initForm(book: Book): void {
    this.book = book;
    let bookForm = this.fb.group(new BookForm(book));
    bookForm = this.toggleSeriesStatus(bookForm)
    this.bookForm.next(bookForm);
    this.toggleOneShot(book.series.oneShot);
  }

  getContractRoles(): Role[] {
    return this.getContractsArray(this.bookForm.getValue()).getRawValue()
      .map(({role}) => role)
  }

  private getContractsArray(book: UntypedFormGroup) {
    return book.get(this.contractControlName) as FormArray<FormGroup<ContractFormType>>;
  }

  addContract(role: Role): void {
    const book = this.bookForm.getValue();
    this.getContractsArray(book)
      .push(ContractFrom.formGroupWithValue({artists: [], role}));

    this.bookForm.next(book);
  }

  addArtist(contractIndex: number, artist: Artist): void {
    const book = this.bookForm.getValue();
    this.getContractArtistsArray(book, contractIndex).push(ArtistForm.formGroupWithValue(artist));

    this.bookForm.next(book);
  }

  toggleOneShot(setOneShot: boolean): void {
    const form = this.bookForm.value;
    let formValue: any;
    const series = (form.controls.series as UntypedFormGroup)
    if (setOneShot) {
      series.disable();
      series.controls.bookType.enable();
      formValue = {
        series: {
          name: form.value.title,
        }
      };
    } else {
      series.enable();
      this.toggleSeriesStatus(form)
      formValue = {
        series: {
          tome: this.book.series.tome,
          name: this.book.series.name,
        }
      };
    }
    form.patchValue(formValue);
    this.bookForm.next(form);
  }

  toggleSeriesStatus(form: UntypedFormGroup): UntypedFormGroup {
    if (form.get('series.seriesCycle').value.name !== null) {
      form.get('series.seriesCycle').enable()
    } else {
      form.get('series.seriesCycle').disable()
    }
    return form;
  }

  deleteContract(i: number): void {
    const book = this.bookForm.getValue();
    this.getContractsArray(book).removeAt(i);

    this.bookForm.next(book);
  }

  deleteArtist(contractIndex: number, indexArtist: number): void {
    const book = this.bookForm.getValue();
    this.getContractArtistsArray(book, contractIndex).removeAt(indexArtist);

    this.bookForm.next(book);
  }

  private getContractArtistsArray(book: UntypedFormGroup, contractIndex: number) {
    return this.getContractsArray(book)
      .controls[contractIndex].get('artists') as FormArray<FormGroup<ArtistFormType>>;
  }

  updateSeries(series: Series): void {
    const form = this.bookForm.value;

    form.patchValue({
      series: {
        id: series.id,
        bookType: series.bookType,
        name: series.name,
      },
      editor: {
        id: null,
        name: series.editor
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  updateEditor(editor: Editor): void {
    const form = this.bookForm.value;

    form.patchValue({
      editor: {
        id: editor.id,
        name: editor.name
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  getCurrentSeries(): Series {
    return this.bookForm.value.get('series').value;
  }

  updateOfferedBookStatus(offered: boolean): void {
    const form = this.bookForm.value;

    form.patchValue({
      metadata: {
        offered,
        price: null,
        priceCurrency: null
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  setCurrencyMandatory(price: number): void {
    const form = this.bookForm.value;
    const priceCurrencyControl = form.get("metadata.priceCurrency")
    if (price === null) {
      priceCurrencyControl.removeValidators(Validators.required)
      priceCurrencyControl.setValue(null)
    } else if (price >= 0) {
      priceCurrencyControl.addValidators(Validators.required)
    }
    this.bookForm.next(form);
  }
}
