import {TestBed} from '@angular/core/testing';

import {ArtistAdministrationService} from './artist-administration.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AuthorAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: ArtistAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ArtistAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['artists']);
      expect(service).toBeTruthy();
    });
  });
});
