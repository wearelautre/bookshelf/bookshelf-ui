import {TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BookAdministrationService} from './book-administration.service';
import {CoreService} from '../../core/services/core.service';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {BookImpl} from '../../core/model/impl/book-impl';
import {BookTypeAdministrationService} from './book-type-administration.service';
import {bookTypeAdministrationServiceMock} from './__mocks__/book-type-administration.service';
import {tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {MatDialogModule} from '@angular/material/dialog';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import clearAllMocks = jest.clearAllMocks;

const mockBook1 = new BookImpl();
mockBook1.title = 'testtest';
mockBook1.isbn = '123456789123';

const mockBook2 = new BookImpl();
mockBook2.title = 'totototo';
mockBook2.isbn = '9876543231012';

describe('BookAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: BookAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule,
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock},
        {provide: BookTypeAdministrationService, useValue: bookTypeAdministrationServiceMock},
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(BookAdministrationService);

    jest.clearAllMocks();
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['books']);
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[SEARCH RESULT OBS]', () => {
      test('Should update the observable', (done) => {
        service[`searchResult`].next({arkId: 'toto'});
        service.searchResult$.subscribe(value => {
          expect(value).toStrictEqual({arkId: 'toto'});
          done();
        });
      });

      test('Should clear the observable', (done) => {
        service[`searchResult`].next({arkId: 'toto'});
        service.clearResults();
        service.searchResult$.subscribe(value => {
          expect(value).toBeNull();
          done();
        });
      });
    });
    describe('[SEARCH BY ISBN]', () => {
      test('Should call isbn search OK', (done) => {
        const mockData = {test: 'test'};
        service.searchBook('isbn').subscribe(value => {
          expect(value).toStrictEqual(mockData);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          done();
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        service.searchResult$.pipe(tap((value) => {
          expect(value).toStrictEqual(null);
          done();
        }));

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush([mockData]);
      });

      test('Should call isbn search OK -- empty results', waitForAsync(() => {
        clearAllMocks();
        const spy = jest.spyOn<any, any>(service, `openDialog`).mockImplementation(() => of(true));
        service.searchBook('isbn').subscribe({
          next: () => {
            expect(spy).toHaveBeenCalledTimes(1);
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        service.searchResult$.pipe(tap((value) => {
          expect(value).toStrictEqual(null);
        }));

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush([]);
      }));

      test('Should call isbn search OK -- empty results -- errors', waitForAsync(() => {
        clearAllMocks();
        const spy = jest.spyOn<any, any>(service, `openDialog`).mockImplementation(() => of(false));
        service.searchBook('isbn').subscribe({
          error: () => {
            expect(spy).toHaveBeenCalledTimes(1);
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        service.searchResult$.pipe(tap((value) => {
          expect(value).toStrictEqual(null);
        }));

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush([]);
      }));

      test('Should call isbn search OK -- null results', waitForAsync(() => {
        clearAllMocks();
        const spy = jest.spyOn<any, any>(service, `openDialog`).mockImplementation(() => of(true));
        service.searchBook('isbn').subscribe({
          next: () => {
            expect(spy).toHaveBeenCalledTimes(1);
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        service.searchResult$.pipe(tap((value) => {
          expect(value).toStrictEqual(null);
        }));

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush(null);
      }));

      test('Should call isbn search OK -- too mush results', waitForAsync(() => {
        const mockData = {test: 'test'};
        clearAllMocks();
        const spy = jest.spyOn<any, any>(console, `warn`).mockImplementation(() => {
        });
        service.searchBook('isbn').subscribe({
          next: (value) => {
            expect(value).toStrictEqual(mockData);
            expect(spy).toHaveBeenNthCalledWith(1, 'More than one result, returning first one');
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        service.searchResult$.pipe(tap((value) => {
          expect(value).toStrictEqual(null);
        }));

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush([mockData, {toto: 'tutu'}]);
      }));

      test('Should call isbn search KO', waitForAsync(() => {
        jest.spyOn<any, any>(service, `openDialog`).mockImplementation(() => of(false));

        service.searchBook('isbn').subscribe({
          error: () => {
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            service.searchResult$.subscribe({
              error: () => {
              }
            });
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush(new ErrorEvent('error'));
      }));

      test('Should call isbn search KO with abort', waitForAsync(() => {
        jest.spyOn<any, any>(service, `openDialog`).mockImplementation(() => of(true));

        service.searchBook('isbn').subscribe({
          error: () => {
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);

            service.searchResult$.subscribe(value => {
              expect(value).toStrictEqual({
                isbn: 'isbn',
                arkId: 'notSet',
                artist: [],
                collection: null,
                cover: null,
                editor: null,
                rawBnFData: null,
                series: null,
                title: null,
                tome: null,
                year: null
              });
            });
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req = httpTestingController.expectOne(`/bnf-api/bnf/search/books?isbn=isbn`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush(new ErrorEvent('error'));
      }));
    });
    describe('[CREATE BOOK]', () => {
      test('Should call book create OK', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `getAll`).mockImplementation(() => of());
        service.create(mockBook1).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(value).toStrictEqual(mockBook1);
          expect(spy).toHaveBeenNthCalledWith(1);
        });

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.body).toStrictEqual(mockBook1);
        expect(req.request.method).toEqual('POST');
        req.flush(mockBook1);
      }));

      test('Should call book create without list update', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `getAll`).mockImplementation(() => of());
        service.createWithOption(mockBook1, false).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(value).toStrictEqual(mockBook1);
          expect(spy).toHaveBeenCalledTimes(0);
        });

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.body).toStrictEqual(mockBook1);
        expect(req.request.method).toEqual('POST');
        req.flush(mockBook1);
      }));

      test('Should call book create KO', waitForAsync(() => {
        service.create(mockBook1).subscribe({
          error: () => {
            // Important to be this otherwhise the test fail
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.body).toStrictEqual(mockBook1);
        expect(req.request.method).toEqual('POST');
        req.error(new ErrorEvent('error'));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        expect(bookTypeAdministrationServiceMock.getAll.mock.calls.length).toStrictEqual(0);

      }));
    });
    describe('[GET ALL]', () => {
      test('Should call get all book create OK', waitForAsync(() => {
        service[`filterStr`] = 'test';
        jest.clearAllMocks();
        service.getAll().subscribe(value => expect(value).toStrictEqual([mockBook1, mockBook2]));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush([mockBook1, mockBook2]);

        service.list$.pipe(tap((list) => expect(list).toStrictEqual([mockBook1, mockBook2])));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
      test('Should call get all book create OK', waitForAsync(() => {
        const bookList = [mockBook1, mockBook2];

        service[`filterStr`] = '';
        jest.clearAllMocks();
        service.getAll().subscribe(value => expect(value).toStrictEqual(bookList));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.flush(bookList);

        service.list$.pipe(tap((list) => expect(list).toStrictEqual(bookList)));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
      test('Should call get all book create KO', waitForAsync(() => {

        service[`filterStr`] = 'test';
        jest.clearAllMocks();
        service.getAll().subscribe({
          error: () => {
            // Important to be this otherwhise the test fail
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('GET');
        req.error(new ErrorEvent('error'));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
    });
    describe('[UPDATE]', () => {
      test('Should call update book OK with reload', waitForAsync(() => {
        jest.clearAllMocks();

        const spy = jest.spyOn<any, any>(service, `getAll`).mockImplementation(() => of());
        service.update(mockBook1).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(spy).toHaveBeenNthCalledWith(1);
          expect(value).toStrictEqual(mockBook1);
        });

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body).toEqual(mockBook1);
        req.flush(mockBook1);

      }));
      test('Should call update book OK without reload', waitForAsync(() => {
        jest.clearAllMocks();

        const spy = jest.spyOn<any, any>(service, `updateAllOnSuccess`).mockImplementation(() => of());
        service.update(mockBook1, false).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(spy).toHaveBeenCalledTimes(0);
          expect(value).toStrictEqual(mockBook1);
        });

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body).toEqual(mockBook1);
        req.flush(mockBook1);

      }));
      test('Should call update book KO', waitForAsync(() => {
        jest.clearAllMocks();
        service.update(mockBook1).subscribe({
          error: () => {
            // Important to be this otherwhise the test fail
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body).toEqual(mockBook1);
        req.error(new ErrorEvent('error'));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
    });
    describe('[PATCH]', () => {
      test('Should call patch book OK with reload', waitForAsync(() => {
        jest.clearAllMocks();

        const spy = jest.spyOn<any, any>(service, `getAll`).mockImplementation(() => of());
        service.patch(mockBook1).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(spy).toHaveBeenNthCalledWith(1);
          expect(value).toStrictEqual(mockBook1);
        });

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PATCH');
        expect(req.request.body).toEqual(mockBook1);
        req.flush(mockBook1);

      }));
      test('Should call patch book OK without reload', waitForAsync(() => {
        jest.clearAllMocks();

        const spy = jest.spyOn<any, any>(service, `updateAllOnSuccess`).mockImplementation(() => of());
        service.patch(mockBook1, false).subscribe(value => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          expect(spy).toHaveBeenCalledTimes(0);
          expect(value).toStrictEqual(mockBook1);
        });

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PATCH');
        expect(req.request.body).toEqual(mockBook1);
        req.flush(mockBook1);

      }));
      test('Should call patch book KO', waitForAsync(() => {
        jest.clearAllMocks();
        service.patch(mockBook1).subscribe({
          error: () => {
            // Important to be this otherwhise the test fail
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PATCH');
        expect(req.request.body).toEqual(mockBook1);
        req.error(new ErrorEvent('error'));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
    });
    describe('[DELETE]', () => {
      test('Should call delete book OK', waitForAsync(() => {
        const spyOnGetAll = jest.spyOn(service, 'getAll').mockImplementation(() => of([]));

        jest.clearAllMocks();
        service.delete(mockBook1).subscribe(value => expect(value).toStrictEqual(mockBook1));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('DELETE');
        req.flush(mockBook1);

        expect(spyOnGetAll).toHaveBeenCalledTimes(0);
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
      test('Should call delete book KO', waitForAsync(() => {
        jest.clearAllMocks();
        service.delete(mockBook1).subscribe({
          error: () => {
            // Important to be this otherwhise the test fail
          }
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        const req = httpTestingController.expectOne(`/api/books/123456789123`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('DELETE');
        req.error(new ErrorEvent('error'));

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      }));
    });
    describe('[COVER]', () => {
      test('should make a POST call on the URL', waitForAsync((done) => {
        const file = new File(['foo'], 'foo.png', {
          type: 'text/plain',
        });
        const isbn = '0123456789';
        service.uploadCover(file, isbn).subscribe(({cover}) =>
          // waitForAsync validations
          expect(cover).toStrictEqual('test')
        );
        const req = httpTestingController.expectOne(`/api/books/${isbn}/cover`);

        // non waitForAsync validations
        expect(req.request.method).toEqual('PUT');
        req.flush({cover: 'test'});
      }));
    });
  });
});
