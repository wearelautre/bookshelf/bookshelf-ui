import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {SeriesCycle} from "../../core/model/series";

@Injectable({
  providedIn: 'root'
})
export class SeriesCycleAdministrationService extends AdministrationService<SeriesCycle> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'series', 'series-cycle');
  }
}
