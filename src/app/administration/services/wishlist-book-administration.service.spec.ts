import {TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreService} from '../../core/services/core.service';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {BookTypeAdministrationService} from './book-type-administration.service';
import {bookTypeAdministrationServiceMock} from './__mocks__/book-type-administration.service';
import {MatDialogModule} from '@angular/material/dialog';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {WishlistBookAdministrationService} from "./wishlist-book-administration.service";
import {of} from "rxjs";

const mockBook1 = {editor: null, isbn: "123456789123", series: null, title: "", cover: 'coverName'};

describe('BookAdministrationService', () => {
    let httpTestingController: HttpTestingController;
    let service: WishlistBookAdministrationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                MatDialogModule,
                NgxTranslateTestingModule
            ],
            providers: [
                {provide: CoreService, useValue: coreServiceMock},
                {provide: BookTypeAdministrationService, useValue: bookTypeAdministrationServiceMock},
            ]
        });
        httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(WishlistBookAdministrationService);

        jest.clearAllMocks();
    });

    afterEach(() => httpTestingController.verify());

    describe('Init test', () => {
        test('should be created', () => {
            expect(service[`apiEndpoints`]).toStrictEqual(['wishlist-books']);
            expect(service).toBeTruthy();
        });

    });
    describe('Typescript test', () => {

        describe('[DELETE]', () => {
            test('Should call delete book OK', waitForAsync(() => {
                const spyOnGetAll = jest.spyOn(service, 'getAll').mockImplementation(() => of([]));

                jest.clearAllMocks();
                service.delete(mockBook1).subscribe(value => expect(value).toStrictEqual(mockBook1));

                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

                const req = httpTestingController.expectOne(`/api/wishlist-books/123456789123`);

                // non waitForAsync validations
                expect(req.request.method).toEqual('DELETE');
                req.flush(mockBook1);

                expect(spyOnGetAll).toHaveBeenCalledTimes(0);
                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }));
            test('Should call delete book KO', waitForAsync(() => {
                jest.clearAllMocks();
                service.delete(mockBook1).subscribe({
                    error: () => {
                        // Important to be this otherwhise the test fail
                    }
                });

                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

                const req = httpTestingController.expectOne(`/api/wishlist-books/123456789123`);

                // non waitForAsync validations
                expect(req.request.method).toEqual('DELETE');
                req.error(new ErrorEvent('error'));

                expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }));
        });
    });
});
