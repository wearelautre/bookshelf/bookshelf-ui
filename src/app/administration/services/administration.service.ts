import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {idType, RestEntity} from '../../core/model/rest-entity';
import {EntityService} from "../../shared/services/entity.service";
import {InjectionToken} from "@angular/core";
import {Utils} from "../../shared/utils";

export const ADMIN_SERVICE = new InjectionToken<AdministrationService<any>>('AdminService');

export abstract class AdministrationService<T extends RestEntity> extends EntityService<T> {

  protected constructor(
    protected http: HttpClient,
    protected coreService: CoreService,
    ...apiEndpoints: string[]
  ) {
    super(http, coreService, ...apiEndpoints)
  }

  create(t: T, ...ids: idType[]): Observable<T> {
    return this.createWithOption(t, true, ...ids)
  }


  createWithOption(t: T, updateAll: boolean = true, ...ids: idType[]): Observable<T> {
    this.coreService.updateLoadingState(true);
    return this.http.post<T>(Utils.buildUrl(this.apiEndpoints, ...ids), t).pipe(
      tap(() => {
        if (updateAll) {
          this.updateAllOnSuccess()
        } else {
          this.coreService.updateLoadingState(false);
        }
      }),
      catchError(err => throwError(() => this.manageError(err)))
    );
  }

  update(t: T, reloadAll: boolean = true, ...ids: idType[]): Observable<T> {
    this.coreService.updateLoadingState(true);
    return this.http.put<T>(Utils.buildUrl(this.apiEndpoints, ...ids, this.getId(t)), t).pipe(
      tap(() => this.afterUpdate(reloadAll)),
      catchError(err => throwError(() => this.manageError(err)))
    );
  }

  patch(t: T, reloadAll: boolean = true, ...ids: idType[]): Observable<T> {
    this.coreService.updateLoadingState(true);
    return this.http.patch<T>(Utils.buildUrl(this.apiEndpoints, ...ids, this.getId(t)), t).pipe(
      tap(() => this.afterUpdate(reloadAll)),
      catchError(err => throwError(() => this.manageError(err)))
    );
  }

  private afterUpdate(reloadAll: boolean) {
    if (reloadAll) {
      this.updateAllOnSuccess();
    } else {
      this.coreService.updateLoadingState(false);
    }
  }

  delete(t: T, ...ids: idType[]): Observable<void> {
    this.coreService.updateLoadingState(true);
    return this.http.delete<void>(Utils.buildUrl(this.apiEndpoints, ...ids, this.getId(t))).pipe(
      tap(() => this.coreService.updateLoadingState(false)),
      catchError(err => throwError(() => this.manageError(err)))
    );
  }

  private updateAllOnSuccess() {
    this.getAll().subscribe();
    this.coreService.updateLoadingState(false);
  }
}
