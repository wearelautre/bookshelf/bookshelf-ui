import {BehaviorSubject, of} from 'rxjs';
import fn = jest.fn;

export const seriesCycleAdministrationServiceMock = {
  list: of([]),
  list$: new BehaviorSubject<any[]>([]),
  getAll: fn(() => of([])),
  search: fn(() => of([])),
  update: fn((series: any) => of(series)),
  delete: fn(() => of({})),
  searchWithOption: fn(() => of({body: []}))
};
