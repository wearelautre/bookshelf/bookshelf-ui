import {of} from 'rxjs';
import fn = jest.fn;

export const wishlistBookAdministrationServiceMock = {
  searchResult: of({}),
  list: of([]),
  list$: of([]),
  getAll: fn(() => of([])),
  update: fn((item: any) => of(item)),
  patch: fn((item: any) => of(item)),
  delete: fn((item: any) => of(null)),
  create: fn(() => of({})),
  search: fn(() => of({})),
  searchBook: fn(() => of({})),
  uploadCover: fn(() => of({}))
};
