import fn = jest.fn;
import {of} from 'rxjs';
import {HttpResponse} from "@angular/common/http";

export const loanAdministrationServiceMock = {
  list$: of([]),
  search: fn(() => of(new HttpResponse({body: []}))),
  create: fn(() => of({})),
  createWithOption: fn(() => of({})),
  delete: fn(() => of({})),
  update: fn(() => of({})),
  getAll: fn((id) => of([])),
};
