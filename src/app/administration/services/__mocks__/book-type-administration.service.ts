import {of} from 'rxjs';
import fn = jest.fn;

export const bookTypeAdministrationServiceMock = {
  list: of([]),
  getAll: fn(() => of([])),
  delete: fn((bookType: any) => of({})),
  search: fn(() => of({body: []})),
  update: fn((bookType: any) => of(bookType))
};
