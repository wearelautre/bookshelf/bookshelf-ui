import fn = jest.fn;
import {of} from 'rxjs';
import {HttpResponse} from "@angular/common/http";

export const borrowerAdministrationServiceMock = {
  list$: of([]),
  search: fn(() => of(new HttpResponse({body: []}))),
  create: fn(() => of({}))
};
