import {of} from 'rxjs';
import fn = jest.fn;

export const bookAdministrationServiceMock = {
  searchResult: of({}),
  list: of([]),
  list$: of([]),
  getAll: fn(() => of([])),
  update: fn((item: any) => of(item)),
  patch: fn((item: any) => of(item)),
  create: fn(() => of({})),
  search: fn(() => of({})),
  searchBook: fn(() => of({})),
  uploadCover: fn(() => of({}))
};
