import fn = jest.fn;
import {of} from 'rxjs';

export const roleAdministrationServiceMock = {
  search: fn(() => of([])),
  searchAutocomplete: fn(() => of([]))
};
