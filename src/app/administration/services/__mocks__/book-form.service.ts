import {of} from 'rxjs';
import {FormGroup} from '@angular/forms';
import {BookForm} from '../../models/form/book-form.model';
import {BookImpl} from '../../../core/model/impl/book-impl';
import fn = jest.fn;

export const bookFormServiceMock = {
  initForm: fn(),
  bookForm$: of(new FormGroup({...new BookForm(new BookImpl())})),
  addContract: fn(),
  deleteContract: fn(),
  addArtist: fn(),
  deleteArtist: fn(),
  toggleOneShot: fn(),
  getCurrentSeries: fn(),
  update: fn((series: any) => of(series)),
  setCurrencyMandatory: fn(),
  updateEditor: fn(),
  updateSeries: fn(),
  updateOfferedBookStatus: fn()
};
