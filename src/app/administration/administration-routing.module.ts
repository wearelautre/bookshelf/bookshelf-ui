import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookAddComponent} from './components/book/creation/book-add/book-add.component';
import {AuthGuard} from '../auth/guards/auth.guard';
import {BookEditionListComponent} from './components/book/edition/book-edition-list/book-edition-list.component';
import {BookTypeGestionComponent} from './components/bookType/gestion/book-type-gestion/book-type-gestion.component';
import {BookAddResolver} from './resolvers/book/book-add-resolver.service';
import {BookEditComponent} from './components/book/edition/book-edit/book-edit.component';
import {BookFormComponent} from './components/book/shared/book-form/book-form.component';
import {AdministrationPageComponent} from "./pages/administration-page/administration-page.component";
import {
  SeriesEditionListComponent
} from './components/series/edition/series-edition-list/series-edition-list.component';
import {
  ArtistEditionListComponent
} from './components/artist/edition/artist-edition-list/artist-edition-list.component';
import {BookEditResolver} from "./resolvers/book/book-edit-resolver";
import {EditionComponent} from "./pages/edition/edition.component";
import {ArtistAdministrationListResolver} from "./resolvers/artist/artist-administration-list-resolver";
import {BookAdministrationListResolver} from "./resolvers/book/book-administration-list-resolver";
import {SeriesAdministrationListResolver} from "./resolvers/series/series-administration-list-resolver";
import {BorrowerAdministrationListResolver} from "./resolvers/borrower/borrower-administration-list.resolver";
import {
  BorrowerEditionListComponent
} from "./components/borrower/edition/borrower-edition-list/borrower-edition-list.component";

const routes: Routes = [
  {
    path: 'administration',
    canActivateChild: [AuthGuard],
    component: AdministrationPageComponent,
    children: [
      {
        path: 'book/add',
        component: BookAddComponent,
        children: [
          {
            path: ':isbn',
            resolve: {data: BookAddResolver},
            component: BookFormComponent,
            data: {edition: false}
          }
        ]
      }, {
        path: 'book/edit',
        component: BookEditComponent,
        children: [
          {
            path: ':isbn',
            resolve: {data: BookEditResolver},
            component: BookFormComponent,
            data: {edition: true},
          }
        ]
      },
      {
        path: 'edition',
        component: EditionComponent,
        children: [{
          path: 'artist',
          component: ArtistEditionListComponent,
          data: {translationRootKey: 'ARTIST'},
          resolve: {resolvedData: ArtistAdministrationListResolver},
          runGuardsAndResolvers: 'always'
        }, {
          path: 'borrower',
          component: BorrowerEditionListComponent,
          data: {translationRootKey: 'BORROWER'},
          resolve: {resolvedData: BorrowerAdministrationListResolver},
          runGuardsAndResolvers: 'always'
        }, {
          path: 'book',
          component: BookEditionListComponent,
          data: {translationRootKey: 'BOOK'},
          resolve: {resolvedData: BookAdministrationListResolver},
          runGuardsAndResolvers: 'always'
        }, {
          path: 'series',
          component: SeriesEditionListComponent,
          data: {translationRootKey: 'SERIES'},
          resolve: {resolvedData: SeriesAdministrationListResolver},
          runGuardsAndResolvers: 'always'
        }]
      },
      {path: 'book-type/gestion', component: BookTypeGestionComponent}
    ]
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdministrationRoutingModule {
}

