import {Component} from '@angular/core';

@Component({
  selector: 'app-book-add',
  template: `
    <div fxLayout="column" class="content">
      <div fxLayout="row" fxLayoutAlign="center">
        <h1 style="text-align: center">{{'BOOK.ADD.TITLE' | translate}}</h1>
      </div>
      <router-outlet></router-outlet>
    </div>
  `
})
export class BookAddComponent {

}
