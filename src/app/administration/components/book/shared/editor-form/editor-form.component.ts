import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import {Editor} from '../../../../../core/model/editor';
import {EditorAdministrationService} from '../../../../services/editor-administration.service';
import {MatFormFieldAppearance} from '@angular/material/form-field';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
    AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";

@Component({
    selector: 'app-editor-form',
    template: `
        <form fxFlex [formGroup]="form">
            <app-auto-complete-input
                    fxFlex="100"
                    [displayLabel]="displayLabel"
                    [appearance]="appearance"
                    [classes]="classes"
                    [form]="form"
                    [placeholder]="'BOOK.FORM.EDITOR.NAME' | translate"
                    (selectItem)="update($event)">
            </app-auto-complete-input>
        </form>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: ADMIN_SERVICE, useClass: EditorAdministrationService
    }]
})
export class EditorFormComponent {

    @Input() form: UntypedFormGroup;
    @Input() appearance: MatFormFieldAppearance = 'outline';
    @Input() classes: string;
    @Input() displayLabel: boolean = true;
    @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Editor>;
    @Output() public selection: EventEmitter<Editor> = new EventEmitter<Editor>();

    /**
     * Update the editor's name input with the selected editor
     * @param value - the editor object selected from the autocomplete directive
     */
    update(value: Editor): void {
        this.selection.emit(value);
    }
}
