import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditorFormComponent} from './editor-form.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
    NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {EditorAdministrationService} from '../../../../services/editor-administration.service';
import {editorAdministrationServiceMock} from '../../../../services/__mocks__/editor-administration.service';
import {
    MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";

describe('EditorFormComponent', () => {
    let component: EditorFormComponent;
    let fixture: ComponentFixture<EditorFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [EditorFormComponent, MockAutoCompleteInputComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                ReactiveFormsModule,
                MatOptionModule,
                MatChipsModule,
                MatFormFieldModule,
                MatAutocompleteModule,
                MatInputModule
            ],
            providers: [
                {provide: EditorAdministrationService, useValue: editorAdministrationServiceMock}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditorFormComponent);
        component = fixture.componentInstance;
        component.form = new FormGroup({name: new FormControl('')});
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    afterEach(() => jest.clearAllMocks());

    describe('Typescript test', () => {
        describe('[UPDATE]', () => {
            test('emit the new value', () => {
                const spy = jest.spyOn(component.selection, 'emit').mockImplementation(() => ({}));
                const editor = {name: 'name'}
                component.update(editor);
                expect(spy).toHaveBeenNthCalledWith(1, editor);
            });
        });
    });
});
