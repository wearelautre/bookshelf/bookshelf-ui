import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RolesSelectionDialogComponent} from './roles-selection-dialog.component';
import {MatButtonModule} from "@angular/material/button";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {MatListModule} from "@angular/material/list";
import {
  NgxTranslateTestingModule
} from "../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MatListOptionMock} from "../../../../../book/components/list/series-display/series-display.component.spec";

const matDialogMock = {
  open: jest.fn(),
  close: jest.fn()
};

describe('RolesSelectionDialogComponent', () => {
  let component: RolesSelectionDialogComponent;
  let fixture: ComponentFixture<RolesSelectionDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RolesSelectionDialogComponent],
      imports: [
        MatDialogModule,
        NgxTranslateTestingModule,
        MatButtonModule,
        MatDialogModule,
        MatListModule
      ],
      providers: [
        {provide: MatDialogRef, useValue: matDialogMock},
        {
          provide: MAT_DIALOG_DATA, useValue: {
            artist: {
              name: 'test'
            },
            action: 'move',
            roles: [{
              role: {
                name: 'Role 1',
                id: 1
              }, index: 1
            }, {
              role: {
                name: 'Role 3'
              }, index: 3
            }]
          }
        }]

    });
    fixture = TestBed.createComponent(RolesSelectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init tests', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript tests', () => {
    test('should close dialog -- empty', () => {
      const spy = jest.spyOn(component.dialogRef, 'close');
      component.close([]);
      expect(spy).toHaveBeenCalledWith([]);
    });
    test('should close dialog -- not empty', () => {
      const spy = jest.spyOn(component.dialogRef, 'close');
      component.close([new MatListOptionMock({role: {name: 'Role 1', id: 1}, index: 1})]);
      expect(spy).toHaveBeenCalledWith([{role: {name: 'Role 1', id: 1}, index: 1}]);
    });
  });
});
