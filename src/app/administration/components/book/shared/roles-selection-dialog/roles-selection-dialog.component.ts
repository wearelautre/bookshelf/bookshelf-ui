import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Artist} from "../../../../../core/model/artist";
import {MatListOption} from "@angular/material/list";
import {Role} from "../../../../../core/model/role";

export interface RolesSelectionDialogComponentModel {
  artist: Artist;
  action: string;
  roles: { role: Role, index: number }[];
}

@Component({
  selector: 'app-roles-selection-dialog',
  template: `
    <h1
      mat-dialog-title>{{'BOOK.FORM.CONTRACT.ARTIST.MODAL_ROLES_SELECTION.TITLE.' + data.action.toUpperCase() | translate:{"artist": data.artist.name} }}</h1>
    <div mat-dialog-content>
      <mat-selection-list #roles>
        <mat-list-option *ngFor="let roleWithIndex of data.roles" [value]="roleWithIndex">
          {{roleWithIndex.role.name}}
        </mat-list-option>
      </mat-selection-list>
    </div>
    <mat-dialog-actions align="end">
      <button mat-button
              mat-dialog-close> {{'BOOK.FORM.CONTRACT.ARTIST.MODAL_ROLES_SELECTION.ACTIONS.CANCEL'| translate}} </button>
      <button mat-button (click)="close(roles.selectedOptions.selected)"
              cdkFocusInitial> {{'BOOK.FORM.CONTRACT.ARTIST.MODAL_ROLES_SELECTION.ACTIONS.' + data.action.toUpperCase() | translate}} </button>
    </mat-dialog-actions>
  `
})
export class RolesSelectionDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: RolesSelectionDialogComponentModel,
    public dialogRef: MatDialogRef<RolesSelectionDialogComponent>
  ) {
  }

  close(selectedRoles: MatListOption[]) {
    this.dialogRef.close(selectedRoles.map(role => role.value));
  }
}
