import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookTypeFormComponent} from './book-type-form.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
    NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {bookTypeAdministrationServiceMock} from '../../../../services/__mocks__/book-type-administration.service';
import {
    MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";

describe('BookTypeFormComponent', () => {
    let component: BookTypeFormComponent;
    let fixture: ComponentFixture<BookTypeFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BookTypeFormComponent, MockAutoCompleteInputComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                ReactiveFormsModule,
                MatOptionModule,
                MatChipsModule,
                MatFormFieldModule,
                MatAutocompleteModule,
                MatInputModule
            ],
            providers: [
                {provide: BookTypeAdministrationService, useValue: bookTypeAdministrationServiceMock}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BookTypeFormComponent);
        component = fixture.componentInstance;
        component.form = new FormGroup({
            name: new FormControl(),
            id: new FormControl()
        });
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });
    describe('Typescript test', () => {
        describe('[UPDATE]', () => {
            test('update form', () => {
                component.update({name: 'name', id: null});
                expect(component.form.value).toStrictEqual({name: 'name', id: null});
            });
        });
    });
});
