import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import {BookType} from '../../../../../core/model/book-type';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {MatFormFieldAppearance} from '@angular/material/form-field';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
    AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";

type T = BookType

@Component({
    selector: 'app-book-type-form',
    template: `
        <app-auto-complete-input
                fxFlex
                [displayLabel]="displayLabel"
                [appearance]="appearance"
                [classes]="classes"
                [form]="form"
                [placeholder]="'BOOK.FORM.BOOK_TYPE' | translate"
                (selectItem)="update($event)">
        </app-auto-complete-input>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: ADMIN_SERVICE, useClass: BookTypeAdministrationService
    }]
})
export class BookTypeFormComponent {

    @Input() form: UntypedFormGroup;
    @Input() appearance: MatFormFieldAppearance = 'outline';
    @Input() classes: string;
    @Input() displayLabel: boolean = true;

    @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<T>;

    update(value: T): void {
        this.form.setValue({name: value.name, id: value.id});
    }
}
