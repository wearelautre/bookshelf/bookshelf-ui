import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-series-form',
  template: '<div>Series Form Mock</div>'
})
export class MockSeriesFormComponent {

  @Input() public form: any;
  @Input() public edition: any;
  @Output() public seriesSelection: any;

}
