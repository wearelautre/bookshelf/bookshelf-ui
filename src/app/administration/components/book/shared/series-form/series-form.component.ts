import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import {Series} from '../../../../../core/model/series';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
    AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {AutoCompleteItem} from "../../../../../shared/shared.module";
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

type T = Series;

class AutoCompleteSeries extends AutoCompleteItem<T> {
    override getDisplay(option: T): string {
        return `${option.name} - <small style="opacity: 0.75">&nbsp;[${option.bookType.name}] ${option.editor} </small>`
    }
}

@Component({
    selector: 'app-series-form',
    template: `
        <form [formGroup]="form" fxLayout="column" fxFlex>
            <div fxLayout="row" style="gap: 1rem">
                <app-auto-complete-input
                        fxFlex="100"
                        [form]="form"
                        [placeholder]="'BOOK.FORM.SERIES.NAME' | translate"
                        (selectItem)="update($event)">
                </app-auto-complete-input>
                <div fxLayoutAlign="center center">
                    <button type="button" mat-button (click)="toggleMore()">
                        <fa-icon [icon]="['fas', getIconName()]"></fa-icon>
                    </button>
                </div>
            </div>
            <div [fxHide]="!display">
                <mat-form-field fxFlex appearance="outline" subscriptSizing="dynamic"
                                class="mff-space-1">
                    <label>
                        <input type="text"
                               [placeholder]="'TODO ADD SERIES DATA' | translate"
                               matInput>
                    </label>
                </mat-form-field>
            </div>
            <div fxLayout="column">
                <mat-slide-toggle
                        [checked]="isInSeriesCycle()"
                        (change)="switchToInSeriesCycle($event)"
                >
                    {{'BOOK.FORM.SERIES.WITH_CYCLE' | translate}}
                </mat-slide-toggle>
                <app-book-series-cycle-form
                        *ngIf="displaySeriesCycle"
                        [seriesId]="form.value.id"
                        [form]="getBookSeriesCycleForm()"></app-book-series-cycle-form>
            </div>
        </form>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: ADMIN_SERVICE, useClass: SeriesAdministrationService
    }, {
        provide: SEARCH_PARAM, useClass: AutoCompleteSeries
    }]
})
export class SeriesFormComponent implements OnInit {

    @Input() public form: UntypedFormGroup;
    @Input() public edition: boolean = false;
    @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Series>;
    @Output() public selection: EventEmitter<Series> = new EventEmitter<Series>();

    display = false;
    displaySeriesCycle = false;

    ngOnInit(): void {
        this.displaySeriesCycle = this.isInSeriesCycle();
    }

    update(series: Series): void {
        this.selection.emit(series);
    }

    isInSeriesCycle(): boolean {
        return this.form.get('seriesCycle').value.name !== null;
    }

    getBookSeriesCycleForm(): UntypedFormGroup {
        return this.form.get('seriesCycle') as UntypedFormGroup;
    }

    switchToInSeriesCycle(event: MatSlideToggleChange) {
        this.displaySeriesCycle = event.checked;
        if (event.checked) {
            this.form.get('seriesCycle').enable();
        } else {
            this.form.get('seriesCycle').disable();
        }
    }

    public toggleMore(): void {
        this.display = !this.display;
    }

    /**
     * Give the good font awesome icon name
     * `caret-up` if the field display is `true`, `caret-down` otherwise
     * Return the good icon name
     */
    getIconName() {
        return this.display ? 'caret-up' : 'caret-down';
    }
}
