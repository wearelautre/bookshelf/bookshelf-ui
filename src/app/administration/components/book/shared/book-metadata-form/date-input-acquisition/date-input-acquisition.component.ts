import {Component, Input} from '@angular/core';
import {UntypedFormControl} from "@angular/forms";
import {MAT_DATE_FORMATS} from "@angular/material/core";

const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-date-input-acquisition',
    template: `
        <mat-form-field fxFlex appearance="outline" subscriptSizing="dynamic" class="mff-space-1">
            <label>
                <input [matDatepicker]="dp" matInput
                       [formControl]="form"
                       [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.ACQUISITION_DATE' | translate"/>
            </label>
            <!--            <mat-hint>DD/MM/YYYY</mat-hint>-->
            <mat-datepicker-toggle matSuffix [for]="dp"></mat-datepicker-toggle>
            <mat-datepicker #dp>
            </mat-datepicker>
        </mat-form-field>
    `,
    providers: [{provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}]
})
export class DateInputAcquisitionComponent {
    @Input()
    form: UntypedFormControl
}
