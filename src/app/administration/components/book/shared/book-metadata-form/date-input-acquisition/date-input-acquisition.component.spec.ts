import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DateInputAcquisitionComponent} from './date-input-acquisition.component';
import {
    NgxTranslateTestingModule
} from "../../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {FormControl, ReactiveFormsModule} from "@angular/forms";

describe('DateInputAcquisitionComponent', () => {
    let component: DateInputAcquisitionComponent;
    let fixture: ComponentFixture<DateInputAcquisitionComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [DateInputAcquisitionComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                MatMomentDateModule,
                ReactiveFormsModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DateInputAcquisitionComponent);
        component = fixture.componentInstance;
        component.form = new FormControl(null)
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
