import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-date-input-acquisition',
  template: `Mock MockDateInputAcquisitionComponent`,
})
export class MockDateInputAcquisitionComponent {
  @Input() form: any
}
