import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-date-input-original-release',
  template: ` Mock DateInputOriginalReleaseComponent`,
})
export class MockDateInputOriginalReleaseComponent {
  @Input() form: any
}
