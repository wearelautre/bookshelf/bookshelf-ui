import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UntypedFormControl, UntypedFormGroup} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {BookFormService} from "../../../../services/book-form.service";

@Component({
    selector: 'app-book-metadata-form',
    template: `
        <form fxFlex="100" [formGroup]="form" fxLayout="column">
            <div fxFlex="100" fxLayout="column" class="section">
                <h4>{{'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.TITLE' | translate}}</h4>
                <div fxLayout="row" class="line-1" fxLayoutGap="1rem">
                    <app-date-input-acquisition [form]="getAcquisitionDateFrom()"></app-date-input-acquisition>
                    <mat-slide-toggle fxFlex #toggle (change)="toggleOfferedStatus($event.checked)"
                                      labelPosition="after" fxFlexAlign="end center">
                        {{'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.GIFT' | translate}}
                    </mat-slide-toggle>
                </div>
                <div fxLayout="row" fxLayoutGap="1rem" *ngIf="!toggle.checked">
                    <mat-form-field fxFlex appearance="outline" subscriptSizing="dynamic" class="mff-space-1">
                        <label>
                            <input formControlName="price"
                                   matInput min="0"
                                   type="number" step="0.1"
                                   [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.PURCHASE_PRICE' | translate"/>
                        </label>
                    </mat-form-field>
                    <mat-form-field fxFlex appearance="outline" subscriptSizing="dynamic" class="mff-space-1">
                        <mat-select formControlName="priceCurrency"
                                    [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.PURCHASE_CURRENCY' | translate">
                            <mat-option [value]="'EUR'">EUR</mat-option>
                            <mat-option [value]="'USD'">USD</mat-option>
                        </mat-select>
                    </mat-form-field>
                </div>
            </div>
            <div fxFlex="100" fxLayout="column" class="section">
                <h4>{{'BOOK.FORM.METADATA.SUBSECTION.EDITION.TITLE' | translate}}</h4>
                <div fxLayout="row" fxLayoutGap="1rem">
                    <mat-form-field fxFlex appearance="outline" subscriptSizing="dynamic" class="mff-space-1">
                        <label>
                            <input matInput type="number" min="0"
                                   formControlName="pageCount"
                                   [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.EDITION.PAGE_COUNT' | translate"/>
                        </label>
                    </mat-form-field>
                    <app-date-input-original-release
                            [form]="getOriginalReleaseDateFrom()"></app-date-input-original-release>
                </div>
            </div>
        </form>
    `,
    styles: [`

      .section {
        margin-bottom: 1rem;
      }

      .line-1 {
        align-items: center;
      }
    `]
})
export class BookMetadataFormComponent implements OnInit {

    @Input() form: UntypedFormGroup;
    @Output() public updateOfferedStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

    toggleOfferedStatus(status: boolean): void {
        this.updateOfferedStatus.emit(status)
    }

    constructor(
        private bookFormService: BookFormService
    ) {
    }

    ngOnInit(): void {
        this.form.get('price').valueChanges
            .pipe(debounceTime(1000))
            .subscribe(v => this.bookFormService.setCurrencyMandatory(v))
    }


    getAcquisitionDateFrom(): UntypedFormControl {
        return this.form.get('acquisitionDate') as UntypedFormControl;
    }

    getOriginalReleaseDateFrom(): UntypedFormControl {
        return this.form.get('originalReleaseDate') as UntypedFormControl;
    }
}
