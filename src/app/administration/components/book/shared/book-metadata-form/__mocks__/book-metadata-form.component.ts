import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-metadata-form',
  template: ` app-book-metadata-form mock `
})
export class MockBookMetadataFormComponent {

  @Input() form: any;

}
