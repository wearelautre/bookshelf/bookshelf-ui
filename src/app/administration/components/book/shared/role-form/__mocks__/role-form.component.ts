import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-role-form',
  template: '<div>Role Form Mock</div>'
})
export class MockRoleFormComponent {

  @Output() addContract = new EventEmitter();
}
