import {ChangeDetectionStrategy, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {RoleAdministrationService} from '../../../../services/role-administration.service';
import {Role} from '../../../../../core/model/role';
import {UntypedFormControl, UntypedFormGroup} from '@angular/forms';
import {
  AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {BookFormService} from "../../../../services/book-form.service";
import {AutoCompleteItem, SearchParam} from "../../../../../shared/shared.module";

type T = Role

class AutoCompleteRoles extends AutoCompleteItem<T> {
    constructor(
        private bookFormService: BookFormService
    ) {
        super();
    }

    override getSearchParam(value: string): SearchParam {
        const criteria = this.bookFormService.getContractRoles().map(role => ({
            name: 'name',
            operation: '!',
            value: `${role.name}`
        }))
        return [
            [{name: 'name', operation: ':', value: `*${value}*`}, ...criteria],
            [{key: 'sort', value: 'count'}]
        ]
    }
}

@Component({
    selector: 'app-role-form',
    template: `
        <form [formGroup]="form" (ngSubmit)="addNewRole()" fxLayout="row" fxLayoutGap="1rem">
            <app-auto-complete-input
                    fxFlex="100"
                    [form]="form"
                    [placeholder]="'BOOK.FORM.CONTRACT.NEW_ROLES' | translate"
                    (selectItem)="addNewRole()">
            </app-auto-complete-input>
            <div fxLayoutAlign="center center">
                <button color="accent" type="submit" mat-icon-button>
                    <fa-icon [icon]="['fas', 'plus-circle']"></fa-icon>
                </button>
            </div>
        </form>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: ADMIN_SERVICE, useClass: RoleAdministrationService
    }, {
        provide: SEARCH_PARAM,
        useClass: AutoCompleteRoles,
        deps: [BookFormService]
    }]
})
export class RoleFormComponent {
    form: UntypedFormGroup = new UntypedFormGroup({
        name: new UntypedFormControl(null),
        id: new UntypedFormControl(null)
    });

    @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Role>;
    @Output() addContract: EventEmitter<Role> = new EventEmitter();

    private clearInput(): void {
        this.form.setValue({name: '', id: null});
    }

    addNewRole(): void {
        this.addContract.emit(this.form.value);
        this.clearInput();
    }
}
