import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RoleFormComponent} from './role-form.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {
    NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RoleAdministrationService} from '../../../../services/role-administration.service';
import {roleAdministrationServiceMock} from '../../../../services/__mocks__/role-administration.service';
import {MatInputModule} from '@angular/material/input';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
    MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";

describe('RoleFormComponent', () => {
    let component: RoleFormComponent;
    let fixture: ComponentFixture<RoleFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [RoleFormComponent, MockAutoCompleteInputComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                FontAwesomeTestingModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatInputModule,
                MatAutocompleteModule
            ]
        }).overrideComponent(RoleFormComponent, {
            set: {
                providers: [
                    {provide: RoleAdministrationService, useValue: roleAdministrationServiceMock}
                ]
            }
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoleFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        describe('[UPDATE ROLE]', () => {
            test('add new role', () => {
                const spy = jest.spyOn(component.addContract, 'emit').mockImplementation(() => ({}));
                const clearInputSpy = jest.spyOn<any, any>(component, `clearInput`);
                component.form.setValue({name: 'role1', id: 1})
                jest.clearAllMocks();
                component.addNewRole();
                expect(spy).toHaveBeenNthCalledWith(1, {name: 'role1', id: 1});
                expect(clearInputSpy).toHaveBeenNthCalledWith(1);
                expect(component.form.value).toStrictEqual({name: '', id: null});
            });
        });
    });
});
