import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injectable,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import {Artist} from '../../../../../core/model/artist';
import {ArtistAdministrationService} from '../../../../services/artist-administration.service';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
  AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {AutoCompleteItem, SearchParam} from "../../../../../shared/shared.module";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {ActionArtistMinimal, ActionArtistTypeEnum} from "../../../../models/action-artist.model";

@Injectable({providedIn: "root"})
export class ArtistServiceForm {
  public series: string;
  public role: string;

  setValues(
    role: string,
    series: string
  ) {
    this.series = series
    this.role = role
  }
}

type T = Artist

export class AutoCompleteArtist extends AutoCompleteItem<T> {
  constructor(
    private artistServiceForm: ArtistServiceForm
  ) {
    super();
  }

  override getSearchParam(value: string): SearchParam {
    return [
      [],
      [
        {key: 'name', value},
        {key: 'withRolesFirst', value: this.artistServiceForm.role},
        {key: 'withSeriesFirst', value: this.artistServiceForm.series}
      ]
    ]
  }
}

@Component({
  selector: 'app-artist-form',
  template: `
    <form [formGroup]="form" fxLayout="row" fxLayoutGap="1rem">
      <app-auto-complete-input
        fxFlex="100"
        [form]="form"
        [placeholder]="'BOOK.FORM.CONTRACT.ARTIST.NAME' | translate">
      </app-auto-complete-input>
      <div fxLayoutAlign="center center">
        <button type="button" color="warn" mat-icon-button matSuffix (click)="delete()">
          <fa-icon [icon]="['fas', 'trash']"></fa-icon>
        </button>
        <button type="button" [matMenuTriggerFor]="menu" mat-icon-button matSuffix>
          <fa-icon [icon]="['fas', 'bars']"></fa-icon>
        </button>
      </div>
    </form>

    <mat-menu #menu="matMenu">
      <button mat-menu-item (click)="move()">{{'BOOK.FORM.CONTRACT.ARTIST.ACTIONS.MOVE' | translate}}</button>
      <button mat-menu-item (click)="duplicate()">{{'BOOK.FORM.CONTRACT.ARTIST.ACTIONS.DUPLICATE' | translate}}</button>
    </mat-menu>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: ADMIN_SERVICE, useClass: ArtistAdministrationService,
  }, {
    provide: SEARCH_PARAM,
    useClass: AutoCompleteArtist,
    deps: [ArtistServiceForm]
  }, ArtistServiceForm]
})
export class ArtistFormComponent implements OnInit {
  @Input() form: UntypedFormGroup;
  @Input() index: number;
  @Input() role: string;
  @Input() series: string;

  @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Artist>;
  @Output() action: EventEmitter<ActionArtistMinimal> = new EventEmitter();

  constructor(
    private artistServiceForm: ArtistServiceForm
  ) {
  }

  ngOnInit(): void {
    this.artistServiceForm.setValues(this.role, this.series)
  }

  emitAction(action: ActionArtistTypeEnum): void {
    this.action.emit({action, index: this.index});
  }

  delete(): void {
    this.emitAction(ActionArtistTypeEnum.DELETE);
  }

  move(): void {
    this.emitAction(ActionArtistTypeEnum.MOVE);
  }

  duplicate(): void {
    this.emitAction(ActionArtistTypeEnum.DUPLICATE);
  }
}
