import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ArtistFormComponent, ArtistServiceForm, AutoCompleteArtist} from './artist-form.component';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ArtistForm} from '../../../../models/form/artist-form.model';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {ArtistAdministrationService} from '../../../../services/artist-administration.service';
import {artistAdministrationServiceMock} from '../../../../services/__mocks__/artist-administration.service';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {Component, Inject, Input, OnInit} from "@angular/core";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {AutoCompleteItem} from "../../../../../shared/shared.module";
import {RestNamedEntity} from "../../../../../core/model/rest-named-entity";
import {ADMIN_SERVICE, AdministrationService} from "../../../../services/administration.service";
import {
  MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";
import {MatMenuModule} from "@angular/material/menu";
import {ActionArtistTypeEnum} from "../../../../models/action-artist.model";


@Component({
  selector: 'app-auto-complete-input',
  template: ` mock auto complete `
})
export class AutoCompleteInputComponent<T extends RestNamedEntity> extends MockAutoCompleteInputComponent implements OnInit {
  @Input() form: any;

  ngOnInit(): void {
    this.service.searchAutocomplete(...this.searchParam.getSearchParam('toto'))
  }

  constructor(
    @Inject(ADMIN_SERVICE) public service: AdministrationService<T>,
    @Inject(SEARCH_PARAM) public searchParam: AutoCompleteItem<T>
  ) {
    super()
  }
}

describe('ArtistFormComponent', () => {
  let component: ArtistFormComponent;
  let fixture: ComponentFixture<ArtistFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ArtistFormComponent,
        AutoCompleteInputComponent
      ],
      imports: [
        NoopAnimationsModule,
        FontAwesomeTestingModule,
        NgxTranslateTestingModule,
        ReactiveFormsModule,
        MatMenuModule,
        MatOptionModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatAutocompleteModule,
        MatInputModule
      ],
      providers: [{
        provide: ADMIN_SERVICE, useValue: artistAdministrationServiceMock,
      }, {
        provide: SEARCH_PARAM,
        useClass: AutoCompleteArtist,
        deps: [ArtistServiceForm]
      }, ArtistServiceForm
      ]
    }).overrideComponent(ArtistFormComponent, {
      set: {
        providers: [
          {provide: ArtistAdministrationService, useValue: artistAdministrationServiceMock}
        ]
      }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistFormComponent);
    component = fixture.componentInstance;
    component.series = "series"
    component.role = "role"
    component.form = ArtistForm.formGroup();
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
      expect(component[`artistServiceForm`].series).toStrictEqual('series')
      expect(component[`artistServiceForm`].role).toStrictEqual('role')
      expect(artistAdministrationServiceMock.searchAutocomplete).toHaveBeenNthCalledWith(1,
        [],
        [
          {key: 'name', value: 'toto'},
          {key: 'withRolesFirst', value: 'role'},
          {key: 'withSeriesFirst', value: 'series'}
        ])
      jest.clearAllMocks();
    });
  });

  describe('Typescript test', () => {
    describe('[emitAction]', () => {
      test('emit action event', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.action, 'emit').mockImplementation(() => ({}));
        component.index = 34;
        component.emitAction(ActionArtistTypeEnum.DELETE);
        expect(spy).toHaveBeenNthCalledWith(1, {action: ActionArtistTypeEnum.DELETE, index: 34});
      });
    });
    describe('[DELETE]', () => {
      test('call emitAction with DELETE', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'emitAction').mockImplementation(() => ({}));
        component.index = 34;
        component.delete();
        expect(spy).toHaveBeenNthCalledWith(1, ActionArtistTypeEnum.DELETE);
      });
    });
    describe('[MOVE]', () => {
      test('call emitAction with MOVE', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'emitAction').mockImplementation(() => ({}));
        component.index = 34;
        component.move();
        expect(spy).toHaveBeenNthCalledWith(1, ActionArtistTypeEnum.MOVE);
      });
    });
    describe('[DUPLICATE]', () => {
      test('call emitAction with DUPLICATE', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component, 'emitAction').mockImplementation(() => ({}));
        component.index = 34;
        component.duplicate();
        expect(spy).toHaveBeenNthCalledWith(1, ActionArtistTypeEnum.DUPLICATE);
      });
    });
  });
});
