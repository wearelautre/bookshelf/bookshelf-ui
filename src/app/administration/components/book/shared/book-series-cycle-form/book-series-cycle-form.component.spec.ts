import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookSeriesCycleFormComponent} from './book-series-cycle-form.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
    NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {SeriesCycleAdministrationService} from '../../../../services/series-cycle-administration.service';
import {seriesCycleAdministrationServiceMock} from '../../../../services/__mocks__/series-cycle-administration.service';
import {
    MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";

describe('SeriesCycleFormComponent', () => {
    let component: BookSeriesCycleFormComponent;
    let fixture: ComponentFixture<BookSeriesCycleFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BookSeriesCycleFormComponent, MockAutoCompleteInputComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                ReactiveFormsModule,
                MatOptionModule,
                MatChipsModule,
                MatFormFieldModule,
                MatAutocompleteModule,
                MatInputModule
            ],
            providers: [
                {provide: SeriesCycleAdministrationService, useValue: seriesCycleAdministrationServiceMock}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BookSeriesCycleFormComponent);
        component = fixture.componentInstance;
        component.form = new FormGroup({name: new FormControl(''), tome: new FormControl(0)});
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    afterEach(() => jest.clearAllMocks());

    describe('Typescript test', () => {
        describe('[UPDATE]', () => {
            test('emit the new value', () => {
                const spy = jest.spyOn(component.selection, 'emit').mockImplementation(() => ({}));
                const seriesCycle = {name: 'name'}
                component.update(seriesCycle);
                expect(spy).toHaveBeenNthCalledWith(1, seriesCycle);
            });
        });
    });
});
