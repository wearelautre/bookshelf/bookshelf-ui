import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-series-cycle-form',
  template: '<div>BookSeriesCycle Form Mock</div>'
})
export class MockBookSeriesCycleFormComponent {

  @Input() form: any;
  @Input() appearance: any;
  @Input() classes: any;

}
