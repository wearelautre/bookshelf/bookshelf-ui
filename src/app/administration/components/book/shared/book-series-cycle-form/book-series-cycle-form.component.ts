import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import {SeriesCycle} from '../../../../../core/model/series';
import {SeriesCycleAdministrationService} from '../../../../services/series-cycle-administration.service';
import {MatFormFieldAppearance} from '@angular/material/form-field';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
    AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {DefaultAutoCompleteItem} from "../../../../../shared/shared.module";
import {idType} from "../../../../../core/model/rest-entity";

@Component({
    selector: 'app-book-series-cycle-form',
    template: `
        <form fxFlex [formGroup]="form" fxLayoutGap="1rem">
            <app-auto-complete-input
                    fxFlex="100"
                    [displayLabel]="displayLabel"
                    [appearance]="appearance"
                    [classes]="classes"
                    [form]="form"
                    [parentId]="seriesId"
                    [placeholder]="'BOOK.FORM.SERIES.CYCLE.NAME' | translate"
                    (selectItem)="update($event)">
            </app-auto-complete-input>
            <mat-form-field fxFlex="25" [formGroup]="form" [appearance]="appearance" subscriptSizing="dynamic"
                            class="mff-space-1">
                <mat-label *ngIf="displayLabel">{{"BOOK.FORM.SERIES.CYCLE.TOME" | translate}}</mat-label>
                <input matInput type="number" min="0" formControlName="tome"/>
            </mat-form-field>
        </form>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: ADMIN_SERVICE, useClass: SeriesCycleAdministrationService
    }, {
        provide: SEARCH_PARAM, useClass: DefaultAutoCompleteItem
    }]
})
export class BookSeriesCycleFormComponent {

    @Input() form: UntypedFormGroup;
    @Input() seriesId: idType = null;
    @Input() appearance: MatFormFieldAppearance = 'outline';
    @Input() classes: string;
    @Input() displayLabel: boolean = true;
    @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<SeriesCycle>;
    @Output() public selection: EventEmitter<SeriesCycle> = new EventEmitter<SeriesCycle>();

    /**
     * Update the seriesCycle's name input with the selected seriesCycle
     * @param value - the seriesCycle object selected from the autocomplete directive
     */
    update(value: SeriesCycle): void {
        this.selection.emit(value);
    }
}
