import {ComponentFixture, fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';

import {ContractFormComponent} from './contract-form.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MockArtistFormComponent} from '../artist-form/__mocks__/artist-form.component';
import {FormArray, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {RoleAdministrationService} from '../../../../services/role-administration.service';
import {roleAdministrationServiceMock} from '../../../../services/__mocks__/role-administration.service';
import {BookFormService} from '../../../../services/book-form.service';
import {bookFormServiceMock} from '../../../../services/__mocks__/book-form.service';
import {ContractFrom} from '../../../../models/form/contract.from.model';
import {BookSeriesImpl} from "../../../../../core/model/impl/book-series-impl";
import {MatButtonModule} from "@angular/material/button";
import {ActionArtistTypeEnum} from "../../../../models/action-artist.model";
import {ArtistForm, ArtistFormType} from "../../../../models/form/artist-form.model";

describe('ContractFormComponent', () => {
  let component: ContractFormComponent;
  let fixture: ComponentFixture<ContractFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ContractFormComponent, MockArtistFormComponent],
      imports: [
        NoopAnimationsModule,
        FontAwesomeTestingModule,
        NgxTranslateTestingModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule
      ],
      providers: [
        {provide: BookFormService, useValue: bookFormServiceMock},
      ]
    }).overrideComponent(ContractFormComponent, {
      set: {
        providers: [
          {provide: RoleAdministrationService, useValue: roleAdministrationServiceMock}
        ]
      }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractFormComponent);
    component = fixture.componentInstance;
    component.form = ContractFrom.formGroupWithValue({artists: [], role: {name: 'role1', id: null}})
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[DELETE]', () => {
      test('emit delete contract event', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.delete, 'emit').mockImplementation(() => ({}));
        component.index = 34;
        component.removeContract();
        expect(spy).toHaveBeenNthCalledWith(1, 34);
      });
      test('call delete contract artist event', () => {
        jest.clearAllMocks();
        component.index = 34;
        component.deleteArtist(1);
        expect(bookFormServiceMock.deleteArtist).toHaveBeenNthCalledWith(1, 34, 1);
      });
    });
    describe('[CURRENT SERIES NAME]', () => {
      test('get current Series Name', () => {
        jest.clearAllMocks();
        bookFormServiceMock.getCurrentSeries.mockImplementation(() => new BookSeriesImpl('TOTO'));
        expect(component.currentSeriesName()).toStrictEqual("TOTO");
      });
    });
    describe('[CURRENT ROLES NAME]', () => {
      test('get current Role Name', () => {
        jest.clearAllMocks();
        component.form.get('role').setValue({name: 'TATA', id: null})
        expect(component.currentRoleName()).toStrictEqual("TATA");
      });
    });
    describe('[ADD]', () => {
      test('call add contract artist event', () => {
        jest.clearAllMocks();
        component.index = 34;
        component.addArtist();
        expect(bookFormServiceMock.addArtist).toHaveBeenNthCalledWith(1, 34, {id: null, name: '', webLinks: []});
      });
    });
    describe('[UPDATE ROLE]', () => {
      test('from autoComplete', () => {
        jest.clearAllMocks();
        component.index = 34;
        component.updateRole({name: 'role1', id: 1});
        expect(component.form.get('role').value).toStrictEqual({name: 'role1', id: 1});
      });
      test('from input', () => {
        jest.clearAllMocks();
        component.form.get('role').setValue({name: 'role4', id: 1});
        component.updateRole();
        expect(component.form.get('role').value).toStrictEqual({name: 'role4', id: null});
      });
    });
    describe('[AUTOCOMPLETE]', () => {
      test('call api on input change role', fakeAsync(() => {
        jest.clearAllMocks();
        const name = 'name';
        component.form.get('role').patchValue({name, id: null});
        tick(300);
        expect(roleAdministrationServiceMock.searchAutocomplete).toHaveBeenNthCalledWith(1, [{
          name: 'name',
          operation: ':',
          value: `*${name}*`
        }], [{"key": "sort", "value": "count"}]);
      }));
      describe('[HANDLE ACTION]', () => {
        test('delete', () => {
          const spy = jest.spyOn(component, 'deleteArtist').mockImplementation(() => ({}));

          component.handleAction({action: ActionArtistTypeEnum.DELETE, index: 1});
          expect(spy).toHaveBeenNthCalledWith(1, 1);
        });
        test('move', () => {
          const spy = jest.spyOn(component.actionArtist, 'emit').mockImplementation(() => ({}));

          (component.form.get('artists') as FormArray<FormGroup<ArtistFormType>>).push(ArtistForm.formGroupWithValue({
            name: 'name1',
            webLinks: [],
            id: 1
          }));
          component.handleAction({action: ActionArtistTypeEnum.MOVE, index: 0});
          expect(spy).toHaveBeenNthCalledWith(1, {
            action: ActionArtistTypeEnum.MOVE,
            artistWithIndex: {artist: {id: 1, name: 'name1'}, index: 0},
          });
        });
        test('duplicate', () => {
          const spy = jest.spyOn(component.actionArtist, 'emit').mockImplementation(() => ({}));

          (component.form.get('artists') as FormArray<FormGroup<ArtistFormType>>).push(ArtistForm.formGroupWithValue({
            name: 'name1',
            webLinks: [],
            id: 1
          }));
          component.handleAction({action: ActionArtistTypeEnum.DUPLICATE, index: 0});
          expect(spy).toHaveBeenNthCalledWith(1, {
            action: ActionArtistTypeEnum.DUPLICATE,
            artistWithIndex: {artist: {id: 1, name: 'name1'}, index: 0},
          });
        });
        test('error', () => {

          (component.form.get('artists') as FormArray<FormGroup<ArtistFormType>>)
            .push(ArtistForm.formGroupWithValue({
              name: 'name1',
              webLinks: [],
              id: 1
            }));
          expect(() => component.handleAction({action: null, index: 0}))
            .toThrow('Action null not supported');
        });
      });
    });
  });
});
