import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {FormGroup, UntypedFormArray, UntypedFormGroup} from '@angular/forms';
import {BookFormService} from '../../../../services/book-form.service';
import {ArtistFormComponent} from '../artist-form/artist-form.component';
import {map, startWith, switchMap} from 'rxjs/operators';
import {RoleAdministrationService} from '../../../../services/role-administration.service';
import {Observable, Subscription} from 'rxjs';
import {Role} from '../../../../../core/model/role';
import {HttpResponse} from "@angular/common/http";
import {ActionArtist, ActionArtistMinimal, duplicateArtist, moveArtist} from "../../../../models/action-artist.model";
import {ContractFormType} from "../../../../models/form/contract.from.model";
import {Artist} from "../../../../../core/model/artist";

@Component({
  selector: 'app-contract-form',
  templateUrl: './contract-form.component.html',
  providers: [RoleAdministrationService]
})
export class ContractFormComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup<ContractFormType>;
  @Input() index: number;
  @Output() delete: EventEmitter<number> = new EventEmitter();
  @Output() actionArtist: EventEmitter<ActionArtist> = new EventEmitter();


  @ViewChildren(ArtistFormComponent) artistFormComponents: QueryList<ArtistFormComponent>;
  roleFilteredOptions$: Observable<Role[]>;

  private subscriptions: Subscription[] = [];
  editRoleName = false;

  constructor(
    private bookFormService: BookFormService,
    private rolesService: RoleAdministrationService
  ) {
  }

  // TODO use AutoCompleteInputComponent ??
  ngOnInit(): void {
    this.roleFilteredOptions$ = this.rolesService.list$;
    this.subscriptions.push(
      this.form.get('role.name').valueChanges
        .pipe(
          startWith(''),
          switchMap((value: string) =>
            this.rolesService.searchAutocomplete(
              [{name: 'name', operation: ':', value: `*${value}*`}],
              [{key: 'sort', value: 'count'}])
          ),
          map((response: HttpResponse<Role[]>) => response.body)
        )
        .subscribe()
    );
  }

  removeContract() {
    this.delete.emit(this.index);
  }

  addArtist() {
    this.bookFormService.addArtist(this.index, {name: '', webLinks: [], id: null});
  }

  handleAction(actionArtist: ActionArtistMinimal) {
    switch (actionArtist.action) {
      case 'delete':
        this.deleteArtist(actionArtist.index);
        break;
      case 'move':
        this.actionArtist.emit(moveArtist(
          this.form.value.artists[actionArtist.index] as Artist,
          this.index,
          actionArtist.index
        ));
        break;
      case 'duplicate':
        this.actionArtist.emit(duplicateArtist(
          this.form.value.artists[actionArtist.index] as Artist,
          actionArtist.index
        ));
        break;
      default:
        throw new Error(`Action ${actionArtist.action} not supported`);
    }
  }

  deleteArtist(artistIndex: number) {
    this.bookFormService.deleteArtist(this.index, artistIndex);
  }

  getArtistsFormGroup(): UntypedFormGroup[] {
    return (this.form.get('artists') as UntypedFormArray).controls as UntypedFormGroup[];
  }

  updateRole(role: Role = null): void {
    const roleControl = this.form.get('role');
    roleControl.setValue(role === null ? {name: roleControl.value.name.trim(), id: null} : {...role, id: role.id});
    this.editRoleName = false;
  }

  currentRoleName(): string {
    return this.form.get('role').value.name;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  nonEmptyArtists() {
    return this.form.value.artists.filter(a => a.name !== '' && a.name !== null).length
  }

  currentSeriesName() {
    return this.bookFormService.getCurrentSeries().name;
  }
}
