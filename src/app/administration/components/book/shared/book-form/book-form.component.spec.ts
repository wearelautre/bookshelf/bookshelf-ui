import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookFormComponent} from './book-form.component';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MockLoaderImgComponent} from '../../../../../shared/loader-img/__mocks__/loader-img.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {bookAdministrationServiceMock} from '../../../../services/__mocks__/book-administration.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MockTitleDisplayComponent} from '../../../../../shared/title-display/__mocks__/title-display.component';
import {MockBookTypeFormComponent} from '../book-type-form/__mocks__/book-type-form.component';
import {MockSeriesFormComponent} from '../series-form/__mocks__/series-form.component';
import {MockEditorFormComponent} from '../editor-form/__mocks__/editor-form.component';
import {MatIconModule} from '@angular/material/icon';
import {MockArtistFormComponent} from '../artist-form/__mocks__/artist-form.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterTestingModule} from '@angular/router/testing';
import {BookImpl} from '../../../../../core/model/impl/book-impl';
import {BookFormService} from '../../../../services/book-form.service';
import {bookFormServiceMock} from '../../../../services/__mocks__/book-form.service';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {By} from '@angular/platform-browser';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {MockRoleFormComponent} from '../role-form/__mocks__/role-form.component';
import {MockContractFormComponent} from '../contract-form/__mocks__/contract-form.component';
import {ActivatedRoute} from "@angular/router";
import {MatDialogModule} from "@angular/material/dialog";
import {TaskService} from "../../../../services/task.service";
import {taskServiceMock} from "../../../../services/__mocks__/task.service";
import {BookSeriesImpl} from "../../../../../core/model/impl/book-series-impl";
import {NotificationService} from "../../../../../shared/services/notification.service";
import {notificationServiceMock} from "../../../../../shared/services/__mocks__/notification.service";
import {MockBookMetadataFormComponent} from "../book-metadata-form/__mocks__/book-metadata-form.component";
import {MatTabsModule} from '@angular/material/tabs';
import {WishlistBookAdministrationService} from "../../../../services/wishlist-book-administration.service";
import {
  wishlistBookAdministrationServiceMock
} from "../../../../services/__mocks__/wishlist-book-administration.service";
import {Series} from "../../../../../core/model/series";
import {ContractFormType} from "../../../../models/form/contract.from.model";
import {RoleFormType} from "../../../../models/form/role-form.model";
import {ActionArtistTypeEnum} from "../../../../models/action-artist.model";
import {RolesSelectionDialogComponent} from "../roles-selection-dialog/roles-selection-dialog.component";

const data = new BehaviorSubject<any>({data: new BookImpl(), edition: false})

const routeMock = {
  data: data,
  snapshot: {
    data: data.value
  }
};

describe('BookFormComponent', () => {
  let component: BookFormComponent;
  let fixture: ComponentFixture<BookFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookFormComponent,
        MockLoaderImgComponent,
        MockTitleDisplayComponent,
        MockBookTypeFormComponent,
        MockSeriesFormComponent,
        MockEditorFormComponent,
        MockArtistFormComponent,
        MockRoleFormComponent,
        MockContractFormComponent,
        MockBookMetadataFormComponent
      ],
      imports: [
        FontAwesomeTestingModule,
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatListModule,
        MatDialogModule,
        MatIconModule,
        MatSnackBarModule,
        MatTabsModule
      ],
      providers: [
        {provide: ActivatedRoute, useValue: routeMock},
        {provide: BookAdministrationService, useValue: bookAdministrationServiceMock},
        {provide: WishlistBookAdministrationService, useValue: wishlistBookAdministrationServiceMock},
        {provide: BookFormService, useValue: bookFormServiceMock},
        {provide: NotificationService, useValue: notificationServiceMock},
        {provide: TaskService, useValue: taskServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(waitForAsync(() => {
    jest.clearAllMocks();
  }));

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
    describe('CREATION', () => {
      test('should create', () => {
        expect(component[`imgPath`]).toStrictEqual('/files/search/covers');
      });
    });
    describe('EDITION', () => {
      test('should create', () => {
        data.next({...data.value, edition: true});
        // component.ngOnInit();
        expect(component[`imgPath`]).toStrictEqual('/files/covers');
      });
    });
  });
  describe('Typescript test', () => {
    test('should call addAuthor', () => {
      component.addContract({name: 'role1', id: null});
      expect(bookFormServiceMock.addContract).toHaveBeenCalledTimes(1);
    });
    test('should return the series default', () => {
      component.form.get('series').get('name').setValue('bonjour')
      expect(component.getSeries()).toStrictEqual({
        bookType: {
          id: null,
          name: null
        },
        seriesCycle: {name: null, id: null, tome: null},
        name: 'bonjour',
        id: null,
        tome: null, oneShot: false,
        status: {
          possessedCount: 0,
          totalCount: 0,
          readCount: 0,
          status: Series.SeriesStatusEnum.UNKNOWN
        },
      });
    });
    test('should call updateOfferedStatus', () => {
      component.onUpdateOfferedStatus(true)
      expect(bookFormServiceMock.updateOfferedBookStatus).toHaveBeenNthCalledWith(1, true);
    });
    test('should call updateSeries', () => {
      component.onSeriesSelection({
        name: "test",
        id: 123456,
        status: {
          readCount: 0,
          possessedCount: 0,
          totalCount: 0,
          status: Series.SeriesStatusEnum.UNKNOWN
        },
      })
      expect(bookFormServiceMock.updateSeries).toHaveBeenNthCalledWith(1, {
        name: "test",
        id: 123456,
        status: {
          possessedCount: 0,
          readCount: 0,
          totalCount: 0,
          status: Series.SeriesStatusEnum.UNKNOWN
        },
      });
    });
    test('should call updateEditor', () => {
      component.onEditorSelection({
        name: "test",
        id: 123456
      })
      expect(bookFormServiceMock.updateEditor).toHaveBeenNthCalledWith(1, {
        name: "test",
        id: 123456
      });
    });

    test('should call deleteAuthor', () => {
      component.deleteContract(1);
      expect(bookFormServiceMock.deleteContract).toHaveBeenNthCalledWith(1, 1);
    });
    test('should call switchToOneShot', () => {
      component.switchToOneShot({checked: false, source: undefined});
      expect(bookFormServiceMock.toggleOneShot).toHaveBeenNthCalledWith(1, false);
    });
    describe('targetIsWishList', () => {
      test('Is WishList', () => {
        component[`selectedTarget`] = 'WISHLIST';
        expect(component.targetIsWishList()).toBeTruthy();
      });
      test('Is not WishList', () => {
        expect(component.targetIsWishList()).toBeFalsy();
      });
    });
    describe('GET TRANSLATE CATEGORY', () => {
      test('should call return ADD', () => {
        data.next({...data.value, edition: false});
        expect(component.translateCategory).toStrictEqual('ADD');
      });
      test('should call return EDIT', () => {
        data.next({...data.value, edition: true});
        expect(component.translateCategory).toStrictEqual('EDIT');
      });
    });
    describe('SUCCESS', () => {
      const b = new BookImpl();
      b.series = new BookSeriesImpl()
      b.series.bookType = {name: 'bookType', id: 98}
      b.isbn = 'isbn';
      test('should call return ADD then move', () => {
        data.next({...data.value, edition: false});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(false)}));
        component[`success`](b);
        expect(spyDialog).toHaveBeenCalledTimes(1);
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'book', 'list'], {
          queryParams: {
            bookType: {
              name: 'bookType',
              id: 98
            }
          }
        });
      });
      test('should call return ADD then stay', () => {
        data.next({...data.value, edition: false});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(true)}));
        component[`success`](b);
        expect(spyDialog).toHaveBeenCalledTimes(1);
        expect(taskServiceMock.getTaskTodo).toHaveBeenCalledTimes(1);
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'administration', 'book', 'add'], {queryParams: {withMenu: true}});
      });
      test('should call return EDIT', () => {
        data.next({...data.value, edition: true});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        component[`success`](b);
        expect(notificationServiceMock.displayMessage).toHaveBeenNthCalledWith(1, `BOOK.EDIT.SUCCESS`, {isbn: 'isbn'});
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'administration', 'edition', 'book']);
      });
    });
    describe('SUBMIT', () => {
      const book = {
        editor: {name: 'editorName', id: 1},
        isbn: null,
        year: null,
        arkId: null,
        cover: null,
        collection: null,
        series: {
          tome: 3,
          name: 'seriesName',
          seriesCycle: {name: null, id: null, tome: null},
          id: 0,
          bookType: {name: 'BD', id: null},
          oneShot: true
        },
        contracts: []
      };
      const expectedBook = {
        ...book,
        series: {
          tome: 3,
          name: 'seriesName',
          seriesCycle: null,
          id: 0,
          bookType: {name: 'BD', id: null},
          oneShot: true
        },
        title: null,
        artist: undefined,
        metadata: {
          acquisitionDate: null,
          offered: false,
          originalReleaseDate: null,
          pageCount: null,
          price: null,
          priceCurrency: null,
        }
      };

      beforeEach(() => {
        component.book = {...book} as BookImpl;
        component.form.patchValue({
          series: {
            tome: 3,
            seriesCycle: {name: null, id: null, tome: null},
            name: 'seriesName',
            bookType: {name: 'BD', id: null}
          },
          editor: {
            id: 1,
            name: 'editorName'
          }
        });
      });
      describe('SUCCESS', () => {
        let spy: jest.SpyInstance<any, unknown[]>;
        beforeEach(() => spy = jest.spyOn<any, any>(component, `success`).mockImplementation(() => ({})));

        test('should create book', waitForAsync(() => {
          component.edition = false;
          component.submit();
          expect(bookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, {});
        }));
        test('should add book to wishlist', waitForAsync(() => {
          component.edition = false;
          component[`selectedTarget`] = 'WISHLIST';
          component.submit();
          expect(wishlistBookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, {});
        }));
        test('should edit book', waitForAsync(() => {
          component.edition = true;
          component.book.cover = 'toto?tata';
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1, {...expectedBook, cover: 'toto'});
          expect(spy).toHaveBeenNthCalledWith(1, {...expectedBook, cover: 'toto'});
        }));
        test('should edit with id', waitForAsync(() => {
          component.form.patchValue({
            series: {
              tome: 3,
              name: 'new seriesName',
              bookType: 'BD'
            },
            editor: {
              name: 'editorName'
            }
          });
          component.edition = true;
          component.book.cover = 'toto?tata';
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1,
            {
              ...expectedBook,
              cover: 'toto',
              series: {...expectedBook.series, id: null, name: "new seriesName"}
            }
          );
          expect(spy).toHaveBeenNthCalledWith(1, {
            ...expectedBook,
            cover: 'toto',
            series: {...expectedBook.series, id: null, name: "new seriesName"}
          });
        }));
      });
      describe('ERROR', () => {
        let spy: jest.SpyInstance<any, unknown[]>;
        beforeEach(() => spy = jest.spyOn<any, any>(component, `error`).mockImplementation(() => ({})));
        test('should not create book', waitForAsync(() => {
          component.edition = false;
          bookAdministrationServiceMock.create.mockImplementation(() => throwError(() => new Error('error')));
          component.submit();
          expect(bookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, new Error('error'), expectedBook);
        }));
        test('should not create wishlist book', waitForAsync(() => {
          component.edition = false;
          component[`selectedTarget`] = 'WISHLIST';
          wishlistBookAdministrationServiceMock.create.mockImplementation(() => throwError(() => new Error('error')));
          component.submit();
          expect(wishlistBookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, new Error('error'), expectedBook);
        }));
        test('should not edit book', waitForAsync(() => {
          component.edition = true;
          bookAdministrationServiceMock.update.mockImplementation(() => throwError(() => new Error('error')));
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, new Error('error'), expectedBook);
        }));
        test('should not edit book WISHLIST', waitForAsync(() => {
          component.edition = true;
          component[`selectedTarget`] = 'WISHLIST';
          try {
            component.submit()
          } catch (e) {
            expect(e.message).toBe('Edition of wishlist book is not supported');
          }
        }));
      });
    });
    describe('getContractsRoleNameList', () => {
      test('should return empty list', () => {
        jest.spyOn<any, any>(component, `getFormContractsFromGroups`)
          .mockReturnValue([]);
        expect(component.getContractsRoleNameList()).toStrictEqual([]);
      });
      test('should return non empty list', () => {
        jest.spyOn<any, any>(component, `getFormContractsFromGroups`)
          .mockReturnValue([new FormGroup<ContractFormType>({
            artists: new FormArray([]),
            role: new FormGroup<RoleFormType>({
              name: new FormControl('role1'),
              id: new FormControl(null)
            }),
          })]);
        expect(component.getContractsRoleNameList()).toStrictEqual([{
          index: 0,
          role: {
            name: 'role1',
            id: null,
          }
        }]);
      });
    });
    describe('onActionArtist', () => {
      test('manage delete', () => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({afterClosed: () => of(true)});
        component.onActionArtist({
          action: ActionArtistTypeEnum.DELETE,
          roleIndex: 0,
          artistWithIndex: {
            artist: {name: 'artist1', id: 1, webLinks: []},
          }
        });
        expect(spy).toHaveBeenCalledTimes(0);
      });
      test('manage default', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({afterClosed: () => of(true)});
        expect(() => component.onActionArtist({
            action: null,
            roleIndex: 0,
            artistWithIndex: {
              artist: {name: 'artist1', id: 1, webLinks: []},
            }
          })
        ).toThrow('Action null not supported');
        expect(spy).toHaveBeenCalledTimes(0);
      }));
      test('manage duplicate', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({
            afterClosed: () => of([
              {role: {name: 'role1', id: 1}, index: 0},
              {role: {name: 'role4', id: 4}, index: 3},
            ])
          });
        const spyGetContractsRoleNameList = jest.spyOn<any, any>(component, `getContractsRoleNameList`).mockReturnValue(['toto'])
        const spyAddArtist = jest.spyOn<any, any>(bookFormServiceMock, `addArtist`)
        const spyDeleteArtist = jest.spyOn<any, any>(bookFormServiceMock, `deleteArtist`)

        component.onActionArtist({
          action: ActionArtistTypeEnum.DUPLICATE,
          roleIndex: 0,
          artistWithIndex: {
            artist: {name: 'artist1', id: 1, webLinks: []},
          }
        });
        expect(spyAddArtist).toHaveBeenCalledWith(0, {name: 'artist1', id: 1, webLinks: []})
        expect(spyAddArtist).toHaveBeenCalledWith(3, {name: 'artist1', id: 1, webLinks: []})
        expect(spyDeleteArtist).toHaveBeenCalledTimes(0)
        expect(spyGetContractsRoleNameList).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, RolesSelectionDialogComponent, {
          width: '450px',
          data: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            action: ActionArtistTypeEnum.DUPLICATE,
            roles: ['toto']
          }
        });
      }));
      test('manage duplicate no roles selected', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({
            afterClosed: () => of([])
          });
        const spyGetContractsRoleNameList = jest.spyOn<any, any>(component, `getContractsRoleNameList`).mockReturnValue(['toto'])
        const spyAddArtist = jest.spyOn<any, any>(bookFormServiceMock, `addArtist`)
        const spyDeleteArtist = jest.spyOn<any, any>(bookFormServiceMock, `deleteArtist`)

        component.onActionArtist({
          action: ActionArtistTypeEnum.DUPLICATE,
          roleIndex: 0,
          artistWithIndex: {
            artist: {name: 'artist1', id: 1, webLinks: []},
          }
        });
        expect(spyAddArtist).toHaveBeenCalledTimes(0)
        expect(spyDeleteArtist).toHaveBeenCalledTimes(0)
        expect(spyGetContractsRoleNameList).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, RolesSelectionDialogComponent, {
          width: '450px',
          data: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            action: ActionArtistTypeEnum.DUPLICATE,
            roles: ['toto']
          }
        });
      }));
      test('manage move', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({
            afterClosed: () => of([
              {role: {name: 'role1', id: 1}, index: 0},
              {role: {name: 'role4', id: 4}, index: 3},
            ])
          });
        const spyGetContractsRoleNameList = jest.spyOn<any, any>(component, `getContractsRoleNameList`).mockReturnValue(['toto'])
        const spyAddArtist = jest.spyOn<any, any>(bookFormServiceMock, `addArtist`)
        const spyDeleteArtist = jest.spyOn<any, any>(bookFormServiceMock, `deleteArtist`)

        component.onActionArtist({
          action: ActionArtistTypeEnum.MOVE,
          roleIndex: 5,
          artistWithIndex: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            index: 9
          }
        });
        expect(spyAddArtist).toHaveBeenCalledWith(0, {name: 'artist1', id: 1, webLinks: []})
        expect(spyAddArtist).toHaveBeenCalledWith(3, {name: 'artist1', id: 1, webLinks: []})
        expect(spyDeleteArtist).toHaveBeenCalledWith(5, 9)
        expect(spyGetContractsRoleNameList).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, RolesSelectionDialogComponent, {
          width: '450px',
          data: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            action: ActionArtistTypeEnum.MOVE,
            roles: ['toto']
          }
        });
        expect(spy).toHaveBeenCalledTimes(1);
      }));
      test('manage move no roles selected', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(component[`dialog`], `open`)
          .mockReturnValue({
            afterClosed: () => of([])
          });
        const spyGetContractsRoleNameList = jest.spyOn<any, any>(component, `getContractsRoleNameList`).mockReturnValue(['toto'])
        const spyAddArtist = jest.spyOn<any, any>(bookFormServiceMock, `addArtist`)
        const spyDeleteArtist = jest.spyOn<any, any>(bookFormServiceMock, `deleteArtist`)

        component.onActionArtist({
          action: ActionArtistTypeEnum.MOVE,
          roleIndex: 5,
          artistWithIndex: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            index: 9
          }
        });
        expect(spyAddArtist).toHaveBeenCalledTimes(0)
        expect(spyDeleteArtist).toHaveBeenCalledTimes(0)
        expect(spyGetContractsRoleNameList).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, RolesSelectionDialogComponent, {
          width: '450px',
          data: {
            artist: {name: 'artist1', id: 1, webLinks: []},
            action: ActionArtistTypeEnum.MOVE,
            roles: ['toto']
          }
        });
        expect(spy).toHaveBeenCalledTimes(1);
      }));
    });
    describe('IMG', () => {
      const mockCover = new File(new Array<Blob>(), 'Mock.png', {type: 'application/zip'});

      beforeEach(() => {
        component.form.patchValue({isbn: 'tata'});
        component.book = {...component.book, cover: 'init'};
      });

      test('should reset image path', () => {
        component.imagePath = 'test';
        component.resetImage();
        expect(component.getImg('toto').src.startsWith('/files/covers/')).toBeTruthy();
      });
      test('should not upload image', () => {
        component.onSelect({
          addedFiles: [],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(component.uploadCover).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover.mock.calls.length).toStrictEqual(0);
      });
      test('should upload image', waitForAsync(() => {
        bookAdministrationServiceMock.uploadCover.mockImplementation(() => of({cover: 'coverTest'}));

        component.onSelect({
          addedFiles: [mockCover],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover).toHaveBeenNthCalledWith(1, mockCover, 'tata');
        expect(component.book.cover).toStrictEqual('coverTest');
        expect(component.uploadCover).toBeFalsy();
      }));
      test('should upload image fail', waitForAsync(() => {
        bookAdministrationServiceMock.uploadCover.mockImplementation(() => throwError(() => new Error('toto')));

        component.onSelect({
          addedFiles: [mockCover],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover).toHaveBeenNthCalledWith(1, mockCover, 'tata');
        expect(component.book.cover).toStrictEqual('init');
        expect(component.uploadCover).toBeFalsy();
      }));
    });
  });
});

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});
