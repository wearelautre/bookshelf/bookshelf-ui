import {Component, OnDestroy, OnInit} from '@angular/core';
import {DisplayImage} from '../../../../../shared/display-image';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {FormArray, FormGroup, UntypedFormControl, UntypedFormGroup} from '@angular/forms';
import {BookFormService} from '../../../../services/book-form.service';
import {Book} from '../../../../../core/model/book';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {TranslateService} from '@ngx-translate/core';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {Editor} from '../../../../../core/model/editor';
import {BookSeries, Series} from '../../../../../core/model/series';
import {NgxDropzoneChangeEvent} from 'ngx-dropzone';
import {Image} from '../../../../../shared/models/image';
import {Role} from '../../../../../core/model/role';
import {TaskService} from "../../../../services/task.service";
import {DialogConfirmComponent} from "../../../../../shared/confirm-modal/dialog-confirm.component";
import {BookImpl} from "../../../../../core/model/impl/book-impl";
import {filter} from "rxjs/operators";
import {NotificationService} from "../../../../../shared/services/notification.service";
import {Moment} from "moment";
import {WishlistBookAdministrationService} from "../../../../services/wishlist-book-administration.service";
import {WishlistBook} from "../../../../../wishlist/models/wishlist-book";
import {RolesSelectionDialogComponent} from "../roles-selection-dialog/roles-selection-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {ActionArtist} from "../../../../models/action-artist.model";
import {ContractFormType} from "../../../../models/form/contract.from.model";
import {EditorFormType} from "../../../../models/form/editor-form.model";

export class BookFormData {
  edition: boolean = false;
  data: Book = new BookImpl();
}

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styles: [`
    .dropzone {
      width: 75%;
      height: 600px;
      background-color: hsla(50, 33%, 25%, .0);

      .cover-wrapper {
        width: 350px;
      }
    }
  `]
})
export class BookFormComponent extends DisplayImage implements OnInit, OnDestroy {
  uploadCover = false;
  private cover: BehaviorSubject<Image> = new BehaviorSubject<Image>(null);

  get cover$(): Observable<Image> {
    return this.cover.asObservable();
  }

  constructor(
    private bookAdministrationService: BookAdministrationService,
    private wishlistBookAdministrationService: WishlistBookAdministrationService,
    private bookFormService: BookFormService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private taskService: TaskService
  ) {
    super('/files/search/covers');
  }

  public form: UntypedFormGroup;
  private subscriptions: Subscription[] = [];
  public contracts: FormArray<FormGroup<ContractFormType>>;
  public editor: FormGroup<EditorFormType>;
  public series: UntypedFormGroup;
  public metadata: UntypedFormGroup;

  public book: Book = null;
  public edition = false;
  public translateCategory = 'ADD'
  public selectedTarget: 'LIBRARY' | 'WISHLIST' = 'LIBRARY';

  ngOnInit(): void {
    this.subscriptions.push(
      this.bookFormService.bookForm$
        .subscribe(bookForm => {
          this.form = bookForm;
          this.contracts = this.form.get('contracts') as FormArray<FormGroup<ContractFormType>>;
          this.editor = this.form.get('editor') as UntypedFormGroup;
          this.series = this.form.get('series') as UntypedFormGroup;
          this.metadata = this.form.get('metadata') as UntypedFormGroup;
        }),
      this.route.data
        .subscribe((data: BookFormData) => {
          this.edition = data.edition
          if (this.edition) {
            this.imagePath = '/files/covers';
            this.translateCategory = 'EDIT';
          } else {
            this.imagePath = '/files/search/covers';
            this.translateCategory = 'ADD';
          }
          this.book = data.data;
          this.bookFormService.initForm(this.book);
          this.cover.next(this.getImg(this.book.cover));
        }),

      (this.form.get('title') as UntypedFormControl).valueChanges
        .pipe(filter(() => this.book.series.oneShot))
        .subscribe(value => {
          this.form.get('series').patchValue({
            name: value
          }, {emitEvent: false});
        })
    );
  }

  addContract(role: Role) {
    this.bookFormService.addContract(role);
  }

  deleteContract(index: number) {
    this.bookFormService.deleteContract(index);
  }

  switchToOneShot(event: MatSlideToggleChange) {
    this.book.series.oneShot = event.checked;
    this.bookFormService.toggleOneShot(event.checked);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  submit(): void {
    const editor = this.buildEditor();
    const series = this.buildSeries();
    const book = this.buildBook(editor, series);
    book.isbn = this.book.isbn;
    if (this.selectedTarget === 'LIBRARY') {
      (this.edition ? this.bookAdministrationService.update(book) : this.bookAdministrationService.create(book))
        .subscribe(
          {
            next: (b) => this.success(b),
            error: (error: HttpErrorResponse) => this.error(error, book)
          }
        );
    } else if (this.selectedTarget === 'WISHLIST') {
      if (this.edition) {
        throw new Error('Edition of wishlist book is not supported');
      }
      const wishlistBook = book as WishlistBook
      this.wishlistBookAdministrationService.create(wishlistBook)
        .subscribe(
          {
            next: (b) => this.success(b),
            error: (error) => this.error(error, book)
          }
        );
    }
  }

  private buildEditor(): Editor {
    return {...this.book.editor, ...this.form.value.editor};
  }

  private buildSeries(): BookSeries {
    const id = this.form.getRawValue().series.id;
    const seriesCycle = this.form.getRawValue().series.seriesCycle.name === null ? null : this.form.getRawValue().series.seriesCycle;
    return {
      ...this.book.series, ...this.form.getRawValue().series,
      seriesCycle,
      id: this.book.series.name !== this.form.getRawValue().series.name ? id : (this.form.getRawValue().series.id || this.book.series.id),
      // editor: editor.name,
      oneShot: this.book.series.oneShot,
    };
  }

  private buildBook(editor: Editor, series: Series): Book {
    this.book.cover = this.book.cover !== null && this.book.cover !== undefined ? this.book.cover.split('?')[0] : null;
    const book = {...this.book, ...this.form.getRawValue(), series, editor, artist: this.form.value.artist};
    if (book.metadata.acquisitionDate !== null) {
      book.metadata.acquisitionDate = (this.form.value.metadata.acquisitionDate as Moment).format('yyyy/MM/DD')
    }
    if (book.metadata.originalReleaseDate !== null) {
      book.metadata.originalReleaseDate = (this.form.value.metadata.originalReleaseDate as Moment).format('yyyy/MM')
    }
    return book
  }

  private success(b: Book | WishlistBook) {
    // TODO move that to a service ?
    this.taskService.getTaskTodo().subscribe();
    if (this.edition) {
      this.notificationService.displayMessage(`BOOK.EDIT.SUCCESS`, {isbn: b.isbn});
      this.router.navigate(['/', 'administration', 'edition', 'book']);
    } else {
      return this.dialog.open(DialogConfirmComponent, {
        width: '450px',
        data: {
          title: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.TITLE'),
          message: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.MESSAGE', {isbn: b.isbn}),
          question: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.QUESTION')
        }
      }).afterClosed().subscribe((res: boolean) => {
        if (!res) {
          this.router.navigate(['/', 'book', 'list'], {queryParams: {bookType: b.series.bookType}});
        } else {
          this.router.navigate(['/', 'administration', 'book', 'add'], {queryParams: {withMenu: true}});
        }
      });
    }
  }

  private error(err: HttpErrorResponse, book: Book) {
    if (err.status === 409) {
      this.notificationService.displayMessage(`BOOK.${this.translateCategory}.ERROR.ALREADY_EXIST`, {
        isbn: book.isbn,
        arkId: book.arkId
      });
    } else {
      this.notificationService.displayMessage(`BOOK.${this.translateCategory}.ERROR.GENERIC`, {isbn: book.isbn});
    }
  }

  resetImage() {
    this.imagePath = '/files/covers';
  }

  onSelect(event: NgxDropzoneChangeEvent): void {
    this.uploadCover = true;
    this.imagePath = '/files/search/covers';
    if (event.addedFiles[0]) {
      this.bookAdministrationService.uploadCover(event.addedFiles[0], this.form.value.isbn)
        .subscribe({
          next: ({cover}) => {
            this.cover.next(this.getImg(cover));
            this.book.cover = cover;
            this.uploadCover = false;
          },
          error: _ => this.uploadCover = false
        });
    }
  }

  getFormContractsFromGroups() {
    return this.contracts.controls;
  }

  getContractsRoleNameList(): { role: Role, index: number }[] {
    return this.getFormContractsFromGroups().map((contract, index) => ({
        role: contract.getRawValue().role,
        index: index
      })
    );
  }

  getBookTypeFormGroup(): UntypedFormGroup {
    return this.series.controls.bookType as UntypedFormGroup;
  }

  getSeries() {
    return {...this.book.series, ...this.form.getRawValue().series};
  }

  onSeriesSelection(series: Series) {
    this.bookFormService.updateSeries(series)
  }

  onEditorSelection(editor: Editor) {
    this.bookFormService.updateEditor(editor)
  }

  onUpdateOfferedStatus(status: boolean) {
    this.bookFormService.updateOfferedBookStatus(status)
  }

  targetIsWishList() {
    return this.selectedTarget === 'WISHLIST';
  }

  onActionArtist(actionArtist: ActionArtist) {
    switch (actionArtist.action) {
      case 'duplicate':
      case 'move':
        this.dialog.open(RolesSelectionDialogComponent, {
          width: '450px',
          data: {
            artist: actionArtist.artistWithIndex.artist,
            action: actionArtist.action,
            roles: this.getContractsRoleNameList()
          }
        })
          .afterClosed()
          .subscribe((roles: { role: Role, index: number }[]) => {
            if ((roles ?? []).length !== 0) {
              roles.forEach(role => this.bookFormService.addArtist(role.index, actionArtist.artistWithIndex.artist))
              if (actionArtist.action === 'move') {
                this.bookFormService.deleteArtist(actionArtist.roleIndex, actionArtist.artistWithIndex.index);
              }
            }
          });
        break;
      case 'delete':
        break;
      default:
        throw new Error(`Action ${actionArtist.action} not supported`);
    }
  }
}
