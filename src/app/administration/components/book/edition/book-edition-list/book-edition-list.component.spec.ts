import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookEditionListComponent} from './book-edition-list.component';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {bookAdministrationServiceMock} from '../../../../services/__mocks__/book-administration.service';
import {MockProgressbarComponent} from '../../../../shared/progressbar/__mocks__/progressbar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {RouterTestingModule} from '@angular/router/testing';
import {BehaviorSubject} from 'rxjs';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../../../shared/services/__mocks__/pagination.service";
import {Book} from "../../../../../core/model/book";

describe('BookEditionListComponent', () => {
  let component: BookEditionListComponent;
  let fixture: ComponentFixture<BookEditionListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BookEditionListComponent, MockProgressbarComponent],
      imports: [
        ReactiveFormsModule,
        NoopAnimationsModule,
        RouterTestingModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatButtonToggleModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatTableModule
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {provide: BookAdministrationService, useValue: bookAdministrationServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookEditionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });


  describe('Typescript test', () => {
    test('get status enum values', waitForAsync(() => {
      jest.clearAllMocks();
      expect(component.statusValues).toStrictEqual(['READ', 'UNREAD', 'READING'])
    }));
    test('get possession status enum values', waitForAsync(() => {
      jest.clearAllMocks();
      expect(component.possessionStatusValues).toStrictEqual(['NOT_POSSESSED', 'POSSESSED'])
    }));
    describe('OnDestroy', () => {
      test('should unsub all subscription', waitForAsync(() => {
        jest.clearAllMocks();
        const obs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
        let value = 0;
        component[`subscriptions`].push(obs.subscribe(v => value = v))
        obs.next(30);
        expect(value).toStrictEqual(30)
        component[`ngOnDestroy`]();
        obs.next(40);
        expect(value).toStrictEqual(30)
      }));
    });

    describe('initForm', () => {
      test('should return an initiate form 1', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            series: {
              tome: 3, name: 'series', status: {
                readCount: 0,
                possessedCount: 0, status: undefined, totalCount: 0
              }
            },
            title: 'title',
            editor: {name: 'editor'},
            contracts: [],
            status: 'READ',
            possessionStatus: 'SOLD',
            metadata: null,
            lent: false
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).value)
          .toStrictEqual({
            possessionStatus: 'SOLD',
            title: 'title',
            series: {tome: 3},
            status: 'READ'
          })
      });
      test('should return an initiate form 2', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            series: {
              tome: 3, name: 'series', status: {
                readCount: 0,
                possessedCount: 0,
                status: undefined, totalCount: 0
              }, oneShot: true
            },
            title: 'title',
            editor: {name: 'editor'},
            contracts: [],
            status: 'READ',
            possessionStatus: 'POSSESSED',
            metadata: null,
            lent: false
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).value)
          .toStrictEqual({
            possessionStatus: 'POSSESSED',
            title: 'title',
            status: 'READ'
          })
      });
    });
    describe('send', () => {
      test('should call the patch call on service', () => {
        jest.clearAllMocks();
        const book: Book = {
          contracts: null, status: undefined, title: "",
          isbn: 'test',
          series: null,
          editor: null,
          metadata: null,
          lent: false
        }
        const uiDatas = {
          form: null,
          item: book,
          start: new BehaviorSubject<boolean>(true),
          isSaved: new BehaviorSubject<boolean>(false)
        }
        component['send'](uiDatas, book)
        expect(component["subscriptions"].length).toStrictEqual(1)
        expect(bookAdministrationServiceMock.patch).toHaveBeenNthCalledWith(1, {
          isbn: 'test',
          contracts: null,
          series: {tome: null},
          status: undefined,
          title: "",
          metadata: null,
          lent: false
        }, false)
        uiDatas.start.subscribe(next => expect(next).toBeFalsy())
        uiDatas.isSaved.subscribe(next => expect(next).toBeTruthy())
      });
    });
    test('should return the searchCriteriaList', () => {
      expect(component[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'title', operation: ':', value: `*toto*`},
          {name: 'editor', operation: ':', value: `*toto*`, or: true},
          {name: 'series', operation: ':', value: `*toto*`, or: true}
        ]);
    });
  });
});
