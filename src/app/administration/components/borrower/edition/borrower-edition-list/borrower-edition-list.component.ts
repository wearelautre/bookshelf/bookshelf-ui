import {Component, OnDestroy, OnInit} from '@angular/core';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {MatPaginatorIntl} from '@angular/material/paginator';
import {TranslateService} from "@ngx-translate/core";
import {MyCustomPaginatorIntlService} from "../../../../../shared/services/my-custom-paginator-intl.service";
import {animate, state, style, transition, trigger} from "@angular/animations";

import {EditionList} from "../../../../shared/edition-list";
import {BorrowerAdministrationService} from "../../../../services/borrower-administration.service";
import {Borrower} from "../../../../../core/model/borrower";
import {LoanAdministrationService} from "../../../../services/loan-administration.service";
import {Loan} from "../../../../../core/model/loan";

type T = Borrower

@Component({
    selector: 'app-borrower-edition-list',
    templateUrl: './borrower-edition-list.component.html',
    styleUrls: ['../../../../administration-edition.scss'],
    providers: [{
        provide: MatPaginatorIntl,
        useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('BORROWERS', translate),
        deps: [TranslateService]
    }],
    styles: [`
      .mat-mdc-cell.cdk-column-loans {
        flex: none;
        width: 75%;
      }

      .mat-mdc-cell.cdk-column-name {
        flex: none;
        width: 25%;
      }

      .mat-mdc-cell.cdk-column-loansExpand {
        flex: none;
        width: 100%;
      }
    `],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ])
    ]
})
export class BorrowerEditionListComponent extends EditionList<T> implements OnInit, OnDestroy {
    public displayedColumns: string[] = ['name', 'loans', 'action'];

    expandedElement: number | null = null;

    constructor(
        adminService: BorrowerAdministrationService,
        paginationService: PaginationService,
        private fb: UntypedFormBuilder,
        route: ActivatedRoute,
        private loanAdminService: LoanAdministrationService
    ) {
        super(adminService, paginationService, route);
    }

    ngOnInit(): void {
        super.onInit()
    }

    ngOnDestroy(): void {
        super.onDestroy()
    }

    getId(index: number, item: Loan) {
        return item.id;
    }

    protected initForm(item: T): UntypedFormGroup {
        return this.fb.group({
            name: this.fb.control(item.name)
        });
    }

    expand(expandedElement: number, index) {
        const borrower = this.uiDatas[index].item
        if (expandedElement === index) {
            this.expandedElement = null
        } else {
            this.loanAdminService.getAll(borrower.id)
                .subscribe(loans => {
                    this.uiDatas[index].item.loans = loans;
                    this.expandedElement = index
                })
        }
    }

    getLoans(index): Loan[] {
        return this.uiDatas[index].item.loans;
    }

    returnLoan(index, i: number): void {
        const borrower = this.uiDatas[index].item
        const book = borrower.loans[i].book
        this.loanAdminService
            .update({...borrower.loans[i], returnDate: new Date(), book: null, bookIsbn: book.isbn}, false, borrower.id)
            .subscribe(loan => borrower.loans[i] = loan)
    }

    deleteLoan(index: number, i: number) {
        const borrower = this.uiDatas[index].item
        const book = borrower.loans[i].book
        this.loanAdminService
            .delete({...borrower.loans[i], book: null, bookIsbn: book.isbn}, borrower.id)
            .subscribe(_ => {
                borrower.loans.splice(i, 1)
                borrower.currentLoansCount = borrower.loans.length
            });
    }
}
