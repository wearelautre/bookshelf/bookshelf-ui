import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WebLinksFormComponent} from './web-links-form.component';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('WebLinksFormComponent', () => {
    let component: WebLinksFormComponent;
    let fixture: ComponentFixture<WebLinksFormComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WebLinksFormComponent],
            imports: [
                NoopAnimationsModule,
                MatSelectModule,
                MatInputModule,
                MatFormFieldModule,
                ReactiveFormsModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WebLinksFormComponent);
        component = fixture.componentInstance;
        component.form = new FormGroup({type: new FormControl(), value: new FormControl()});
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
            jest.clearAllMocks();
        });
    });
});
