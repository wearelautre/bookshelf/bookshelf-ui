import {Component, Input} from '@angular/core';
import {WebLink} from '../../../../../core/model/web-link';
import {UntypedFormGroup} from '@angular/forms';
import TypeData = WebLink.TypeData;

@Component({
    selector: 'app-web-links-form',
    template: `
        <div class="web-links-form-wrapper" [formGroup]="form">
            <mat-form-field appearance="outline" class="editable-field mff-space-1" subscriptSizing="dynamic">
                <mat-select formControlName="type">
                    <mat-option [value]="type.type" *ngFor="let type of types"> {{type.label}} </mat-option>
                </mat-select>
            </mat-form-field>
            <mat-form-field appearance="outline" class="editable-field mff-space-1" subscriptSizing="dynamic">
                <input matInput formControlName="value">
            </mat-form-field>
        </div>
    `,
    styles: [`
      .web-links-form-wrapper {
        display: flex;
        flex-direction: row;
        gap: 1rem;
      }
    `]
})
export class WebLinksFormComponent {

    @Input() public types: TypeData[] = []
    @Input() public form: UntypedFormGroup;

}
