import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ArtistEditionListComponent} from './artist-edition-list.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {
    NgxTranslateTestingModule
} from "../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatFormFieldModule} from '@angular/material/form-field';
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {MockWebLinkListComponent} from "../../../../../shared/web-link-list/__mocks__/web-link-list.component";
import {MockProgressbarComponent} from "../../../../shared/progressbar/__mocks__/progressbar.component";
import {MockCreateWebLinksComponent} from "../create-web-links/__mocks__/create-web-links.component";
import {MockWebLinksFormComponent} from "../web-links-form/__mocks__/web-links-form.component";
import {ArtistAdministrationService} from "../../../../services/artist-administration.service";
import {artistAdministrationServiceMock} from "../../../../services/__mocks__/artist-administration.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {BehaviorSubject} from "rxjs";
import {Artist} from "../../../../../core/model/artist";
import {WebLink} from "../../../../../core/model/web-link";
import {RouterTestingModule} from "@angular/router/testing";
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../../../shared/services/__mocks__/pagination.service";

describe('ArtistEditionListComponent', () => {
    let component: ArtistEditionListComponent;
    let fixture: ComponentFixture<ArtistEditionListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                ArtistEditionListComponent,
                MockWebLinkListComponent,
                MockProgressbarComponent,
                MockCreateWebLinksComponent,
                MockWebLinksFormComponent
            ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                NgxTranslateTestingModule,
                FontAwesomeTestingModule,
                MatPaginatorModule,
                MatTableModule,
                ReactiveFormsModule,
                MatFormFieldModule
            ],
            providers: [
                {provide: PaginationService, useValue: paginationServiceMock},
                {provide: ArtistAdministrationService, useValue: artistAdministrationServiceMock}
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ArtistEditionListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
            jest.clearAllMocks();
        });
    });

    describe('Typescript test', () => {
        describe('OnDestroy', () => {
            test('should unsub all subscription', waitForAsync(() => {
                jest.clearAllMocks();
                const obs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
                let value = 0;
                component[`subscriptions`].push(obs.subscribe(v => value = v))
                obs.next(30);
                expect(value).toStrictEqual(30)
                component[`ngOnDestroy`]();
                obs.next(40);
                expect(value).toStrictEqual(30)
            }));
        });

        describe('getWebLinksFormArray', () => {
            test('should return the web links for index', waitForAsync(() => {
                jest.clearAllMocks();
                const item: Artist = {name: 'toto', webLinks: [{type: 'TWITTER', value: 'val'}]}
                const form: FormGroup = new FormGroup({
                    name: new FormControl(item.name),
                    webLinks: new FormControl(item.webLinks.map(webLink => new FormGroup({
                        type: new FormControl(webLink.type, Validators.required),
                        value: new FormControl(webLink.value, Validators.required),
                        id: new FormControl(null)
                    })))
                });
                component.uiDatas = [{form, start: new BehaviorSubject(false), item, isSaved: new BehaviorSubject(false)}]
                expect(component.getWebLinksFormArray(0).value.length).toStrictEqual(1)
                expect(component.getWebLinksFormArray(0).value[0].value).toStrictEqual({
                    id: null,
                    type: "TWITTER",
                    value: "val"
                })
            }));
        });

        describe('initForm', () => {
            test('should return the artist form no web links', waitForAsync(() => {
                jest.clearAllMocks();
                const item: Artist = {name: 'toto', webLinks: []}
                const form: FormGroup = new FormGroup({
                    name: new FormControl(item.name),
                    webLinks: new FormArray([])
                });
                expect(component[`initForm`](item).value).toStrictEqual(form.value)
            }));
            test('should return the artist form with web links', waitForAsync(() => {
                jest.clearAllMocks();
                const webLink: WebLink = {id: null, type: 'TWITTER', value: 'val'};
                const item: Artist = {name: 'toto', webLinks: [webLink]}
                const form: FormGroup = new FormGroup({
                    name: new FormControl(item.name),
                    webLinks: new FormArray([
                        new FormGroup({
                            type: new FormControl(webLink.type, Validators.required),
                            value: new FormControl(webLink.value, Validators.required),
                            id: new FormControl(null)
                        })
                    ])
                });
                expect(component[`initForm`](item).value).toStrictEqual(form.value)
            }));
        });

        describe('createWeLinkForm', () => {
            test('should return a new web link form', waitForAsync(() => {
                jest.clearAllMocks();
                const webLink: WebLink = {type: 'TWITTER', value: 'val'};
                expect(component.createWeLinkForm(webLink).value).toStrictEqual(new FormGroup({
                    type: new FormControl(webLink.type, Validators.required),
                    value: new FormControl(webLink.value, Validators.required),
                    id: new FormControl(null)
                }).value)
            }));
        });

        describe('addWebLink', () => {
            test('should add a new web link', waitForAsync(() => {
                jest.clearAllMocks();
                const webLink: WebLink = {type: 'TWITTER', value: 'val'};
                const spyCreateWeLinkForm = jest.spyOn<any, any>(component, `createWeLinkForm`).mockImplementation(() => ({}))
                const spyGetWebLinksFormArray = jest.spyOn<any, any>(component, `getWebLinksFormArray`).mockImplementation(() => [])

                component.addWebLink(webLink, 23)
                expect(spyCreateWeLinkForm).toHaveBeenNthCalledWith(1, webLink)
                expect(spyGetWebLinksFormArray).toHaveBeenNthCalledWith(1, 23)
            }));
        });

        describe('deleteWebLink', () => {
            test('should remove a web link', waitForAsync(() => {
                jest.clearAllMocks();
                const formArray = new FormArray([])
                const spyGetWebLinksFormArray = jest.spyOn<any, any>(component, `getWebLinksFormArray`).mockImplementation(() => formArray)
                const spyArray = jest.spyOn<any, any>(formArray, `removeAt`).mockImplementation(() => ({}))

                component.deleteWebLink(23, 2)
                expect(spyGetWebLinksFormArray).toHaveBeenNthCalledWith(1, 23)
                expect(spyArray).toHaveBeenNthCalledWith(1, 2)
            }));
        });
    });
});
