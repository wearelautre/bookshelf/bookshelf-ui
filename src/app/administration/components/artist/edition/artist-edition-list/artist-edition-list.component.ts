import {Component, OnDestroy, OnInit} from '@angular/core';
import {EditionList} from '../../../../shared/edition-list';
import {UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Artist} from '../../../../../core/model/artist';
import {ArtistAdministrationService} from '../../../../services/artist-administration.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {WebLink} from '../../../../../core/model/web-link';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {TranslateService} from '@ngx-translate/core';
import {MyCustomPaginatorIntlService} from "../../../../../shared/services/my-custom-paginator-intl.service";
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {ActivatedRoute} from "@angular/router";
import TypeData = WebLink.TypeData;

type T = Artist

@Component({
    selector: 'app-artist-edition-list',
    templateUrl: './artist-edition-list.component.html',
    styleUrls: ['../../../../administration-edition.scss'],
    providers: [
        {
            provide: MatPaginatorIntl,
            useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('ARTISTS', translate),
            deps: [TranslateService]
        }
    ],
    styles: [`
      .mat-mdc-cell.cdk-column-webLinks {
        flex: none;
        width: 75%;
      }

      .artist-weblinks-list-element {
        display: flex;
        flex-direction: row;
        gap: 1rem;
        align-items: center;
      }

      .mat-mdc-cell.cdk-column-webLinksExpand {
        flex: none;
        width: 100%;
      }

      .mat-mdc-cell.cdk-column-action {
        flex: none;
        width: 5%;
      }
    `],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class ArtistEditionListComponent extends EditionList<T> implements OnInit, OnDestroy {
    public displayedColumns: string[] = ['name', 'webLinks', 'action'];

    expandedElement: number | null;
    types: TypeData[] = WebLink.typeDatas();

    constructor(
        adminService: ArtistAdministrationService,
        paginationService: PaginationService,
        private fb: UntypedFormBuilder,
        route: ActivatedRoute
    ) {
        super(adminService, paginationService, route);
    }

    ngOnInit(): void {
        super.onInit()
    }

    ngOnDestroy(): void {
        super.onDestroy()
    }

    protected initForm(item: T): UntypedFormGroup {
        return this.fb.group({
            name: this.fb.control(item.name),
            webLinks: this.fb.array(item.webLinks.map(webLink => this.createWeLinkForm(webLink)))
        });
    }

    getWebLinksFormArray(index: number): UntypedFormArray {
        return this.uiDatas[index].form.controls.webLinks as UntypedFormArray;
    }

    addWebLink(webLink: WebLink, index: number) {
        this.getWebLinksFormArray(index).push(this.createWeLinkForm(webLink))
    }

    createWeLinkForm(webLink: WebLink): UntypedFormGroup {
        return this.fb.group({
            type: this.fb.control(webLink.type, Validators.required),
            value: this.fb.control(webLink.value, Validators.required),
            id: this.fb.control(null)
        })
    }

    deleteWebLink(index: number, i: number) {
        this.getWebLinksFormArray(index).removeAt(i);
    }
}
