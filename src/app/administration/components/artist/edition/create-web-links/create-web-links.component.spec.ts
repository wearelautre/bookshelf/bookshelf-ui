import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CreateWebLinksComponent} from './create-web-links.component';
import {MockWebLinksFormComponent} from "../web-links-form/__mocks__/web-links-form.component";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {ReactiveFormsModule} from "@angular/forms";

describe('CreateWebLinksComponent', () => {
  let component: CreateWebLinksComponent;
  let fixture: ComponentFixture<CreateWebLinksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateWebLinksComponent, MockWebLinksFormComponent ],
      imports: [
        FontAwesomeTestingModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWebLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
      jest.clearAllMocks();
    });
  });

  describe('Typescript test', () => {
    describe('submit', () => {
      test('should emit and reset the form', waitForAsync(() => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.create, `emit`).mockImplementation(() => ({}));
        component.form.patchValue({type: 'type', value: 'value', id: 'id'})
        component.submit();
        expect(spy).toHaveBeenNthCalledWith(1, {type: 'type', value: 'value', id: 'id'})
        expect(component.form.value).toStrictEqual({type: null, value: null, id: null})
      }));
    });
    describe('isFormValid', () => {
      test('should be valid', waitForAsync(() => {
        jest.clearAllMocks();
        component.form.patchValue({type: 'type', value: 'value', id: 'id'})
        component.form.markAsDirty()
        expect(component.isFormValid()).toBeTruthy()
      }));
      test('should be not valid', waitForAsync(() => {
        jest.clearAllMocks();
        expect(component.isFormValid()).toBeFalsy()
      }));
    });
  });

});
