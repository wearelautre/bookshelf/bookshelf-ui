import {Component, EventEmitter, Input, Output} from '@angular/core';
import {WebLink} from '../../../../../core/model/web-link';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import TypeData = WebLink.TypeData;

@Component({
    selector: 'app-create-web-links',
    template: `
        <div class="artist-weblinks-form-creation-wrapper">
            <app-web-links-form [form]="form" [types]="types"></app-web-links-form>
            <button mat-button (click)="submit()" [disabled]="!isFormValid()" color="primary">
                <fa-icon [icon]="['fas', 'check-circle']"></fa-icon>
            </button>
        </div>
    `,
    styles: [`
      div.artist-weblinks-form-creation-wrapper {
        display: flex;
        flex-direction: row;
        gap: 1rem;
        align-items: center;

      }
    `]
})
export class CreateWebLinksComponent {

    @Input() public types: TypeData[] = []
    @Output() public create: EventEmitter<WebLink> = new EventEmitter<WebLink>()
    form: UntypedFormGroup = this.fb.group({
        type: this.fb.control(null, Validators.required),
        value: this.fb.control(null, Validators.required),
        id: null
    });

    constructor(
        private fb: UntypedFormBuilder
    ) {
    }

    submit() {
        this.create.emit(this.form.value)
        this.form.reset({type: null, value: null, id: null})
    }

    isFormValid() {
        return this.form.valid && !this.form.pristine
    }
}
