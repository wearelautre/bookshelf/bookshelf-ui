import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-create-web-links',
  template: `<div>Mock create web links</div>`,
})
export class MockCreateWebLinksComponent {

  @Input() public types: any[] = []
  @Output() public create: any;

}
