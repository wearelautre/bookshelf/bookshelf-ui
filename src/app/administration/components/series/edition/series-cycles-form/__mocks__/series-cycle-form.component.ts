import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-series-cycle-form',
  template: `<div>Mock series cycle form</div>`
})
export class MockSeriesCycleFormComponent {

  @Input() public types: any[] = []
  @Input() public form: any;

}
