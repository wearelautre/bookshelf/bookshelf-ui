import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SeriesCyclesFormComponent} from './series-cycles-form.component';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('SeriesCyclesFormComponent', () => {
    let component: SeriesCyclesFormComponent;
    let fixture: ComponentFixture<SeriesCyclesFormComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SeriesCyclesFormComponent],
            imports: [
                NoopAnimationsModule,
                MatSelectModule,
                MatInputModule,
                MatFormFieldModule,
                ReactiveFormsModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SeriesCyclesFormComponent);
        component = fixture.componentInstance;
        component.form = new FormGroup({name: new FormControl(), id: new FormControl()});
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
            jest.clearAllMocks();
        });
    });
});
