import {Component, Input} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';

@Component({
    selector: 'app-series-cycle-form',
    template: `
        <div [formGroup]="form">
            <mat-form-field appearance="outline" class="editable-field mff-space-1" subscriptSizing="dynamic">
                <input matInput formControlName="name">
            </mat-form-field>
        </div>
    `
})
export class SeriesCyclesFormComponent {
    @Input() public form: UntypedFormGroup;
}
