import {Component, OnDestroy, OnInit} from '@angular/core';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {Series, SeriesCycle} from '../../../../../core/model/series';
import {UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {EditionList} from '../../../../shared/edition-list';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {MyCustomPaginatorIntlService} from "../../../../../shared/services/my-custom-paginator-intl.service";
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {ActivatedRoute} from "@angular/router";
import {SearchCriteria} from "../../../../../shared/services/entity.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {SeriesForm} from "../../../../models/form/series.from.model";

type T = Series;

@Component({
  selector: 'app-series-edition-list',
  templateUrl: './series-edition-list.component.html',
  styleUrls: ['../../../../administration-edition.scss'],
  styles: [`
    .mat-mdc-cell.cdk-column-editor {
      flex: none;
      width: 25%;
    }

    .series-cycle-list-element {
      display: flex;
      flex-direction: row;
      gap: 1rem;
      align-items: center;
    }

    .mat-mdc-cell.cdk-column-action {
      flex: none;
      width: 5%;
    }
  `],
  providers: [{
    provide: MatPaginatorIntl,
    useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('SERIES', translate),
    deps: [TranslateService]
  }],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class SeriesEditionListComponent extends EditionList<T> implements OnInit, OnDestroy {
  public displayedColumns: string[] = ['editor', 'bookType', 'name', 'totalCount', 'status', 'cycles', 'oneShot', 'action'];

  expandedElement: number | null;


  get seriesStatusEnum(): Series.SeriesStatusEnum[] {
    return Series.SeriesPublicationStatusEnum.values()
  }


  constructor(
    adminService: SeriesAdministrationService,
    paginationService: PaginationService,
    private fb: UntypedFormBuilder,
    route: ActivatedRoute
  ) {
    super(adminService, paginationService, route);
  }

  ngOnInit(): void {
    super.onInit()
  }

  ngOnDestroy(): void {
    super.onDestroy()
  }

  isRemovable(index: number): boolean {
    return +this.uiDatas[index].item.status.possessedCount === 0;
  }

  onFinish(index: number): void {
    const uiData = this.uiDatas[index]
    const series = {
      ...uiData.item, ...uiData.form.value,
      status: {status: uiData.form.value.status, totalCount: uiData.form.value.totalCount}
    }
    series.editor = uiData.form.value.editor.name;
    this.send(uiData, series)
  }

  protected initForm(item: T): UntypedFormGroup {
    const form = this.fb.group({
      ...new SeriesForm(item),
      editor: this.fb.group({
        id: null,
        name: item.editor,
      }),
    });
    if (item.oneShot) {
      form.controls['totalCount'].disable();
    }
    return form;
  }

  switchToOneShot(event: MatSlideToggleChange, index: number) {
    this.uiDatas[index].item.oneShot = event.checked
    if (this.uiDatas[index].item.oneShot) {
      this.uiDatas[index].form.controls['name'].disable()
    } else {
      this.uiDatas[index].form.controls['name'].enable()
    }
  }

  getCyclesFormArray(index: number): UntypedFormArray {
    return this.uiDatas[index].form.controls.cycles as UntypedFormArray;
  }

  addSeriesCycle(seriesCycle: SeriesCycle, index: number) {
    this.getCyclesFormArray(index).push(this.createSeriesCycleForm(seriesCycle))
  }

  createSeriesCycleForm(seriesCycle: SeriesCycle): UntypedFormGroup {
    return this.fb.group({
      name: this.fb.control(seriesCycle.name, Validators.required),
      id: this.fb.control(null)
    })
  }

  getBookTypeFormGroup(index: number): UntypedFormGroup {
    return this.uiDatas[index].form.controls.bookType as UntypedFormGroup
  }

  getEditorGroup(index: number): UntypedFormGroup {
    return this.uiDatas[index].form.controls.editor as UntypedFormGroup
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [{name: 'name', operation: ':', value: `*${search}*`}]
  }

  deleteCycle(index: number, i: number) {
    this.getCyclesFormArray(index).removeAt(i);
  }
}
