import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SeriesEditionListComponent} from './series-edition-list.component';
import {seriesAdministrationServiceMock} from '../../../../services/__mocks__/series-administration.service';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MockProgressbarComponent} from '../../../../shared/progressbar/__mocks__/progressbar.component';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatFormFieldModule} from '@angular/material/form-field';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BehaviorSubject} from 'rxjs';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../../../shared/services/__mocks__/pagination.service";
import {RouterTestingModule} from "@angular/router/testing";
import {MockEditorFormComponent} from "../../../book/shared/editor-form/__mocks__/editor-form.component";
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MockBookTypeFormComponent} from "../../../book/shared/book-type-form/__mocks__/book-type-form.component";
import {MockListFilterComponent} from "../../../../../shared/component/list-filter/__mocks__/list-filter.component";
import {
  MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";
import {Series} from "../../../../../core/model/series";

describe('SeriesEditionListComponent', () => {
  let component: SeriesEditionListComponent;
  let fixture: ComponentFixture<SeriesEditionListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesEditionListComponent, MockProgressbarComponent, MockBookTypeFormComponent, MockEditorFormComponent, MockListFilterComponent, MockAutoCompleteInputComponent],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatTableModule,
        MatSlideToggleModule
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {provide: SeriesAdministrationService, useValue: seriesAdministrationServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesEditionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('OnDestroy', () => {
      test('should unsub all subscription', waitForAsync(() => {
        jest.clearAllMocks();
        const obs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
        let value = 0;
        component[`subscriptions`].push(obs.subscribe(v => value = v))
        obs.next(30);
        expect(value).toStrictEqual(30)
        component[`ngOnDestroy`]();
        obs.next(40);
        expect(value).toStrictEqual(30)
      }));
    });
    describe('isRemovable', () => {
      test('should return true', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component.isRemovable(0)).toBeTruthy()
      });
      test('should return false', () => {
        jest.clearAllMocks();
        component.uiDatas = [];
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              possessedCount: 30,
              readCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component.isRemovable(0)).toBeFalsy()
      });
    });
    describe('switchToOneShot', () => {
      test('should enable name control', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: new FormGroup({name: new FormControl()}), isSaved: undefined, start: undefined,
          item: {
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        component.switchToOneShot({checked: false, source: null}, 0)
        expect(component.uiDatas[0].form.controls['name'].enabled).toBeTruthy()
      });
      test('should disable name control', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: new FormGroup({name: new FormControl()}), isSaved: undefined, start: undefined,
          item: {
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        component.switchToOneShot({checked: true, source: null}, 0)
        expect(component.uiDatas[0].form.controls['name'].enabled).toBeFalsy()
      });
    });
    describe('onFinish', () => {
      test('should call send form the extended class', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn<any, any>(component, `send`).mockImplementation()
        component.uiDatas.push({
          form: new FormGroup({
            name: new FormControl(),
            editor: new FormGroup({name: new FormControl('editor')})
          }),
          isSaved: undefined, start: undefined,
          item: {
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        component.onFinish(0);
        expect(spy).toHaveBeenNthCalledWith(1, component.uiDatas[0], {
          name: null,
          editor: 'editor',
          status: {
            totalCount: undefined,
            status: undefined
          },
        })
      });
    });
    describe('initForm', () => {
      test('should return an initiate form is not one shot', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: 'editor',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              possessedCount: 0,
              readCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).value)
          .toStrictEqual({
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            status: "UNKNOWN",
            totalCount: 0,
            cycles: []
          })
      });
      test('should return an initiate form is one shot', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: null,
            name: null,
            bookType: {name: 'bookType', id: 89},
            editor: 'editor',
            oneShot: true,
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            id: null,
            name: null,
            bookType: {name: 'bookType', id: 89},
            editor: {
              id: null,
              name: 'editor'
            },
            cycles: [],
            status: "UNKNOWN",
            totalCount: 0,
          })
      });
      test('should return an initiate form with bookType undifined', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: null,
            name: null,
            bookType: undefined,
            editor: 'editor',
            oneShot: true,
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            cycles: [],
            status: "UNKNOWN",
            totalCount: 0,
          })
      });
      test('should return an initiate form with bookType name and id null', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: 'editor',
            oneShot: true,
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              possessedCount: 0,
              readCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            cycles: [],
            status: "UNKNOWN",
            totalCount: 0,
          })
      });
      test('should return an initiate form with bookType name and id null with cycles', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: 'editor',
            oneShot: true,
            cycles: [
              {name: 'cycle 1', id: 1},
              {name: 'cycle 2', id: 2}
            ],
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              possessedCount: 0,
              readCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            id: null,
            name: null,
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            cycles: [
              {name: 'cycle 1', id: 1},
              {name: 'cycle 2', id: 2}
            ],
            status: "UNKNOWN",
            totalCount: 0,
          })
      });
    });
    describe('getBookTypeControl', () => {
      test('should return the bookType FromControl', () => {
        component.uiDatas.push({
          form: new FormGroup({
            bookType: new FormGroup({
              name: new FormControl('best bookType'),
              id: new FormControl(89)
            })
          }),
          isSaved: undefined, start: undefined,
          item: {
            bookType: {name: 'best bookType', id: 89},
            editor: 'editor',
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              readCount: 0,
              possessedCount: 0,
              totalCount: 0
            }
          }
        })
        expect(component.getBookTypeFormGroup(0).value)
          .toStrictEqual({name: 'best bookType', id: 89})
      });
    });
    describe('getEditorGroup', () => {
      test('should return the editor FromControl', () => {
        component.uiDatas.push({
          form: new FormGroup({
            editor: new FormGroup({
              name: new FormControl('editorName')
            })
          }),
          isSaved: undefined, start: undefined,
          item: {
            bookType: {name: 'bookType', id: 89},
            editor: 'editor',
            name: 'series',
            status: {
              status: Series.SeriesStatusEnum.UNKNOWN,
              possessedCount: 0,
              totalCount: 0,
              readCount: 0,
            }
          }
        })
        expect(component.getEditorGroup(0).value)
          .toStrictEqual({name: 'editorName'})
      });
    });
    test('should return the searchCriteriaList', () => {
      expect(component[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'name', operation: ':', value: `*toto*`}
        ]);
    });
  });

  describe('deleteCycle', () => {
    test('should remove a web link', waitForAsync(() => {
      jest.clearAllMocks();
      const formArray = new FormArray([])
      const spyGetCyclesFormArray = jest.spyOn<any, any>(component, `getCyclesFormArray`).mockImplementation(() => formArray)
      const spyArray = jest.spyOn<any, any>(formArray, `removeAt`).mockImplementation(() => ({}))

      component.deleteCycle(23, 2)
      expect(spyGetCyclesFormArray).toHaveBeenNthCalledWith(1, 23)
      expect(spyArray).toHaveBeenNthCalledWith(1, 2)
    }));
  });

  describe('getCyclesFormArray', () => {
    test('should get an empty array', waitForAsync(() => {
      jest.clearAllMocks();

      component.uiDatas.push({
        form: new FormGroup({
          cycles: new FormArray([])
        }),
        isSaved: undefined, start: undefined,
        item: null
      });

      expect(component.getCyclesFormArray(0).value).toStrictEqual([])
    }));
    test('should get a non empty array', waitForAsync(() => {
      jest.clearAllMocks();
      component.uiDatas.push({
        form: new FormGroup({
          cycles: new FormArray([
            new FormGroup({name: new FormControl('cycle 1'), id: new FormControl(1)}),
          ])
        }),
        isSaved: undefined, start: undefined,
        item: null
      });

      expect(component.getCyclesFormArray(0).getRawValue()).toStrictEqual([{
        name: 'cycle 1',
        id: 1
      }])
    }));
  });
});
