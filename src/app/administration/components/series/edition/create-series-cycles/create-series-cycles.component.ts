import {Component, EventEmitter, Output} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {SeriesCycle} from "../../../../../core/model/series";

@Component({
    selector: 'app-create-series-cycles',
    template: `
        <div class="series-cycle-form-creation-wrapper">
            <app-series-cycle-form [form]="form"></app-series-cycle-form>
            <button mat-button (click)="submit()" [disabled]="!isFormValid()" color="primary">
                <fa-icon [icon]="['fas', 'check-circle']"></fa-icon>
            </button>
        </div>
    `,
    styles: [`
      div.series-cycle-form-creation-wrapper {
        display: flex;
        flex-direction: row;
        gap: 1rem;
        align-items: center;
      }
    `]
})
export class CreateSeriesCyclesComponent {

    @Output() public create: EventEmitter<SeriesCycle> = new EventEmitter<SeriesCycle>()
    form: UntypedFormGroup = this.fb.group({
        name: this.fb.control(null, Validators.required),
        id: null
    });

    constructor(
        private fb: UntypedFormBuilder
    ) {
    }

    submit() {
        this.create.emit(this.form.value)
        this.form.reset({name: null, id: null})
    }

    isFormValid() {
        return this.form.valid && !this.form.pristine
    }
}
