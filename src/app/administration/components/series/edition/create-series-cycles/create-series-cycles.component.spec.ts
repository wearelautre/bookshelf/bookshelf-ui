import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateSeriesCyclesComponent} from './create-series-cycles.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MockSeriesCycleFormComponent} from "../series-cycles-form/__mocks__/series-cycle-form.component";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";

describe('CreateSeriesCyclesComponent', () => {
  let component: CreateSeriesCyclesComponent;
  let fixture: ComponentFixture<CreateSeriesCyclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSeriesCyclesComponent, MockSeriesCycleFormComponent ],
      imports: [
        ReactiveFormsModule,
        FontAwesomeTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSeriesCyclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
