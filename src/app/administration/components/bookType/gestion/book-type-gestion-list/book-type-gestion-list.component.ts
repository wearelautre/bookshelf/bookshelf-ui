import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {BookType} from '../../../../../core/model/book-type';

@Component({
    selector: 'app-book-type-gestion-list',
    template: `
        <div class="list">
            <app-book-type-edition-list-display
                    *ngFor="let bookType of (bookTypeList$|async)"
                    [bookType]=bookType>
            </app-book-type-edition-list-display>
            <app-book-type-creation-list-display></app-book-type-creation-list-display>
        </div>
    `,
    styles: [`
      .list {
        display: flex;
        flex-direction: column;

      }
    `]
})
export class BookTypeGestionListComponent {

    bookTypeList$: Observable<BookType[]> = this.bookTypeAdministrationService.list$;

    constructor(
        private bookTypeAdministrationService: BookTypeAdministrationService
    ) {
    }
}
