import {Component, OnInit} from '@angular/core';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {Observable} from 'rxjs';
import {BookType} from '../../../../../core/model/book-type';

@Component({
  selector: 'app-book-type-gestion',
  template: `
    <div fxLayout="column" class="content">
      <div fxLayout="row" fxLayoutAlign="center">
        <h1 style="text-align: center">{{'BOOK_TYPE.GESTION.TITLE' | translate}}</h1>
      </div>
      <div fxLayout="row" fxLayoutAlign="space-around">
        <div fxFlex="75">
          <app-book-type-gestion-list style="padding-top: 20px"
                                      fxLayout="row"
                                      *ngIf="(list$ | async)">
          </app-book-type-gestion-list>
        </div>
      </div>
    </div>
  `
})
export class BookTypeGestionComponent implements OnInit {

  hasResult = false;
  list$: Observable<BookType[]> = this.bookTypeService.list$;

  constructor(private bookTypeService: BookTypeAdministrationService) {
  }

  ngOnInit() {
    this.bookTypeService.getAll().subscribe();
  }
}
