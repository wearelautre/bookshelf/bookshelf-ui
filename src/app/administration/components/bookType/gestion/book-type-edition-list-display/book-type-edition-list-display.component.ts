import {Component, Input, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {BookType} from '../../../../../core/model/book-type';
import {HasTimedProgressBar} from '../../../../../shared/has-timed-progress-bar';
import {switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-book-type-edition-list-display',
    template: `
        <form class="list-line" [formGroup]="form">
            <div [ngClass]="{'edition-line': true, 'saved': (isSaved$ | async)}">
                <mat-form-field [matBadge]="getNbBooks()" class="mff-space-1" fxFlex="100" matBadgePosition="before"
                                matBadgeOverlap="false"
                                subscriptSizing="dynamic"
                                appearance="outline">
                    <label>
                        <input matInput formControlName="name">
                    </label>
                </mat-form-field>

                <div class="action-column-edition">
                    <div class="action-column-edition-buttons">
                        <button type="button" [disabled]="!isRemovable()" mat-button color="warn" (click)="delete()">
                            <fa-icon [icon]="['fas', 'trash']"></fa-icon>
                        </button>
                    </div>
                    <mat-progress-bar *ngIf="progressBarState.display"
                                      [value]="getProgressBarValue(remainingSeconds|async)"
                                      [mode]="progressBarState.type"></mat-progress-bar>
                </div>
            </div>
        </form>
    `,
    styles: [`
      .list-line {
        display: flex;
        flex-direction: row;
        justify-content: center;
        gap: 1rem;

        .action-column-edition {
          display: flex;
          justify-content: center;
          flex-direction: column;
        }

        .action-column-edition-buttons {
          display: flex;
          justify-content: end;
          flex-direction: row;
        }
      }
    `]
})
export class BookTypeEditionListDisplayComponent extends HasTimedProgressBar implements OnInit {

    @Input()
    bookType: BookType = {nbSeries: 0, id: 0, name: ''};
    form: UntypedFormGroup = this.fb.group({
        name: this.fb.control(''),
    });

    constructor(
        private fb: UntypedFormBuilder,
        private adminService: BookTypeAdministrationService
    ) {
        super();
    }

    ngOnInit() {
        this.init();
    }

    initForm(): void {
        this.form.setValue({
            name: this.bookType.name
        });
    }

    submit() {
        this.progressBarState = {display: true, type: 'indeterminate'};
        const bookType = this.form.value;
        bookType.id = this.bookType.id;
        this.adminService.update(bookType).subscribe(value => {
            this.bookType = value;
            this.adminService.getAll().subscribe();
            this.updateIsSaved();
            this.hideProgressBar();
        });
    }

    delete() {
        this.adminService.delete(this.bookType).pipe(switchMap(() => this.adminService.getAll())).subscribe();
    }

    // Workaround to fix a bug on nbBook set to 0 on input
    getNbBooks(): string {
        return (this.bookType.nbSeries || 0).toString();
    }

    isRemovable(): boolean {
        return this.bookType.nbSeries === 0;
    }
}
