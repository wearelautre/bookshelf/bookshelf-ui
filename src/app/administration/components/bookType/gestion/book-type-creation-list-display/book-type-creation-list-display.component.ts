import {Component} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';

@Component({
    selector: 'app-book-type-creation-list-display',
    template: `
        <form class="list-line" [formGroup]="form" (ngSubmit)="submit()">
            <div>
                <mat-form-field fxFlex="100" appearance="outline" subscriptSizing="dynamic">
                    <label>
                        <input [placeholder]="'BOOK_TYPE.CREATION.PLACEHOLDER_NAME' | translate" matInput
                               formControlName="name">
                    </label>
                </mat-form-field>

                <div class="action-column-edition">
                    <div class="action-column-edition-buttons">
                        <button type="submit" [disabled]="false" mat-button color="primary">
                            <fa-icon [icon]="['fas', 'check']"></fa-icon>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    `,
    styles: [`
      .list-line {
        display: flex;
        flex-direction: row;
        justify-content: center;
        gap: 1rem;

        .action-column-edition {
          display: flex;
          justify-content: center;
          flex-direction: column;
        }

        .action-column-edition-buttons {
          display: flex;
          justify-content: end;
          flex-direction: row;
        }
      }
    `]
})
export class BookTypeCreationListDisplayComponent {

    form: UntypedFormGroup = this.fb.group({
        name: this.fb.control(''),
    });

    constructor(
        private fb: UntypedFormBuilder,
        private bookTypeAdministrationService: BookTypeAdministrationService
    ) {
    }

    submit() {
        this.bookTypeAdministrationService.create(this.form.value).subscribe();
        this.form.reset();
    }
}
