import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {TaskService} from "../../../services/task.service";
import {Task} from "../../../models/task";

@Component({
    selector: 'app-task-menu',
    template: `
        <div fxLayout="row">
            <div fxFlex="100" fxLayout="column" *ngIf="(collapsed | async) === false">
                <div fxLayout="row" fxLayoutAlign="center">
                    <h2> {{'ADMINISTRATION.TASK.MENU.TITLE' | translate}} </h2>
                </div>
                <div fxLayout="column" style="width: 100%" fxLayoutGap="1rem">
                    <div fxFlex *ngIf="(taskList$ | async)?.length === 0" fxLayoutAlign="center">
                        <span>{{'ADMINISTRATION.TASK.MENU.NO_TASK' | translate}}</span>
                    </div>
                    <mat-card *ngFor="let task of taskList$ | async">
            <span class="mat-h3">
              {{task.extra}}
            </span>
                        <mat-card-actions align="end">
                            <button mat-button
                                    [routerLink]="['/', 'administration', 'book', 'add', task.extra]"
                                    [queryParams]="{withMenu: true}">
                                <fa-icon [icon]="['fas', 'search']"></fa-icon>
                            </button>
                            <button mat-button color="warn" (click)="delete(task)">
                                <fa-icon [icon]="['fas', 'trash']"></fa-icon>
                            </button>
                        </mat-card-actions>
                    </mat-card>
                </div>
            </div>
            <button mat-icon-button
                    *ngIf="collapsed | async"
                    [routerLink]="currentRoute | async"
                    [queryParams]="{withMenu: true}">
                <fa-icon [icon]="['fas', 'bars']"></fa-icon>
            </button>
            <button mat-icon-button
                    *ngIf="(collapsed | async) === false"
                    [routerLink]="currentRoute | async"
                    [queryParams]="{}">
                <fa-icon [icon]="['fas', 'times']"></fa-icon>
            </button>
        </div>`
})
export class TaskMenuComponent implements OnInit {

    collapsed: Observable<boolean> = this.route.queryParams.pipe(map(param => !(param['withMenu'] ?? false)));
    currentRoute = this.route.url.pipe(map(() => ['/', ...location.pathname.split('/').filter(s => s !== '')]));

    taskList$ = this.taskService.listTodo$;

    @Output() updateWidth: EventEmitter<string> = new EventEmitter<string>()

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private taskService: TaskService
    ) {
    }

    ngOnInit(): void {
        this.taskService.getTaskTodo().subscribe();
        this.collapsed.subscribe(collapsed => this.updateWidth.emit(collapsed ? 'none' : '15'));
    }

    delete(task: Task) {
        this.taskService.delete(task).subscribe()
    }
}

