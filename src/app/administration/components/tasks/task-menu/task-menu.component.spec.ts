import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TaskMenuComponent} from './task-menu.component';
import {RouterTestingModule} from "@angular/router/testing";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {TaskService} from "../../../services/task.service";
import {taskServiceMock} from "../../../services/__mocks__/task.service";
import {ActivatedRoute, Params, UrlSegment} from "@angular/router";
import {BehaviorSubject, Subscription} from "rxjs";
import {NgxTranslateTestingModule} from "../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {Task} from "../../../models/task";

const queryParams = new BehaviorSubject<Params>({})

const routeMock: Partial<ActivatedRoute> = {
    queryParams,
    url: new BehaviorSubject<UrlSegment[]>([])
}

describe('TaskMenuComponent', () => {
    let sub: Subscription = null;
    let component: TaskMenuComponent;
    let fixture: ComponentFixture<TaskMenuComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TaskMenuComponent],
            imports: [
                NgxTranslateTestingModule,
                RouterTestingModule,
                FontAwesomeTestingModule,
                MatCardModule,
                MatButtonModule
            ],
            providers: [
                {provide: ActivatedRoute, useValue: routeMock},
                {provide: TaskService, useValue: taskServiceMock}
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TaskMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {
        if (sub) {
            sub.unsubscribe()
        }
    })

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });

        test('should have init value if withMenu param is not present', waitForAsync(() => {
            sub = component.collapsed.subscribe(value => expect(value).toBeTruthy())
        }));
        test('should have init value if withMenu param is present', waitForAsync(() => {
            queryParams.next({withMenu: true})
            sub = component.collapsed.subscribe(value => expect(value).toBeFalsy());
        }));
    });

    describe('Typescript tests', () => {
        test('should call delete on task service', () => {
            const task = {id: 2} as Task;
            component.delete(task)
            expect(taskServiceMock.delete).toHaveBeenNthCalledWith(1, task);
        });
    });
});
