import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskHeaderMenuComponent} from './task-header-menu.component';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {MatBadgeModule} from "@angular/material/badge";
import {RouterTestingModule} from "@angular/router/testing";
import {TaskService} from "../../../services/task.service";
import {taskServiceMock} from "../../../services/__mocks__/task.service";
import {NgxTranslateTestingModule} from "../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";

describe('TaskHeaderMenuComponent', () => {
    let component: TaskHeaderMenuComponent;
    let fixture: ComponentFixture<TaskHeaderMenuComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TaskHeaderMenuComponent],
            imports: [
                NgxTranslateTestingModule,
                RouterTestingModule,
                MatListModule,
                MatMenuModule,
                MatBadgeModule,
                FontAwesomeTestingModule
            ], providers: [
                {provide: TaskService, useValue: taskServiceMock}
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TaskHeaderMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });
});
