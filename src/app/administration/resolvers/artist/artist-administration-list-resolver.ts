import {Injectable} from '@angular/core';
import {Artist} from "../../../core/model/artist";
import {ArtistAdministrationService} from "../../services/artist-administration.service";
import {ListResolver} from "../../../shared/resolvers/list-resolver";

@Injectable({
  providedIn: 'root'
})export class ArtistAdministrationListResolver extends ListResolver<Artist, ArtistAdministrationService> {

  constructor(
    service: ArtistAdministrationService
  ) {
    super(service)
  }
}
