import {Injectable} from '@angular/core';
import {ListResolver} from "../../../shared/resolvers/list-resolver";
import {SeriesAdministrationService} from "../../services/series-administration.service";
import {Series} from "../../../core/model/series";
import {SearchCriteria} from "../../../shared/services/entity.service";

@Injectable({
  providedIn: 'root'
})
export class SeriesAdministrationListResolver extends ListResolver<Series, SeriesAdministrationService> {

  constructor(
    service: SeriesAdministrationService
  ) {
    super(service)
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [
      {name: 'name', operation: ':', value: `*${search}*`}
    ]
  }
}
