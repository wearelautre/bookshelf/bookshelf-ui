import {SeriesAdministrationListResolver} from "./series-administration-list-resolver";
import {TestBed} from "@angular/core/testing";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {SeriesAdministrationService} from "../../services/series-administration.service";
import {seriesAdministrationServiceMock} from "../../services/__mocks__/series-administration.service";

describe('SeriesAdministrationListResolver', () => {
  let service: SeriesAdministrationListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SeriesAdministrationListResolver,
        {provide: SeriesAdministrationService, useValue: seriesAdministrationServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    service = TestBed.inject(SeriesAdministrationListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should return the searchCriteriaList', () => {
      expect(service[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'name', operation: ':', value: `*toto*`}
        ]);
    });
  });
});
