import {BookAdministrationListResolver} from "./book-administration-list-resolver";
import {TestBed} from "@angular/core/testing";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {BookAdministrationService} from "../../services/book-administration.service";
import {bookAdministrationServiceMock} from "../../services/__mocks__/book-administration.service";

describe('BookAdministrationListResolver', () => {
  let service: BookAdministrationListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BookAdministrationListResolver,
        {provide: BookAdministrationService, useValue: bookAdministrationServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    service = TestBed.inject(BookAdministrationListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should return the searchCriteriaList', () => {
      expect(service[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'title', operation: ':', value: `*toto*`},
          {name: 'editor', operation: ':', value: `*toto*`, or: true},
          {name: 'series', operation: ':', value: `*toto*`, or: true}
        ]);
    });
  });
});
