import {TestBed} from '@angular/core/testing';

import {BookEditResolver} from './book-edit-resolver';
import {ActivatedRouteSnapshot} from '@angular/router';
import {BookImpl} from '../../../core/model/impl/book-impl';
import {BookService} from '../../../book/services/book.service';
import {bookServiceMock} from '../../../book/services/__mocks__/book.service';

describe('BookEditResolver', () => {
  let service: BookEditResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          {provide: BookService, useValue: bookServiceMock}
        ]
      }
    );
    service = TestBed.inject(BookEditResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should create', (done) => {
      const route = new ActivatedRouteSnapshot();
      route.params = {isbn: '1234567890123'};

      const mockFromBook = jest.fn();
      mockFromBook.mockReturnValue({test: 'test'});

      BookImpl.fromBook = mockFromBook;

      service.resolve(route, undefined).subscribe(value => {
        expect(value).toStrictEqual({test: 'test'});
        done();
      });
      expect(bookServiceMock.getById).toHaveBeenNthCalledWith(1, '1234567890123');
    });
  });
});
