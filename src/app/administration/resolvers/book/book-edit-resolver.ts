import {Injectable} from '@angular/core';
import {Book} from '../../../core/model/book';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BookImpl} from '../../../core/model/impl/book-impl';
import {BookService} from "../../../book/services/book.service";

@Injectable({
  providedIn: 'root'
})
export class BookEditResolver  {

  constructor(
    private bookService: BookService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Book> {
    return this.bookService.getById(route.params.isbn)
      .pipe(map((book) => BookImpl.fromBook(book)));
  }
}
