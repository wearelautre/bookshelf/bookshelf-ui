import {Injectable} from '@angular/core';
import {Book} from '../../../core/model/book';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {BookAdministrationService} from '../../services/book-administration.service';
import {map} from 'rxjs/operators';
import {BookImpl} from "../../../core/model/impl/book-impl";

@Injectable({
  providedIn: 'root'
})
export class BookAddResolver  {

  constructor(
    private bookAdministrationService: BookAdministrationService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Book> {
    // TODO add control on isbn format
    return this.bookAdministrationService.searchBook(route.params.isbn)
      .pipe(
        map((bookSearch) => BookImpl.fromBookSearch(bookSearch))
      );
  }
}
