import {TestBed} from '@angular/core/testing';

import {BorrowerAdministrationListResolver} from './borrower-administration-list.resolver';
import {PaginationService} from "../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {borrowerAdministrationServiceMock} from "../../services/__mocks__/borrower-administration.service";
import {BorrowerAdministrationService} from "../../services/borrower-administration.service";

describe('BorrowerAdministrationListResolver', () => {
  let resolver: BorrowerAdministrationListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BorrowerAdministrationListResolver,
        {provide: BorrowerAdministrationService, useValue: borrowerAdministrationServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    resolver = TestBed.inject(BorrowerAdministrationListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(resolver).toBeTruthy();
    });
  });
});
