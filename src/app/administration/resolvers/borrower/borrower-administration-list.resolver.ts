import {Injectable} from '@angular/core';
import {ListResolver} from "../../../shared/resolvers/list-resolver";
import {BorrowerAdministrationService} from "../../services/borrower-administration.service";
import {Borrower} from "../../../core/model/borrower";

@Injectable({
  providedIn: 'root'
})
export class BorrowerAdministrationListResolver extends ListResolver<Borrower, BorrowerAdministrationService> {
  constructor(
    service: BorrowerAdministrationService
  ) {
    super(service)
  }
}
