import {Injectable} from '@angular/core';
import {Book} from "../../core/model/book";
import {BookService} from "../services/book.service";
import {ListResolver} from "../../shared/resolvers/list-resolver";
import {PaginationService} from "../../shared/services/pagination.service";

@Injectable({
  providedIn: 'root'
})
export class BookListResolver extends ListResolver<Book, BookService> {

  private static pageSize: number = 100

  constructor(
    service: BookService,
    paginationService: PaginationService,
  ) {
    super(service, BookListResolver.pageSize, {
      name: 'possessionStatus',
      operation: '!',
      value: `SOLD`
    }, paginationService);
  }
}
