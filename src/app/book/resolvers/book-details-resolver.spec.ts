import {TestBed, waitForAsync} from '@angular/core/testing';

import {BookDetailsResolver} from './book-details-resolver';
import {BookService} from '../services/book.service';
import {bookServiceMock} from '../services/__mocks__/book.service';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {ActivatedRouteSnapshot} from '@angular/router';
import {of} from 'rxjs';
import {BookImpl} from '../../core/model/impl/book-impl';
import {BookSeriesImpl} from '../../core/model/impl/book-series-impl';

describe('BookDetailsResolver', () => {
  let service: BookDetailsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceMock},
      ]
    });
    service = TestBed.inject(BookDetailsResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('TypeScript test', () => {
    test('should get event for book isbn', waitForAsync(() => {
      jest.clearAllMocks();
      const route = new ActivatedRouteSnapshot();
      const isbn = '1234567890129';
      route.params = {isbn};

      const mockBook = new BookImpl();
      const mockSeries = new BookSeriesImpl();
      mockSeries.name = 's';
      mockBook.series = mockSeries;
      mockBook.isbn = isbn;

      bookServiceMock.getById.mockImplementationOnce(() => of(mockBook));

      service.resolve(route, undefined).subscribe((value) => {
        expect(bookServiceMock.getById).toHaveBeenNthCalledWith(1, isbn);
        expect(bookServiceMock.getNextIsbn).toHaveBeenNthCalledWith(1, isbn);
        expect(bookServiceMock.getPreviousIsbn).toHaveBeenNthCalledWith(1, isbn, true);

        expect(value).toStrictEqual({
          book: mockBook,
          asNext$: "1234567891230",
          asPrevious$: "0321987654321"
        });
      });
    }));
  });
});
