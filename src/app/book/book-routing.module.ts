import {NgModule} from '@angular/core';
import {Data, RouterModule, Routes} from '@angular/router';
import {BookListComponent} from './pages/book-list/book-list.component';
import {BookDashboardComponent} from './pages/book-dashboard/book-dashboard.component';
import {BookDetailsResolver} from './resolvers/book-details-resolver';
import {BookDetailsComponent} from './pages/book-details/book-details.component';
import {BookListResolver} from './resolvers/book-list-resolver';
import {BookToDisplay} from "./models/book-to-display";

export interface BookDetailsData extends Data {
  bookToDisplay?: BookToDisplay
}

const booksRoutes: Routes = [
  {path: '', component: BookDashboardComponent},
  {
    path: 'list',
    resolve: {resolvedData: BookListResolver},
    runGuardsAndResolvers: 'paramsOrQueryParamsChange',
    component: BookListComponent,
    children: [{
      path: 'detail/:isbn',
      resolve: {bookToDisplay: BookDetailsResolver},
      component: BookDetailsComponent
    }]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(booksRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BookRoutingModule {
}

