import {Component, OnDestroy, OnInit,} from '@angular/core';
import {BehaviorSubject, Observable, skip, Subscription, withLatestFrom} from 'rxjs';
import {SeriesByGroupContainer} from '../../../core/model/series-by-group-container';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatestWith, filter, map} from "rxjs/operators";
import {BookService} from "../../services/book.service";
import {BookFilterService} from "../../services/book-filter.service";
import {ScrollEventService} from "../../../shared/services/scroll-event.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {HttpResponse} from "@angular/common/http";
import {mapToResolvedDataPaginated, ResolvedDataPaginated} from "../../../shared/resolvers/list-resolver";
import {Book} from "../../../core/model/book";
import {SearchCriteria} from "../../../shared/services/entity.service";

@Component({
  selector: 'app-book-list',
  template: `
    <mat-sidenav-container class="book-list-container" fxFlex>
      <mat-sidenav-content class="content">
        <div fxFlex.lt-lg="100" fxFlexOffset.gt-md="10" fxFlex.gt-md="80">
          <app-book-list-action-bar style="overflow-y: hidden;"></app-book-list-action-bar>
          <app-group-display *ngFor="let group of filteredGroupList$ | async"
                             [seriesContainer]="(filteredBooks$ | async).get(group)"
                             [groupName]="group">
          </app-group-display>
          <div fxLayoutAlign="center">
            <mat-progress-spinner
              *ngIf="nextPageLoading$ | async"
              class="example-margin"
              color="primary"
              mode="indeterminate">
            </mat-progress-spinner>
          </div>
        </div>
      </mat-sidenav-content>
      <mat-sidenav mode="over" (openedStart)="deactivateScroll()" [opened]="openSidebar" class="right-side-nav"
                   position="end"
                   (closedStart)="clearBookToDisplay()">
        <router-outlet (activate)="openSidebar = true" (deactivate)="openSidebar = false"></router-outlet>
      </mat-sidenav>
    </mat-sidenav-container>
  `,
  styles: [`
    .right-side-nav {
      position: fixed;
      width: 50%
    }
  `],
  providers: [
    PaginationService
  ]
})
export class BookListComponent implements OnInit, OnDestroy {
  defaultSearchCriteriaList: SearchCriteria[] = [{
    name: 'possessionStatus',
    operation: '!',
    value: `SOLD`
  }]

  filteredBooks$: Observable<SeriesByGroupContainer> = this.bookService.list$
    .pipe(map(books => this.bookService.groupBy(books)))
  filteredGroupList$: Observable<string[]> = this.filteredBooks$
    .pipe(map(seriesByEditorContainer => Array.from(seriesByEditorContainer.keys())));
  openSidebar: boolean;

  private subscriptions: Subscription[] = [];

  private nextPageLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  get nextPageLoading$(): Observable<boolean> {
    return this.nextPageLoading.asObservable();
  }

  // TODO then add a filter in the book list
  constructor(
    private router: Router,
    private bookFilterService: BookFilterService,
    private scrollEventService: ScrollEventService,
    private bookService: BookService,
    private route: ActivatedRoute,
    private paginationService: PaginationService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.route.data.subscribe({
        next: (data) => {
          this.bookService.updateList(data.resolvedData.items)
          this.paginationService.updatePagination(data.resolvedData)
        }
      }),

      this.scrollEventService.bottomReached$.pipe(
        withLatestFrom(
          this.nextPageLoading$,
          this.bookFilterService.filter$,
          this.bookFilterService.direction$,
          this.bookFilterService.sort$
        ),
        filter(([_, nextPageAlreadyLoading, _bookFilter, _direction, _sort]) => !nextPageAlreadyLoading),
      ).subscribe(([_, _nextPageAlreadyLoading, bookFilter, direction, sort]) => {
        const nextPage = this.paginationService.getNextPage()
        if (nextPage !== null) {
          this.search(bookFilter, sort, direction, nextPage)
        }
      }),

      this.bookFilterService.filter$.pipe(
        combineLatestWith(
          this.bookFilterService.direction$,
          this.bookFilterService.sort$
        ),
        skip(1))
        .subscribe(([bookFilter, direction, sort]) => this.search(bookFilter, sort, direction, 0))
    );
  }

  clearBookToDisplay() {
    this.router.navigate(['/', 'book', 'list'], {queryParamsHandling: 'preserve'})
      .then(() => this.activateScroll());
  }

  private search(bookFilter, sort, direction, page = 0): void {
    const nextPage = page > 0;
    if (nextPage) {
      this.nextPageLoading.next(true);
    }
    this.bookService.search(
      this.getSearchCriteriaList(bookFilter, sort), page, 100, true, page == 0, direction, `${sort}`
    )
      .pipe(map((response: HttpResponse<Book[]>) => mapToResolvedDataPaginated(response)))
      .subscribe({
        next: (data: ResolvedDataPaginated<Book>) => {
          this.bookService.updateList(data.items, nextPage)
          this.paginationService.updatePagination(data)
          if (nextPage) {
            this.nextPageLoading.next(false);
          }
        }
      });
  }

  private getSearchCriteriaList(bookFilter, sort) {
    return [...this.defaultSearchCriteriaList, ...this.bookService.getSearchParam(
      (bookFilter.status ? bookFilter.status : []),
      (bookFilter.bookTypes ? bookFilter.bookTypes : []).join(','),
      (bookFilter.groups ? bookFilter.groups : []),
      (bookFilter.series ? bookFilter.series : []),
      sort
    )]
  }

  deactivateScroll() {
    this.scrollEventService.toggleScroll(false)
  }

  activateScroll() {
    this.scrollEventService.toggleScroll(true)
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
