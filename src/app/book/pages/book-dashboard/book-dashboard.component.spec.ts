import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {BookDashboardComponent} from './book-dashboard.component';
import {MockItemCardComponent} from '../../components/dashboard/book-card/__mocks__/item-card.component';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreService} from '../../../core/services/core.service';
import {ProfileService} from '../../services/profile.service';
import {coreServiceMock} from '../../../core/services/__mocks__/core.service';
import {profileServiceMock} from '../../services/__mocks__/profile.service';
import {MockBookCardListComponent} from '../../components/dashboard/book-card-list/__mocks__/book-card-list.component';
import {
  MockProfileInformationsCardComponent
} from '../../components/dashboard/profile-informations-card/__mocks__/profile-informations-card.component';
import {LastAddedItemsStoreService} from '../../services/store/last-added-items-store.service';
import {NextItemsToReadStoreService} from '../../services/store/next-items-to-read-store.service';
import {pageStoreServiceMock} from '../../services/store/__mocks__/page-store-service.mock';
import {bookServiceMock} from "../../services/__mocks__/book.service";
import {Book} from "../../../core/model/book";
import {BookService} from "../../services/book.service";
import {of} from "rxjs";
import StatusEnum = Book.StatusEnum;

const lastAddedBookStoreServiceMock = pageStoreServiceMock;
const nextBookToReadStoreServiceMock = pageStoreServiceMock;

describe('BookDashboardComponent', () => {
  let component: BookDashboardComponent;
  let fixture: ComponentFixture<BookDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookDashboardComponent,
        MockItemCardComponent,
        MockBookCardListComponent,
        MockProfileInformationsCardComponent
      ],
      imports: [
        FontAwesomeTestingModule,
        NgxTranslateTestingModule,
        RouterTestingModule,
        AngularAuthOidcClientTestingModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceMock},
        {provide: CoreService, useValue: coreServiceMock},
        {provide: ProfileService, useValue: profileServiceMock},
        {provide: LastAddedItemsStoreService, useValue: lastAddedBookStoreServiceMock},
        {provide: NextItemsToReadStoreService, useValue: nextBookToReadStoreServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should call profileService LastAddedBooks', waitForAsync(() => {
      jest.clearAllMocks();
      component.moreLastAddedBooks();
      expect(lastAddedBookStoreServiceMock.getNextPage).toHaveBeenNthCalledWith(1, 5);
    }));
    test('should call profileService NextBookToRead', waitForAsync(() => {
      jest.clearAllMocks();
      component.moreNextBookToRead();
      expect(nextBookToReadStoreServiceMock.getNextPage).toHaveBeenNthCalledWith(1, 5);
    }));
    test('should call profileService updateBook', waitForAsync(() => {
      jest.clearAllMocks();
      profileServiceMock.patchItem.mockImplementation(() => of('test'))
      component.setItemToRead(123);
      expect(profileServiceMock.patchItem).toHaveBeenCalledWith(123, {id: 123, status: StatusEnum.READ});
      expect(nextBookToReadStoreServiceMock.reloadSameList).toHaveBeenCalledTimes(1);
      expect(profileServiceMock.getItemsStatus).toHaveBeenCalledTimes(1);
    }));
  });
});
