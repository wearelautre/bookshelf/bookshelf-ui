import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfileService} from '../../services/profile.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {LastAddedItemsStoreService} from '../../services/store/last-added-items-store.service';
import {NextItemsToReadStoreService} from '../../services/store/next-items-to-read-store.service';
import {map, switchMap} from 'rxjs/operators';
import {forkJoin, Subscription} from "rxjs";
import {Book} from "../../../core/model/book";
import StatusEnum = Book.StatusEnum;

@Component({
  selector: 'app-book-dashboard',
  templateUrl: './book-dashboard.component.html'
})
export class BookDashboardComponent implements OnInit, OnDestroy {

  isAuthenticated$ = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));
  lastAddedItems$ = this.lastAddedBookStore.list$;
  nextBookToRead$ = this.nextBookToReadStore.list$;
  asMoreBookToRead$ = this.nextBookToReadStore.isLastPage$.pipe(map(value => !value));

  public hasData = 0;
  private subscriptions: Subscription[] = [];

  constructor(
    private profileService: ProfileService,
    private lastAddedBookStore: LastAddedItemsStoreService,
    private nextBookToReadStore: NextItemsToReadStoreService,
    private oidcSecurityService: OidcSecurityService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(
      this.profileService.getProfileInformations().subscribe(),
      this.lastAddedBookStore.list$.subscribe(list => this.hasData = list.length)
    );
  }

  moreLastAddedBooks() {
    this.lastAddedBookStore
      .getNextPage(this.lastAddedBookStore.getNextPageNumber())
      .subscribe();
  }

  moreNextBookToRead() {
    this.nextBookToReadStore
      .getNextPage(this.nextBookToReadStore.getNextPageNumber())
      .subscribe();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  setItemToRead(itemId: number) {
    this.profileService.patchItem(itemId, {id: itemId, status: StatusEnum.READ}).pipe(
      switchMap(() => forkJoin([
          this.profileService.getItemsStatus(),
          this.nextBookToReadStore.reloadSameList()
        ])
      ),
    ).subscribe()
  }
}
