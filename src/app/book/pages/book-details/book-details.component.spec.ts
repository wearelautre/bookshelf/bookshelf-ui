import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookDetailsComponent} from './book-details.component';
import {MatListModule} from '@angular/material/list';
import {MockLoaderImgComponent} from '../../../shared/loader-img/__mocks__/loader-img.component';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {MockWebLinkListComponent} from '../../../shared/web-link-list/__mocks__/web-link-list.component';
import {
    MockBookDetailsGlobalInformationComponent
} from "../../components/details/book-details-global-information/__mocks__/book-details-global-information.component";
import {BookImpl} from "../../../core/model/impl/book-impl";
import {
    MockBookDetailsArtistsListComponent
} from "../../components/details/book-details-artists-list/__mocks__/book-details-artists-list.component";
import {
    MockBookDetailsHeaderComponent
} from "../../components/details/book-details-header/__mocks__/book-details-header.component";

const routeMock = {
    data: of({
        bookToDisplay: {
            book: {
                bookType: 'test',
                artist: [],
                editor: {name: 'test1'},
                series: {name: 'series'},
                status: 'UNREAD',
                title: 'tata',
                cover: 'toto'
            },
            asNext: null,
            asPrevious: null
        }
    })
};

Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    })),
});

describe('BookDetailsComponent', () => {
    let component: BookDetailsComponent;
    let fixture: ComponentFixture<BookDetailsComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BookDetailsComponent, MockLoaderImgComponent, MockWebLinkListComponent,
                MockBookDetailsGlobalInformationComponent,
                MockBookDetailsArtistsListComponent,
                MockBookDetailsHeaderComponent
            ],
            imports: [
                NgxTranslateTestingModule,
                RouterTestingModule,
                FlexLayoutModule,
                FontAwesomeTestingModule,
                MatListModule,
            ],
            providers: [
                {provide: ActivatedRoute, useValue: routeMock},
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BookDetailsComponent);
        component = fixture.componentInstance;
        component.bookEvent = {
            book: new BookImpl(),
            asNext$: of("next"),
            asPrevious$: of("previous")
        }
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', waitForAsync(() => {
            expect(component).toBeTruthy();

            component.img.subscribe(img => {
                expect(img.src.startsWith('/files/covers/toto?random=')).toBeTruthy();
                expect(img.alt).toStrictEqual('toto');
            })
        }));
    });
});
