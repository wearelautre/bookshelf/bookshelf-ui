import {Component} from '@angular/core';
import {DisplayImage} from '../../../shared/display-image';
import {ActivatedRoute, Router} from '@angular/router';
import {Image} from '../../../shared/models/image';
import {Observable} from 'rxjs';
import {BookDetailsData} from "../../book-routing.module";
import {map} from "rxjs/operators";
import {BookToDisplay} from "../../models/book-to-display";

@Component({
  selector: 'app-book-details',
  template: `
    <div fxLayout="column" *ngIf="(data | async).bookToDisplay as bookEvent" class="book-details-container">
      <app-book-details-header
        [book]="bookEvent.book"
        [nextIsbn]="bookEvent.asNext$ | async"
        [previousIsbn]="bookEvent.asPrevious$ | async">
      </app-book-details-header>
      <div fxFlex fxLayoutAlign="space-between">
        <div fxFlex="25">
          <app-book-details-global-information [book]="bookEvent.book"></app-book-details-global-information>
        </div>
        <div fxFlex="30">
          <app-book-details-artists-list [contracts]="bookEvent.book.contracts"></app-book-details-artists-list>
        </div>
        <div class="right-panel">
          <div class="cover">
            <app-loader-img [img]="img | async"></app-loader-img>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .right-panel {
      flex: 1;
      display: flex;
      justify-content: center;

      .cover {
        width: 350px
      }
    }

    .book-details-container {
      padding: 20px
    }
  `]
})
export class BookDetailsComponent extends DisplayImage {

  data: Observable<BookDetailsData> = this.route.data;
  bookEvent: BookToDisplay | null = null;
  img: Observable<Image> = this.route.data.pipe(map(data => this.getImg(data.bookToDisplay.book.cover)));

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super('/files/covers');
  }
}
