import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {BookListComponent} from './pages/book-list/book-list.component';
import {BookRoutingModule} from './book-routing.module';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  faArrowLeft,
  faCaretDown,
  faCaretUp,
  faChevronLeft,
  faChevronRight,
  faFilter,
  faSortAlphaDown,
  faSortAlphaUp
} from '@fortawesome/free-solid-svg-icons';
import {SeriesDisplayComponent} from './components/list/series-display/series-display.component';
import {GroupDisplayComponent} from './components/list/group-display/group-display.component';
import {BookDetailsComponent} from './pages/book-details/book-details.component';
import {BookListFilterComponent} from './components/list/book-list-filter/book-list-filter.component';
import {BookListActionBarComponent} from './components/list/book-list-action-bar/book-list-action-bar.component';
import {BookDashboardComponent} from './pages/book-dashboard/book-dashboard.component';
import {ItemCardComponent} from './components/dashboard/book-card/item-card.component';
import {
  ProfileInformationsCardComponent
} from './components/dashboard/profile-informations-card/profile-informations-card.component';
import {BookCardListComponent} from './components/dashboard/book-card-list/book-card-list.component';
import {
  BookDetailsNavigationComponent
} from './components/details/book-details-navigation/book-details-navigation.component';
import {BookStatusComponent} from '../shared/component/book-status/book-status.component';
import {MultiLineListItemComponent} from './components/list/multi-line-list-item/multi-line-list-item.component';
import {
  BookDetailsGlobalInformationComponent
} from './components/details/book-details-global-information/book-details-global-information.component';
import {BookDetailsHeaderComponent} from './components/details/book-details-header/book-details-header.component';
import {
  BookDetailsArtistsListComponent,
} from './components/details/book-details-artists-list/book-details-artists-list.component';
import {MatRippleModule} from "@angular/material/core";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    BookRoutingModule,
    MatListModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatChipsModule,
    FontAwesomeModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatCheckboxModule,
    MatCardModule,
    MatRippleModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    BookListComponent,
    SeriesDisplayComponent,
    GroupDisplayComponent,
    BookDetailsComponent,
    BookListFilterComponent,
    BookListActionBarComponent,
    BookDashboardComponent,
    ItemCardComponent,
    ProfileInformationsCardComponent,
    BookCardListComponent,
    BookDetailsNavigationComponent,
    BookStatusComponent,
    MultiLineListItemComponent,
    BookDetailsGlobalInformationComponent,
    BookDetailsHeaderComponent,
    BookDetailsArtistsListComponent
  ]
})
export class BookModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faFilter);
    library.addIcons(faSortAlphaUp);
    library.addIcons(faSortAlphaDown);
    library.addIcons(faChevronRight);
    library.addIcons(faChevronLeft);
    library.addIcons(faCaretUp);
    library.addIcons(faCaretDown);
    library.addIcons(faArrowLeft);
  }
}
