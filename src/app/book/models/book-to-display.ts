import {Book} from "../../core/model/book";
import {Observable} from "rxjs";

export interface BookToDisplay {
  book: Book | null;
  asNext$: Observable<string | null>;
  asPrevious$: Observable<string | null>;
}
