import {TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {BookService} from './book.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxTranslateTestingModule} from '../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {SeriesInfo} from '../../core/model/series-by-group-container';
import {BookImpl} from '../../core/model/impl/book-impl';
import {BookSeriesImpl} from '../../core/model/impl/book-series-impl';
import {EditorImpl} from '../../core/model/impl/editor-impl';
import {Book} from '../../core/model/book';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {CoreService} from '../../core/services/core.service';
import {of} from 'rxjs';
import {bookFilterServiceMock} from "./__mocks__/book-filter.service";
import {BookFilterService} from "./book-filter.service";
import {SortOrder} from "../../core/model/sort-order.enum";
import {Sort} from "../../core/model/sort.enum";
import {HttpResponse} from "@angular/common/http";
import {PaginationService} from "../../shared/services/pagination.service";
import {paginationServiceMock} from "../../shared/services/__mocks__/pagination.service";
import {Series} from "../../core/model/series";
import clearAllMocks = jest.clearAllMocks;

const getBook = (editor, series, i, oneShot = false): BookImpl => {
  const b = new BookImpl();
  b.title = `${series} T${i}`;
  b.isbn = i;
  b.series = new BookSeriesImpl();
  b.series.tome = i;
  b.series.name = `${series}`;
  b.series.oneShot = oneShot;
  b.editor = new EditorImpl();
  b.editor.name = `${editor}`;
  return b;
};

describe('BookService', () => {
  let httpTestingController: HttpTestingController;
  let service: BookService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock},
        {provide: BookFilterService, useValue: bookFilterServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(BookService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[CREATE GROUPS]', () => {
      test('Create a new group', () => {
        const acc = new Map();
        expect(BookService[`createGroups`](acc, 'group1').has('group1')).toBeTruthy();
      });
      test('Do not an already existing group', () => {
        let acc = new Map();
        acc.set('group1', new Map<string, SeriesInfo>());
        acc = BookService[`createGroups`](acc, 'group1');
        expect(acc.has('group1')).toBeTruthy();
        expect(acc.size).toStrictEqual(1);
      });
    });
    describe('[CLASS SERIES BY GROUP]', () => {
      test('Add series on existing groups', () => {
        const group = 'group1';

        const s = new BookSeriesImpl();
        s.name = 'series1';
        s.status = {
          readCount: 0,
          possessedCount: 67,
          totalCount: 0,
          status: Series.SeriesStatusEnum.UNKNOWN
        };
        const b = new BookImpl();
        b.series = s;

        let acc = new Map();
        acc.set(group, new Map<string, SeriesInfo>());
        acc = BookService[`classSeriesByGroup`](acc, b, group);

        expect(acc.get(group).has(s.name)).toBeTruthy();
        expect(acc.get(group).get(s.name).status.possessedCount).toStrictEqual(67);
      });

      test('Do not add null series', () => {
        const group = 'group1';

        const b = new BookImpl();
        b.series = null;

        let acc = new Map();
        acc.set(group, new Map<string, SeriesInfo>());
        acc = BookService[`classSeriesByGroup`](acc, b, group);

        expect(acc.get(group).size).toStrictEqual(0);
      });

      test('Do not add series on group null', () => {
        const s = new BookSeriesImpl();
        s.name = 'series1';

        const b = new BookImpl();
        b.series = s;

        let acc = new Map();
        acc = BookService[`classSeriesByGroup`](acc, b, null);

        expect(acc.size).toStrictEqual(0);
      });

      test('Do not add series if series already present', () => {
        const group = 'group1';

        const s = new BookSeriesImpl();
        s.name = 'series1';
        s.status = {
          readCount: 0,
          possessedCount: 67,
          totalCount: 0,
          status: Series.SeriesStatusEnum.UNKNOWN
        };
        const b = new BookImpl();
        b.series = s;

        let acc = new Map();
        acc.set(group, new Map<string, SeriesInfo>());
        acc = BookService[`classSeriesByGroup`](acc, b, group);
        acc = BookService[`classSeriesByGroup`](acc, b, group);

        expect(acc.get(group).size).toStrictEqual(1);
        expect(acc.get(group).has(s.name)).toBeTruthy();
        expect(acc.get(group).get(s.name).status.possessedCount).toStrictEqual(67);
      });
    });
    describe('[ADD BOOK]', () => {
      test('Do not add book if series null', () => {
        const group = 'group1';

        const b = new BookImpl();
        b.series = null;

        let acc = new Map();
        acc.set(group, new Map<string, SeriesInfo>());
        acc = BookService[`addBook`](acc, b, false);

        expect(acc.get(group).size).toStrictEqual(0);
      });
      test('Do not add book if editor null', () => {
        const group = 'group1';

        const b = new BookImpl();
        b.series = new BookSeriesImpl();
        b.editor = null;

        let acc = new Map();
        acc.set(group, new Map<string, SeriesInfo>());
        acc = BookService[`addBook`](acc, b, false);

        expect(acc.get(group).size).toStrictEqual(0);
      });
      test('Add book with groupByEditor true', () => {
        const b = new BookImpl();
        b.title = 'Watchmen Title';
        b.series = new BookSeriesImpl();
        b.series.name = 'Watchmen';
        b.editor = new EditorImpl();
        b.editor.name = 'toto';

        let acc = new Map();
        acc = BookService[`addBook`](acc, b, true);

        expect(acc.size).toStrictEqual(1);
        expect(acc.get('toto').size).toStrictEqual(1);
        expect(acc.get('toto').get('Watchmen').books.length).toStrictEqual(1);
        expect(acc.get('toto').get('Watchmen').books[0]).toStrictEqual(b);
      });
      test('Add book with groupByEditor false', () => {
        const b = new BookImpl();
        b.title = 'Watchmen Title';
        b.series = new BookSeriesImpl();
        b.series.name = 'Watchmen';
        b.editor = new EditorImpl();
        b.editor.name = 'toto';

        let acc = new Map();
        acc = BookService[`addBook`](acc, b, false);

        expect(acc.size).toStrictEqual(1);
        expect(acc.get('W').size).toStrictEqual(1);
        expect(acc.get('W').get('Watchmen').books.length).toStrictEqual(1);
        expect(acc.get('W').get('Watchmen').books[0]).toStrictEqual(b);
      });
    });
    describe('[GROUP BY]', () => {
      const mockData = (): Book[] => {
        return [
          getBook('DC Comics', 'Watchmen', 4),
          getBook('DC Comics', 'Watchmen', 1),
          getBook('DC Comics', 'Umbrella Academy', 10),
          getBook('DC Comics', 'Umbrella Academy', 2),
        ];
      };

      test('Group by editor', () => {
        bookFilterServiceMock.isGroupByEditor.mockImplementation(() => true)
        const acc = service.groupBy(mockData());
        expect(acc.get('DC Comics').get('Umbrella Academy').books[0].series.tome).toStrictEqual(10);
        expect(acc.get('DC Comics').get('Umbrella Academy').books[1].series.tome).toStrictEqual(2);
        expect(acc.get('DC Comics').get('Watchmen').books[0].series.tome).toStrictEqual(4);
        expect(acc.get('DC Comics').get('Watchmen').books[1].series.tome).toStrictEqual(1);
      });

      test('Group by series', () => {
        bookFilterServiceMock.isGroupByEditor.mockImplementation(() => false)
        const acc = service.groupBy(mockData());
        expect(acc.get('U').get('Umbrella Academy').books[0].series.tome).toStrictEqual(10);
        expect(acc.get('U').get('Umbrella Academy').books[1].series.tome).toStrictEqual(2);
        expect(acc.get('W').get('Watchmen').books[0].series.tome).toStrictEqual(4);
        expect(acc.get('W').get('Watchmen').books[1].series.tome).toStrictEqual(1);
      });
    });

    describe('[BULK PATCH]', () => {
      test('Bulk patch OK', waitForAsync(() => {
        clearAllMocks();
        const b1 = getBook('e', 's', 123456789123);
        const b2 = getBook('e', 's', 234567891234);

        const spy = jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({body: []})));

        const books: Book[] = [b1, b2];
        service.bulkPatch(books).subscribe((next) => {
          expect(next).toStrictEqual([BookImpl.fromBook(b1), BookImpl.fromBook(b2)]);
          expect(spy).toHaveBeenNthCalledWith(1, [], 0, 100, false, true, SortOrder.ASC, Sort.SERIES);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/123456789123`);
        const req2 = httpTestingController.expectOne(`/api/books/234567891234`);

        expect(req1.request.method).toEqual('PATCH');
        expect(req2.request.method).toEqual('PATCH');
        req1.flush(BookImpl.fromBook(b1));
        req2.flush(BookImpl.fromBook(b2));

      }));

      test('Bulk patch OK with filter', waitForAsync(() => {
        clearAllMocks();
        const b1 = getBook('e', 's', 123456789123);
        const b2 = getBook('e', 's', 234567891234);

        const spy = jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({body: []})));
        bookFilterServiceMock.getCurrentFilters.mockImplementation(() => ({
          bookFilter: {status: null, series: ['series'], groups: ['groups'], bookTypes: ['bookTypes1', 'bookTypes2']},
          direction: SortOrder.DESC,
          sort: Sort.EDITOR
        }));

        const books: Book[] = [b1, b2];
        service.bulkPatch(books).subscribe((next) => {
          expect(next).toStrictEqual([BookImpl.fromBook(b1), BookImpl.fromBook(b2)]);
          expect(spy).toHaveBeenNthCalledWith(1, [{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[bookTypes1,bookTypes2]",
          },
            {
              "name": "editor",
              "operation": ":",
              "or": false,
              "value": "groups",
            },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "series",
            }], 0, 100, false, true, SortOrder.DESC, Sort.EDITOR);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/123456789123`);
        const req2 = httpTestingController.expectOne(`/api/books/234567891234`);

        expect(req1.request.method).toEqual('PATCH');
        expect(req2.request.method).toEqual('PATCH');
        req1.flush(BookImpl.fromBook(b1));
        req2.flush(BookImpl.fromBook(b2));

      }));
      test('Bulk patch OK with filter 2', waitForAsync(() => {
        clearAllMocks();
        const b1 = getBook('e', 's', 123456789123);
        const b2 = getBook('e', 's', 234567891234);

        const spy = jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({body: []})));
        bookFilterServiceMock.getCurrentFilters.mockImplementation(() => ({
          bookFilter: {
            status: ['UNREAD'],
            series: ['series'],
            groups: ['groups'],
            bookTypes: ['bookTypes1', 'bookTypes2']
          },
          direction: SortOrder.DESC,
          sort: Sort.EDITOR
        }));

        const books: Book[] = [b1, b2];
        service.bulkPatch(books).subscribe((next) => {
          expect(next).toStrictEqual([BookImpl.fromBook(b1), BookImpl.fromBook(b2)]);
          expect(spy).toHaveBeenNthCalledWith(1, [{
            "name": "status",
            "operation": ":",
            "or": false,
            "value": "UNREAD",
          }, {
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[bookTypes1,bookTypes2]",
          },
            {
              "name": "editor",
              "operation": ":",
              "or": false,
              "value": "groups",
            },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "series",
            }], 0, 100, false, true, SortOrder.DESC, Sort.EDITOR);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/123456789123`);
        const req2 = httpTestingController.expectOne(`/api/books/234567891234`);

        expect(req1.request.method).toEqual('PATCH');
        expect(req2.request.method).toEqual('PATCH');
        req1.flush(BookImpl.fromBook(b1));
        req2.flush(BookImpl.fromBook(b2));

      }));
      test('Bulk patch OK without filter', waitForAsync(() => {
        clearAllMocks();
        const b1 = getBook('e', 's', 123456789123);
        const b2 = getBook('e', 's', 234567891234);

        const spy = jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({body: []})));
        bookFilterServiceMock.getCurrentFilters.mockImplementation(() => ({
          bookFilter: {status: [], series: null, groups: null, bookTypes: null},
          direction: SortOrder.DESC,
          sort: Sort.EDITOR
        }));

        const books: Book[] = [b1, b2];
        service.bulkPatch(books).subscribe((next) => {
          expect(next).toStrictEqual([BookImpl.fromBook(b1), BookImpl.fromBook(b2)]);
          expect(spy).toHaveBeenNthCalledWith(1, [], 0, 100, false, true, SortOrder.DESC, Sort.EDITOR);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        });

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/123456789123`);
        const req2 = httpTestingController.expectOne(`/api/books/234567891234`);

        expect(req1.request.method).toEqual('PATCH');
        expect(req2.request.method).toEqual('PATCH');
        req1.flush(BookImpl.fromBook(b1));
        req2.flush(BookImpl.fromBook(b2));

      }));
      test('Bulk patch KO', waitForAsync(() => {
        clearAllMocks();
        const b1 = getBook('e', 's', 123456789123);
        const b2 = getBook('e', 's', 234567891234);

        const spy = jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({body: []})));

        const books: Book[] = [b1, b2];
        service.bulkPatch(books).subscribe({
            error: () => {
              expect(spy).toHaveBeenCalledTimes(0);
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }
          }
        );

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/123456789123`);
        const req2 = httpTestingController.expectOne(`/api/books/234567891234`);

        expect(req1.request.method).toEqual('PATCH');
        expect(req2.request.method).toEqual('PATCH');
        req1.error(new ErrorEvent('error'));
      }));
    });

    describe('[GET NEXT ISBN]', () => {
      beforeEach(() => {
        service[`list`].next([
          {...new BookImpl(), isbn: "1234567890"},
          {...new BookImpl(), isbn: "1234567891"},
          {...new BookImpl(), isbn: "1234567892"}
        ])
      });
      test('Not last item', waitForAsync(() => {
        clearAllMocks();
        service.getNextIsbn("1234567890").subscribe((next) => {
          expect(next).toStrictEqual("1234567891");
        })
      }));
      test('Last item', waitForAsync(() => {
        clearAllMocks();
        service.getNextIsbn("1234567892").subscribe((next) => {
          expect(next).toStrictEqual(null);
        })
      }));
    });

    describe('[GET PREVIOUS ISBN]', () => {
      beforeEach(() => {
        service[`list`].next([
          {...new BookImpl(), isbn: "1234567890"},
          {...new BookImpl(), isbn: "1234567891"},
          {...new BookImpl(), isbn: "1234567892"}
        ])
      });
      test('Not first item', waitForAsync(() => {
        clearAllMocks();
        service.getPreviousIsbn("1234567892").subscribe((next) => {
          expect(next).toStrictEqual("1234567891");
        })
      }));
      test('First item', waitForAsync(() => {
        clearAllMocks();
        service.getPreviousIsbn("1234567890").subscribe((next) => {
          expect(next).toStrictEqual(null);
        })
      }));
      test('fetch if not found next page null', waitForAsync(() => {
        clearAllMocks();
        service.getPreviousIsbn("1234567890", true)
          .subscribe((next) => expect(next).toStrictEqual(null))
      }));
      test('fetch if not found', waitForAsync(() => {
        clearAllMocks();
        const nextPage = 1;
        paginationServiceMock.getNextPage.mockImplementation(() => nextPage);
        jest.spyOn(service, 'search').mockImplementation(() => of(new HttpResponse<Book[]>({
          body: [{
            ...new BookImpl(),
            isbn: "1234567893"
          }]
        })));
        service.getPreviousIsbn("1234567890", true)
          .subscribe((next) => expect(next).toStrictEqual('1234567893'))
      }));

    });
    describe('[GET BOOK BY ISBN]', () => {
      test('getBook', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.getById("1234567892").subscribe(book => {
          expect(book).toStrictEqual(mockB)
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne(`/api/books/1234567892`);

        expect(req1.request.method).toEqual('GET');
        req1.flush(mockB);
      }));
    });
    describe('[GET PARAMS]', () => {
      test('getBook', waitForAsync(() => {
        expect(service['getSearchParam']([], "", [], [], Sort.SERIES)).toStrictEqual([]);
      }));
      test('getBook sort editor', waitForAsync(() => {
        expect(service['getSearchParam']([], "", [], [], Sort.EDITOR)).toStrictEqual([]);
      }));
      test('getBook with book type', waitForAsync(() => {
        expect(service['getSearchParam']([], "BD", [], [], Sort.SERIES))
          .toStrictEqual([{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          }]);
      }));
      test('getBook getBook with series groups search', waitForAsync(() => {
        expect(service['getSearchParam']([], "BD", ["a", "b"], [], Sort.SERIES))
          .toStrictEqual([{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "a*",
            },
            {
              "name": "series",
              "operation": ":",
              "or": true,
              "value": "b*",
            }]);
      }));
      test('getBook with series groups search and series name', waitForAsync(() => {
        expect(service['getSearchParam']([], "BD", ["a", "b"], ["test"], Sort.SERIES))
          .toStrictEqual([{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "test",
            }]);
      }));
      test('getBook with editors groups search and series name desc and editor sort', waitForAsync(() => {
        expect(service['getSearchParam']([], "BD", ["a", "b"], ["test"], Sort.EDITOR))
          .toStrictEqual([{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          },
            {
              "name": "editor",
              "operation": ":",
              "or": false,
              "value": "a",
            },
            {
              "name": "editor",
              "operation": ":",
              "or": true,
              "value": "b",
            },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "test",
            }]);
      }));
      test('getBook with editor groups empty search and series name desc and editor sort', waitForAsync(() => {
        expect(service['getSearchParam']([], "BD", [], ["test"], Sort.EDITOR))
          .toStrictEqual([{
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "test",
            }]);
      }));
      test('getBook with editor groups empty search and series name desc and editor sort with status', waitForAsync(() => {
        expect(service['getSearchParam'](['UNREAD', 'READ'], "BD", [], ["test"], Sort.EDITOR))
          .toStrictEqual([{
            "name": "status",
            "operation": ":",
            "or": false,
            "value": "UNREAD",
          },
            {
              "name": "status",
              "operation": ":",
              "or": true,
              "value": "READ",
            },
            {
              "name": "bookType",
              "operation": ":",
              "or": false,
              "value": "[BD]",
            },
            {
              "name": "series",
              "operation": ":",
              "or": false,
              "value": "test",
            }]);
      }));
    });
    describe('[GET BOOKS]', () => {
      test('getBook', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([], 1, 100).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?page=1&size=100&direction=ASC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook sort editor', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([], 0, 200, true, false, SortOrder.ASC, Sort.EDITOR).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?page=0&size=200&sort=EDITOR&direction=ASC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook minimal', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "bookType",
          "operation": ":",
          "or": false,
          "value": "[BD]",
        }], 0, 200).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=bookType:%5BBD%5D&page=0&size=200&direction=ASC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook with series groups search', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "bookType",
          "operation": ":",
          "or": false,
          "value": "[BD]",
        },
          {
            "name": "series",
            "operation": ":",
            "or": false,
            "value": "a*",
          },
          {
            "name": "series",
            "operation": ":",
            "or": true,
            "value": "b*",
          }]).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=bookType:%5BBD%5D%7Cseries:a*%7C\'series:b*&page=0&size=5&direction=ASC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook with series groups search and series name', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "bookType",
          "operation": ":",
          "or": false,
          "value": "[BD]",
        },
          {
            "name": "series",
            "operation": ":",
            "or": false,
            "value": "test",
          }]).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=bookType:%5BBD%5D%7Cseries:test&page=0&size=5&direction=ASC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook with editors groups search and series name desc and editor sort', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "bookType",
          "operation": ":",
          "or": false,
          "value": "[BD]",
        },
          {
            "name": "editor",
            "operation": ":",
            "or": false,
            "value": "a",
          },
          {
            "name": "editor",
            "operation": ":",
            "or": true,
            "value": "b",
          },
          {
            "name": "series",
            "operation": ":",
            "or": false,
            "value": "test",
          }], 0, 5, true, true, SortOrder.DESC, Sort.EDITOR).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=bookType:%5BBD%5D%7Ceditor:a%7C\'editor:b%7Cseries:test&page=0&size=5&sort=EDITOR&direction=DESC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook with editor groups empty search and series name desc and editor sort', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "bookType",
          "operation": ":",
          "or": false,
          "value": "[BD]",
        },
          {
            "name": "series",
            "operation": ":",
            "or": false,
            "value": "test",
          }], 0, 100, false, false, SortOrder.DESC, Sort.EDITOR).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=bookType:%5BBD%5D%7Cseries:test&page=0&size=100&sort=EDITOR&direction=DESC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
      test('getBook with editor groups empty search and series name desc and editor sort with status', waitForAsync(() => {
        clearAllMocks();
        const mockB = new BookImpl();

        service.search([{
          "name": "status",
          "operation": ":",
          "or": false,
          "value": "UNREAD",
        },
          {
            "name": "status",
            "operation": ":",
            "or": true,
            "value": "READ",
          },
          {
            "name": "bookType",
            "operation": ":",
            "or": false,
            "value": "[BD]",
          },
          {
            "name": "series",
            "operation": ":",
            "or": false,
            "value": "test",
          }], 4, 100, false, false, SortOrder.DESC, Sort.EDITOR).subscribe(({body: books}) => {
          expect(books).toStrictEqual([{...mockB}])
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        })

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false);
        const req1 = httpTestingController.expectOne({
          method: 'GET',
          url: '/api/books?search=status:UNREAD%7C\'status:READ%7CbookType:%5BBD%5D%7Cseries:test&page=4&size=100&sort=EDITOR&direction=DESC&allResults=false'
        });

        expect(req1.request.method).toEqual('GET');
        req1.flush([{...mockB}]);
      }));
    });
  });
});
