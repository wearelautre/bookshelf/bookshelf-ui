import {TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ProfileService} from './profile.service';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {CoreService} from '../../core/services/core.service';
import {LastAddedItemsStoreService} from './store/last-added-items-store.service';
import {pageStoreServiceMock} from './store/__mocks__/page-store-service.mock';
import {NextItemsToReadStoreService} from './store/next-items-to-read-store.service';
import {LibraryInformations} from "../../core/model/profile";
import {MinimalLibraryItem, MinimalLibraryItemWithCover} from "../../core/model/library-item";
import {Page} from "../../core/model/page";
import spyOn = jest.spyOn;

const lastAddedBookStoreServiceMock = pageStoreServiceMock;
const nextBookToReadStoreServiceMock = pageStoreServiceMock;

describe('ProfileService', () => {
  let httpTestingController: HttpTestingController;
  let service: ProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock},
        {provide: LastAddedItemsStoreService, useValue: lastAddedBookStoreServiceMock},
        {provide: NextItemsToReadStoreService, useValue: nextBookToReadStoreServiceMock}
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ProfileService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('BehaviorSubject => Observable', () => {
      const itemsStatus = {value: 50, total: 600};
      const libraryCount = {
        booksCount: 0,
        bookTypesCount: 0,
        bookTypesBooks: []
      };
      test('itemsStatus', waitForAsync(() => {
        service[`itemsStatus`].next(itemsStatus);
        service.itemsStatus$.subscribe(list => expect(list).toStrictEqual(itemsStatus));
      }));
      test('libraryCount', waitForAsync(() => {
        service[`libraryCount`].next(libraryCount);
        service.libraryCount$.subscribe(list => expect(list).toStrictEqual(libraryCount));
      }));
    });
    describe('API Call', () => {
      describe('getProfileInformations', () => {
        const pageMock: Page<MinimalLibraryItem | MinimalLibraryItemWithCover> = {
          list: [],
          currentPage: 0,
          totalPages: 4,
          totalElements: 20
        };
        test('OK', waitForAsync(() => {
          jest.clearAllMocks();
          const spyNextBookStatus = spyOn<any, any>(service[`itemsStatus`], `next`).mockImplementation(() => {
          });
          const spyNextlibraryCount = spyOn<any, any>(service[`libraryCount`], `next`).mockImplementation(() => {
          });

          service.getProfileInformations().subscribe((page) => {
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
            expect(lastAddedBookStoreServiceMock.addPage).toHaveBeenNthCalledWith(1, pageMock);
            expect(nextBookToReadStoreServiceMock.addPage).toHaveBeenNthCalledWith(1, pageMock);
            expect(spyNextBookStatus).toHaveBeenNthCalledWith(1, {value: 1, total: 2});
            expect(spyNextlibraryCount).toHaveBeenNthCalledWith(1, {
              booksCount: 4,
              bookTypesCount: 1,
              bookTypesBooks: []
            });
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          });

          const req = httpTestingController.expectOne(`/api/libraries/1`);

          // non waitForAsync validations
          expect(req.request.method).toEqual('GET');
          req.flush({
            lastAddedItems: pageMock,
            nextItemsToRead: pageMock,
            itemsStatInfo: {value: 1, total: 2},
            libraryCount: {
              booksCount: 4,
              bookTypesCount: 1,
              bookTypesBooks: []
            }
          } as LibraryInformations);
        }));
        test('ERROR', waitForAsync(() => {
          jest.clearAllMocks();
          service.getProfileInformations().subscribe({
            error: (error) => {
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }
          });

          const req = httpTestingController.expectOne(`/api/libraries/1`);

          // non waitForAsync validations
          expect(req.request.method).toEqual('GET');
          req.flush(null, {
            status: 500,
            statusText: 'Internal Server Error'
          });
        }));
      });
      describe('getNextBookToReadNextPage', () => {
        test('default', waitForAsync(() => {
          jest.clearAllMocks();
          service.getNextBookToReadNextPage();
          expect(nextBookToReadStoreServiceMock.getNextPage).toHaveBeenNthCalledWith(1, 0, 5);
        }));
        test('Not default', waitForAsync(() => {
          jest.clearAllMocks();
          service.getNextBookToReadNextPage(5, 40);
          expect(nextBookToReadStoreServiceMock.getNextPage).toHaveBeenNthCalledWith(1, 5, 40);
        }));
      });
      describe('getBookStatus', () => {
        test('OK', waitForAsync(() => {
          jest.clearAllMocks();
          const spyNextBookStatus = spyOn<any, any>(service[`itemsStatus`], `next`).mockImplementation(() => {
          });
          service.getItemsStatus().subscribe((page) => {
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
            expect(spyNextBookStatus).toHaveBeenNthCalledWith(1, {value: 1, total: 2});
            expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
          });

          const req = httpTestingController.expectOne(`/api/libraries/1/status`);

          // non waitForAsync validations
          expect(req.request.method).toEqual('GET');
          req.flush({value: 1, total: 2});
        }));
        test('ERROR', waitForAsync(() => {
          jest.clearAllMocks();
          service.getItemsStatus().subscribe({
            error: _ => {
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }
          });

          const req = httpTestingController.expectOne(`/api/libraries/1/status`);

          // non waitForAsync validations
          expect(req.request.method).toEqual('GET');
          req.flush(null, {
            status: 500,
            statusText: 'Internal Server Error'
          });
        }));
      });
    });
  });
});
