import fn = jest.fn;
import {of} from 'rxjs';

export const bookListServiceMock = {
  changeDisplay: fn(() => {
  }),
  changeSortOrder: fn(() => {
  }),
  isSortOrderAsc: fn(() => {
  }),
  filteredGroupList$: of({}),
  filteredBooks$: of({}),
  searchResult$: of(new Map()),
  filter: jest.fn(() => {
  }),
  updateBookList: fn(() => of({})),
};
