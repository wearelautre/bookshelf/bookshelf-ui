import {of} from 'rxjs';
import fn = jest.fn;

export const bookServiceMock = {
  list$: of([]),
  groupBy: jest.fn(() => (new Map())),
  search: fn(() => of({body: [], headers: new Map()})),
  bulkDelete: jest.fn(() => of([])),
  bulkPatch: jest.fn(() => of([])),
  getById: jest.fn(() => of({})),
  findBookByIsbn: jest.fn(() => of({})),
  getNextIsbn: jest.fn(() => "1234567891230"),
  getPreviousIsbn: jest.fn(() => "0321987654321"),
  getSearchParam: jest.fn(() => []),
  updateList: jest.fn(() => []),
};
