import {of} from 'rxjs';
import fn = jest.fn;

export const profileServiceMock = {
  seriesStatus$: of({value: 0, total: 0}),
  bookStatus$: of({value: 0, total: 0}),
  lastAddedBooks$: of([]),
  getProfileInformations: fn(() => of({})),
  getLastAddedBooksNextPage: fn(() => of({})),
  getNextBookToReadNextPage: fn(() => of({})),
  getItemsStatus: fn(() => of({})),
  patchItem: fn(() => of({})),
};

