import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {forkJoin, of, switchMap, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Book} from '../../core/model/book';
import {CoreService} from '../../core/services/core.service';
import {BookBySeriesContainer, SeriesByGroupContainer, SeriesInfo} from '../../core/model/series-by-group-container';
import {Sort} from "../../core/model/sort.enum";
import {BookFilterService} from "./book-filter.service";
import {EntityService, SearchCriteria} from "../../shared/services/entity.service";
import {PaginationService} from "../../shared/services/pagination.service";
import {mapToResolvedDataPaginated} from "../../shared/resolvers/list-resolver";

@Injectable({
  providedIn: 'root'
})
export class BookService extends EntityService<Book> {

  constructor(
    http: HttpClient,
    coreService: CoreService,
    private paginationService: PaginationService,
    private bookFilterService: BookFilterService
  ) {
    super(http, coreService, 'books')
  }

  private static createGroups(accumulator: SeriesByGroupContainer, group: string) {
    if (!accumulator.get(group)) {
      accumulator.set(group, new Map<string, SeriesInfo>());
    }
    return accumulator;
  }

  private static classSeriesByGroup(accumulator: SeriesByGroupContainer, b: Book, groupName: string) {
    const group = accumulator.get(groupName);
    if (b.series && b.series.name && group && !group.has(b.series.name)) {
      group.set(b.series.name, {
        editor: b.editor.name,
        status: b.series.status,
        books: [],
        oneShot: b.series.oneShot
      });
    }
    return accumulator;
  }

  private static addBook(accumulator: SeriesByGroupContainer, b: Book, groupByEditor: boolean = false) {
    if (b.editor && b.editor.name && b.series && b.series.name) {
      const group = groupByEditor ? b.editor.name : b.series.name.charAt(0).toLocaleUpperCase();
      accumulator = BookService.createGroups(accumulator, group);
      accumulator = BookService.classSeriesByGroup(accumulator, b, group);
      const series = accumulator.get(group);
      if (series != null) {
        const seriesInfo: SeriesInfo = series.get(b.series.name) as SeriesInfo;
        if (seriesInfo) {
          seriesInfo.books.push(b);
        }
      }
    } else {
      if (!b.editor) {
        console.error('Error on book data [missing editor]', b);
      } else if (!b.series) {
        console.error('Error on book data [missing series]', b);
      } else {
        console.error('Error on book data', b);
      }
    }
    return accumulator;
  }

  public groupBy(books: Book[]): SeriesByGroupContainer {
    return books.reduce((accumulator: SeriesByGroupContainer, book: Book) =>
      BookService.addBook(accumulator, book, this.bookFilterService.isGroupByEditor()), new Map<string, BookBySeriesContainer>()
    );
  }

  private static toSeriesSearchCriteria(value: string, index) {
    return BookService.toSearchCriteria('series', value, index);
  }

  private static toEditorsSearchCriteria(value: string, index) {
    return BookService.toSearchCriteria('editor', value, index);
  }

  private static toSearchCriteria(name: string, value: string, index) {
    return {name, operation: ':', value, or: index > 0};
  }

  getSearchParam(status: string[], bookType: string, groups: string[], series: string[], sort: Sort): SearchCriteria[] {
    let searchParam = [];

    if (status.length > 0) {
      searchParam.push(...status.map((status, index) => BookService.toSearchCriteria('status', status, index)))
    }

    if (bookType) {
      searchParam.push(...[BookService.toSearchCriteria('bookType', `[${bookType}]`, 0)])
    }

    if (sort === Sort.EDITOR) {
      if (groups.length > 0) {
        searchParam.push(...groups.map(BookService.toEditorsSearchCriteria))
      }

      if (series.length > 0) {
        searchParam.push(...series.map(BookService.toSeriesSearchCriteria))
      }

      return searchParam
    } else {
      if (groups.length > 0 && series.length == 0) {
        return [...searchParam, ...groups.map((value, index) => BookService.toSeriesSearchCriteria(`${value}*`, index))]
      }

      if (series.length > 0) {
        return [...searchParam, ...series.map(BookService.toSeriesSearchCriteria)]
      }
    }

    return searchParam;
  }

  bulkPatch(books: Book[]) {
    this.coreService.updateLoadingState(true);
    return forkJoin(books.map(book => this.http.patch<Book>('/api/books/' + book.isbn, {status: book.status})))
      .pipe(
        tap(() => {
          const {bookFilter, direction, sort} = this.bookFilterService.getCurrentFilters();
          this.search(
            this.getSearchParam(
              (bookFilter.status ? bookFilter.status : []),
              (bookFilter.bookTypes ? bookFilter.bookTypes : []).join(','),
              (bookFilter.groups ? bookFilter.groups : []),
              (bookFilter.series ? bookFilter.series : []),
              sort
            ),
            0,
            100,
            false,
            true,
            direction,
            sort
          )
            .subscribe();
          this.coreService.updateLoadingState(false);
        }),
        catchError(err => {
          this.coreService.updateLoadingState(false);
          console.error('an error occured!', err);
          return throwError(() => new Error(err));
        }));
  }

  getNextIsbn(isbn: string) {
    return this.list.pipe(
      map(list => {
        const index = list.findIndex((book) => book.isbn === isbn)
        if (index !== -1 && index <= this.list.value.length) {
          if (list[index + 1] !== undefined) {
            return list[index + 1].isbn
          }
        }
        return null
      })
    );
  }

  getPreviousIsbn(isbn: string, fetchIfNotPresent: boolean = false) {
    return this.list.pipe(
      map((list): [Book[], number, string] => {
        const index = list.findIndex((book) => book.isbn === isbn)
        if (index !== 0) {
          if (this.list.value[index - 1] !== undefined) {
            return [list, index, list[index - 1].isbn]
          }
        }
        return [list, index, null]
      }),
      switchMap(([list, index, isbn]) => {
        if (fetchIfNotPresent && isbn === null) {
          const page = this.paginationService.getNextPage()
          if (page !== null) {
            return this.search(
              this.paginationService.getCurrentPagination().searchCriteriaList,
              page,
              this.paginationService.getCurrentPagination().pageSize,
              true,
              page == 0
            ).pipe(
              map((response: HttpResponse<Book[]>) => {
                  const data = mapToResolvedDataPaginated(response)
                  this.paginationService.updatePagination(data)
                  this.updateList(data.items, true)
                  return list[index - 1]?.isbn
                }
              ),
            )
          }
        }
        return of(isbn)
      })
    );
  }
}
