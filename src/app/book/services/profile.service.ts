import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {catchError, tap} from 'rxjs/operators';
import {LibraryInformations} from '../../core/model/profile';
import {ItemStatus} from '../../shared/models/item-status';
import {LastAddedItemsStoreService} from './store/last-added-items-store.service';
import {NextItemsToReadStoreService} from './store/next-items-to-read-store.service';
import {LibraryCount} from "../../core/model/library-count";
import {MinimalLibraryItem} from "../../core/model/library-item";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  accountId = 1;

  get libraryCount$(): Observable<LibraryCount> {
    return this.libraryCount.asObservable();
  }

  get itemsStatus$(): Observable<ItemStatus> {
    return this.itemsStatus.asObservable();
  }

  constructor(
    private http: HttpClient,
    private coreService: CoreService,
    private lastAddedItems: LastAddedItemsStoreService,
    private nextItemsToReadStore: NextItemsToReadStoreService
  ) {
  }

  private libraryCount: BehaviorSubject<LibraryCount> = new BehaviorSubject<LibraryCount>({
    booksCount: 0,
    bookTypesCount: 0,
    bookTypesBooks: []
  });
  private itemsStatus: BehaviorSubject<ItemStatus> = new BehaviorSubject<ItemStatus>({value: 0, total: 0});

  /**
   * @deprecated
   * @todo rename to getLibraryInformations
   */
  getProfileInformations() {
    this.coreService.updateLoadingState(true);
    return this.http.get<LibraryInformations>(`/api/libraries/${this.accountId}`, {observe: 'response'}).pipe(
      tap(res => {
        this.lastAddedItems.addPage(res.body.lastAddedItems);
        this.nextItemsToReadStore.addPage(res.body.nextItemsToRead);

        this.itemsStatus.next(res.body.itemsStatInfo);
        this.libraryCount.next(res.body.libraryCount);

        this.coreService.updateLoadingState(false);
      }),
      catchError(err => {
        console.error('an error occured!', err);
        this.coreService.updateLoadingState(false);
        return throwError(() => new Error(err));
      }));
  }

  patchItem(itemId: number, data: Partial<MinimalLibraryItem>) {
    return this.http.patch<MinimalLibraryItem>(`/api/libraries/${this.accountId}/items/${itemId}`, data);
  }

  getItemsStatus() {
    this.coreService.updateLoadingState(true);
    return this.http.get<ItemStatus>(`/api/libraries/${this.accountId}/status`).pipe(
      tap(res => {
        this.itemsStatus.next(res)
        this.coreService.updateLoadingState(false);
      }),
      catchError(err => {
        console.error('an error occured!', err);
        this.coreService.updateLoadingState(false);
        // return the current status in case of error
        return of(this.itemsStatus.value);
      }));
  }

  getNextBookToReadNextPage(
    page: number = 0,
    size: number = 5,
  ) {
    this.nextItemsToReadStore.getNextPage(page, size).subscribe();
  }
}
