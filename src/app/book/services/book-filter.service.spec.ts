import {TestBed, waitForAsync} from '@angular/core/testing';

import {BookFilterService} from './book-filter.service';
import {Sort} from "../../core/model/sort.enum";
import {SortOrder} from "../../core/model/sort-order.enum";

describe('BookFilterService', () => {
  let service: BookFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookFilterService);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('BehaviorSubject => Observable', () => {
      test('sort', waitForAsync(() => {
        service[`sort`].next(Sort.EDITOR);
        service.sort$.subscribe(list => expect(list).toStrictEqual(Sort.EDITOR));
      }));
      test('direction', waitForAsync(() => {
        service[`direction`].next(SortOrder.DESC);
        service.direction$.subscribe(list => expect(list).toStrictEqual(SortOrder.DESC));
      }));
      test('filter', waitForAsync(() => {
        service[`filter`].next({status: [], series: ['series'], groups: ['groups'], bookTypes: []});
        service.filter$.subscribe(list => expect(list).toStrictEqual({
          status: [],
          series: ['series'],
          groups: ['groups'],
          bookTypes: []
        }));
      }));
    });
    describe('groupByEditors$', () => {
      test('isTrue', waitForAsync(() => {
        service[`sort`].next(Sort.EDITOR);
        service.groupByEditors$.subscribe(boolean => expect(boolean).toBeTruthy());
      }));
      test('isFalse', waitForAsync(() => {
        service[`sort`].next(Sort.SERIES);
        service.groupByEditors$.subscribe(boolean => expect(boolean).toBeFalsy());
      }));
    });
    describe('isSortOrderAsc$', () => {
      test('isTrue', waitForAsync(() => {
        service[`direction`].next(SortOrder.ASC);
        service.isSortOrderAsc$.subscribe(boolean => expect(boolean).toBeTruthy());
      }));
      test('isFalse', waitForAsync(() => {
        service[`direction`].next(SortOrder.DESC);
        service.isSortOrderAsc$.subscribe(boolean => expect(boolean).toBeFalsy());
      }));
    });
    describe('changeSortOrder', () => {
      test('ASC to DESC', waitForAsync(() => {
        service[`direction`].next(SortOrder.ASC);
        service.changeSortOrder();
        service.direction$.subscribe(list => expect(list).toStrictEqual(SortOrder.DESC));
      }));
      test('DESC to ASC', waitForAsync(() => {
        service[`direction`].next(SortOrder.DESC);
        service.changeSortOrder();
        service.direction$.subscribe(list => expect(list).toStrictEqual(SortOrder.ASC));
      }));
    });
    describe('changeDisplay', () => {
      test('EDITOR', waitForAsync(() => {
        service.changeDisplay(true);
        service.sort$.subscribe(list => expect(list).toStrictEqual(Sort.EDITOR));
      }));
      test('SERIES', waitForAsync(() => {
        service.changeDisplay(false);
        service.sort$.subscribe(list => expect(list).toStrictEqual(Sort.SERIES));
      }));
    });
    describe('isGroupByEditor', () => {
      test('isTrue', waitForAsync(() => {
        service[`sort`].next(Sort.EDITOR);
        expect(service.isGroupByEditor()).toBeTruthy()
      }));
      test('isFalse', waitForAsync(() => {
        service[`sort`].next(Sort.SERIES);
        expect(service.isGroupByEditor()).toBeFalsy()
      }));
    });
    describe('updateFilter', () => {
      test('update', waitForAsync(() => {
        service.updateFilter({status: [], series: ['series'], groups: ['groups'], bookTypes: []})
        service.filter$.subscribe(list => expect(list).toStrictEqual({
          status: [],
          series: ['series'],
          groups: ['groups'],
          bookTypes: []
        }));
      }));
    });
    describe('getCurrentFilters', () => {
      test('get', waitForAsync(() => {
        service[`filter`].next({status: [], series: ['series'], groups: ['groups'], bookTypes: []})
        service[`direction`].next(SortOrder.DESC)
        service[`sort`].next(Sort.EDITOR)
        expect(service.getCurrentFilters()).toStrictEqual({
          bookFilter: {status: [], series: ['series'], groups: ['groups'], bookTypes: []},
          direction: SortOrder.DESC,
          sort: Sort.EDITOR
        });
      }));
    });
  });
});
