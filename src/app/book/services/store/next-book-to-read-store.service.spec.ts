import {TestBed} from '@angular/core/testing';

import {NextItemsToReadStoreService} from './next-items-to-read-store.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CoreService} from '../../../core/services/core.service';
import {coreServiceMock} from '../../../core/services/__mocks__/core.service';

describe('NextBookToReadStoreService', () => {
  let service: NextItemsToReadStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock}
      ]
    });
    service = TestBed.inject(NextItemsToReadStoreService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
    test('apiEndpoint OK', () => {
      expect(service[`url`]).toStrictEqual('/api/libraries/1/next-to-read');
    });
  });
});
