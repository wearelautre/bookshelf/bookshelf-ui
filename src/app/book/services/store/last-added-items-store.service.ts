import {Injectable} from '@angular/core';
import {PageStoreService} from './page-store.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../../core/services/core.service';
import {MinimalLibraryItem} from "../../../core/model/library-item";

@Injectable({
  providedIn: 'root'
})
export class LastAddedItemsStoreService extends PageStoreService<MinimalLibraryItem> {
  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, '/api/libraries/{{accountId}}/last-added');
  }

  override getUrlParams(): { [param: string]: string | number } {
    return {
      accountId: 1
    };
  }
}
