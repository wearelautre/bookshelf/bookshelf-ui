import {TestBed, waitForAsync} from '@angular/core/testing';

import {PageStoreService} from './page-store.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CoreService} from '../../../core/services/core.service';
import {coreServiceMock} from '../../../core/services/__mocks__/core.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PaginationStatus} from '../../../shared/models/pagination';
import {of} from "rxjs";
import spyOn = jest.spyOn;

@Injectable({
  providedIn: 'root'
})
class TestPageStoreService extends PageStoreService<string> {

  constructor(http: HttpClient, coreService: CoreService) {
    super(http, coreService, '/api/test/string');
  }
}

describe('PageStoreService', () => {
  let httpTestingController: HttpTestingController;
  let service: TestPageStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock}
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TestPageStoreService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('BehaviorSubject => Observable', () => {
      const bookList = [];
      test('list', waitForAsync(() => {
        service[`list`].next(bookList);
        service.list$.subscribe(list => expect(list).toStrictEqual([]));
      }));
      test('isLastPage', waitForAsync(() => {
        service[`isLastPage`].next(true);
        service.isLastPage$.subscribe(list => expect(list).toStrictEqual(true));
      }));
    });
  });
  describe('updateList', () => {
    const s1 = 's1';
    const s2 = 's2';

    test('all element not displayed', waitForAsync(() => {
      service[`list`].next([s1]);

      const paginationStatus: PaginationStatus = {currentPage: 0, totalPages: 4, totalElements: 20};
      service[`updateList`]([s2], paginationStatus);
      service.list$.subscribe(list => expect(list).toStrictEqual([s2]));
      service.isLastPage$.subscribe(b => expect(b).toBeFalsy());
    }));
    test('all element displayed', waitForAsync(() => {
      service[`list`].next([s1]);

      const paginationStatus: PaginationStatus = {currentPage: 4, totalPages: 4, totalElements: 20};
      service[`updateList`]([s2], paginationStatus, true);
      service.list$.subscribe(list => expect(list).toStrictEqual([s1, s2]));
      service.isLastPage$.subscribe(b => expect(b).toBeTruthy());
    }));
  });
  describe('addPage', () => {
    const s1 = 's1';
    const s2 = 's2';

    test('with concat', waitForAsync(() => {
      service[`list`].next([s1]);
      const spy = spyOn<any, any>(service, `updateList`).mockImplementation(() => {
      });
      service.addPage({list: [s1, s2], currentPage: 4, totalPages: 4, totalElements: 20}, true);
      expect(spy).toHaveBeenNthCalledWith(1, [s1, s2], {currentPage: 4, totalPages: 4, totalElements: 20}, true);
    }));
    test('without concat', waitForAsync(() => {
      service[`list`].next([s1]);
      const spy = spyOn<any, any>(service, `updateList`).mockImplementation(() => {
      });
      service.addPage({list: [s1, s2], currentPage: 4, totalPages: 4, totalElements: 20}, false);
      expect(spy).toHaveBeenNthCalledWith(1, [s1, s2], {currentPage: 4, totalPages: 4, totalElements: 20}, false);
    }));
  });
  describe('getNextPage', () => {
    const s1 = 's1';
    const s2 = 's2';

    test('default', waitForAsync(() => {
      jest.clearAllMocks();
      const spyAddPage = spyOn<any, any>(service, `addPage`).mockImplementation(() => {
      });

      service.getNextPage().subscribe(() => {
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        expect(spyAddPage).toHaveBeenNthCalledWith(1, {
          list: [s1, s2],
          currentPage: 0,
          totalPages: 4,
          totalElements: 20
        }, false);
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      });

      const req = httpTestingController.expectOne(`/api/test/string?page=0&size=5`);

      // non waitForAsync validations
      expect(req.request.method).toEqual('GET');
      req.flush({
        list: [s1, s2],
        currentPage: 0,
        totalPages: 4,
        totalElements: 20
      });
    }));
    test('not default', waitForAsync(() => {
      jest.clearAllMocks();
      const spyAddPage = spyOn<any, any>(service, `addPage`).mockImplementation(() => {
      });

      service.getNextPage(4, 20).subscribe(() => {
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
        expect(spyAddPage).toHaveBeenNthCalledWith(1, {
          list: [s1, s2],
          currentPage: 4,
          totalPages: 4,
          totalElements: 20
        }, true);
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
      });
      const req = httpTestingController.expectOne(`/api/test/string?page=4&size=20`);

      // non waitForAsync validations
      expect(req.request.method).toEqual('GET');
      req.flush({
        list: [s1, s2],
        currentPage: 4,
        totalPages: 4,
        totalElements: 20
      });
    }));
    test('ERROR', waitForAsync(() => {
      jest.clearAllMocks();
      service.getNextPage().subscribe({
        error: (error) => {
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        }
      });

      const req = httpTestingController.expectOne(`/api/test/string?page=0&size=5`);

      // non waitForAsync validations
      expect(req.request.method).toEqual('GET');
      req.error(new ErrorEvent('error'));
    }));
  });
  describe('getNextPageNumber', () => {
    test('isNotLastPage', waitForAsync(() => {
      service[`paginationStatus`] = {currentPage: 1, totalPages: 10, totalElements: 0};
      expect(service.getNextPageNumber()).toStrictEqual(2);
    }));
    test('isLastPage', waitForAsync(() => {
      service[`paginationStatus`] = {currentPage: 9, totalPages: 10, totalElements: 0};
      expect(service.getNextPageNumber()).toStrictEqual(9);
    }));
  });
  describe('reloadSameList', () => {
    test('reload empty list', waitForAsync(() => {
      service[`paginationStatus`] = {currentPage: 1, totalPages: 10, totalElements: 0};
      const spy = jest.spyOn(service, 'getNextPage').mockImplementation(() => of({
        list: [],
        currentPage: 0,
        totalPages: 0,
        totalElements: 0
      }))
      service[`list`].next([])
      expect(service.reloadSameList().subscribe(() => {
        expect(service[`paginationStatus`].currentPage).toStrictEqual(0)
      }));

      expect(spy).toHaveBeenNthCalledWith(1, 0, 10)
    }));
    test('reload list not empty 8 elements', waitForAsync(() => {
      service[`paginationStatus`] = {currentPage: 1, totalPages: 10, totalElements: 0};
      const spy = jest.spyOn(service, 'getNextPage').mockImplementation(() => of({
        list: [],
        currentPage: 0,
        totalPages: 0,
        totalElements: 0
      }))
      service[`list`].next(['test', 'test2', 'test', 'test2', 'test', 'test2', 'test', 'test2', 'test', 'test2'])
      expect(service.reloadSameList().subscribe(() => {
        expect(service[`paginationStatus`].currentPage).toStrictEqual(1)
      }));

      expect(spy).toHaveBeenNthCalledWith(1, 0, 10)
    }));
    test('reload list not empty 11 elements', waitForAsync(() => {
      service[`paginationStatus`] = {currentPage: 1, totalPages: 10, totalElements: 0};
      const spy = jest.spyOn(service, 'getNextPage').mockImplementation(() => of({
        list: [],
        currentPage: 0,
        totalPages: 0,
        totalElements: 0
      }))
      service[`list`].next(['test', 'test2', 'test', 'test2', 'test', 'test2', 'test', 'test2', 'test2', 'test', 'test2', 'test', 'test2', 'test2', 'test2'])
      expect(service.reloadSameList().subscribe(() => {
        expect(service[`paginationStatus`].currentPage).toStrictEqual(2)
      }));

      expect(spy).toHaveBeenNthCalledWith(1, 0, 10)
    }));
  });
});
