import {of} from 'rxjs';

export const pageStoreServiceMock = {
  list$: of([]),
  isLastPage$: of(true),
  addPage: jest.fn(() => {
  }),
  getNextPage: jest.fn(() => of({})),
  getNextPageNumber: jest.fn(() => 5),
  reloadSameList: jest.fn(() => of({}))
};
