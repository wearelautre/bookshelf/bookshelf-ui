import {TestBed} from '@angular/core/testing';

import {LastAddedItemsStoreService} from './last-added-items-store.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CoreService} from '../../../core/services/core.service';
import {coreServiceMock} from '../../../core/services/__mocks__/core.service';

describe('LastAddedBookStoreService', () => {
  let service: LastAddedItemsStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock}
      ]
    });
    service = TestBed.inject(LastAddedItemsStoreService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
    test('apiEndpoint OK', () => {
      expect(service[`url`]).toStrictEqual('/api/libraries/1/last-added');
    });
  });
});
