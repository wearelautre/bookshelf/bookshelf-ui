import {Injectable} from '@angular/core';
import {PageStoreService} from './page-store.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../../core/services/core.service';
import {MinimalLibraryItemWithCover} from "../../../core/model/library-item";


@Injectable({
  providedIn: 'root'
})
export class NextItemsToReadStoreService extends PageStoreService<MinimalLibraryItemWithCover> {
  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, '/api/libraries/{{accountId}}/next-to-read');
  }

  override getUrlParams(): { [param: string]: string | number } {
    return {
      accountId: 1
    };
  }
}
