import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ItemCardComponent} from './item-card.component';
import {MatCardModule} from '@angular/material/card';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MockLoaderImgComponent} from '../../../../shared/loader-img/__mocks__/loader-img.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {
  MockSeriesNameDisplayComponent
} from "../../../../shared/component/series-name-display/__mocks__/series-name-display.component";
import {
  AngularAuthOidcClientTestingModule
} from "../../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module";
import {MinimalLibraryItemWithCover} from "../../../../core/model/library-item";

const itemMock: MinimalLibraryItemWithCover = {
  id: 198,
  lent: false,
  status: 'UNREAD',
  possessionStatus: 'POSSESSED',
  book: {
    cover: 'coverName',
    isbn: 'isbn',
    title: 'title',
    editor: {name: 'name'},
    series: {
      tome: 1,
      status: {
        readCount: 0,
        possessedCount: 0,
        status: undefined,
        totalCount: 0
      }, name: 'name', oneShot: true,
      bookType: {name: 'bookType', id: 89}
    },
  },
};

describe('BookCardComponent', () => {
  let component: ItemCardComponent;
  let fixture: ComponentFixture<ItemCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ItemCardComponent, MockLoaderImgComponent, MockSeriesNameDisplayComponent],
      imports: [
        AngularAuthOidcClientTestingModule,
        NgxTranslateTestingModule,
        MatCardModule,
        MatExpansionModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });

    test('should init without book', () => {
      expect(component.img).toBeNull();
    });

    test('should init with book', () => {
      component.item = itemMock;
      component.ngOnInit();
      expect(component.img.alt).toStrictEqual('coverName');
      expect(component.img.src.startsWith('/files/covers/coverName')).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('emit book isbn', () => {
      component.item = itemMock
      const spy = jest.spyOn(component.readItem, 'emit').mockImplementation(() => ({}));
      component.onCheck({checked: true, source: undefined});
      expect(spy).toHaveBeenNthCalledWith(1, 198);
    });
  });
});
