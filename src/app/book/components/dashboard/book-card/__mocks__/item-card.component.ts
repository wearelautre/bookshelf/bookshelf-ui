import {Component, Input} from '@angular/core';
import {MinimalLibraryItem} from "../../../../../core/model/library-item";

@Component({
  selector: 'app-item-card',
  template: '<div>Mock item-card</div>'
})
export class MockItemCardComponent {
  @Input() item: MinimalLibraryItem;

}
