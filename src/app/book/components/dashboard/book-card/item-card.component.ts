import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DisplayImage} from '../../../../shared/display-image';
import {Image} from '../../../../shared/models/image';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {OidcSecurityService} from "angular-auth-oidc-client";
import {MinimalLibraryItemWithCover} from "../../../../core/model/library-item";

@Component({
  selector: 'app-item-card',
  template: `
    <div class="item-card" *ngIf="item">
      <div class="left-panel">
        <app-loader-img [img]="img">
        </app-loader-img>
      </div>
      <div class="right-panel">
        <div class="book-title">
          <div>
            <h3 class="title">
              {{ item.book.title }}
              <span *ngIf="!item.book.series?.oneShot && item.book.series?.tome !== null">
                (T{{ item.book.series?.tome }})
              </span>
            </h3>
            <app-series-name-display [series]="item.book.series"></app-series-name-display>
          </div>
          <div>
            <mat-checkbox *ngIf="isAuthenticated$ | async" disableRipple
                          (change)="onCheck($event)"></mat-checkbox>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .item-card {
      display: flex;
      flex-direction: row;
      gap: 1.5rem;
      padding-bottom: 0.5rem;

      .left-panel {
        width: 150px;
      }

      .right-panel {
        flex: 1;
        display: flex;
        flex-direction: column;
        place-content: stretch space-between;

        .book-title {
          display: flex;
          flex-direction: row;
          place-content: center space-between;

          .title {
            margin-bottom: 0;
          }
        }
      }
    }
  `]
})
export class ItemCardComponent extends DisplayImage implements OnInit {
  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  @Input() item: MinimalLibraryItemWithCover

  public img: Image;

  @Output() readItem: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private oidcSecurityService: OidcSecurityService,
  ) {
    super('/files/covers');
  }

  ngOnInit(): void {
    this.img = this.item?.book ? this.getImg(this.item.book.cover) : null;
  }

  onCheck(event: MatCheckboxChange) {
    if (event.checked) {
      this.readItem.emit(this.item.id)
    }
  }
}
