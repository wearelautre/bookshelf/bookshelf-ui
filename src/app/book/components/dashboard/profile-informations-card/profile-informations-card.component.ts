import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {ProfileService} from '../../../services/profile.service';
import {ItemStatus} from '../../../../shared/models/item-status';
import {LibraryCount} from "../../../../core/model/library-count";

@Component({
  selector: 'app-profile-informations-card',
  template: `
    <mat-card>
      <mat-card-header>
        <h1>{{ 'DASHBOARD.PROFILE_INFO.TITLE'| translate }}</h1>
      </mat-card-header>
      <mat-card-content fxLayout="column" fxLayoutAlign="space-around"
                        *ngIf="libraryCount$ | async as libraryCount">
        <mat-divider style="padding-bottom: 1rem"></mat-divider>
        <div fxFlex fxLayout="row" fxLayoutAlign="space-around" style="padding-bottom: 1rem">
          <div fxFlex fxLayout="row" fxLayoutAlign="start">
            <span style="font-size: 20px"> {{ libraryCount.booksCount }}
              <span class="mat-small"> {{ 'DASHBOARD.PROFILE_INFO.LIBRARY_STAT.BOOKS.LABEL' | translate }} </span>
          </span>
          </div>
          <div fxFlex fxLayout="row" fxLayoutAlign="start">
            <span style="font-size: 20px"> {{ libraryCount.bookTypesCount }}
              <span class="mat-small"> {{ 'DASHBOARD.PROFILE_INFO.LIBRARY_STAT.BOOK_TYPES.LABEL' | translate }} </span>
          </span>
          </div>
        </div>
        <mat-divider style="padding-bottom: 1rem"></mat-divider>
        <div fxFlex fxLayout="row" fxLayoutGap="1rem" fxLayoutAlign="center" style="padding-bottom: 1rem">
          <div fxFlex fxLayout="column" fxLayoutAlign="center center"
               *ngFor="let bookTypesBook of libraryCount.bookTypesBooks">
            <span style="font-size: 20px">{{ bookTypesBook.value }}</span>
            <span class="mat-small"> {{ bookTypesBook.key }} </span>
          </div>
        </div>
        <div fxFlex fxLayout="row" fxLayoutGap="1rem">
          <div fxFlex fxLayout="column" *ngIf="(bookStatus$ | async) as itemStatus">
            <mat-progress-bar mode="determinate"
                              [value]="getProgressBarValue(itemStatus)"></mat-progress-bar>
            <div style="padding-top: 0.5rem" fxLayout="row" fxLayoutAlign="center">
              <span class="mat-small">
                {{ getValue(itemStatus) }} {{ 'DASHBOARD.PROFILE_INFO.BOOKS.STATUS.PROGRESS_BAR.LABEL' | translate }}
              </span>
            </div>
          </div>
        </div>
      </mat-card-content>
    </mat-card>
  `
})
export class ProfileInformationsCardComponent {

  public libraryCount$: Observable<LibraryCount> = this.profileService.libraryCount$;
  public bookStatus$: Observable<ItemStatus> = this.profileService.itemsStatus$;

  constructor(
    private profileService: ProfileService
  ) {
  }

  getProgressBarValue(itemStatus: ItemStatus): number {
    return (itemStatus.value * 100) / itemStatus.total;
  }

  getValue(itemStatus: ItemStatus): number {
    return itemStatus.total - itemStatus.value;
  }
}
