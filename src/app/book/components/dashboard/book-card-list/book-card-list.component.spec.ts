import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookCardListComponent} from './book-card-list.component';
import {MatCardModule} from '@angular/material/card';
import {MockItemCardComponent} from '../book-card/__mocks__/item-card.component';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {MinimalLibraryItemWithCover} from "../../../../core/model/library-item";

describe('BookCardListComponent', () => {
  let component: BookCardListComponent;
  let fixture: ComponentFixture<BookCardListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BookCardListComponent, MockItemCardComponent],
      imports: [
        MatCardModule,
        MatExpansionModule,
        NgxTranslateTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('emit book isbn', () => {
      const spy = jest.spyOn(component.readItem, 'emit').mockImplementation(() => ({}));
      component.onReadItem(145);
      expect(spy).toHaveBeenNthCalledWith(1, 145);
    });

    test('getNextPage', () => {
      jest.clearAllMocks();
      const spy = jest.spyOn(component.nextPage, 'emit').mockImplementation(() => ({}));
      component.getNextPage();
      expect(spy).toHaveBeenNthCalledWith(1);
    });

    test('trackBy', () => {
      jest.clearAllMocks();
      expect(component.trackBy(2, {id: 45678} as MinimalLibraryItemWithCover)).toStrictEqual(45678);
    });
  });
});
