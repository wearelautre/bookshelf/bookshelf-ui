import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Book} from '../../../../../core/model/book';

@Component({
  selector: 'app-book-card-list',
  template: '<div>Mock Book Card List</div>'
})
export class MockBookCardListComponent {
  @Input() title: string;
  @Input() list: Book[] = [];
  @Input() nextPageDisabled = false;
  @Output() nextPage: EventEmitter<void> = new EventEmitter<void>();
}
