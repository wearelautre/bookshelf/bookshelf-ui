import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MinimalLibraryItemWithCover} from "../../../../core/model/library-item";

@Component({
  selector: 'app-book-card-list',
  template: `
    <mat-card>
      <mat-card-header>
        <h1>{{ title }}</h1>
      </mat-card-header>
      <mat-card-content>
        <div fxLayout="column">
          <app-item-card
            *ngFor="let item of list; trackBy:trackBy"
            [item]="item"
            (readItem)="onReadItem($event)"
            fxFlex>
          </app-item-card>
        </div>
      </mat-card-content>
      <mat-card-actions>
        <div fxFlex fxLayoutAlign="end" fxLayoutGap="10px">
          <button mat-button (click)="getNextPage()" [disabled]="nextPageDisabled">
            {{ 'DASHBOARD.BOOK_LIST.BUTTONS.DISPLAY_MORE'| translate }}
          </button>
        </div>
      </mat-card-actions>
    </mat-card>
  `
})
export class BookCardListComponent {
  @Input() title: string;
  @Input() list: MinimalLibraryItemWithCover[] = [];
  @Input() nextPageDisabled = false;
  @Output() nextPage: EventEmitter<void> = new EventEmitter<void>();
  @Output() readItem: EventEmitter<number> = new EventEmitter<number>();

  trackBy(index: number, item: MinimalLibraryItemWithCover) {
    return item.id
  }

  getNextPage() {
    this.nextPage.emit();
  }

  onReadItem(itemId: number) {
    this.readItem.emit(itemId)
  }
}
