import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-details-artists-list',
  template: `mock app-book-details-artists-list`
})
export class MockBookDetailsArtistsListComponent {

  @Input() contracts: any[];
}
