import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookDetailsArtistsListComponent} from './book-details-artists-list.component';
import {MockWebLinkListComponent} from "../../../../shared/web-link-list/__mocks__/web-link-list.component";
import {MatListModule} from '@angular/material/list';
import {RouterTestingModule} from "@angular/router/testing";

describe('BookDetailsArtistsListComponent', () => {
    let component: BookDetailsArtistsListComponent;
    let fixture: ComponentFixture<BookDetailsArtistsListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [BookDetailsArtistsListComponent, MockWebLinkListComponent],
            imports: [
                RouterTestingModule,
                MatListModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(BookDetailsArtistsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create the app', () => {
            expect(component).toBeTruthy();
        });
    });
});
