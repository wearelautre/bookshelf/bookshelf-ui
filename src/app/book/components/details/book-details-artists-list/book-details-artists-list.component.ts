import {Component, Input} from '@angular/core';
import {Contract} from "../../../../core/model/contract";

@Component({
  selector: 'app-book-details-artists-list',
  template: `
    <mat-list dense>
      <mat-list-item class="book-details-list-item" *ngFor="let contract of contracts" >
        <div fxFlex="100" fxLayout="column" fxLayoutAlign="start start">
          <h4 mat-line>
            <strong>{{contract.role.name | uppercase}}</strong>
          </h4>
          <span mat-line *ngFor="let artist of contract.artists"
                class="clickable"
                [routerLink]="['/artist', 'details', artist.id]">
            {{artist.name}}
            <app-web-link-list [webLinks]="artist.webLinks"></app-web-link-list>
          </span>
        </div>
      </mat-list-item>
    </mat-list>
  `,
  styles: [`
    h4 {
      margin: 0;
    }
    span {
      opacity: 0.5;
    }
  `]
})
export class BookDetailsArtistsListComponent {

  @Input() contracts: Contract[];
}
