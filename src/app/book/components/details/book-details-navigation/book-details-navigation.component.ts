import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-details-navigation',
  template: `
    <div fxLayoutGap="10px" class="nav-button">
      <button
        type="button" mat-flat-button
        [disabled]="previous === null"
        [routerLink]="['/', 'book', 'list', 'detail', previous]"
        [queryParamsHandling]="'preserve'">
        <fa-icon [icon]="['fas', 'chevron-left']"></fa-icon>
        <span style="padding-left: 10px">{{'BOOK.LIST.DETAILS.BUTTON.PREVIOUS' | translate}}</span>
      </button>
      <button
        type="button" mat-flat-button
        [disabled]="next === null"
        [routerLink]="['/', 'book', 'list', 'detail', next]"
        [queryParamsHandling]="'preserve'">
        <span>{{'BOOK.LIST.DETAILS.BUTTON.NEXT' | translate}}</span>
        <fa-icon style="padding-left: 10px" [icon]="['fas', 'chevron-right']"></fa-icon>
      </button>
    </div>`
})
export class BookDetailsNavigationComponent {

  @Input() next: string | null = null
  @Input() previous: string | null = null

}
