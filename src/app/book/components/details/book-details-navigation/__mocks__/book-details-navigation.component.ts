import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-details-navigation',
  template: `mock app-book-details-navigation`
})
export class MockBookDetailsNavigationComponent {

  @Input() next: any
  @Input() previous: any

}
