import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-details-header',
  template: `mock app-book-details-header`
})
export class MockBookDetailsHeaderComponent {

  @Input() book: any;
  @Input() nextIsbn: any;
  @Input() previousIsbn: any;

}
