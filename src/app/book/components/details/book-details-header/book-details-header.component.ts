import {Component, Input} from '@angular/core';
import {Book} from "../../../../core/model/book";

@Component({
  selector: 'app-book-details-header',
  template: `
    <div fxLayoutAlign="space-between center">
      <div fxFlex=" 70">
        <app-title-display
          [series]="book.series"
          [title]="book.title"
          [class]="'mat-h2'">
        </app-title-display>
        <app-book-status [status]="book.status"></app-book-status>
      </div>
      <div fxFlex fxLayoutAlign="center center">
        <app-book-details-navigation
          [next]="nextIsbn"
          [previous]="previousIsbn">
        </app-book-details-navigation>
      </div>
    </div>
  `,
  styles: [`
    app-book-status {
      display: block;

      ::ng-deep {
        .status-label {
          opacity: 0.75;
        }
      }
    }
  `]
})
export class BookDetailsHeaderComponent {

  @Input() book: Book;
  @Input() nextIsbn: string;
  @Input() previousIsbn: string;

}
