import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookDetailsGlobalInformationComponent} from './book-details-global-information.component';
import {MockMultiLineListItemComponent} from "../../list/multi-line-list-item/__mocks__/multi-line-list-item.component";
import {MatListModule} from '@angular/material/list';
import {BookImpl} from "../../../../core/model/impl/book-impl";

describe('BookDetailsGlobalInformationComponent', () => {
    let component: BookDetailsGlobalInformationComponent;
    let fixture: ComponentFixture<BookDetailsGlobalInformationComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [BookDetailsGlobalInformationComponent, MockMultiLineListItemComponent],
            imports: [
                MatListModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(BookDetailsGlobalInformationComponent);
        component = fixture.componentInstance;
        component.book = new BookImpl();
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create the app', () => {
            expect(component).toBeTruthy();
        });
    });
});
