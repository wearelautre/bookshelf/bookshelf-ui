import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-details-global-information',
  template: `mock app-book-details-global-information`
})
export class MockBookDetailsGlobalInformationComponent {

  @Input() book: any;

}
