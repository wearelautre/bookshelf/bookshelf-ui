import {Component, Input} from '@angular/core';
import {Book} from "../../../../core/model/book";

@Component({
  selector: 'app-book-details-global-information',
  template: `
    <mat-list dense>
      <app-multi-line-list-item [label]="'TITLE'" [value]="book.title"></app-multi-line-list-item>
      <app-multi-line-list-item *ngIf="!book.series.oneShot" [label]="'SERIES'"
                                [value]="book.series.name"></app-multi-line-list-item>
      <app-multi-line-list-item *ngIf="book.series.seriesCycle" [label]="'CYCLE'"
                                [value]="book.series.seriesCycle.name"></app-multi-line-list-item>
      <app-multi-line-list-item *ngIf="book.series.tome" [label]="'TOME'"
                                [value]="book.series.tome"></app-multi-line-list-item>
      <app-multi-line-list-item *ngIf="book.series.seriesCycle" [label]="'CYCLE_TOME'"
                                [value]="book.series.seriesCycle.tome"></app-multi-line-list-item>
      <app-multi-line-list-item [label]="'YEAR'" [value]="book.year"></app-multi-line-list-item>
      <app-multi-line-list-item [label]="'EDITOR'" [value]="book.editor.name"></app-multi-line-list-item>
    </mat-list>
  `
})
export class BookDetailsGlobalInformationComponent {

  @Input() book: Book;

}
