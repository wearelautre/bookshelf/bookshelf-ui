import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MultiLineListItemComponent} from './multi-line-list-item.component';
import {NgxTranslateTestingModule} from "../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MatListModule} from '@angular/material/list';

describe('MultiLineListItemComponent', () => {
    let component: MultiLineListItemComponent;
    let fixture: ComponentFixture<MultiLineListItemComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MultiLineListItemComponent],
            imports: [
                NgxTranslateTestingModule,
                MatListModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MultiLineListItemComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create the app', () => {
            expect(component).toBeTruthy();
        });
    });
});
