import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListActionBarComponent} from './book-list-action-bar.component';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {BookFilterService} from "../../../services/book-filter.service";
import {bookFilterServiceMock} from "../../../services/__mocks__/book-filter.service";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {SeriesAdministrationService} from "../../../../administration/services/series-administration.service";
import {BookTypeAdministrationService} from "../../../../administration/services/book-type-administration.service";
import {EditorAdministrationService} from "../../../../administration/services/editor-administration.service";
import {
    editorAdministrationServiceMock
} from "../../../../administration/services/__mocks__/editor-administration.service";
import {
    seriesAdministrationServiceMock
} from "../../../../administration/services/__mocks__/series-administration.service";
import {
    bookTypeAdministrationServiceMock
} from "../../../../administration/services/__mocks__/book-type-administration.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {of, Subscription} from "rxjs";
import clearAllMocks = jest.clearAllMocks;

Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    })),
});


describe('BookListActionBarComponent', () => {
    let component: BookListActionBarComponent;
    let fixture: ComponentFixture<BookListActionBarComponent>;

    let subscriptions: Subscription[] = [];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BookListActionBarComponent],
            imports: [
                NoopAnimationsModule,
                FontAwesomeTestingModule,
                FlexLayoutModule,
                NgxTranslateTestingModule,
                MatSlideToggleModule,
                MatButtonModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatOptionModule,
                MatSelectModule
            ],
            providers: [
                {provide: BookFilterService, useValue: bookFilterServiceMock},
                {provide: EditorAdministrationService, useValue: editorAdministrationServiceMock},
                {provide: SeriesAdministrationService, useValue: seriesAdministrationServiceMock},
                {provide: BookTypeAdministrationService, useValue: bookTypeAdministrationServiceMock}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BookListActionBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();


        component.form.reset({
            bookTypes: [],
            groups: [],
            series: []
        })
        bookFilterServiceMock.groupByEditors$.next(false)

    });

    afterEach(() => {
        subscriptions.forEach((subscription) => subscription.unsubscribe());
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        test('changeSortOrderAction', () => {
            component.changeSortOrderAction();
            expect(bookFilterServiceMock.changeSortOrder).toHaveBeenNthCalledWith(1);
        });
        test('changeDisplayAction', () => {
            component.changeDisplayAction({source: null, checked: true});
            expect(bookFilterServiceMock.changeDisplay).toHaveBeenNthCalledWith(1, true);
        });
        describe('getGroups', () => {
            test('onInit', waitForAsync(() => {
                clearAllMocks();
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => of({body: [{name: 'test'}, {name: 'azerty'}]}))
                subscriptions.push(
                    component[`getGroups`]().subscribe(groups => {
                        expect(groups).toStrictEqual(['t', 'a'])
                        expect(seriesAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [], {
                            "direction": "ASC",
                            "updateList": true,
                            "withLoader": true
                        })
                    })
                );
            }));
            test('BookType Changes and not grouped by editor', waitForAsync(() => {
                clearAllMocks();
                component.form.patchValue({bookTypes: ['b1', 'b2']})
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => of({body: [{name: 'test'}, {name: 'azerty'}]}))
                subscriptions.push(
                    component[`getGroups`]().subscribe(groups => {
                        expect(groups).toStrictEqual(['t', 'a'])
                        expect(seriesAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [{
                            name: 'bookType', operation: ':', value: `[b1,b2]`,
                        }], {"direction": "ASC", "updateList": true, "withLoader": true})
                    })
                );
            }));
            test('BookType Changes and grouped by editor', waitForAsync(() => {
                component.form.patchValue({bookTypes: ['b1', 'b2']})
                clearAllMocks();
                bookFilterServiceMock.groupByEditors$.next(true)
                editorAdministrationServiceMock.searchWithOption.mockImplementation(() => of({body: [{name: 'test'}, {name: 'azerty'}]}))
                subscriptions.push(
                    component[`getGroups`]().subscribe(groups => {
                        expect(groups).toStrictEqual(['test', 'azerty'])
                        expect(editorAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [{
                            name: 'bookType', operation: ':', value: `[b1,b2]`,
                        }], {"direction": "ASC", "updateList": true, "withLoader": true})
                    })
                );
            }));
        });
        describe('series', () => {
            test('onInit', waitForAsync(() => {
                clearAllMocks();
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => of({body: [{name: 'test'}, {name: 'azerty'}]}))
                subscriptions.push(
                    component.series.subscribe(series => expect(series).toStrictEqual([]))
                );
            }));
            test('groups Changes and not grouped by editor', waitForAsync(() => {
                clearAllMocks();
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => {
                    seriesAdministrationServiceMock.list$.next([{name: 'test'}, {name: 'azerty'}])
                    return of({body: [{name: 'test'}, {name: 'azerty'}]})
                })
                component.form.patchValue({bookTypes: ['test', 'toto']}, {emitEvent: false})
                component.form.patchValue({status: ['UNREAD', 'READ']}, {emitEvent: false})
                component.form.patchValue({groups: ['a', 'b']})
                subscriptions.push(
                    component.series.subscribe(series => {
                        expect(seriesAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [{
                            name: 'bookType', operation: ':', value: `[test,toto]`, or: false
                        }, {
                            name: 'status', operation: ':', value: `UNREAD`, or: false
                        }, {
                            name: 'status', operation: ':', value: `READ`, or: true
                        }, {
                            name: 'name', operation: ':', value: `a*`, or: false
                        }, {
                            name: 'name', operation: ':', value: `b*`, or: true
                        }], {"direction": "ASC", "updateList": true, "withLoader": true})
                        expect(series).toStrictEqual(['test', 'azerty']);
                    })
                );
            }));
            test('groups Changes and not grouped by editor with bookTypes null', waitForAsync(() => {
                clearAllMocks();
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => {
                    seriesAdministrationServiceMock.list$.next([{name: 'test'}, {name: 'azerty'}])
                    return of({body: [{name: 'test'}, {name: 'azerty'}]})
                })
                component.form.patchValue({bookTypes: null}, {emitEvent: false})
                component.form.patchValue({status: null}, {emitEvent: false})
                component.form.patchValue({groups: ['a', 'b']})
                subscriptions.push(
                    component.series.subscribe(series => {
                        expect(seriesAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [{
                            name: 'name', operation: ':', value: `a*`, or: false
                        }, {
                            name: 'name', operation: ':', value: `b*`, or: true
                        }], {"direction": "ASC", "updateList": true, "withLoader": true})
                        expect(series).toStrictEqual(['test', 'azerty']);
                    })
                );
            }));
            test('groups Changes and grouped by editor', waitForAsync(() => {
                seriesAdministrationServiceMock.searchWithOption.mockImplementation(() => {
                    seriesAdministrationServiceMock.list$.next([{name: 'test'}, {name: 'azerty'}])
                    return of({body: [{name: 'test'}, {name: 'azerty'}]})
                });
                component.form.patchValue({groups: ['b1', 'b2']})
                clearAllMocks();
                bookFilterServiceMock.groupByEditors$.next(true)
                subscriptions.push(
                    component.series.subscribe(series => {
                        expect(seriesAdministrationServiceMock.searchWithOption).toHaveBeenNthCalledWith(1, [{
                            name: 'editor', operation: ':', value: `b1`, or: false
                        }, {
                            name: 'editor', operation: ':', value: `b2`, or: true
                        }], {"direction": "ASC", "updateList": true, "withLoader": true})
                        expect(series).toStrictEqual(['test', 'azerty']);
                    })
                );
            }));
        });
    });
});

