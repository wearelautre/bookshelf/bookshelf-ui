import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {Observable, Subscription} from 'rxjs';
import {BookFilterService} from "../../../services/book-filter.service";
import {BookType} from "../../../../core/model/book-type";
import {BookTypeAdministrationService} from "../../../../administration/services/book-type-administration.service";
import {AbstractControl, UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {EditorAdministrationService} from "../../../../administration/services/editor-administration.service";
import {SeriesAdministrationService} from "../../../../administration/services/series-administration.service";
import {combineLatestWith, map, startWith, switchMap, tap} from "rxjs/operators";
import {SearchOptionsImpl} from "../../../../shared/services/entity.service";
import {Book, BookStatus} from "../../../../core/model/book";


@Component({
    selector: 'app-book-list-action-bar',
    templateUrl: './book-list-action-bar.component.html'
})
export class BookListActionBarComponent implements OnInit, OnDestroy {

    get statusValues(): BookStatus[] {
        return Book.BookStatus.values()
    }

    form: UntypedFormGroup = this.fb.group({
        status: this.fb.control([]),
        bookTypes: this.fb.control([]),
        groups: this.fb.control([]),
        series: this.fb.control([])
    });

    public isGroupedByEditors$: Observable<boolean> = this.bookFilterService.groupByEditors$;
    public isSortOrderAsc$: Observable<boolean> = this.bookFilterService.isSortOrderAsc$;
    public bookTypes$: Observable<BookType[]> = this.bookTypeService.list$;

    public groups: Observable<string[]> = this.getGroups()
        .pipe(tap((groups) => this.cleanForm(this.form.get('groups'), groups)))

    public series: Observable<string[]> = this.seriesService.list$
        .pipe(map(values => values.map(value => value.name)), tap((series) => this.cleanForm(this.form.get('series'), series)));

    constructor(
        private fb: UntypedFormBuilder,
        private bookFilterService: BookFilterService,
        private editorService: EditorAdministrationService,
        private seriesService: SeriesAdministrationService,
        private bookTypeService: BookTypeAdministrationService
    ) {
    }

    protected subscriptions: Subscription[] = [];

    ngOnInit(): void {
        this.subscriptions.push(
            this.bookTypeService.search().subscribe(),
            this.form.valueChanges.subscribe(value => this.bookFilterService.updateFilter(value)),

            this.bookFilterService.groupByEditors$.pipe(
                combineLatestWith<any, any[]>(this.form.get('groups').valueChanges.pipe(startWith([]))),
            )
                .subscribe(([groupByEditors, groups]) => {
                    const bookTypes = this.form.get('bookTypes').value || []
                    const bookTypesCriteriaList = bookTypes.length > 0 ? [{
                        name: 'bookType',
                        operation: ':',
                        value: `[${bookTypes.join(',')}]`,
                        or: false
                    }] : []

                    this.seriesService.searchWithOption(
                        [
                            ...bookTypesCriteriaList,
                            ...(this.form.get('status').value || [] as []).map((value, index) => ({
                                name: 'status',
                                operation: ':',
                                value,
                                or: index > 0
                            })),
                            ...(groups as []).map((value, index) => ({
                                name: groupByEditors ? 'editor' : 'name',
                                operation: ':',
                                value: groupByEditors ? `${value}` : `${value}*`,
                                or: index > 0
                            }))
                        ],
                        new SearchOptionsImpl())
                        .subscribe()
                })
        );
    }

    changeSortOrderAction() {
        this.bookFilterService.changeSortOrder();
    }

    changeDisplayAction($event: MatSlideToggleChange) {
        this.bookFilterService.changeDisplay($event.checked);
    }

    private getGroups(): Observable<string[]> {
        return this.bookFilterService.groupByEditors$.pipe(
            combineLatestWith<any, any[]>(this.form.get('bookTypes').valueChanges.pipe(startWith([]))),
            switchMap(([groupByEditors, bookTypes]) => {
                const searchCriteriaList = bookTypes.length > 0 ? [{
                    name: 'bookType', operation: ':', value: `[${bookTypes.join(',')}]`,
                }] : []
                if (groupByEditors) {
                    return this.editorService.searchWithOption(searchCriteriaList, new SearchOptionsImpl())
                        .pipe(map(values => values.body.map(value => value.name)))
                } else {
                    return this.seriesService.searchWithOption(searchCriteriaList, new SearchOptionsImpl())
                        .pipe(map(values => Array.from(new Set(values.body.map(value => value.name.charAt(0))))))
                }
            }))
    }

    private cleanForm(control: AbstractControl, data: string[]) {
        control.patchValue(control.value.filter(d => data.indexOf(d) >= 0))
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }
}
