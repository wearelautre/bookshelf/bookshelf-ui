import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {GroupDisplayComponent} from './group-display.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BookService} from '../../../services/book.service';
import {bookServiceMock} from '../../../services/__mocks__/book.service';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatExpansionModule} from '@angular/material/expansion';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {SeriesDisplayComponent} from '../series-display/series-display.component';
import {MockSeriesDisplayComponent} from '../series-display/__mocks__/series-display.component';

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

describe('GroupDisplayComponent', () => {
  let component: GroupDisplayComponent;
  let fixture: ComponentFixture<GroupDisplayComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GroupDisplayComponent, MockSeriesDisplayComponent],
      imports: [
        FlexLayoutModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatExpansionModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('BehaviorSubject => Observable', () => {
      test('orderedSeries', waitForAsync(() => {
        component[`seriesList`].next(['s1']);
        component.seriesList$.subscribe(list => expect(list).toStrictEqual(['s1']));
      }));
      test('bookBySeries', waitForAsync(() => {
        const m = new Map();
        m.set('S', {s: 0, books: []});
        component[`booksBySeries`].next(m);
        component.booksBySeries$.subscribe(map => expect(map).toStrictEqual(m));
      }));
    });
    describe('setter seriesContainer', () => {
      test('set null value', waitForAsync(() => {
        const spySeriesList = jest.spyOn<any, any>(component[`seriesList`], `next`);
        const spyBooksBySeries = jest.spyOn<any, any>(component[`booksBySeries`], `next`);
        component.seriesContainer = null;
        expect(spySeriesList).toHaveBeenCalledTimes(0);
        expect(spyBooksBySeries).toHaveBeenCalledTimes(0);
      }));
      test('set non null value', waitForAsync(() => {
        const m = new Map();
        m.set('S', {
          status: {
            possessedCount: 0, status: undefined, totalCount: 0
          }, books: []
        });

        const spySeriesList = jest.spyOn<any, any>(component[`seriesList`], `next`);
        const spyBooksBySeries = jest.spyOn<any, any>(component[`booksBySeries`], `next`);
        component.seriesContainer = m;
        expect(spySeriesList).toHaveBeenNthCalledWith(1, ['S']);
        expect(spyBooksBySeries).toHaveBeenNthCalledWith(1, m);
      }));
    });
    describe('isAllPanelOpened', () => {
      test('should return true', waitForAsync(() => {
        component.series = [];
        component.series.push(TestBed.createComponent(MockSeriesDisplayComponent).componentInstance as SeriesDisplayComponent);
        component.series.push(TestBed.createComponent(MockSeriesDisplayComponent).componentInstance as SeriesDisplayComponent);

        const spy1 = jest.spyOn(component.series[0], `getPanelState`).mockImplementation(() => true);
        const spy2 = jest.spyOn(component.series[1], `getPanelState`).mockImplementation(() => true);

        expect(component.isAllPanelOpened()).toBeTruthy();
        expect(spy1).toHaveBeenCalledTimes(1);
        expect(spy2).toHaveBeenCalledTimes(1);
      }));
    });
    test('should return false', waitForAsync(() => {
      component.series = [];
      component.series.push(TestBed.createComponent(MockSeriesDisplayComponent).componentInstance as SeriesDisplayComponent);
      component.series.push(TestBed.createComponent(MockSeriesDisplayComponent).componentInstance as SeriesDisplayComponent);

      const spy1 = jest.spyOn(component.series[0], `getPanelState`).mockImplementation(() => true);
      const spy2 = jest.spyOn(component.series[1], `getPanelState`).mockImplementation(() => false);

      expect(component.isAllPanelOpened()).toBeFalsy();
      expect(spy1).toHaveBeenCalledTimes(1);
      expect(spy2).toHaveBeenCalledTimes(1);
    }));
  });
});
