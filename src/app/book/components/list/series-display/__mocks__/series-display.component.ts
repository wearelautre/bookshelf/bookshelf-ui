import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-series-display',
  template: '<div>Mock Series Display</div>'
})
export class MockSeriesDisplayComponent {

  @Input()
  series: string;

  @Input()
  seriesData: any;

  getPanelState(): boolean {
    return true;
  }

}
