import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {SeriesInfo} from '../../../../core/model/series-by-group-container';
import {Book} from '../../../../core/model/book';
import {MatExpansionPanel} from '@angular/material/expansion';
import {MatListOption} from '@angular/material/list';
import {BookService} from '../../../services/book.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {Observable} from 'rxjs';
import {filter, map} from "rxjs/operators";
import {LendModalComponent} from "../../../../shared/component/lend-modal/lend-modal.component";
import {TranslateService} from "@ngx-translate/core";
import {NotificationService} from "../../../../shared/services/notification.service";
import {SelectionModel} from "@angular/cdk/collections";
import {MatDialog} from "@angular/material/dialog";
import BookStatusEnum = Book.StatusEnum;

type EnumValues = keyof typeof BookStatusEnum;

@Component({
  selector: 'app-series-display',
  templateUrl: './series-display.component.html',
  styleUrls: ['./series-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SeriesDisplayComponent implements OnInit {

  display = false;

  @ViewChild(MatExpansionPanel) matExpansionPanel!: MatExpansionPanel;

  @Input()
  series: string | null = null;

  readingState: string

  @Input()
  seriesData: SeriesInfo = {
    status: {status: "UNKNOWN", totalCount: 0, possessedCount: 0, readCount: 0},
    books: [],
    oneShot: false
  };

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  get statusValues(): EnumValues[] {
    return Book.BookStatus.values()
  }

  constructor(
    public dialog: MatDialog,
    private oidcSecurityService: OidcSecurityService,
    private bookService: BookService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
  ) {
  }

  ngOnInit() {
    this.readingState = this.getSeriesReadingStatus();
  }

  private getSeriesReadingStatus(): string {
    let readingStatusKey;
    if (this.getProgressTotalValue() !== null && this.getProgressTotalValue() <= 0) {
      readingStatusKey = 'NOT_STARTED';
    } else if (this.getProgressTotalValue() < 100 && this.getProgressTotalValue() > 0) {
      readingStatusKey = 'IN_PROGRESS';
    } else if (this.getProgressTotalValue() >= 100) {
      readingStatusKey = 'COMPLETED';
    } else {
      readingStatusKey = 'UNKNOWN';
    }
    return this.translateService.instant(`BOOK.LIST.SERIES.READING_STATUS.${readingStatusKey}`);
  }

  getReadBooksCount() {
    return this.seriesData.status.readCount;
  }

  getPossessedCount() {
    return this.seriesData.status?.possessedCount ?? null;
  }

  getProgressReadValue(): number {
    if (this.seriesData.status) {
      if (this.seriesData.status.totalCount <= 0) {
        return -1;
      }

      if (this.seriesData.status.possessedCount > 0) {
        return (this.getReadBooksCount() * 100) / this.seriesData.status.possessedCount;
      }
    }

    return 0;
  }

  getProgressTotalValue(): number {
    if (this.seriesData.status) {
      if (this.seriesData.status.totalCount <= 0) {
        return -1;
      }

      if (this.seriesData.status.possessedCount > 0) {
        return (this.getReadBooksCount() * 100) / this.seriesData.status.totalCount;
      }
    }
    return 0;
  }

  getProgressPossessedValue(): number {
    if (this.seriesData.status && this.seriesData.status.possessedCount > 0) {
      return this.seriesData.status.possessedCount * 100 / this.seriesData.status.totalCount
    }
    return 0;
  }

  getPanelState(): boolean {
    return this.matExpansionPanel.expanded;
  }

  changeBookState(selected: MatListOption[], newState: EnumValues) {
    this.bookService.bulkPatch(selected.map(matOption => {
      const book: Book = {...matOption.value};
      book.status = newState;
      return book;
    })).subscribe();
  }

  displayProgress(): boolean {
    return this.seriesData.status && this.seriesData.status.possessedCount > 0 && this.seriesData.status.totalCount > 0;
  }

  displayBooks($event: boolean) {
    if ($event) {
      this.display = true
    }
  }

  hideBooks() {
    this.display = false
  }

  openLendModal(selectedOption: MatListOption) {
    this.dialog
      .open<LendModalComponent, boolean>(LendModalComponent, {
        data: selectedOption.value,
        width: '500px'
      })
      .afterClosed()
      .pipe(filter(res => res !== 'CANCEL'))
      .subscribe(({state, name, title}: { state: string, name: string, title: string }) =>
        this.notificationService.displayMessage(`LOAN.CREATION.NOTIFICATION.${state}`, {name, title})
      );
  }

  disableLendButton(selectedOptions: SelectionModel<MatListOption>) {
    return !(selectedOptions.selected.length === 1 && !selectedOptions.selected[0].value.lent)
  }

  getButtonText(selectedOptions: SelectionModel<MatListOption>) {
    let key = 'BOOK.LIST.SERIES.ACTION_ROW.LEND';
    if (selectedOptions.selected.length === 1 && selectedOptions.selected[0].value.lent) {
      key = 'BOOK.LIST.SERIES.ACTION_ROW.ALREADY_LENT'
    }
    return this.translateService.instant(key);
  }
}
