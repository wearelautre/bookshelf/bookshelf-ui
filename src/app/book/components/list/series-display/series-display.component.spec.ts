import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SeriesDisplayComponent} from './series-display.component';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule, MatListOption} from '@angular/material/list';
import {bookServiceMock} from '../../../services/__mocks__/book.service';
import {BookService} from '../../../services/book.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {RouterTestingModule} from '@angular/router/testing';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {MatDialogModule} from "@angular/material/dialog";
import {of} from "rxjs";
import {LendModalComponent} from "../../../../shared/component/lend-modal/lend-modal.component";
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {
  MockAutoCompleteInputComponent
} from "../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";
import {SelectionModel} from "@angular/cdk/collections";
import {ElementRef} from "@angular/core";
import spyOn = jest.spyOn;

export class MatListOptionMock extends MatListOption {
  constructor(private v = null) {
    super(new ElementRef<HTMLElement>({nodeName: 'tutu'} as HTMLElement),
      undefined,
      undefined,
      undefined,
      undefined);
  }

  get value() {
    return this.v;
  }
}

describe('SeriesDisplayComponent', () => {
  let component: SeriesDisplayComponent;
  let fixture: ComponentFixture<SeriesDisplayComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesDisplayComponent, MockAutoCompleteInputComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatMenuModule,
        MatDialogModule,
        AngularAuthOidcClientTestingModule,
        MatListModule,
        MatSnackBarModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('get enum values', waitForAsync(() => {
      jest.clearAllMocks();
      expect(component.statusValues).toStrictEqual(['READ', 'UNREAD', 'READING'])
    }));
    test('should return the number of read books', waitForAsync(() => {
      // return this.seriesData.books.filter(b => b.status === 'READ').length;
      component.seriesData.status.readCount = 1;
      expect(component.getReadBooksCount()).toStrictEqual(1);
    }));
    describe('getProgressReadValue', () => {
      test('should return 0: status null', waitForAsync(() => {
        component.seriesData.status = null;
        expect(component.getProgressReadValue()).toEqual(0);
      }));
      test('should return -1', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 0, possessedCount: 0, readCount: 0};
        expect(component.getProgressReadValue()).toStrictEqual(-1);
      }));
      test('should return 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 1, possessedCount: 0, readCount: 0};
        expect(component.getProgressReadValue()).toStrictEqual(0);
      }));
      test('should return % of of read books', waitForAsync(() => {
        // return this.seriesData.books.filter(b => b.status === 'READ').length;
        component.seriesData.status = {
          possessedCount: 2, status: undefined, totalCount: 2, readCount: 1
        };
        expect(component.getProgressReadValue()).toStrictEqual(50);
      }));
    });
    describe('getPossessedCount', () => {
      test('should return null: status null', waitForAsync(() => {
        component.seriesData.status = null;
        expect(component.getPossessedCount()).toBeNull();
      }));
      test('should return the data', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 0, possessedCount: 6, readCount: 0};
        expect(component.getPossessedCount()).toEqual(6);
      }));
    });
    describe('getProgressTotalValue', () => {
      test('should return 0: status null', waitForAsync(() => {
        component.seriesData.status = null;
        expect(component.getProgressTotalValue()).toEqual(0);
      }));
      test('should return -1', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 0, possessedCount: 0, readCount: 0};
        expect(component.getProgressTotalValue()).toStrictEqual(-1);
      }));
      test('should return 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 1, possessedCount: 0, readCount: 0};
        expect(component.getProgressTotalValue()).toStrictEqual(0);
      }));
      test('should return % of of read books', waitForAsync(() => {
        // return this.seriesData.books.filter(b => b.status === 'READ').length;
        component.seriesData.status = {
          possessedCount: 2, status: undefined, totalCount: 4, readCount: 1
        };
        expect(component.getProgressTotalValue()).toStrictEqual(25);
      }));
    });
    test('should return panel state', waitForAsync(() => {
      const spy = spyOn(component[`matExpansionPanel`], `expanded`, 'get').mockImplementation(() => true);
      expect(component.getPanelState()).toBeTruthy();
      expect(spy).toHaveBeenCalledTimes(1);
    }));
    test('should call update state for each selected book', waitForAsync(() => {
      const b1 = {
        title: 'title1',
        editor: {name: 'editor', id: undefined},
        bookType: 'BD',
        contracts: [],
        series: null,
        status: 'READ'
      };

      component.changeBookState([new MatListOptionMock(b1)], 'UNREAD');
      expect(bookServiceMock.bulkPatch).toHaveBeenNthCalledWith(1, [{...b1, status: 'UNREAD'}]);
    }));
    test('should set true to display', waitForAsync(() => {
      component.displayBooks(true);
      expect(component.display).toBeTruthy();
    }));
    test('should not change display', waitForAsync(() => {
      component.displayBooks(true);
      component.displayBooks(false);
      expect(component.display).toBeTruthy();
    }));
    test('should not change display', waitForAsync(() => {
      component.displayBooks(true);
      component.hideBooks();
      expect(component.display).toBeFalsy();
    }));
    test('open lend modal', waitForAsync(() => {
      const matListOption = new MatListOptionMock({test: 'toto'})
      const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(false)}));
      component.openLendModal(matListOption);
      expect(spyDialog).toHaveBeenNthCalledWith(1, LendModalComponent, {
        data: {test: 'toto'},
        width: '500px'
      });
    }));
    describe('disableLendButton', () => {
      test('Yes : 1 selected and not already lent', waitForAsync(() => {
        const matListOption = new MatListOptionMock({lent: false})
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption]);
        expect(component.disableLendButton(selectedOptions)).toBeFalsy();
      }));
      test('No : 2 selected ', waitForAsync(() => {
        const matListOption1 = new MatListOptionMock({lent: false})
        matListOption1[`_value`] = {lent: false}
        const matListOption2 = new MatListOptionMock({lent: false})
        matListOption2[`_value`] = {lent: false}
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption1, matListOption2]);
        expect(component.disableLendButton(selectedOptions)).toBeTruthy();
      }));
      test('No : 1 selected and already lent', waitForAsync(() => {
        const matListOption = new MatListOptionMock({lent: true})
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption]);
        expect(component.disableLendButton(selectedOptions)).toBeTruthy();
      }));
    });
    describe('getButtonText', () => {
      test('Yes : 1 selected and not already lent', waitForAsync(() => {
        const matListOption = new MatListOptionMock({lent: false})
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption]);
        expect(component.getButtonText(selectedOptions)).toEqual('BOOK.LIST.SERIES.ACTION_ROW.LEND');
      }));
      test('No : 2 selected ', waitForAsync(() => {
        const matListOption1 = new MatListOptionMock({lent: false})
        const matListOption2 = new MatListOptionMock({lent: false})
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption1, matListOption2]);
        expect(component.getButtonText(selectedOptions)).toEqual('BOOK.LIST.SERIES.ACTION_ROW.LEND');
      }));
      test('No : 1 selected and already lent', waitForAsync(() => {
        const matListOption = new MatListOptionMock({lent: true})
        const selectedOptions = new SelectionModel<MatListOption>(true, [matListOption]);
        expect(component.getButtonText(selectedOptions)).toEqual('BOOK.LIST.SERIES.ACTION_ROW.ALREADY_LENT');
      }));
    });
    describe('displayProgress', () => {
      test('should return false: status null', waitForAsync(() => {
        component.seriesData.status = null;
        expect(component.displayProgress()).toBeFalsy();
      }));
      test('should return false: possessedCount = 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 1, possessedCount: 0, readCount: 0};
        expect(component.displayProgress()).toBeFalsy();
      }));
      test('should return false: totalCount = 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 0, possessedCount: 1, readCount: 0};
        expect(component.displayProgress()).toBeFalsy();
      }));
      test('should return true: totalCount = 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 1, possessedCount: 1, readCount: 0};
        expect(component.displayProgress()).toBeTruthy();
      }));
    });
    describe('getProgressPossessedValue', () => {
      test('should return 50%', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 10, possessedCount: 5, readCount: 0};
        expect(component.getProgressPossessedValue()).toEqual(50);
      }));
      test('should return 0: status null', waitForAsync(() => {
        component.seriesData.status = null;
        expect(component.getProgressPossessedValue()).toEqual(0);
      }));
      test('should return 0: possessedCount = 0', waitForAsync(() => {
        component.seriesData.status = {status: "UNKNOWN", totalCount: 1, possessedCount: 0, readCount: 0};
        expect(component.getProgressPossessedValue()).toEqual(0);
      }));
    });
    describe('getSeriesReadingStatus', () => {
      test('should return NOT_STARTED => -1', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(-1);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.NOT_STARTED');
      }));
      test('should return NOT_STARTED => 0', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(0);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.NOT_STARTED');
      }));
      test('should return IN_PROGRESS => 1', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(1);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.IN_PROGRESS');
      }));
      test('should return IN_PROGRESS => 99', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(99);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.IN_PROGRESS');
      }));
      test('should return COMPLETED => 100', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(100);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.COMPLETED');
      }));
      test('should return COMPLETED => 12345', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockReturnValue(12345);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.COMPLETED');
      }));
      test('should return UNKNOWN => null', waitForAsync(() => {
        spyOn(component, `getProgressTotalValue`).mockImplementation(() => null);
        expect(component[`getSeriesReadingStatus`]()).toEqual('BOOK.LIST.SERIES.READING_STATUS.UNKNOWN');
      }));
    });
  });
});
