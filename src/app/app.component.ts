import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {AfterViewInit, Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {ScrollEventService} from "./shared/services/scroll-event.service";
import {filter, map, pairwise, throttleTime} from "rxjs/operators";

@Component({
  selector: 'app-root',
  template: `
    <div fxLayout="column" fxLayoutAlign="" class="wrapper">
      <app-header></app-header>
      <div fxLayout="column" class="content-wrapper">
        <cdk-virtual-scroll-viewport [ngClass]="{'activated-scroll': scrollActivated$ | async}"
          #scroller itemSize="1000"
          class="page" fxLayout="space-between">
          <div>
            <app-loader-bar></app-loader-bar>
            <div>
              <router-outlet></router-outlet>
            </div>
          </div>
          <div class="footer">
            <app-footer></app-footer>
          </div>
        </cdk-virtual-scroll-viewport>
      </div>
    </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('scroller') scroller: CdkVirtualScrollViewport;

  scrollActivated$ = this.scrollEventService.scrollActivated$

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private scrollEventService: ScrollEventService,
    private ngZone: NgZone
  ) {
  }

  ngOnInit() {
    this.oidcSecurityService.checkAuth().subscribe();
  }

  ngAfterViewInit(): void {
    this.scroller.elementScrolled().pipe(
      map(() => this.scroller.measureScrollOffset('bottom')),
      pairwise(),
      filter(([y1, y2]) => (y2 < y1 && y2 < 500)),
      throttleTime(200)
    ).subscribe(() =>
      this.ngZone.run(() => this.scrollEventService.emitBottomReached())
    );
  }
}
