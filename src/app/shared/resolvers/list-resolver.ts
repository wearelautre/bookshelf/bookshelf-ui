import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable, tap} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {EntityService, SearchCriteria} from "../services/entity.service";
import {environment} from "../../../environments/environment";
import {PaginationService} from "../services/pagination.service";

export interface ResolvedDataPaginated<T> {
  items: T[];
  pagination: {
    totalCount: number;
    pageSize: number;
    totalPage: number;
    pageNumber: number;
  };
  filter: string;
}


export function mapToResolvedDataPaginated<T>(response: HttpResponse<T[]>, filter: string = null): ResolvedDataPaginated<T> {
  return ({
    items: response.body,
    pagination: {
      totalCount: +response.headers.get("X-Total-Count"),
      pageSize: +response.headers.get("X-Page-Size"),
      totalPage: +response.headers.get("X-Total-Page"),
      pageNumber: +response.headers.get("X-Page-Number")
    },
    filter
  })
}

export class ListResolver<T, S extends EntityService<T>>  {

  constructor(
    private service: S,
    private defaultPageSize: number = environment.DEFAULT_PAGE_SIZE,
    private defaultSearch: SearchCriteria | null = null,
    protected paginationService?: PaginationService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedDataPaginated<T>> {
    const queryParams = route.queryParams;
    let searchCriteriaList: SearchCriteria[] = [];
    if (this.defaultSearch !== null) {
      searchCriteriaList.push(this.defaultSearch)
    }
    if (queryParams.search) {
      searchCriteriaList = [...searchCriteriaList, ...this.getSearchCriteriaList(queryParams.search)]
    }
    return this.service.search(
      searchCriteriaList,
      queryParams.page,
      queryParams.size ?? this.defaultPageSize,
    ).pipe(
      map((response: HttpResponse<T[]>) => mapToResolvedDataPaginated(response, queryParams.search)),
      tap((resolvedData: ResolvedDataPaginated<T>) => {
        if (this.paginationService) {
          this.paginationService.updatePaginationAndFilterAndSearchCriteriaList(resolvedData, searchCriteriaList);
        }
      })
    );
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [{name: 'name', operation: ':', value: `*${search}*`}]
  }
}
