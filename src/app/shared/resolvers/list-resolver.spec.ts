import {TestBed, waitForAsync} from '@angular/core/testing';
import {ActivatedRouteSnapshot} from '@angular/router';
import {Injectable} from "@angular/core";
import {ListResolver} from "./list-resolver";
import {RestEntity} from "../../core/model/rest-entity";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {CoreService} from "../../core/services/core.service";
import {EntityService} from "../services/entity.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {coreServiceMock} from "../../core/services/__mocks__/core.service";
import {of} from "rxjs";
import {environment} from "../../../environments/environment";
import {paginationServiceMock} from "../services/__mocks__/pagination.service";
import {PaginationService} from "../services/pagination.service";

class MockClass implements RestEntity {
  id?: number;
}

@Injectable({
  providedIn: 'root'
})
class TestService extends EntityService<MockClass> {

  constructor(http: HttpClient, coreService: CoreService) {
    super(http, coreService, 'mockClass');
  }
}

@Injectable()
export class TestListResolver extends ListResolver<MockClass, TestService> {

  constructor(
    service: TestService,
    paginationService?: PaginationService
  ) {
    super(service, 10, null, paginationService)
  }
}

describe('ListResolver', () => {
  let service: TestListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        TestListResolver,
        {provide: CoreService, useValue: coreServiceMock},
        TestService
      ]
    });
    service = TestBed.inject(TestListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('TypeScript test', () => {
    test('should load data with search', waitForAsync(() => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`service`], "search").mockImplementation(() => of(new HttpResponse({body: []})))
      const route = new ActivatedRouteSnapshot();
      route.queryParams = {search: 'BD', page: 6, size: 5};
      service.resolve(route, undefined).subscribe(resolvedData =>
        expect(resolvedData).toStrictEqual({
          "filter": "BD",
          "items": [],
          "pagination": {"pageNumber": 0, "pageSize": 0, "totalCount": 0, "totalPage": 0}
        }));

      expect(spy).toHaveBeenNthCalledWith(1, [{name: 'name', operation: ':', value: `*BD*`}], 6, 5);
    }));
    test('should load data without search', waitForAsync(() => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`service`], "search").mockImplementation(() => of(new HttpResponse({body: []})))
      const route = new ActivatedRouteSnapshot();
      route.queryParams = {page: 7, size: 5};
      service.resolve(route, undefined).subscribe(resolvedData =>
        expect(resolvedData).toStrictEqual({
          "filter": null,
          "items": [],
          "pagination": {"pageNumber": 0, "pageSize": 0, "totalCount": 0, "totalPage": 0}
        }));

      expect(spy).toHaveBeenNthCalledWith(1, [], 7, 5);
    }));
    test('should load data without search and size', waitForAsync(() => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`service`], "search").mockImplementation(() => of(new HttpResponse({body: []})))
      const route = new ActivatedRouteSnapshot();
      route.queryParams = {page: 7};
      service.resolve(route, undefined).subscribe(resolvedData =>
        expect(resolvedData).toStrictEqual({
          "filter": null,
          "items": [],
          "pagination": {"pageNumber": 0, "pageSize": 0, "totalCount": 0, "totalPage": 0}
        }));

      expect(spy).toHaveBeenNthCalledWith(1, [], 7, environment.DEFAULT_PAGE_SIZE);
    }));
    test('should load data without search and size with defaultSearch', waitForAsync(() => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`service`], "search").mockImplementation(() => of(new HttpResponse({body: []})))
      const route = new ActivatedRouteSnapshot();
      service[`defaultSearch`] = {name: 'test', value: 'tutu', operation: "azerty"}
      route.queryParams = {page: 7};
      service.resolve(route, undefined).subscribe(resolvedData =>
        expect(resolvedData).toStrictEqual({
          "filter": null,
          "items": [],
          "pagination": {"pageNumber": 0, "pageSize": 0, "totalCount": 0, "totalPage": 0}
        }));

      expect(spy).toHaveBeenNthCalledWith(1, [{
        name: 'test',
        value: 'tutu',
        operation: "azerty"
      }], 7, environment.DEFAULT_PAGE_SIZE);
    }));
    test('should load data without search and size with defaultSearch with pagination update', waitForAsync(() => {
      jest.clearAllMocks();
      // @ts-ignore
      service[`paginationService`] = paginationServiceMock;
      const spy = jest.spyOn(service[`service`], "search").mockImplementation(() => of(new HttpResponse({body: []})))
      const spyUpdate = jest.spyOn(service[`paginationService`], "updatePaginationAndFilterAndSearchCriteriaList")
        .mockImplementation(() => {
        });
      const route = new ActivatedRouteSnapshot();
      service[`defaultSearch`] = {name: 'test', value: 'tutu', operation: "azerty"}
      route.queryParams = {page: 7};
      service.resolve(route, undefined).subscribe(resolvedData =>
        expect(resolvedData).toStrictEqual({
          "filter": null,
          "items": [],
          "pagination": {"pageNumber": 0, "pageSize": 0, "totalCount": 0, "totalPage": 0}
        }));

      expect(spy).toHaveBeenNthCalledWith(1, [{
        name: 'test',
        value: 'tutu',
        operation: "azerty"
      }], 7, environment.DEFAULT_PAGE_SIZE);
      expect(spyUpdate).toHaveBeenNthCalledWith(1, {
        filter: null,
        items: [],
        pagination: {pageNumber: 0, pageSize: 0, totalCount: 0, totalPage: 0}
      }, [{name: "test", operation: "azerty", value: "tutu"}]);
    }));
  });
});
