export interface PaginationEvent {
  page: number;
  size: number;
}

export interface PaginationStatus {
  currentPage: number;
  totalPages: number;
  totalElements: number;
}

export class PaginationStatusImpl implements PaginationStatus {
  constructor(
    public currentPage: number,
    public totalPages: number,
    public totalElements: number
  ) {
  }
}
