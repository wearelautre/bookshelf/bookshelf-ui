export interface ItemStatus {
  value: number;
  total: number;
}
