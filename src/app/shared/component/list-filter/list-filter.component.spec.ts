import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ListFilterComponent} from './list-filter.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {NgxTranslateTestingModule} from '../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {of} from "rxjs";

describe('ListFilterComponent', () => {
    let component: ListFilterComponent;
    let fixture: ComponentFixture<ListFilterComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ListFilterComponent],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                FontAwesomeTestingModule,
                NgxTranslateTestingModule,
                MatFormFieldModule,
                MatInputModule,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListFilterComponent);
        component = fixture.componentInstance;
        component.initFilter$ = of(null)
        fixture.detectChanges();
    });


    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('Typescript test', () => {
        describe('updateFilter', () => {
            test('should emit', waitForAsync(() => {
                jest.clearAllMocks();
                const spy = jest.spyOn<any, any>(component[`filter`], 'emit').mockImplementation(() => ({}));
                component.form.patchValue({filter: 'str'})
                expect(spy).toHaveBeenNthCalledWith(1, 'str');
            }));
        });
        describe('set value', () => {
            test('should patch the form with value', waitForAsync(() => {
                jest.clearAllMocks();
                const spyPatchValue = jest.spyOn<any, any>(component[`form`], 'patchValue').mockImplementation(() => ({}));
                const spyMarkAsDirty = jest.spyOn<any, any>(component[`form`], 'markAsDirty').mockImplementation(() => ({}));
                component.value = 'str';
                expect(spyPatchValue).toHaveBeenNthCalledWith(1, {filter: 'str'});
                expect(spyMarkAsDirty).toHaveBeenNthCalledWith(1);
            }));
            test('should not patch the form if value is null', waitForAsync(() => {
                jest.clearAllMocks();
                const spyPatchValue = jest.spyOn<any, any>(component[`form`], 'patchValue').mockImplementation(() => ({}));
                const spyMarkAsDirty = jest.spyOn<any, any>(component[`form`], 'markAsDirty').mockImplementation(() => ({}));
                component.value = null;
                expect(spyPatchValue).toHaveBeenCalledTimes(0);
                expect(spyMarkAsDirty).toHaveBeenCalledTimes(0);
            }));
        });
        describe('clear', () => {
            test('should clear the form', waitForAsync(() => {
                jest.clearAllMocks();
                const spyReset = jest.spyOn<any, any>(component[`form`], 'reset').mockImplementation(() => ({}));
                const spyMarkAsPristine = jest.spyOn<any, any>(component[`form`], 'markAsPristine').mockImplementation(() => ({}));
                component.clear();
                expect(spyReset).toHaveBeenNthCalledWith(1);
                expect(spyMarkAsPristine).toHaveBeenNthCalledWith(1);
            }));
        });
    });
});
