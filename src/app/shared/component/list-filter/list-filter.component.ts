import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {Observable} from "rxjs";

@Component({
    selector: 'app-list-filter',
    template: `
        <form [formGroup]="form">
            <mat-form-field class="mff-space-1 width-100" appearance="outline" subscriptSizing="dynamic">
                <label>
                    <input matInput placeholder="{{ translateKey | translate}}" formControlName="filter">
                </label>
                <button mat-icon-button matSuffix *ngIf="displayIcon()" (click)="clear()">
                    <fa-icon [icon]="['fas', 'trash']"></fa-icon>
                </button>
            </mat-form-field>
        </form>
    `,
    styles: [`
      form.list-filter-form {
        display: flex;
        flex-direction: column;
      }
    `]
})
export class ListFilterComponent implements OnInit {

    @Output()
    filter: EventEmitter<string> = new EventEmitter<string>();

    @Input()
    initFilter$: Observable<string | null>;

    form: UntypedFormGroup = this.fb.group({
        filter: this.fb.control(null)
    });

    @Input()
    translateKey: string = 'GENERIC.FILTER.PLACEHOLDER';

    @Input()
    set value(filter: string) {
        if (filter) {
            this.form.patchValue({filter})
            this.form.markAsDirty()
        }
    }

    constructor(
        private fb: UntypedFormBuilder
    ) {
    }

    ngOnInit(): void {
        if (this.initFilter$) {
            this.initFilter$.subscribe(initFilter => this.form.get('filter').setValue(initFilter, {emitEvent: false}))
        }
        this.form.valueChanges.subscribe(value => this.filter.emit(value.filter))
    }

    clear() {
        this.form.reset()
        this.form.markAsPristine()
    }

    displayIcon() {
        return this.form.get('filter').value !== null
    }
}
