import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-list-filter',
  template: '<div>Mock Series List Filter</div> '
})
export class MockListFilterComponent {

  @Output() public filter: EventEmitter<any> = new EventEmitter<any>();
  @Input() public translateKey: any;
  @Input() public value: any;
  @Input() public initFilter$: any;
}
