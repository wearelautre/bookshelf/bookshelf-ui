import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookStatusReadIconComponent} from './book-status-read-icon.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";

describe('BookStatusReadIconComponent', () => {
  let component: BookStatusReadIconComponent;
  let fixture: ComponentFixture<BookStatusReadIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookStatusReadIconComponent ],
      imports: [
        NgxTranslateTestingModule,
        FontAwesomeTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusReadIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
