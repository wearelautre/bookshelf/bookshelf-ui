import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-read-icon',
  template: `
    <span>
      <fa-icon [icon]="['fas', 'book-open']"></fa-icon>
      <span *ngIf="withText">{{'BOOK.STATE.READ'  | translate }}</span>
    </span>
  `,
  styles: [`
    fa-icon {
      padding-right: 10px;
    }
  `]
})
export class BookStatusReadIconComponent {
  @Input() withText: boolean = true;
}

