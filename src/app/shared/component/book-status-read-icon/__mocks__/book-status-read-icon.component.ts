import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-read-icon',
  template: `
    mock icon read
  `
})
export class MockBookStatusReadIconComponent {
  @Input() withText: boolean = true;
}

