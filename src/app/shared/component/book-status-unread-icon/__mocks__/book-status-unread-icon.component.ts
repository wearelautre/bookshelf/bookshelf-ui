import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-unread-icon',
  template: `
    mock icon unread
  `
})
export class MockBookStatusUnreadIconComponent {
  @Input() withText: any;
}
