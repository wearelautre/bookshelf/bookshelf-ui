import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-unread-icon',
  template: `
    <span>
      <fa-icon [icon]="['fas', 'book']"></fa-icon>
      <span *ngIf="withText" class="status-label">{{'BOOK.STATE.UNREAD'  | translate }}</span>
    </span>
  `,
  styles: [`
    fa-icon {
      padding-right: 10px;
    }
  `]
})
export class BookStatusUnreadIconComponent {
  @Input() withText: boolean = true;
}
