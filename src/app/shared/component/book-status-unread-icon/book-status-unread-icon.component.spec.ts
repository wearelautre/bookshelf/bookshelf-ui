import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookStatusUnreadIconComponent} from './book-status-unread-icon.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";

describe('StatusIconComponent', () => {
  let component: BookStatusUnreadIconComponent;
  let fixture: ComponentFixture<BookStatusUnreadIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookStatusUnreadIconComponent ],
      imports: [
        NgxTranslateTestingModule,
        FontAwesomeTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusUnreadIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
