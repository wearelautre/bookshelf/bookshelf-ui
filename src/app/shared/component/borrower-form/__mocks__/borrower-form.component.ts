import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-borrower-form',
  template: `
    mock borrower form
  `
})
export class MockBorrowerFormComponent {
  @Input() form: any;
}
