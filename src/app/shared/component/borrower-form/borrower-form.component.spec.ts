import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BorrowerFormComponent} from './borrower-form.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MockAutoCompleteInputComponent} from "../auto-complete-input/__mocks__/auto-complete-input.component";
import {FormGroup, ReactiveFormsModule} from "@angular/forms";

describe('BorrowerFormComponent', () => {
  let component: BorrowerFormComponent;
  let fixture: ComponentFixture<BorrowerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        BorrowerFormComponent,
        MockAutoCompleteInputComponent
      ],
      imports: [
        ReactiveFormsModule,
        NgxTranslateTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerFormComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({})
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
