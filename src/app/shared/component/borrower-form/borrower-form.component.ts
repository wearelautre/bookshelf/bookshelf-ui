import {ChangeDetectionStrategy, Component, Input, ViewChild,} from '@angular/core';
import {UntypedFormGroup} from "@angular/forms";
import {BorrowerAdministrationService} from "../../../administration/services/borrower-administration.service";
import {ADMIN_SERVICE} from "../../../administration/services/administration.service";
import {AutoCompleteInputComponent} from "../auto-complete-input/auto-complete-input.component";
import {Borrower} from "../../../core/model/borrower";

@Component({
  selector: 'app-borrower-form',
  template: `
    <form [formGroup]="form" fxLayout="row">
      <app-auto-complete-input fxFlex="100"
                               [form]="form"
                               [placeholder]="'BORROWER.FORM.NAME' | translate">
      </app-auto-complete-input>
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: ADMIN_SERVICE, useClass: BorrowerAdministrationService
  }]
})
export class BorrowerFormComponent {
  @Input() form: UntypedFormGroup;
  @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Borrower>;
}
