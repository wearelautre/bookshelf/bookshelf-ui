import {Component, Input} from '@angular/core';
import {BookSeries} from "../../../core/model/series";

@Component({
  selector: 'app-series-name-display',
  template: `
    <div class="series-name-wrapper">
      <span class="mat-small" *ngIf="series.oneShot; else notOneShot">
        {{'SERIES.ONE_SHOT' | translate}}
      </span>

      <ng-template #notOneShot>
        <span style="opacity: 0.5" class="mat-small" *ngIf="!series.oneShot">
          {{ series.name }}
          <span *ngIf="series.seriesCycle !== null"> - {{series.seriesCycle.name}}</span>
        </span>
      </ng-template>
    </div>
  `,
  styles: [`
    .series-name-wrapper {
      opacity: 0.5
    }
  `]
})
export class SeriesNameDisplayComponent {

  @Input() series: BookSeries;

}
