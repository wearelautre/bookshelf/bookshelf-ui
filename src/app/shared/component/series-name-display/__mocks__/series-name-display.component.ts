import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-series-name-display',
  template: `
    mock series name display
  `
})
export class MockSeriesNameDisplayComponent {

  @Input() series: any;

}
