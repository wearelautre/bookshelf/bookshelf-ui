import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-reading-icon',
  template: `
    <span>
      <fa-icon [icon]="['fas', 'book-reader']"></fa-icon>
      <span *ngIf="withText">{{'BOOK.STATE.READING'  | translate }}</span>
    </span>
  `,
  styles: [`
    fa-icon {
      padding-right: 10px;
    }
  `]
})
export class BookStatusReadingIconComponent {
  @Input() withText: boolean = true;
}
