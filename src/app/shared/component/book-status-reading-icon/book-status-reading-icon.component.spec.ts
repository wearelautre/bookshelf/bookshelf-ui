import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookStatusReadingIconComponent} from './book-status-reading-icon.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";

describe('BookStatusReadingIconComponent', () => {
  let component: BookStatusReadingIconComponent;
  let fixture: ComponentFixture<BookStatusReadingIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookStatusReadingIconComponent ],
      imports: [
        NgxTranslateTestingModule,
        FontAwesomeTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusReadingIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
