import {Component, Input} from '@angular/core';
import {Book} from "../../../core/model/book";

@Component({
  selector: 'app-book-status',
  template: `
    <span [ngSwitch]="status">
      <app-book-status-unread-icon *ngSwitchCase="'UNREAD'" [withText]="withText"></app-book-status-unread-icon>
      <app-book-status-reading-icon *ngSwitchCase="'READING'" [withText]="withText"></app-book-status-reading-icon>
      <app-book-status-read-icon *ngSwitchCase="'READ'" [withText]="withText"></app-book-status-read-icon>
    </span>
  `
})
export class BookStatusComponent {
  @Input() withText: boolean = true;
  @Input() status: Book.StatusEnum;
}
