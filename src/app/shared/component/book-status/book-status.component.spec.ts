import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookStatusComponent} from './book-status.component';
import {MockBookStatusReadIconComponent} from "../book-status-read-icon/__mocks__/book-status-read-icon.component";
import {
  MockBookStatusReadingIconComponent
} from "../book-status-reading-icon/__mocks__/book-status-reading-icon.component";
import {
  MockBookStatusUnreadIconComponent
} from "../book-status-unread-icon/__mocks__/book-status-unread-icon.component";

describe('BookStatusComponent', () => {
  let component: BookStatusComponent;
  let fixture: ComponentFixture<BookStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        BookStatusComponent,
        MockBookStatusUnreadIconComponent,
        MockBookStatusReadingIconComponent,
        MockBookStatusReadIconComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });
});
