import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {AutoCompleteInputComponent} from './auto-complete-input.component';
import {RestNamedEntity} from "../../../core/model/rest-named-entity";
import {ADMIN_SERVICE} from "../../../administration/services/administration.service";
import {of} from "rxjs";
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {SEARCH_PARAM} from "../../services/entity.service";
import {DefaultAutoCompleteItem} from "../../shared.module";
import fn = jest.fn;

class MockClass implements RestNamedEntity {
    id?: number;
    name?: string;
}

export const administrationServiceMock = {
    list: of([]),
    list$: of([]),
    getAll: fn(() => of([])),
    search: fn(() => of([])),
    searchAutocomplete: fn(() => of([])),
    update: fn((series: any) => of(series)),
    delete: fn(() => of({}))
};

function getSearchParam(value: string) {
    return [
        [{name: 'name', operation: ':', value: `*${value}*`}],
        0,
        5,
        false
    ]
}

describe('AutoCompleteInputComponent', () => {
    let component: AutoCompleteInputComponent<MockClass>;
    let fixture: ComponentFixture<AutoCompleteInputComponent<MockClass>>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AutoCompleteInputComponent],
            providers: [
                {provide: ADMIN_SERVICE, useValue: administrationServiceMock},
                {provide: SEARCH_PARAM, useClass: DefaultAutoCompleteItem}
            ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                MatInputModule,
                MatAutocompleteModule,
                MatFormFieldModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AutoCompleteInputComponent) as ComponentFixture<AutoCompleteInputComponent<MockClass>>;
        component = fixture.componentInstance;
        component.form = new FormGroup({
            id: new FormControl(),
            name: new FormControl()
        })
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
            jest.clearAllMocks();
        });
    });

    describe('Typescript test', () => {
        describe('[UPDATE]', () => {
            test('update  control', () => {
                component.update({name: 'test', id: 12});
                expect(component.form.value.id).toStrictEqual(12);
                expect(component.form.value.name).toStrictEqual('test');
            });
        });
        describe('[GET_START_VALUE]', () => {
            test('get start value on null value', () => {
                expect(component.getStartValue()).toStrictEqual('');
            });
            test('get start value on non null value', () => {
                const name = 'name';
                component.form.patchValue({name});
                expect(component.getStartValue()).toStrictEqual('name');
            });
        });
        describe('[DISPLAY FN]', () => {
            test('object as string', () => {
                expect(component.displayFn('toto')).toStrictEqual('toto');
            });
            test('object as Object whithout name', () => {
                expect(component.displayFn({name: null})).toStrictEqual('');
            });
            test('object as string', () => {
                expect(component.displayFn({name: 'null'})).toStrictEqual('null');
            });
        });
        describe('[GET DISPLAY]', () => {
            test('object as string', () => {
                expect(component.getDisplay({name: 'test'})).toStrictEqual('test');
            });
        });
        describe('[AUTOCOMPLETE]', () => {
            test('call api on input change object', fakeAsync(() => {
                jest.clearAllMocks();
                component.setFocus()
                const name = 'name';
                component.form.setValue({name, id: null});
                tick(300);
                expect(administrationServiceMock.searchAutocomplete).toHaveBeenNthCalledWith(2, [{
                    name: 'name',
                    operation: ':',
                    value: `*${name}*`
                }], null, null);
            }));
            test('call api on input change object name undefined', fakeAsync(() => {
                jest.clearAllMocks();
                fixture.detectChanges();
                const name = undefined;
                component.form.patchValue({name});
                tick(300);
                expect(administrationServiceMock.search).toHaveBeenCalledTimes(0);
            }));
        });
    });
});
