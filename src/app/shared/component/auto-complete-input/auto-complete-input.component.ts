import {Component, ElementRef, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormControl, UntypedFormGroup, Validators} from "@angular/forms";
import {Observable, of, Subscription} from "rxjs";
import {filter, map, startWith, switchMap, tap} from "rxjs/operators";
import {HttpResponse} from "@angular/common/http";
import {ADMIN_SERVICE, AdministrationService} from "../../../administration/services/administration.service";
import {RestNamedEntity} from "../../../core/model/rest-named-entity";
import {SEARCH_PARAM} from "../../services/entity.service";
import {MatFormFieldAppearance} from '@angular/material/form-field';
import {AutoCompleteItem} from "../../shared.module";
import {idType} from "../../../core/model/rest-entity";

@Component({
    selector: 'app-auto-complete-input',
    template: `
        <mat-form-field fxFlex="100"
                        class="mff-space-1"
                        [appearance]="appearance"
                        [class]="classes"
                        [floatLabel]="'auto'"
                        subscriptSizing="dynamic"
        >
            <mat-label *ngIf="displayLabel">{{placeholder}}</mat-label>
            <input matInput
                   #input
                   type="text"
                   [formControl]="getControl()"
                   aria-label="name"
                   (focus)="setFocus()"
                   [matAutocomplete]="auto"
                   [required]="isControlRequired()"/>
            <mat-autocomplete
                    #auto="matAutocomplete"
                    [displayWith]="displayFn"
                    (optionSelected)="update($event.option.value)">
                <mat-option
                        *ngFor="let option of filteredOptions$ | async"
                        [value]="option"
                        [innerHTML]="getDisplay(option)">
                </mat-option>
            </mat-autocomplete>
            <mat-error
                    *ngIf="form.get('name').hasError('required')">{{'BOOK.FORM.ERRORS.REQUIRED' | translate}}</mat-error>
        </mat-form-field>
    `
})
export class AutoCompleteInputComponent<T extends RestNamedEntity> implements OnInit, OnDestroy {

    @Input() displayLabel: boolean = true;
    @Input() appearance: MatFormFieldAppearance = 'outline';
    @Input() classes: string;

    @Input() set parentId(parentId: idType) {
        this._parentId = parentId;
        this.form.get('name').markAsUntouched();
        this.enableAutoComplete = parentId !== null
    };

    private _parentId = null
    private enableAutoComplete: boolean = true;

    constructor(
        @Inject(ADMIN_SERVICE) public service: AdministrationService<T>,
        @Inject(SEARCH_PARAM) public searchParam: AutoCompleteItem<T>
    ) {
    }

    @Input() form: UntypedFormGroup;
    @Input() placeholder: string;

    @Output() selectItem: EventEmitter<T> = new EventEmitter<T>();

    @ViewChild('input') input: ElementRef<HTMLInputElement>;

    filteredOptions$: Observable<T[]> = of([]);
    private subscriptions: Subscription[] = [];

    getStartValue() {
        return this.form.get('name').value ?? ''
    }

    ngOnInit(): void {
        this.filteredOptions$ = this.service.list$;
    }

    displayFn(value: T | string): string {
        if (value === undefined || value === null) {
            return ''
        }
        return typeof value === 'string' ? value : ((value && value.name) ? value.name : '');
    }

    update(value: T): void {
        this.input.nativeElement.blur()
        this.form.patchValue({
            id: value.id,
            name: value.name
        }, {emitEvent: false});
        this.selectItem.emit(value)
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    getControl(): UntypedFormControl {
        return this.form.get('name') as UntypedFormControl;
    }

    getDisplay(option: T): string {
        return this.searchParam.getDisplay(option)
    }

    isControlRequired() {
        return this.getControl().hasValidator(Validators.required)
    }

    setFocus() {
        if (this.enableAutoComplete && !this.form.get('name').touched) {
            this.subscriptions.push(
                this.form.get('name').valueChanges.pipe(
                    tap(() => this.form.get('id').patchValue(null, {emitEvent: false})),
                    startWith(this.getStartValue()),
                    tap(value => this.searchParam.sideEffect(this.form, value)),
                    filter((value: string | T) => typeof value === 'string'),
                    switchMap((value: string) => this.service.searchAutocomplete(...this.searchParam.getSearchParam(value), null, this._parentId)),
                    map((response: HttpResponse<T[]>) => response.body)
                ).subscribe()
            )
        }
    }
}
