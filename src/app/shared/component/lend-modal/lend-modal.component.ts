import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {LoanAdministrationService} from "../../../administration/services/loan-administration.service";
import {BorrowerAdministrationService} from "../../../administration/services/borrower-administration.service";
import {Book} from "../../../core/model/book";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {NotificationService} from "../../services/notification.service";

@Component({
    selector: 'app-lend-modal',
    template: `
        <h1 mat-dialog-title>
            {{ 'BORROWER.CREATION.MODAL.TITLE' | translate }}
        </h1>
        <div mat-dialog-content fxLayout="column" fxLayoutGap="1rem">
            <div>
                {{'BORROWER.CREATION.MODAL.MESSAGES.BOOK_TO_LEND' | translate }}:
                <app-title-display
                        [series]="data.series" [title]="data.title">
                </app-title-display>
            </div>
            <div fxLayout="row" fxLayoutGap="1rem">
                <app-borrower-form fxFlex [form]="form"></app-borrower-form>
            </div>
            <div style="padding-left: 1.5rem; padding-right: 1.5rem" fxLayout="column"
                 *ngIf="borrowerNeedToBeCreated()">
        <span
                style="padding-bottom: 0.5rem; color: red"> {{ 'BORROWER.CREATION.MODAL.MESSAGES.NOT_CREATED' | translate }} </span>
                <button mat-button mat-flat-button color="primary" (click)="createBorrower()">
                    {{ 'BORROWER.CREATION.MODAL.BUTTONS.CREATE' | translate }}
                </button>
            </div>
        </div>
        <mat-dialog-actions align="end">
            <button mat-button mat-dialog-close="CANCEL">{{ 'GENERIC.DIALOG.CONFIRM.KO' | translate }}</button>
            <button mat-button [disabled]="!isFormValid()" (click)="lendTheBook()"
                    cdkFocusInitial> {{ 'GENERIC.DIALOG.CONFIRM.OK' | translate }}</button>
        </mat-dialog-actions>
    `
})
export class LendModalComponent {

    form: UntypedFormGroup = this.fb.group({
        name: this.fb.control(null),
        id: this.fb.control(null)
    })

    constructor(
        private fb: UntypedFormBuilder,
        private borrowerService: BorrowerAdministrationService,
        private loanService: LoanAdministrationService,
        @Inject(MAT_DIALOG_DATA) public data: Book,
        private notificationService: NotificationService,
        public dialogRef: MatDialogRef<LendModalComponent>
    ) {

    }

    borrowerNeedToBeCreated(): boolean {
        return (this.form.value.name !== null && !this.form.value.name.empty && this.form.value.id === null)
    }

    isFormValid(): boolean {
        return !this.borrowerNeedToBeCreated()
    }

    createBorrower() {
        this.borrowerService.create({name: this.form.value.name})
            .subscribe({
                next: (borrower) => {
                    const name = borrower.name
                    this.form.setValue({name, id: borrower.id})
                    this.notificationService.displayMessage(`BORROWER.CREATION.NOTIFICATION.SUCCESS`, {name})
                },
                error: () =>
                    this.notificationService.displayMessage(`BORROWER.CREATION.NOTIFICATION.ERROR`, {})
            })
    }

    lendTheBook() {
        this.loanService.createWithOption({bookIsbn: this.data.isbn, borrowDate: new Date()}, false, this.form.value.id)
            .subscribe({
                next: () => this.dialogRef.close({
                    state: 'SUCCESS',
                    name: this.form.value.name,
                    title: LendModalComponent.getTitleBookDisplay(this.data),
                }), error: () => this.dialogRef.close({state: 'ERROR'})
            })
    }

    private static getTitleBookDisplay(data: Book) {
        let t = data.title
        if (data.series !== undefined && !data.series.oneShot) {
            if (data.series.tome !== null && data.series.tome !== undefined) {
                t = `${data.series.name} (T${data.series.tome}) ${t}`
            } else {
                t = `${data.series.name} ${t}`
            }
        }

        return t;
    }
}
