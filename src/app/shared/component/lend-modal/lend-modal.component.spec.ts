import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LendModalComponent} from './lend-modal.component';
import {BorrowerAdministrationService} from "../../../administration/services/borrower-administration.service";
import {
    borrowerAdministrationServiceMock
} from "../../../administration/services/__mocks__/borrower-administration.service";
import {LoanAdministrationService} from "../../../administration/services/loan-administration.service";
import {loanAdministrationServiceMock} from "../../../administration/services/__mocks__/loan-administration.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {ReactiveFormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {MatButtonModule} from '@angular/material/button';
import {MockTitleDisplayComponent} from "../../title-display/__mocks__/title-display.component";
import {MockBorrowerFormComponent} from "../borrower-form/__mocks__/borrower-form.component";
import {BookImpl} from "../../../core/model/impl/book-impl";
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {of} from "rxjs";
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NotificationService} from "../../services/notification.service";
import {notificationServiceMock} from "../../services/__mocks__/notification.service";

const mockData = new BookImpl();
const matDialogMock = {
    open: jest.fn(),
    close: jest.fn()
};

describe('LendModalComponent', () => {
    let component: LendModalComponent;
    let fixture: ComponentFixture<LendModalComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LendModalComponent, MockTitleDisplayComponent, MockBorrowerFormComponent],
            imports: [
                NoopAnimationsModule,
                NgxTranslateTestingModule,
                MatDialogModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatSnackBarModule
            ],
            providers: [
                {provide: NotificationService, useValue: notificationServiceMock},
                {provide: BorrowerAdministrationService, useValue: borrowerAdministrationServiceMock},
                {provide: LoanAdministrationService, useValue: loanAdministrationServiceMock},
                {provide: MatDialogRef, useValue: matDialogMock},
                {provide: MAT_DIALOG_DATA, useValue: mockData}
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LendModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('Init test', () => {
        test('should create', () => {
            expect(component).toBeTruthy();
            jest.clearAllMocks();
        });
    });
    describe('Typescript test', () => {
        describe('[borrowerNeedToBeCreated]', () => {
            test('borrower need to be created id null', () => {
                component.form.setValue({
                    name: 'test',
                    id: null
                })
                expect(component.borrowerNeedToBeCreated()).toBeTruthy();
            });
            test('borrower already created', () => {
                component.form.setValue({
                    name: 'test',
                    id: 12
                })
                expect(component.borrowerNeedToBeCreated()).toBeFalsy();
            });
            test('no borrower name', () => {
                component.form.setValue({
                    name: null,
                    id: null
                })
                expect(component.borrowerNeedToBeCreated()).toBeFalsy();
            });
        });
        describe('[isFormValid]', () => {
            test('formNotValid', () => {
                jest.clearAllMocks()
                jest.spyOn(component, `borrowerNeedToBeCreated`)
                    .mockImplementation(() => true)
                expect(component.isFormValid()).toBeFalsy();
            });
            test('formValid', () => {
                jest.clearAllMocks()
                jest.spyOn(component, `borrowerNeedToBeCreated`)
                    .mockImplementation(() => false)
                expect(component.isFormValid()).toBeTruthy();
            });
        });
        describe('[createBorrower]', () => {
            jest.clearAllMocks()
            test('create', () => {
                component.form.setValue({
                    name: 'test',
                    id: null
                })
                borrowerAdministrationServiceMock.create.mockImplementation(() => of({
                    name: 'test',
                    id: 12
                }))
                component.createBorrower()
                expect(borrowerAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, {name: 'test'});
                expect(component.form.value).toStrictEqual({name: 'test', id: 12});
            });
        });
        describe('[lendTheBook]', () => {
            test('lend', waitForAsync(() => {
                jest.clearAllMocks()
                component.form.setValue({name: 'test', id: 12})
                component.data.isbn = 'test'
                component.lendTheBook()
                const borrowDate = new Date()

                expect(loanAdministrationServiceMock.createWithOption)
                    .toHaveBeenNthCalledWith(
                        1,
                        {bookIsbn: 'test', borrowDate: expect.toBeBeforeOrEqualTo(borrowDate)},
                        false,
                        12
                    );
                expect(component.form.value).toStrictEqual({
                    name: 'test',
                    id: 12
                });
                expect(matDialogMock.close).toHaveBeenCalledTimes(1);
            }));
            describe('[getTitleBookDisplay]', () => {
                test('series with tome', waitForAsync(() => {
                    jest.clearAllMocks()
                    const book = new BookImpl()
                    book.title = 'test'
                    book.series.tome = 4
                    book.series.name = "Series"

                    expect(LendModalComponent[`getTitleBookDisplay`](book)).toEqual("Series (T4) test");
                }));
                test('series with tome null', waitForAsync(() => {
                    jest.clearAllMocks()
                    const book = new BookImpl()
                    book.title = 'test'
                    book.series.tome = null
                    book.series.name = "Series"

                    expect(LendModalComponent[`getTitleBookDisplay`](book)).toEqual("Series test");
                }));
                test('series with tome undefined', waitForAsync(() => {
                    jest.clearAllMocks()
                    const book = new BookImpl()
                    book.title = 'test'
                    book.series.tome = undefined
                    book.series.name = "Series"

                    expect(LendModalComponent[`getTitleBookDisplay`](book)).toEqual("Series test");
                }));
                test('series undefined', waitForAsync(() => {
                    jest.clearAllMocks()
                    const book = new BookImpl()
                    book.title = 'test'
                    book.series = undefined

                    expect(LendModalComponent[`getTitleBookDisplay`](book)).toEqual("test");
                }));
                test('series oneShot', waitForAsync(() => {
                    jest.clearAllMocks()
                    const book = new BookImpl()
                    book.title = 'test'
                    book.series.name = "Series"
                    book.series.oneShot = true

                    expect(LendModalComponent[`getTitleBookDisplay`](book)).toEqual("test");
                }));
            });
        });
    });
});
