import {BehaviorSubject} from 'rxjs';
import {UntypedFormGroup} from '@angular/forms';

export interface UiData<T> {
  form: UntypedFormGroup;
  item: T;
  isSaved: BehaviorSubject<boolean>;
  start: BehaviorSubject<boolean>;
}

