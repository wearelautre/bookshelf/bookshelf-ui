import {DisplayImage} from './display-image';
import {ImageImpl} from './models/impl/image-impl';

describe('DisplayImage', () => {

  describe('Init test', () => {
    test('should create', () => {
      expect(new DisplayImage('toto')).toBeTruthy();
    });
  });

  describe('Business test', () => {
    test('should create an correct image', () => {
      const url = 'test';
      const displayImage = new DisplayImage(url);
      const img = displayImage.getImg('toto');
      const newImg = new ImageImpl(`${url}/toto`, 'toto');
      expect(img.src.split('?')[0]).toStrictEqual(newImg.src);
      expect(img.alt).toStrictEqual(newImg.alt);
    });

    test('should create a null image', () => {
      const url = 'test';
      const displayImage = new DisplayImage(url);
      expect(displayImage.getImg(null)).toBeNull();
    });
  });
});
