import {Injectable} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(
        private translateService: TranslateService,
        private snackBar: MatSnackBar
    ) {
    }

    public displayMessage(translateString: string, params: any, config = {duration: 6000}) {
        this.snackBar.open(
            this.translateService.instant(translateString, params),
            this.translateService.instant('SNACKBAR.ACTION.CLOSE'),
            config
        );
    }
}
