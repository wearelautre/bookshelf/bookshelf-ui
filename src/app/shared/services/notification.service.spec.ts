import {TestBed} from '@angular/core/testing';

import {NotificationService} from './notification.service';
import {NgxTranslateTestingModule} from "../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MatSnackBarModule} from '@angular/material/snack-bar';

describe('NotificationService', () => {
    let service: NotificationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxTranslateTestingModule,
                MatSnackBarModule
            ]
        });
        service = TestBed.inject(NotificationService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
