import {TestBed, waitForAsync} from '@angular/core/testing';

import {EntityService, SearchPaginationImpl} from './entity.service';
import {coreServiceMock} from "../../core/services/__mocks__/core.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {CoreService} from "../../core/services/core.service";
import {RestEntity} from "../../core/model/rest-entity";
import {Injectable} from "@angular/core";
import {AdministrationService} from "../../administration/services/administration.service";
import {HttpClient} from "@angular/common/http";
import {SortOrder} from "../../core/model/sort-order.enum";
import {skip} from "rxjs";

class MockClass implements RestEntity {
  id?: number;
}

@Injectable({
  providedIn: 'root'
})
class TestAdministrationService extends AdministrationService<MockClass> {

  constructor(http: HttpClient, coreService: CoreService) {
    super(http, coreService, 'mockClass');
  }
}

describe('EntityService', () => {
  let httpTestingController: HttpTestingController;
  let service: EntityService<MockClass>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock},
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TestAdministrationService);
  });

  afterEach(() => {
    httpTestingController.verify();
    jest.clearAllMocks();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['mockClass']);
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[SEARCH]', () => {
      test('search with default option', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchWithOption().subscribe((value) => {
          expect(value.body).toStrictEqual([]);
          expect(spy).toHaveBeenNthCalledWith(1, value.body);
        });
        const req = httpTestingController.expectOne('/api/mockClass?direction=ASC&allResults=true');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search without pagination', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchWithOption([], {
          pagination: null,
          withLoader: false,
          updateList: true,
          direction: SortOrder.ASC
        }).subscribe((value) => {
          expect(value.body).toStrictEqual([]);
          expect(spy).toHaveBeenNthCalledWith(1, value.body);
        });
        const req = httpTestingController.expectOne('/api/mockClass?direction=ASC&allResults=true');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search without pagination', waitForAsync(() => {
        service.searchWithOption([], {
          pagination: new SearchPaginationImpl(),
          withLoader: false,
          updateList: false,
          direction: SortOrder.ASC
        }).subscribe((value) => {
          expect(value.body).toStrictEqual([]);
        });
        const req = httpTestingController.expectOne('/api/mockClass?page=0&size=5&direction=ASC&allResults=false');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search without params', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.search().subscribe((value) => {
          expect(value.body).toStrictEqual([]);
          expect(spy).toHaveBeenNthCalledWith(1, value.body);
        });
        const req = httpTestingController.expectOne('/api/mockClass?page=0&size=5&direction=ASC&allResults=false');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search with params', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.search([{name: 'toto', operation: ':', value: 'test'}])
          .subscribe((value) => {
            expect(value.body).toStrictEqual([]);
            expect(spy).toHaveBeenNthCalledWith(1, value.body);
          });
        const req = httpTestingController.expectOne('/api/mockClass?search=toto:test&page=0&size=5&direction=ASC&allResults=false');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search with params but no list update', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.search([{name: 'toto', operation: ':', value: 'test'}], 8, 15, false, false)
          .subscribe((value) => {
            expect(value.body).toStrictEqual([]);
            expect(spy).toHaveBeenCalledTimes(0);
          });
        const req = httpTestingController.expectOne('/api/mockClass?search=toto:test&page=8&size=15&direction=ASC&allResults=false');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('error', waitForAsync(() => {
        service.search([{name: 'toto', operation: ':', value: 'test'}], 8, 15, true, false)
          .subscribe({
            error: (error) => {
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }
          });
        const req = httpTestingController.expectOne('/api/mockClass?search=toto:test&page=8&size=15&direction=ASC&allResults=false');
        expect(req.request.method).toStrictEqual('GET');
        req.error(new ErrorEvent('error'));
      }));
    });
    describe('[SEARCH AUTOCOMPLETE]', () => {
      test('search with default option', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchAutocomplete().subscribe((value) => {
          expect(value.body).toStrictEqual([]);
          expect(spy).toHaveBeenNthCalledWith(1, value.body);
        });
        const req = httpTestingController.expectOne('/api/mockClass/autocomplete');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search with search', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchAutocomplete([{name: 'toto', operation: ':', value: 'test'}]).subscribe((value) => {
          expect(value.body).toStrictEqual([]);
          expect(spy).toHaveBeenNthCalledWith(1, value.body);
        });
        const req = httpTestingController.expectOne('/api/mockClass/autocomplete?search=toto:test');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search with search and additional param', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchAutocomplete([{name: 'toto', operation: ':', value: 'test'}], [{key: "key", value: "value"}, {key: "key1", value: "value1"}])
          .subscribe((value) => {
            expect(value.body).toStrictEqual([]);
            expect(spy).toHaveBeenNthCalledWith(1, value.body);
          });
        const req = httpTestingController.expectOne('/api/mockClass/autocomplete?search=toto:test&key=value&key1=value1');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('search with only additional param', waitForAsync(() => {
        const spy = jest.spyOn<any, any>(service, `updateList`).mockImplementation(() => {
        });
        service.searchAutocomplete([], [{key: 'key', value: 'value'}, {key: 'key1', value: 'value1'}])
          .subscribe((value) => {
            expect(value.body).toStrictEqual([]);
            expect(spy).toHaveBeenNthCalledWith(1, value.body);
          });
        const req = httpTestingController.expectOne('/api/mockClass/autocomplete?key=value&key1=value1');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      }));
      test('error', waitForAsync(() => {
        service.searchAutocomplete([{name: 'toto', operation: ':', value: 'test'}])
          .subscribe({
            error: (error) => {
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false);
            }
          });
        const req = httpTestingController.expectOne('/api/mockClass/autocomplete?search=toto:test');
        expect(req.request.method).toStrictEqual('GET');
        req.error(new ErrorEvent('error'));
      }));
    });
    describe('[GET_BY_ID]', () => {
      test('search with default option', waitForAsync(() => {
        service.getById('test').subscribe((value) => {
          expect(value).toStrictEqual({toto: 'toto'});
          expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
        });
        const req = httpTestingController.expectOne('/api/mockClass/test');
        expect(req.request.method).toStrictEqual('GET');
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);

        req.flush({toto: 'toto'});
      }));
      test('error', waitForAsync(() => {
        service.getById('test')
          .subscribe({
            error: _ => {
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true);
              expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(2, false);
            }
          });
        const req = httpTestingController.expectOne('/api/mockClass/test');
        expect(req.request.method).toStrictEqual('GET');
        req.error(new ErrorEvent('error'));
      }));
    });
    describe('[UPDATE LIST]', () => {
      test('Append data', waitForAsync(() => {
        jest.clearAllMocks()
        service[`list`].next([{id: 3}])
        service.list$.pipe(skip(1)).subscribe((value) =>
          expect(value).toStrictEqual([{id: 3}, {id: 0}])
        );
        service.updateList([{id: 0}], true)
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false);
      }));
      test('Replace data', waitForAsync(() => {
        jest.clearAllMocks()
        service[`list`].next([{id: 3}])
        service.list$.pipe(skip(1)).subscribe((value) =>
          expect(value).toStrictEqual([{id: 0}])
        );
        service.updateList([{id: 0}])
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false);
      }));
    });
  });
});
