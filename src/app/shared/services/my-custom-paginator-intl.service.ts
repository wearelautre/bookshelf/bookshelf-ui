import {Injectable} from '@angular/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {Subject} from "rxjs";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class MyCustomPaginatorIntlService extends MatPaginatorIntl {
    changes = new Subject<void>();

    constructor(
        itemTranslateName: string,
        private translate: TranslateService
    ) {
        super();
        this.translate.onLangChange.subscribe(_ => this.changes.next());
        this.translate.get([
            'PAGINATOR.FIRST_PAGE',
            'PAGINATOR.LAST_PAGE',
            'PAGINATOR.NEXT_PAGE',
            'PAGINATOR.PREVIOUS_PAGE'
        ])
            .subscribe(translation => {
                this.firstPageLabel = translation['PAGINATOR.FIRST_PAGE'];
                this.lastPageLabel = translation['PAGINATOR.LAST_PAGE'];
                this.nextPageLabel = translation['PAGINATOR.NEXT_PAGE'];
                this.previousPageLabel = translation['PAGINATOR.PREVIOUS_PAGE'];
            });
        translate.get(`PAGINATOR.ITEMS.${itemTranslateName}`).subscribe(item => {
            this.translate.get('PAGINATOR.ITEMS_PER_PAGE', {item})
                .subscribe(translation => this.itemsPerPageLabel = translation);
        });
    }

    getRangeLabel = (page: number, pageSize: number, length: number): string => {
        const amountPages = length === 0 ? 1 : Math.ceil(length / pageSize);
        return this.translate.instant(`PAGINATOR.PAGE_OF`, {page: page + 1, amountPages});
    }
}
