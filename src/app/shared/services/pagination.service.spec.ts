import {TestBed, waitForAsync} from '@angular/core/testing';

import {PaginationService} from './pagination.service';

describe('PaginationService', () => {
  let service: PaginationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaginationService);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('updatePaginationAndFilter', () => {
      test('should update all pagination state and filter', waitForAsync(() => {
        service.updatePaginationAndFilter({
          items: [],
          pagination: {
            pageSize: 1,
            pageNumber: 2,
            totalPage: 3,
            totalCount: 4
          },
          filter: 'toto'
        });
        service.totalCount$.subscribe(value => expect(value).toStrictEqual(4));
        service.pageSize$.subscribe(value => expect(value).toStrictEqual(1));
        service.currentPage$.subscribe(value => expect(value).toStrictEqual(2));
        service.filterStr$.subscribe(value => expect(value).toStrictEqual('toto'));
      }));
    });
    describe('onNewPage', () => {
      test('should update the newPageEvent obs', waitForAsync(() => {
        service.onNewPage({size: 1, page: 2});
        service.newPageEvent$.subscribe(value => expect(value).toStrictEqual({size: 1, page: 2}));
      }));
    });
    describe('getNextPage', () => {
      test('Should return the next possible page', waitForAsync(() => {
        service[`totalPage`].next(3);
        service[`currentPage`].next(1);
        expect(service.getNextPage()).toStrictEqual(2);
      }));
      test('Should return null', waitForAsync(() => {
        service[`totalPage`].next(3);
        service[`currentPage`].next(3);
        expect(service.getNextPage()).toBeNull();
      }));
    });
    describe('getCurrentPagination', () => {
      test('Should return the next possible page', waitForAsync(() => {
        service[`pageSize`].next(3);
        service[`currentPage`].next(1);
        service[`filterStr`].next('test');
        expect(service.getCurrentPagination()).toStrictEqual({
          filterStr: 'test',
          pageSize: 3,
          currentPage: 1,
          searchCriteriaList: []
        });
      }));
    });
  });
});
