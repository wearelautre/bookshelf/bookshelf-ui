import {TestBed, waitForAsync} from '@angular/core/testing';

import {ScrollEventService} from './scroll-event.service';

describe('ScrollEventService', () => {
  let service: ScrollEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScrollEventService);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('bottomReached', () => {
      test('should emit an event', waitForAsync(() => {
        service.emitBottomReached();
        service.bottomReached$.subscribe(value => expect(value).toBeUndefined());
      }));
    });

    describe('scrollActivated', () => {
      test('should emit the value true', waitForAsync(() => {
        service.toggleScroll(true);
        service.scrollActivated$.subscribe(value => expect(value).toBeTruthy());
      }));
      test('should emit the value false', waitForAsync(() => {
        service.toggleScroll(false);
        service.scrollActivated$.subscribe(value => expect(value).toBeFalsy());
      }));
    });
  });
});
