import {Subject} from "rxjs";

export const scrollEventServiceMock = {
  emitBottomReached: jest.fn(),
  bottomReached$: new Subject<void>(),
  toggleScroll: jest.fn()
}
