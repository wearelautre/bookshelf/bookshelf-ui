import {of} from "rxjs";
import fn = jest.fn;

export const paginationServiceMock = {
  totalCount$: of(0),
  pageSize$: of(0),
  currentPage$: of(0),
  filterStr$: of(0),
  newPageEvent$: of({page: 0, size: 0}),
  updatePagination: fn((_) => {
  }),
  updatePaginationAndFilter: fn((_) => {
  }),
  updatePaginationAndFilterAndSearchCriteriaList: fn((_) => {
  }),
  onNewPage: fn((_) => {
  }),
  getNextPage: fn(() => null),
  getCurrentPagination: fn(() => ({
    filterStr: "", pageSize: 5, currentPage: 0
  }))
};
