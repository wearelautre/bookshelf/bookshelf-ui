import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ScrollEventService {

  private bottomReached: Subject<void> = new Subject();

  get bottomReached$(): Observable<void> {
    return this.bottomReached.asObservable();
  }

  private scrollActivated: BehaviorSubject<boolean> = new BehaviorSubject(true);

  get scrollActivated$(): Observable<boolean> {
    return this.scrollActivated.asObservable();
  }

  emitBottomReached() {
    this.bottomReached.next();
  }

  toggleScroll(activate: boolean) {
    this.scrollActivated.next(activate);
  }
}
