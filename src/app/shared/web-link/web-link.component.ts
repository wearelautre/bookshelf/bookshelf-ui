import {Component, Input} from '@angular/core';
import {WebLink} from '../../core/model/web-link';
import {IconProp} from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'app-web-link',
  template: `
    <a style="text-decoration: none; color: white" [href]="href" target="_blank">
        <fa-icon *ngIf="icon" [icon]="icon" style="padding-right: 10px"></fa-icon>
    </a>
  `
})
export class WebLinkComponent {

  public icon: IconProp;
  public href: string;

  @Input()
  set webLink(webLink: WebLink) {
    const data = WebLink.typeData(webLink.type)
    this.icon = data.icon
    this.href = `${data.urlPrefix}${webLink.value}`
  }
}
