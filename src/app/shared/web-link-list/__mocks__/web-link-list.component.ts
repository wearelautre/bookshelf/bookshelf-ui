import {Component, Input} from '@angular/core';
import {WebLink} from '../../../core/model/web-link';

@Component({
  selector: 'app-web-link-list',
  template: '<div> Web link list Display</div>'
})
export class MockWebLinkListComponent {

  @Input()
  webLinks: WebLink[];
}
