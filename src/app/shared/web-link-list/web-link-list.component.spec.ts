import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WebLinkListComponent} from './web-link-list.component';
import {MockWebLinkComponent} from '../web-link/__mocks__/web-link.component';

describe('WebLinksComponent', () => {
  let component: WebLinkListComponent;
  let fixture: ComponentFixture<WebLinkListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebLinkListComponent, MockWebLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLinkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
