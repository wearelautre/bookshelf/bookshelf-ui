import {Component, Input} from '@angular/core';
import {WebLink} from '../../core/model/web-link';

@Component({
  selector: 'app-web-link-list',
  template: `
    <ng-container *ngFor="let webLink of webLinks">
      <app-web-link [webLink]="webLink"></app-web-link>
    </ng-container>
  `
})
export class WebLinkListComponent {

  @Input() webLinks: WebLink[] = []
}
