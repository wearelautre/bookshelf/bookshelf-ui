import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {FormsModule, ReactiveFormsModule, UntypedFormGroup} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LoaderImgComponent} from './loader-img/loader-img.component';
import {LoaderBarComponent} from './loader-bar/loader-bar.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {TitleDisplayComponent} from './title-display/title-display.component';
import {MatCardModule} from '@angular/material/card';
import {WebLinkListComponent} from './web-link-list/web-link-list.component';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {WebLinkComponent} from './web-link/web-link.component';
import {faBarcode, faBook, faBookOpen, faBookReader, faLink} from '@fortawesome/free-solid-svg-icons';
import {faArtstation, faDeviantart, faFacebook, faInstagram, faTwitter} from '@fortawesome/free-brands-svg-icons';
import {DialogConfirmComponent} from './confirm-modal/dialog-confirm.component';
import {TranslateModule} from '@ngx-translate/core';
import {ProgressbarComponent} from '../administration/shared/progressbar/progressbar.component';
import {RouterModule} from "@angular/router";
import {ListFilterComponent} from "./component/list-filter/list-filter.component";
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSidenavModule} from "@angular/material/sidenav";
import {SeriesNameDisplayComponent} from './component/series-name-display/series-name-display.component';
import {BookStatusUnreadIconComponent} from './component/book-status-unread-icon/book-status-unread-icon.component';
import {BookStatusReadIconComponent} from './component/book-status-read-icon/book-status-read-icon.component';
import {BookStatusReadingIconComponent} from './component/book-status-reading-icon/book-status-reading-icon.component';
import {LendModalComponent} from './component/lend-modal/lend-modal.component';
import {BorrowerFormComponent} from './component/borrower-form/borrower-form.component';
import {MatOptionModule} from '@angular/material/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {AutoCompleteInputComponent} from './component/auto-complete-input/auto-complete-input.component';
import {SEARCH_PARAM, SearchCriteria} from "./services/entity.service";
import {RestNamedEntity} from "../core/model/rest-named-entity";
import {KeyValue} from "../core/model/key-value";

export type SearchParam = [SearchCriteria[]?, KeyValue[]?];

export class AutoCompleteItem<T extends RestNamedEntity> {
  getSearchParam(value: string): SearchParam {
    return [
      [{name: 'name', operation: ':', value: `*${value}*`}]
    ]
  }

  getDisplay(option: T): string {
    return option.name
  }

  sideEffect(form: UntypedFormGroup, value: string): void {
    // Nothing by default
  }
}

export class DefaultAutoCompleteItem extends AutoCompleteItem<RestNamedEntity> {

}

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatCardModule,
    FontAwesomeModule,
    MatTableModule,
    MatPaginatorModule,
    MatOptionModule,
    MatAutocompleteModule
  ],
  declarations: [
    LoaderImgComponent,
    LoaderBarComponent,
    TitleDisplayComponent,
    WebLinkListComponent,
    WebLinkComponent,
    DialogConfirmComponent,
    ListFilterComponent,
    ProgressbarComponent,
    SeriesNameDisplayComponent,
    BookStatusUnreadIconComponent,
    BookStatusReadIconComponent,
    BookStatusReadingIconComponent,
    LendModalComponent,
    BorrowerFormComponent,
    AutoCompleteInputComponent
  ],
  exports: [
    RouterModule,
    ListFilterComponent,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatBadgeModule,
    LoaderImgComponent,
    LoaderBarComponent,
    TitleDisplayComponent,
    WebLinkListComponent,
    FontAwesomeModule,
    ProgressbarComponent,
    MatTableModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatCardModule,
    SeriesNameDisplayComponent,
    BookStatusUnreadIconComponent,
    BookStatusReadingIconComponent,
    BookStatusReadIconComponent,
    AutoCompleteInputComponent
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    {provide: SEARCH_PARAM, useClass: DefaultAutoCompleteItem}
  ]
})

export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faLink, faTwitter, faInstagram, faFacebook, faArtstation, faDeviantart, faBookOpen, faBookReader, faBook, faBarcode);
  }
}
