import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-title-display',
  template: '<div> Title Display Mock</div>'
})
export class MockTitleDisplayComponent {

  @Input() title: any;
  @Input() tome: any;
  @Input() series: any;
  @Input() isbn: any;

  constructor() {
  }
}
