import {Component, Input} from '@angular/core';
import {BookSeries} from '../../core/model/series';

@Component({
  selector: 'app-title-display',
  template: `
    <span [ngClass]="class">
    <ng-container class="mat-h2" *ngIf="series !== undefined && !series.oneShot">
      {{series.name}}
      <ng-container class="mat-h2" *ngIf="series.tome !== null && series.tome !== undefined">
          (T{{series.tome}})
        </ng-container>
      <span *ngIf="series.seriesCycle !== null && series.seriesCycle.name !== null && series.seriesCycle.tome !== null">
        - {{series.seriesCycle.name}} (T{{series.seriesCycle.tome}})
      </span>
    </ng-container>
      {{title}}
    </span>
    <span *ngIf="isbn !== null" class="mat-small"> {{isbn}}</span>
  `
})
export class TitleDisplayComponent {

  @Input() title: string;
  @Input() series: BookSeries;
  @Input() isbn: string = null;
  @Input() class: string;

}
