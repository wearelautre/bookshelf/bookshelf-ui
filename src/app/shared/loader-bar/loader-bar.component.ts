import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {CoreService} from '../../core/services/core.service';

@Component({
  selector: 'app-loader-bar',
  template: `
    <mat-progress-bar *ngIf="isLoading$ | async"
                      [color]="'primary'"
                      [mode]="'indeterminate'">
    </mat-progress-bar>`,
  styles: [`
    mat-progress-bar {
      margin-bottom: 20px;
      position: absolute;
      z-index: 999
    }
  `]
})
export class LoaderBarComponent {

  isLoading$: Observable<boolean> = this.coreService.isLoading$;

  constructor(private coreService: CoreService) {
  }

}
