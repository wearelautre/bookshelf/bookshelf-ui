import {idType} from "../core/model/rest-entity";

export class Utils {
  static getMapKeysAsArray(map: Map<string, any>): string [] {
    return Array.from(map.keys());
  }

  static buildUrl(apiEndpoints: string[], ...ids: idType[]) {
    let path = '/api'
    apiEndpoints.forEach((item, i) => {
      path = `${path}/${item}`
      if (ids[i]) {
        path = `${path}/${ids[i]}`
      }
    })
    return path;
  }
}
